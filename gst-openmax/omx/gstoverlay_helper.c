/*
 * Copyright (c) 2009-2011, NVIDIA CORPORATION.  All rights reserved.
 *
 * Based on code copyright/by:
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "config.h"
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <stdlib.h>
#include "gstoverlay_helper.h"

#define INDEX_REDIRECT  0
#define INDEX_SRC_RECT  1
#define INDEX_DST_RECT  2
#define INDEX_ROTATION  3
#if 0
#define INDEX_WM_DELETE_WINDOW 4
#define INDEX_WM_PROTOCOLS 5
#endif

#define INDEX_BRIGHTNESS  4
#define INDEX_CONTRAST    5
#define INDEX_SATURATION  6
#define INDEX_HUE         7
#define INDEX_WM_DELETE_WINDOW 8
#define INDEX_WM_PROTOCOLS     9
#define NUM_INDICES (INDEX_WM_PROTOCOLS+1)

static char *_atoms_names[NUM_INDICES] = {
   "NVOVERLAY_REDIRECT",
   "NVOVERLAY_SRC_RECTANGLE",
   "NVOVERLAY_DST_RECTANGLE",
   "NVOVERLAY_ROTATION",
   "NVOVERLAY_BRIGHTNESS",
   "NVOVERLAY_CONTRAST",
   "NVOVERLAY_SATURATION",
   "NVOVERLAY_HUE",
   "WM_DELETE_WINDOW",
   "WM_PROTOCOLS"
};

static Atom _atoms_list[NUM_INDICES];

static Display *private_display = NULL;

static ListElement *window_list = NULL;

void UpdateProperty(ListElement *element, Atom property);

Display *InitDisplay(void)
{
   if(!(private_display = XOpenDisplay(NULL))) return 0;

   XInternAtoms(private_display, _atoms_names, NUM_INDICES, False, _atoms_list);

   return private_display;
}

ListElement * FindElement(Window win)
{
    ListElement *element = window_list;

    while(element) {
       if(element->win == win) break;
       element = element->next;
    }

    return element;
}

#define PI   3.14159265358979323846

void
UpdateProperty(ListElement *element, Atom property)
{
   Atom actual_type_return;
   int actual_format_return, tmp;
   unsigned long nitems_return, bytes_after_return;
   unsigned char *prop_return = NULL;

   if(property == _atoms_list[INDEX_REDIRECT]) {
      XGetWindowProperty(private_display, element->win,
                         property, 0, 1, False,
                         XA_CARDINAL, &actual_type_return,
                         &actual_format_return, &nitems_return,
                         &bytes_after_return, &prop_return);

      if(actual_type_return == None || (!prop_return)) {
         element->info.type = NV_OVERLAY_WINDOW_TYPE_NOT_OVERLAY;
      } else {
         element->info.type = *((int*)prop_return);
      }
   } else
   if(property == _atoms_list[INDEX_SRC_RECT]) {
      XGetWindowProperty(private_display, element->win,
                         property, 0, 2, False, 
                         XA_RECTANGLE, &actual_type_return, 
                         &actual_format_return, &nitems_return,
                         &bytes_after_return, &prop_return);

      if(actual_type_return == None || (!prop_return)) {
         element->info.src_rect_valid = 0;
      } else {
         element->info.src_rect_valid = 1;
         element->info.src_rect = *((XRectangle*)prop_return);
      }
   } else
   if(property == _atoms_list[INDEX_DST_RECT]) {
      XGetWindowProperty(private_display, element->win,
                         property, 0, 2, False, 
                         XA_RECTANGLE, &actual_type_return,
                         &actual_format_return, &nitems_return,
                         &bytes_after_return, &prop_return);

      if(actual_type_return == None || (!prop_return)) {
         element->info.dst_rect_valid = 0;
      } else {
         element->info.dst_rect_valid = 1;
         element->info.dst_rect = *((XRectangle*)prop_return);
      }
   } else
   if(property == _atoms_list[INDEX_ROTATION]) {
      XGetWindowProperty(private_display, element->win,
                         property, 0, 1, False, 
                         XA_CARDINAL, &actual_type_return, 
                         &actual_format_return, &nitems_return,
                         &bytes_after_return, &prop_return);

      if(actual_type_return == None || (!prop_return)) {
         element->info.rotation = 0;
      } else {
         element->info.rotation = *((int*)prop_return);
      }
   } else
   if(property == _atoms_list[INDEX_BRIGHTNESS]) {
      XGetWindowProperty(private_display, element->win,
                         property, 0, 1, False,
                         XA_INTEGER, &actual_type_return,
                         &actual_format_return, &nitems_return,
                         &bytes_after_return, &prop_return);

      if(actual_type_return == None || (!prop_return)) {
         element->info.brightness_valid = 0;
      } else {
         element->info.brightness_valid = 1;
         tmp = *((int*)prop_return);
         element->info.brightness = tmp / 10000.0f;
      }
   } else
   if(property == _atoms_list[INDEX_CONTRAST]) {
      XGetWindowProperty(private_display, element->win,
                         property, 0, 1, False,
                         XA_INTEGER, &actual_type_return,
                         &actual_format_return, &nitems_return,
                         &bytes_after_return, &prop_return);

      if(actual_type_return == None || (!prop_return)) {
         element->info.contrast_valid = 0;
      } else {
         element->info.contrast_valid = 1;
         tmp = *((int*)prop_return);
         element->info.contrast = tmp / 10000.0f;
      }
   } else
   if(property == _atoms_list[INDEX_SATURATION]) {
      XGetWindowProperty(private_display, element->win,
                         property, 0, 1, False,
                         XA_INTEGER, &actual_type_return,
                         &actual_format_return, &nitems_return,
                         &bytes_after_return, &prop_return);

      if(actual_type_return == None || (!prop_return)) {
         element->info.saturation_valid = 0;
      } else {
         element->info.saturation_valid = 1;
         tmp = *((int*)prop_return);
         element->info.saturation = tmp / 10000.0f;
      }
   } else
   if(property == _atoms_list[INDEX_HUE]) {
      XGetWindowProperty(private_display, element->win,
                         property, 0, 1, False,
                         XA_INTEGER, &actual_type_return,
                         &actual_format_return, &nitems_return,
                         &bytes_after_return, &prop_return);

      if(actual_type_return == None || (!prop_return)) {
         element->info.hue_valid = 0;
      } else {
         element->info.hue_valid = 1;
         tmp = *((int*)prop_return);
         element->info.hue = ((float)tmp * PI) / (100.0f * 180.f);
      }
   }

   if(prop_return)
      XFree(prop_return);
}

int
AddWindow(Window win)
{
   ListElement *element;

   if(!(element = calloc(1, sizeof(ListElement)))) return 0;

   element->win = win;

   /* enlist */
   element->next = window_list;
   window_list = element;

   /* listen for events related to this window */
   XSetWMProtocols(private_display, win,
                   _atoms_list + INDEX_WM_DELETE_WINDOW, 1);
   XSelectInput(private_display, win, StructureNotifyMask | PropertyChangeMask);

   UpdateProperty(element, _atoms_list[INDEX_REDIRECT]);
   UpdateProperty(element, _atoms_list[INDEX_SRC_RECT]);
   UpdateProperty(element, _atoms_list[INDEX_DST_RECT]);
   UpdateProperty(element, _atoms_list[INDEX_ROTATION]);
   UpdateProperty(element, _atoms_list[INDEX_BRIGHTNESS]);
   UpdateProperty(element, _atoms_list[INDEX_CONTRAST]);
   UpdateProperty(element, _atoms_list[INDEX_SATURATION]);
   UpdateProperty(element, _atoms_list[INDEX_HUE]);

   return 1;
}

void
RemoveWindow(Window win)
{
   ListElement *prev = NULL, *element = window_list;

   while(element) {
       if(element->win == win) {
          if(prev) prev->next = element->next;
          else window_list = element->next;

          free(element);
          return;
       }
       prev = element;
       element = element->next;
   }
}

static void CheckEvents(void)
{
    XEvent event;
    ListElement *element;

    while(XPending(private_display)) {
       XNextEvent(private_display, &event);

       switch(event.type) {
       case ClientMessage:
          if((event.xclient.message_type == _atoms_list[INDEX_WM_PROTOCOLS]) &&
             (event.xclient.data.l[0] == _atoms_list[INDEX_WM_DELETE_WINDOW]))
          {
              RemoveWindow(event.xclient.window);
          }
          break;
       case DestroyNotify:
          RemoveWindow(event.xdestroywindow.window);
          break;
       case PropertyNotify:
          if((element = FindElement(event.xproperty.window))) 
             UpdateProperty(element, event.xproperty.atom);
          break;
       default:
          break; 
       }
    }
}

//int
NvOverlayWindowInfo * 
NvOverlayGetWindowInfo(
   Window win
)
//   NvOverlayWindowInfo *info

{
   ListElement *element;

   if(!private_display)
      if(!InitDisplay()) return NULL;

   /* update status on our known windows */
   CheckEvents();

   /* see if this window is already in our list */
   if(!(element = FindElement(win))) {
       /* if not add it */
       if(!AddWindow(win)) return NULL;

       /* update status again now that our list of known windows
          has enlarged by one */
       CheckEvents();

       /* see if our window still exists after updates */
       if(!(element = FindElement(win))) return NULL;
    }

   //*info = element->info;

   return &element->info;
}
