/*
 * Copyright (C) 2007-2009 Nokia Corporation.
 * Copyright (c) 2009-2011, NVIDIA CORPORATION.  All rights reserved.
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "gstomx_wmvdec.h"
#include "gstomx.h"
#include "string.h"
#include "stdlib.h"
#include <stdio.h>

static GstOmxBaseVideoDecClass *parent_class;

enum Profile {
    Simple = 0,
    Main = 1,
    Advanced = 3
};

static GstCaps *
generate_sink_template (void)
{
    GstCaps *caps;
    GstStructure *struc;

    caps = gst_caps_new_empty ();

    struc = gst_structure_new ("video/x-wmv",
                               "width", GST_TYPE_INT_RANGE, 16, 4096,
                               "height", GST_TYPE_INT_RANGE, 16, 4096,
                               "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1,
                               NULL);

    gst_caps_append_structure (caps, struc);

    return caps;
}

void InitBits(unsigned char *buf, int size);
int ReadBits(int numbits);
void parse_sequence(unsigned char *buf , int size , GstOmxWmvDec *omx_vc1dec);
void ParseFrameHeader(GstOmxWmvDec *omx_vc1dec , int *isIframe);

struct sBitStream {
    unsigned char *buffer;
    unsigned int counter;
    unsigned int size;
    unsigned int scratch;
}BitStream;

void InitBits(unsigned char *buf, int size)
{
    BitStream.buffer = buf;
    BitStream.size = size;
    BitStream.scratch = 0;
    BitStream.counter = 0;
}

int ReadBits(int numbits)
{
    unsigned int x=0, y=0,z;
    if(!(BitStream.counter))
    {
        BitStream.scratch = (BitStream.buffer[0]<<24) | (BitStream.buffer[1]<<16) |\
                (BitStream.buffer[2]<<8) | (BitStream.buffer[3]);
        BitStream.buffer += 4;
        BitStream.counter = 32;
    }
    if(numbits > BitStream.counter)
    {
        x = ReadBits(BitStream.counter);
        y = BitStream.counter;
        numbits -= BitStream.counter;
        BitStream.counter = 0;
        BitStream.scratch = (BitStream.buffer[0]<<24) | (BitStream.buffer[1]<<16) |\
                                            (BitStream.buffer[2]<<8) | (BitStream.buffer[3]);
        BitStream.buffer += 4;
        BitStream.counter = 32;
    }
    if(numbits < BitStream.counter)
    {
        z = BitStream.scratch & (((1<<numbits)-1)<<(32-numbits));
        z = z >> (32-numbits);
        x = (x<<y) | (z);
        BitStream.scratch <<= numbits;
        BitStream.counter -= numbits;
    }
    return x;
}

void parse_sequence(unsigned char *buf , int size , GstOmxWmvDec *omx_vc1dec)
{
    int bits;

    if(size < 4) return;


      InitBits(buf, size);

    omx_vc1dec->profile = ReadBits(2);
    if(omx_vc1dec->profile == Simple || omx_vc1dec->profile == Main)
        printf("Simple/Main Profile \n");
    else if(omx_vc1dec->profile == Advanced)
    {
        printf("Advanced Profile \n");
        omx_vc1dec->seekable = TRUE;
        return;
    }
    ReadBits(2); //Reserved Bits
    ReadBits(3);
    ReadBits(5);
    ReadBits(1); //Loop Filter
    ReadBits(1); //Resvd
    ReadBits(1); //Resvd
    ReadBits(1); //Resvd
    ReadBits(1); //Fast Mocomp
    bits = ReadBits(1); //Extendedmv
    if(bits && omx_vc1dec->profile == Simple)
        omx_vc1dec->profile = Main;
    ReadBits(2); //Quant
    ReadBits(1); //transform
    ReadBits(1); //resvd
    ReadBits(1); //Overlap
    ReadBits(1); //Sync
    ReadBits(1); //Range Red
    omx_vc1dec->maxBFrames = ReadBits(3); //B frames
    ReadBits(2); //Quantizer
    omx_vc1dec->frameInterP = ReadBits(1);
    omx_vc1dec->seekable = TRUE;
}

void ParseFrameHeader(GstOmxWmvDec *omx_vc1dec , int *isIframe)
{
    int i;
    int pictype;
    *isIframe = 0;

    if(omx_vc1dec->profile == Simple || omx_vc1dec->profile == Main)
    {
        if(omx_vc1dec->frameInterP)
            ReadBits(1);
        ReadBits(2); //Skip Frame
        pictype = ReadBits(1);
        if(pictype) {
            *isIframe = 0;
        }
        else {
            if(omx_vc1dec->maxBFrames == 0)
                *isIframe = 1;
            else {
                pictype = ReadBits(2);
                if(!pictype)
                    *isIframe = 1;
            }
        }
    }
    else if(omx_vc1dec->profile == Advanced)
    {
        for(i = 0; (i < 4) && (ReadBits(1) != 0) ; i++);
        if(i == 2)
            *isIframe = 1;
    }

}

static gboolean
sink_setcaps (GstPad *pad,
              GstCaps *caps)
{
    GstStructure *structure;
    GstOmxBaseVideoDec *self;
    GstOmxWmvDec *omx_vc1dec;
    GstOmxBaseFilter *omx_base;
    GOmxCore *gomx;
    OMX_PARAM_PORTDEFINITIONTYPE param;
    gint width = 0;
    gint height = 0;
    gdouble framerate_num,framerate_denom;
    self = GST_OMX_BASE_VIDEODEC (GST_PAD_PARENT (pad));
    omx_base = GST_OMX_BASE_FILTER (self);
    omx_vc1dec = GST_OMX_WMVDEC(gst_pad_get_parent (pad));

    gomx = (GOmxCore *) omx_base->gomx;
    g_return_val_if_fail (gst_caps_get_size (caps) == 1, FALSE);

    structure = gst_caps_get_structure (caps, 0);
    if(omx_vc1dec->codec_data != NULL)
        gst_buffer_unref(omx_vc1dec->codec_data);
    omx_vc1dec->codec_data = NULL;
    omx_vc1dec->wvc1 = FALSE;
    gst_structure_get_int (structure, "width", &width);
    gst_structure_get_int (structure, "height", &height);

    if (!width || !height)
    {
        width = 720;
        height = 480;
        GST_ELEMENT_WARNING(gomx->object,STREAM,DECODE,(NULL),
                          ("Received Invalid Width/Height - using 720/480"));
    }

    {
        const GValue *framerate = NULL;
        framerate = gst_structure_get_value (structure, "framerate");
        if (framerate)
        {
            self->framerate_num = gst_value_get_fraction_numerator (framerate);
            self->framerate_denom = gst_value_get_fraction_denominator (framerate);
            framerate_num = (gdouble)self->framerate_num;
            framerate_denom = (gdouble)self->framerate_denom;

            omx_base->duration = (GstClockTime)((framerate_denom /framerate_num)*GST_SECOND );
        }
    }

    memset (&param, 0, sizeof (param));
    param.nSize = sizeof (OMX_PARAM_PORTDEFINITIONTYPE);
    param.nVersion.s.nVersionMajor = 1;
    param.nVersion.s.nVersionMinor = 1;

    omx_vc1dec->seekable = FALSE;
    omx_vc1dec->frameInterP = 0;
    omx_vc1dec->maxBFrames = 0;


    {
        const GValue *codec_data;
        guint32 fourcc;
        GstBuffer *buffer;
        GstBuffer *final;
        gint index=0;
        codec_data = gst_structure_get_value (structure, "codec_data");
        if (codec_data && width && height)
        {
            unsigned int total_size = 0;
            buffer = gst_value_get_buffer (codec_data);
            if (gst_structure_get_fourcc (structure, "format", &fourcc))
            {
                switch (fourcc)
                {
                    case GST_MAKE_FOURCC ('W', 'V', 'C', '1'):
                            omx_vc1dec->wvc1 = TRUE;
                        break;
                    case GST_MAKE_FOURCC ('w', 'v', 'c', '1'):
                            omx_vc1dec->wvc1 = TRUE;
                        break;
                }
            }
            if(omx_vc1dec->wvc1) //Handle WVC1 format
            {
                unsigned char *extradata = buffer->data;
                unsigned int startcode = 0xffffffff;
                guint length = buffer->size;
                if(extradata[0])  //openmax decoder want this to be skipped
                {
                        extradata++;
                        length--;
                }
                final = gst_buffer_new_and_alloc(length);
                memcpy(GST_BUFFER_DATA(final),extradata,length);
                omx_vc1dec->codec_data = final;
                length = GST_BUFFER_SIZE(final);
                extradata = GST_BUFFER_DATA(final);
                while((startcode != 0x0000010f) && (length > 0))
                {
                    startcode = (startcode<<8) | *(extradata++);
                    length--;
                }
                parse_sequence(extradata, length, omx_vc1dec);
            }
            else if((buffer->size > 3) && buffer->data[3]!=0xc5)
            {
                //Lets code Annex J and L of the SMPTE VC-1 specification
                char seq_l1[] = { 0xff,0xff,0xff,0xc5};
                char seq_l2[] = { 0x4,0,0,0 };
                char seq_l3[] = { 0xc,0,0,0} ;
                total_size = 4 + 32;
                final = gst_buffer_new_and_alloc(total_size);
                memcpy(GST_BUFFER_DATA(final) + index,seq_l1,4);
                index += 4;
                memcpy(GST_BUFFER_DATA(final) + index,seq_l2,4);
                index += 4;
                memcpy(GST_BUFFER_DATA(final) + index,GST_BUFFER_DATA(buffer),4);
                index += 4;
                parse_sequence(GST_BUFFER_DATA(buffer),
                                GST_BUFFER_SIZE(buffer),
                                omx_vc1dec);
                memcpy(GST_BUFFER_DATA(final) + index,&height,4);
                index += 4;
                memcpy(GST_BUFFER_DATA(final) + index,&width,4);
                index += 4;
                memcpy(GST_BUFFER_DATA(final) + index,seq_l3,4);
                index += 4;
                memset(GST_BUFFER_DATA(final) + index,0,12);
                omx_vc1dec->codec_data = final;
            }
        }
    }
   /* Input port configuration. */
    {
        param.nPortIndex = 0;
        OMX_GetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
        param.format.video.nFrameWidth = width;
        param.format.video.nFrameHeight = height;

        OMX_SetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
    }
    /* Output port configuration. */
    {
        param.nPortIndex = 1;
        OMX_GetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
        param.format.video.nFrameWidth = width;
        param.format.video.nFrameHeight = height;

        OMX_SetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
    }

    gst_object_unref(GST_ELEMENT(omx_vc1dec));

    return gst_pad_set_caps (pad, caps);
}

static GstFlowReturn gst_omx_vc1dec_pad_chain (GstPad *pad, GstBuffer *buf)
{
        GstOmxBaseFilter *omx_base;
        GstOmxWmvDec *omx_vc1dec;
        GstFlowReturn result = GST_FLOW_ERROR;
        guint size;
        guint8 *data;
        guint32 start_code;
        int isIframe= 0;
        omx_base = GST_OMX_BASE_FILTER (GST_PAD_PARENT (pad));
        omx_vc1dec = GST_OMX_WMVDEC(gst_pad_get_parent (pad));

        if ((omx_base->currplayback_rate > 2.0 ||
                omx_base->currplayback_rate < 0.0 ) &&
                    (omx_vc1dec->seekable == TRUE))
        {
            data = GST_BUFFER_DATA (buf);
            size = GST_BUFFER_SIZE (buf);
            start_code = (data[0]<<24) | (data[1]<<16) |
                              (data[2] << 8) | (data[3]);
            if((start_code&0xffffff00)==0x00000100)
            {
                data += 4;
                size -= 4;
                if(start_code == 0x00000010F)
                    parse_sequence(data, size, omx_vc1dec);
                while(start_code != 0x00000010D) {
                    start_code = (start_code << 8) | *data++;
                    size--;
                }
            }
            if(size > 4)
            {
                InitBits(data,size);
                ParseFrameHeader(omx_vc1dec, &isIframe);
                if(!isIframe) {
                    gst_buffer_unref(buf);
                    buf = NULL;
                    return GST_FLOW_OK;
                }
            }
        }

        if( omx_vc1dec->base_chain_func && buf != NULL )
        {
            if(omx_vc1dec->codec_data)
            {
				gst_buffer_ref(omx_vc1dec->codec_data);
                omx_base->codec_data = omx_vc1dec->codec_data;
            }
            if(omx_vc1dec->wvc1)   //Handle WVC1 format
            {
                //my code here
                GstBuffer *finalVC1Buf = NULL;
                data = GST_BUFFER_DATA (buf);
                size = GST_BUFFER_SIZE (buf);

                start_code = (buf->data[0]<<24) | (buf->data[1]<<16) | (buf->data[2]<<8) | buf->data[3];
                if(start_code != 0x10D &&  start_code != 0x10E)
                {
                    finalVC1Buf = gst_buffer_new_and_alloc(size+4);
                    finalVC1Buf->data[0] = 0;
                    finalVC1Buf->data[1] = 0;
                    finalVC1Buf->data[2] = 1;
                    finalVC1Buf->data[3] = 0xD;
                    memcpy(GST_BUFFER_DATA(finalVC1Buf)+4,GST_BUFFER_DATA (buf),size);
                    gst_buffer_copy_metadata(finalVC1Buf,buf,GST_BUFFER_COPY_FLAGS | GST_BUFFER_COPY_TIMESTAMPS);
                    result = omx_vc1dec->base_chain_func(pad, finalVC1Buf);
                    gst_buffer_unref(buf);
                    buf = NULL;
                }
                else
                {
                    result = omx_vc1dec->base_chain_func(pad, buf);
                }
            }
            else
            {
                result = omx_vc1dec->base_chain_func(pad, buf);
            }
            if(omx_vc1dec->codec_data)
            {
                gst_buffer_unref(omx_vc1dec->codec_data);
                omx_vc1dec->codec_data = NULL;
            }
        }
        else
        {
            printf("No PadChain Function \n");
        }
        gst_object_unref(GST_ELEMENT(omx_vc1dec));
        return result;
}

static void
type_base_init (gpointer g_class)
{
    GstElementClass *element_class;

    element_class = GST_ELEMENT_CLASS (g_class);

    gst_element_class_set_details_simple (element_class,
                                          "OpenMAX IL WMV video decoder",
                                          "Codec/Decoder/Video",
                                          "Decodes video in WMV format with OpenMAX IL",
                                          "Felipe Contreras");

    {
        GstPadTemplate *template;

        template = gst_pad_template_new ("sink", GST_PAD_SINK,
                                         GST_PAD_ALWAYS,
                                         generate_sink_template ());

        gst_element_class_add_pad_template (element_class, template);
    }
}

static void
gst_omx_vc1dec_finalize (GObject *obj)
{
    GstOmxWmvDec *omx_vc1dec;
    omx_vc1dec = GST_OMX_WMVDEC(obj);
    if(omx_vc1dec->codec_data)
    {
        gst_buffer_unref(omx_vc1dec->codec_data);
        omx_vc1dec->codec_data = NULL;
    }
    omx_vc1dec->base_chain_func = NULL;
    G_OBJECT_CLASS(parent_class)->finalize(obj);
}
static void
type_class_init (gpointer g_class,
                 gpointer class_data)
{
    GObjectClass *gobject_class;
//    GstElementClass *gstelement_class;
    gobject_class = (GObjectClass *) g_class;
//    gstelement_class = (GstElementClass *) g_class;

    parent_class = g_type_class_ref (GST_OMX_BASE_VIDEODEC_TYPE);
    gobject_class->finalize = gst_omx_vc1dec_finalize;
}
static void
type_instance_init (GTypeInstance *instance,
                    gpointer g_class)
{
    GstOmxBaseVideoDec *omx_base;
    GstOmxBaseFilter *omx_base_filter;
    GstOmxWmvDec *omx_vc1dec;
    omx_base = GST_OMX_BASE_VIDEODEC (instance);
    omx_base_filter = GST_OMX_BASE_FILTER (instance);
    omx_vc1dec = GST_OMX_WMVDEC(instance);
    omx_base->compression_format = OMX_VIDEO_CodingWMV;
    gst_pad_set_setcaps_function (omx_base_filter->sinkpad, sink_setcaps);
    /* initialize h decoder specific data */
    omx_vc1dec->codec_data = NULL;
    omx_vc1dec->base_chain_func = NULL;
    omx_vc1dec->wvc1 = FALSE;
    /* replace base chain func */
    omx_vc1dec->base_chain_func = GST_PAD_CHAINFUNC(omx_base_filter->sinkpad);
    gst_pad_set_chain_function (omx_base_filter->sinkpad, gst_omx_vc1dec_pad_chain);



}

GType
gst_omx_wmvdec_get_type (void)
{
    static GType type = 0;

    if (G_UNLIKELY (type == 0))
    {
        GTypeInfo *type_info;

        type_info = g_new0 (GTypeInfo, 1);
        type_info->class_size = sizeof (GstOmxWmvDecClass);
        type_info->base_init = type_base_init;
        type_info->class_init = type_class_init;
        type_info->instance_size = sizeof (GstOmxWmvDec);
        type_info->instance_init = type_instance_init;

        type = g_type_register_static (GST_OMX_BASE_VIDEODEC_TYPE, "GstOmxWmvDec", type_info, 0);

        g_free (type_info);
    }

    return type;
}
