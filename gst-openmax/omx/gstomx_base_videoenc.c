/*
 * Copyright (C) 2007-2009 Nokia Corporation.
 * Copyright (c) 2009-2011, NVIDIA CORPORATION.  All rights reserved.
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "gstomx_base_videoenc.h"
#include "gstomx.h"
#include "gstomx_nvutils.h"
#include <string.h>             /* for strcmp */

#ifdef TEGRA
#include "NVOMX_IndexExtensions.h"
#endif

enum
{
  ARG_0,
  ARG_BITRATE,
  ARG_QUALITY,
  ARG_RC_MODE,
  ARG_INTRA_FRAME_INTERVAL
};

#define DEFAULT_BITRATE              4000000
#define DEFAULT_QUALITY_LEVEL        2
#define DEFAULT_INTRA_FRAME_INTERVAL 60
#define DEFAULT_RC_MODE              NVX_VIDEO_RateControlMode_VBR

#define GST_TYPE_OMX_VID_ENC_RCMODE (gst_omx_videnc_rc_mode_get_type ())
static GType
gst_omx_videnc_rc_mode_get_type (void)
{
  static volatile gsize rcmode_type_type = 0;
  static const GEnumValue rcmode_type[] = {
    {NVX_VIDEO_RateControlMode_CBR, "GST_OMX_VIDENC_RCMODE_TYPE_CONSTANT", "cbr"},
    {NVX_VIDEO_RateControlMode_VBR, "GST_OMX_VIDENC_RCMODE_TYPE_VARIABLE", "vbr"},
    {0, NULL, NULL}
  };

  if (g_once_init_enter (&rcmode_type_type)) {
    GType tmp = g_enum_register_static ("GstOmxVideoEncRCModeType", rcmode_type);
    g_once_init_leave (&rcmode_type_type, tmp);
  }

  return (GType) rcmode_type_type;
}

GSTOMX_BOILERPLATE (GstOmxBaseVideoEnc, gst_omx_base_videoenc, GstOmxBaseFilter,
    GST_OMX_BASE_FILTER_TYPE);

static GstCaps *
generate_sink_template (void)
{
  GstCaps *caps;
  GstStructure *struc, *struc1, *struc2;

  caps = gst_caps_new_empty ();

  struc = gst_structure_new ("video/x-raw-yuv",
      "width", GST_TYPE_INT_RANGE, 16, 4096,
      "height", GST_TYPE_INT_RANGE, 16, 4096,
      "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1, NULL);

  struc1 = gst_structure_new ("video/x-nv-yuv",
      "width", GST_TYPE_INT_RANGE, 16, 4096,
      "height", GST_TYPE_INT_RANGE, 16, 4096,
      "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1, NULL);
  struc2 = gst_structure_new ("video/x-nvrm-yuv",
      "width", GST_TYPE_INT_RANGE, 16, 4096,
      "height", GST_TYPE_INT_RANGE, 16, 4096,
      "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1, NULL);

  {
    GValue list;
    GValue val;

    list.g_type = val.g_type = 0;

    g_value_init (&list, GST_TYPE_LIST);
    g_value_init (&val, GST_TYPE_FOURCC);

    gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('I', '4', '2', '0'));
    gst_value_list_append_value (&list, &val);

    gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('Y', 'U', 'Y', '2'));
    gst_value_list_append_value (&list, &val);

    gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('U', 'Y', 'V', 'Y'));
    gst_value_list_append_value (&list, &val);

    gst_structure_set_value (struc, "format", &list);
    gst_structure_set_value (struc1, "format", &list);
    gst_structure_set_value (struc2, "format", &list);

    g_value_unset (&val);
    g_value_unset (&list);
  }

  gst_caps_append_structure (caps, struc);
  gst_caps_append_structure (caps, struc1);
  gst_caps_append_structure (caps, struc2);

  return caps;
}

static void
type_base_init (gpointer g_class)
{
  GstElementClass *element_class;

  element_class = GST_ELEMENT_CLASS (g_class);

  {
    GstPadTemplate *template;

    template = gst_pad_template_new ("sink", GST_PAD_SINK,
        GST_PAD_ALWAYS, generate_sink_template ());

    gst_element_class_add_pad_template (element_class, template);
  }
}

static void
set_property (GObject * obj,
    guint prop_id, const GValue * value, GParamSpec * pspec)
{
  GstOmxBaseVideoEnc *self;

  self = GST_OMX_BASE_VIDEOENC (obj);

  switch (prop_id) {
    case ARG_BITRATE:
      self->bitrate = g_value_get_uint (value);
      break;
    case ARG_QUALITY:
      self->quality_level = g_value_get_uint (value);
      break;
    case ARG_RC_MODE:
      self->rc_mode = g_value_get_enum (value);
      break;
    case ARG_INTRA_FRAME_INTERVAL:
      self->iframeinterval = g_value_get_uint (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, prop_id, pspec);
      break;
  }
}

static void
get_property (GObject * obj, guint prop_id, GValue * value, GParamSpec * pspec)
{
  GstOmxBaseVideoEnc *self;

  self = GST_OMX_BASE_VIDEOENC (obj);

  switch (prop_id) {
    case ARG_BITRATE:
      g_value_set_uint (value, self->bitrate);
      break;
    case ARG_QUALITY:
      g_value_set_uint (value, self->quality_level);
      break;
    case ARG_RC_MODE:
      g_value_set_enum (value, self->rc_mode);
      break;
    case ARG_INTRA_FRAME_INTERVAL:
      g_value_set_uint (value, self->iframeinterval);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, prop_id, pspec);
      break;
  }
}

static void
type_class_init (gpointer g_class, gpointer class_data)
{
  GObjectClass *gobject_class;

  gobject_class = G_OBJECT_CLASS (g_class);

  /* Properties stuff */
  {
    gobject_class->set_property = set_property;
    gobject_class->get_property = get_property;

    g_object_class_install_property (gobject_class, ARG_BITRATE,
        g_param_spec_uint ("bitrate", "Bit-rate",
            "Encoding bit-rate",
            0, G_MAXUINT, DEFAULT_BITRATE,
            G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, ARG_QUALITY,
        g_param_spec_uint ("quality-level", "quality-level",
            "Encoding quality-level",
            0, 2, DEFAULT_QUALITY_LEVEL,
            G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, ARG_RC_MODE,
        g_param_spec_enum ("rc-mode", "rc-mode",
            "Encoding rate control mode",
            GST_TYPE_OMX_VID_ENC_RCMODE, DEFAULT_RC_MODE,
            G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, ARG_INTRA_FRAME_INTERVAL,
        g_param_spec_uint ("iframeinterval", "Intra Frame interval",
            "Encoding Intra Frame occurance frequency",
            0, G_MAXUINT, DEFAULT_INTRA_FRAME_INTERVAL,
            G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  }
}

static gboolean
sink_setcaps (GstPad * pad, GstCaps * caps)
{
  GstStructure *structure;
  GstOmxBaseVideoEnc *self;
  GstOmxBaseFilter *omx_base;
  GOmxCore *gomx;
  OMX_COLOR_FORMATTYPE color_format = OMX_COLOR_FormatUnused;
  gint width = 0;
  gint height = 0;
  const GValue *framerate = NULL;

  self = GST_OMX_BASE_VIDEOENC (GST_PAD_PARENT (pad));
  omx_base = GST_OMX_BASE_FILTER (self);
  gomx = (GOmxCore *) omx_base->gomx;

  omx_base->in_port->buffer_type = BUFFER_TYPE_EGLIMAGE;

  GST_INFO_OBJECT (self, "setcaps (sink): %" GST_PTR_FORMAT, caps);

  g_return_val_if_fail (gst_caps_get_size (caps) == 1, FALSE);

  structure = gst_caps_get_structure (caps, 0);

  gst_structure_get_int (structure, "width", &width);
  gst_structure_get_int (structure, "height", &height);

  if (strcmp (gst_structure_get_name (structure), "video/x-raw-yuv") == 0 ||
      strcmp (gst_structure_get_name (structure), "video/x-nv-yuv") == 0 ||
      strcmp (gst_structure_get_name (structure), "video/x-nvrm-yuv") == 0) {
    guint32 fourcc;

    framerate = gst_structure_get_value (structure, "framerate");
    if (framerate) {
      self->framerate_num = gst_value_get_fraction_numerator (framerate);
      self->framerate_denom = gst_value_get_fraction_denominator (framerate);
    }

    if (gst_structure_get_fourcc (structure, "format", &fourcc)) {
      switch (fourcc) {
        case GST_MAKE_FOURCC ('I', '4', '2', '0'):
          color_format = OMX_COLOR_FormatYUV420PackedPlanar;
          break;
        case GST_MAKE_FOURCC ('Y', 'U', 'Y', '2'):
          color_format = OMX_COLOR_FormatYCbYCr;
          break;
        case GST_MAKE_FOURCC ('U', 'Y', 'V', 'Y'):
          color_format = OMX_COLOR_FormatCbYCrY;
          break;
      }
    }

    if (strcmp (gst_structure_get_name (structure), "video/x-nv-yuv") == 0) {
#ifdef TEGRA
      gstomx_use_nvbuffer_extension (gomx->omx_handle, omx_base->in_port->port_index);
#endif
      omx_base->in_port->buffer_type = BUFFER_TYPE_NVBUFFER;
      omx_base->out_port->buffer_type = BUFFER_TYPE_RAW;
      omx_base->share_input_buffer = FALSE;
      gomx->share_input_buffer = FALSE;
      omx_base->share_output_buffer = TRUE;
      gomx->share_output_buffer = TRUE;
    }
    else if (strcmp (gst_structure_get_name (structure), "video/x-nvrm-yuv") == 0) {
#ifdef TEGRA
      gstomx_use_nvrmsurf_extension (gomx->omx_handle);
#endif
      omx_base->in_port->buffer_type = BUFFER_TYPE_NVRMBUFFER;
      omx_base->out_port->buffer_type = BUFFER_TYPE_RAW;
      omx_base->share_input_buffer = FALSE;
      gomx->share_input_buffer = FALSE;
      omx_base->share_output_buffer = TRUE;
      gomx->share_output_buffer = TRUE;
    }
  }

  {
    OMX_PARAM_PORTDEFINITIONTYPE param;

    G_OMX_INIT_PARAM (param);

    /* Input port configuration. */
    {
      param.nPortIndex = omx_base->in_port->port_index;
      OMX_GetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);

      param.format.video.nFrameWidth = width;
      param.format.video.nFrameHeight = height;
      param.format.video.eColorFormat = color_format;
      param.format.video.eCompressionFormat = OMX_VIDEO_CodingUnused;;

      if (framerate) {
        /* convert to Q.16 */
        param.format.video.xFramerate =
            (gst_value_get_fraction_numerator (framerate) << 16) /
            gst_value_get_fraction_denominator (framerate);
      }

      OMX_SetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
    }
  }

  if ((1920 == width) && (1080 == height))
  {
    self->quality_level = 1;
  }

  return gst_pad_set_caps (pad, caps);
}

static void
omx_setup (GstOmxBaseFilter * omx_base)
{
  GstOmxBaseVideoEnc *self;
  GOmxCore *gomx;

  OMX_IMAGE_PARAM_QFACTORTYPE oQFactor;
  OMX_VIDEO_PARAM_BITRATETYPE oBitRate;

  self = GST_OMX_BASE_VIDEOENC (omx_base);
  gomx = (GOmxCore *) omx_base->gomx;

  GST_INFO_OBJECT (omx_base, "begin");

  {
    OMX_PARAM_PORTDEFINITIONTYPE param;

    G_OMX_INIT_PARAM (param);

    /* Output port configuration. */
    {
      param.nPortIndex = omx_base->out_port->port_index;
      OMX_GetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);

      param.format.video.eCompressionFormat = self->compression_format;
      OMX_SetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
    }

    {
      gstomx_set_video_encoder_ratecontrolmode (gomx->omx_handle, self->rc_mode);
    }

    {
      G_OMX_INIT_PARAM(oBitRate);
      if (self->bitrate != 0)
         oBitRate.nTargetBitrate = self->bitrate;

      oBitRate.eControlRate = OMX_Video_ControlRateConstantSkipFrames;

      OMX_SetParameter(gomx->omx_handle, OMX_IndexParamVideoBitrate, &oBitRate);
    }

    {
      G_OMX_INIT_PARAM(oQFactor);

      switch (self->quality_level)
      {
        case 0: oQFactor.nQFactor = 33; break;
        case 1: oQFactor.nQFactor = 66; break;
        case 2: oQFactor.nQFactor = 100; break;
        default: oQFactor.nQFactor = 66; break;
      }
      OMX_SetParameter(gomx->omx_handle, OMX_IndexParamQFactor, &oQFactor);
    }

    {
      gstomx_set_video_encoder_temporaltradeoff (gomx->omx_handle);
    }

    {
      if (self->ifi_setup != NULL)
         self->ifi_setup(omx_base);
    }

    {
      gstomx_set_video_encoder_property (gomx->omx_handle);
    }

    if(self->configure_output_format)
    {
        self->configure_output_format(omx_base);
    }

  }

  GST_INFO_OBJECT (omx_base, "end");
}

static void
setup_ports (GstOmxBaseFilter *self)
{
  GOmxCore *core;
  OMX_PARAM_PORTDEFINITIONTYPE param;
  core = self->gomx;

  G_OMX_INIT_PARAM(param);

  /* Input port configuration. */
  param.nPortIndex = 0;
  OMX_GetParameter (core->omx_handle, OMX_IndexParamPortDefinition, &param);
  self->in_port = g_omx_core_setup_port (core, &param);
  gst_pad_set_element_private (self->sinkpad, self->in_port);

  /* Output port configuration. */
  param.nPortIndex = 1;
  OMX_GetParameter (core->omx_handle, OMX_IndexParamPortDefinition, &param);
  self->out_port = g_omx_core_setup_port (core, &param);
  gst_pad_set_element_private (self->srcpad, self->out_port);
}

static void
type_instance_init (GTypeInstance * instance, gpointer g_class)
{
  GstOmxBaseFilter *omx_base;
  GstOmxBaseVideoEnc *self;

  omx_base = GST_OMX_BASE_FILTER (instance);
  self = GST_OMX_BASE_VIDEOENC (instance);

  omx_base->omx_setup = omx_setup;
  omx_base->setup_ports = setup_ports;

  gst_pad_set_setcaps_function (omx_base->sinkpad, sink_setcaps);

  self->bitrate = DEFAULT_BITRATE;
  self->rc_mode = DEFAULT_RC_MODE;
  self->quality_level = DEFAULT_QUALITY_LEVEL;
  self->iframeinterval = DEFAULT_INTRA_FRAME_INTERVAL;
}
