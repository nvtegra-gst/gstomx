/*
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * NVIDIA Corporation and its licensors retain all intellectual property
 * and proprietary rights in and to this software, related documentation
 * and any modifications thereto.  Any use, reproduction, disclosure or
 * distribution of this software and related documentation without an express
 * license agreement from NVIDIA Corporation is strictly prohibited.
 *
 * version: 0.2
 */

#ifndef __GST_OMX_CAMERA2_PRIV_H__
#define __GST_OMX_CAMERA2_PRIV_H__

#include <OMX_Core.h>
#include <OMX_Component.h>
#include <glib-object.h>
#include "gstnvcameradefs.h"
#include "gstomx_nvutils.h"

G_BEGIN_DECLS

#define GST_OMX_CAMERA_PREVIEW_PORT 0
#define GST_OMX_CAMERA_STILL_PORT 1
#define GST_OMX_CAMERA_VIDEO_PORT 5

typedef enum
{
  AFNone      =  0,
  AFFullRange = (1 << 0),
  AFInfMode   = (1 << 1),
  AFMacroMode = (1 << 2)
} AlgSubType;

gboolean gst_omx_camera_set_mode (OMX_HANDLETYPE omx_handle, GstNvCameraMode mode);
gboolean gst_omx_camera_get_mode (OMX_HANDLETYPE omx_handle, GstNvCameraMode* mode);
gboolean gst_omx_camera_set_capture (OMX_HANDLETYPE omx_handle, gboolean sbool);
gboolean gst_omx_camera_get_capture (OMX_HANDLETYPE omx_handle, gboolean *sbool);
gboolean gst_omx_camera_set_zoom (OMX_HANDLETYPE omx_handle, gfloat zoom);
gboolean gst_omx_camera_get_zoom (OMX_HANDLETYPE omx_handle, gfloat* zoom);
gboolean gst_omx_camera_set_pause_after_capture (OMX_HANDLETYPE omx_handle, gboolean sbool);
gboolean gst_omx_camera_get_pause_after_capture (OMX_HANDLETYPE omx_handle, gboolean* sbool);
gboolean gst_omx_camera_set_flash_mode (OMX_HANDLETYPE omx_handle, GstFlashMode flash);
gboolean gst_omx_camera_get_flash_mode (OMX_HANDLETYPE omx_handle, GstFlashMode *flash);
gboolean gst_omx_camera_set_white_balance_mode (OMX_HANDLETYPE omx_handle, GstWhiteBalanceMode wb);
gboolean gst_omx_camera_get_white_balance_mode (OMX_HANDLETYPE omx_handle, GstWhiteBalanceMode *wb);
gboolean gst_omx_camera_set_iso_speed (OMX_HANDLETYPE omx_handle, guint iso);
gboolean gst_omx_camera_get_iso_speed (OMX_HANDLETYPE omx_handle, guint* iso);
gboolean gst_omx_camera_set_exposure (OMX_HANDLETYPE omx_handle, guint32 exp);
gboolean gst_omx_camera_get_exposure (OMX_HANDLETYPE omx_handle, guint32* exp);
gboolean gst_omx_camera_set_metering_mode (OMX_HANDLETYPE omx_handle, GstNvMeteringMode mt);
gboolean gst_omx_camera_get_metering_mode (OMX_HANDLETYPE omx_handle, GstNvMeteringMode* mt);
gboolean gst_omx_camera_set_color_tone (OMX_HANDLETYPE omx_handle, GstColourToneMode ct);
gboolean gst_omx_camera_get_color_tone (OMX_HANDLETYPE omx_handle, GstColourToneMode* ct);
gboolean gst_omx_camera_set_brightness (OMX_HANDLETYPE omx_handle, gint brightness);
gboolean gst_omx_camera_get_brightness (OMX_HANDLETYPE omx_handle, gint* brightness);
gboolean gst_omx_camera_set_contrast (OMX_HANDLETYPE omx_handle, gint contrast);
gboolean gst_omx_camera_get_contrast (OMX_HANDLETYPE omx_handle, gint* contrast);
gboolean gst_omx_camera_set_saturation (OMX_HANDLETYPE omx_handle, gint saturation);
gboolean gst_omx_camera_get_saturation (OMX_HANDLETYPE omx_handle, gint* saturation);
gboolean gst_omx_camera_set_ev_compensation (OMX_HANDLETYPE omx_handle, gfloat evCompensation);
gboolean gst_omx_camera_get_ev_compensation (OMX_HANDLETYPE omx_handle, gfloat* evCompensation);
gboolean gst_omx_camera_set_rotation (OMX_HANDLETYPE omx_handle, guint port, GstNvRotation rt);
gboolean gst_omx_camera_get_rotation (OMX_HANDLETYPE omx_handle, guint port, GstNvRotation* rt);
gboolean gst_omx_camera_set_viewfinder (OMX_HANDLETYPE omx_handle, gboolean sbool);
gboolean gst_omx_camera_get_viewfinder (OMX_HANDLETYPE omx_handle, gboolean* sbool);
gboolean gst_omx_camera_set_continuous_autofocus (OMX_HANDLETYPE omx_handle, gboolean sbool);
gboolean gst_omx_camera_get_continuous_autofocus (OMX_HANDLETYPE omx_handle, gboolean* sbool);
gboolean gst_omx_camera_set_sensor (OMX_HANDLETYPE omx_handle, GstNvSensor sensor);
gboolean gst_omx_camera_get_sensor (OMX_HANDLETYPE omx_handle, GstNvSensor* sensor);
gboolean gst_omx_camera_set_capture_paused (OMX_HANDLETYPE omx_handle, gboolean sbool);
gboolean gst_omx_camera_get_capture_paused (OMX_HANDLETYPE omx_handle, gboolean *sbool);
gboolean gst_omx_camera_set_hue (OMX_HANDLETYPE omx_handle, gint hue);
gboolean gst_omx_camera_get_hue (OMX_HANDLETYPE omx_handle, gint* hue);
gboolean gst_omx_camera_set_enable_edge_enhancement (OMX_HANDLETYPE omx_handle, gboolean sbool);
gboolean gst_omx_camera_get_enable_edge_enhancement (OMX_HANDLETYPE omx_handle, gboolean* sbool);
gboolean gst_omx_camera_set_auto_framerate (OMX_HANDLETYPE omx_handle, guint32 val);
gboolean gst_omx_camera_get_auto_framerate (OMX_HANDLETYPE omx_handle, guint32* val);
gboolean gst_omx_camera_set_edge_enhancement (OMX_HANDLETYPE omx_handle, gfloat val);
gboolean gst_omx_camera_get_edge_enhancement (OMX_HANDLETYPE omx_handle, gfloat* val);
gboolean gst_omx_camera_set_flicker_reduction_mode (OMX_HANDLETYPE omx_handle, GstFlickerReductionMode fr);
gboolean gst_omx_camera_get_flicker_reduction_mode (OMX_HANDLETYPE omx_handle, GstFlickerReductionMode* fr);
gboolean gst_omx_camera_set_stereo_mode (OMX_HANDLETYPE omx_handle, GstNvStereoMode stm);
gboolean gst_omx_camera_get_stereo_mode (OMX_HANDLETYPE omx_handle, GstNvStereoMode* stm);
gboolean gst_omx_camera_set_focus_pos (OMX_HANDLETYPE omx_handle, glong focus_pos);
gboolean gst_omx_camera_queryCAFState (OMX_HANDLETYPE omx_handle, gboolean* converged);
gboolean gst_omx_camera_programCAFLock (OMX_HANDLETYPE omx_handle, gboolean sbool);
gboolean gst_omx_camera_get_focuser_parameters (OMX_HANDLETYPE omx_handle, glong* minpos,
    glong* maxpos, glong* macropos, gboolean* infAF, gboolean* macroAF, gboolean* FocusSupport);
gboolean gst_omx_camera_get_flash_parameters (OMX_HANDLETYPE omx_handle, gboolean* FlashSupport);
gboolean gst_omx_camera_set_auto_focus (OMX_HANDLETYPE omx_handle, gboolean sbool,
    gboolean af, gboolean awb, gboolean ae, gint timeout, AlgSubType algSubType);
gboolean gst_omx_camera_set_noise_reduction (OMX_HANDLETYPE omx_handle, GstPhotographyNoiseReduction anr_mode,
    gulong lThreshold, gulong cThreshold);
gboolean gst_omx_camera_get_noise_reduction (OMX_HANDLETYPE omx_handle, GstPhotographyNoiseReduction* anr_mode,
    gulong* lThreshold, gulong* cThreshold);
gboolean gst_omx_camera_set_scene_mode (OMX_HANDLETYPE omx_handle, GstSceneMode sm);

G_END_DECLS

#endif
