/*
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * Based on code copyright/by:
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "gstomx_mpeg2dec.h"
#include "gstomx.h"
#include "gstomx_nvutils.h"
#include "string.h"
#include "stdlib.h"
#include <stdio.h>

static GstOmxBaseVideoDecClass *parent_class;

static int frameratetab_num[9] = {0, 24000, 24, 25, 30000, 30, 50, 60000, 60};
static int frameratetab_denom[9] = {1, 1001, 1, 1, 1001, 1, 1, 1001, 1};

static GstCaps *
generate_sink_template (void)
{
    GstCaps *caps;
    GstStructure *struc;

    caps = gst_caps_new_empty ();

    struc = gst_structure_new ("video/mpeg",
                            "mpegversion", GST_TYPE_INT_RANGE, 1, 2,
                            "systemstream", G_TYPE_BOOLEAN, FALSE, NULL,
                            "width", GST_TYPE_INT_RANGE, 16, 4096,
                            "height", GST_TYPE_INT_RANGE, 16, 4096,
                            "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1,
                             NULL);

    gst_caps_append_structure (caps, struc);

    return caps;
}
static gboolean
sink_setcaps (GstPad *pad,
              GstCaps *caps)
{
    GstStructure *structure;
    GstOmxBaseVideoDec *self;
    GstOmxMpeg2Dec *omx_mpeg2dec;
    GstOmxBaseFilter *omx_base;
    GOmxCore *gomx;
    OMX_PARAM_PORTDEFINITIONTYPE param;
    gint width = 0;
    gint height = 0;
    gdouble framerate_num,framerate_denom;
//    gint profile = 0;
    self = GST_OMX_BASE_VIDEODEC (GST_PAD_PARENT (pad));
    omx_base = GST_OMX_BASE_FILTER (self);
    omx_mpeg2dec = GST_OMX_MPEG2DEC(gst_pad_get_parent (pad));

    gomx = (GOmxCore *) omx_base->gomx;
    g_return_val_if_fail (gst_caps_get_size (caps) == 1, FALSE);

    structure = gst_caps_get_structure (caps, 0);
    if(omx_mpeg2dec->codec_data != NULL)
        gst_buffer_unref(omx_mpeg2dec->codec_data);
    omx_mpeg2dec->codec_data = NULL;
    gst_structure_get_int (structure, "width", &width);
    gst_structure_get_int (structure, "height", &height);

    if (!width || !height) {
      //FIXME
      /* bail out with arbirary width and height */
      width = 720;
      height = 480;
      GST_ELEMENT_WARNING(gomx->object,STREAM,DECODE,(NULL),
	  ("Received Invalid Width/Height - using 720/480"));
    }

    {
        const GValue *framerate = NULL;
        framerate = gst_structure_get_value (structure, "framerate");
        if (framerate)
        {
            self->framerate_num = gst_value_get_fraction_numerator (framerate);
            self->framerate_denom = gst_value_get_fraction_denominator (framerate);
            framerate_num = (gdouble)self->framerate_num;
            framerate_denom = (gdouble)self->framerate_denom;

            omx_base->duration = (GstClockTime)((framerate_denom /framerate_num)*GST_SECOND );
        }
    }
    memset (&param, 0, sizeof (param));
    param.nSize = sizeof (OMX_PARAM_PORTDEFINITIONTYPE);
    param.nVersion.s.nVersionMajor = 1;
    param.nVersion.s.nVersionMinor = 1;
    {
        const GValue *codec_data;
        GstBuffer *buffer;
        codec_data = gst_structure_get_value (structure, "codec_data");
        if (codec_data && width && height)
        {
            buffer = gst_value_get_buffer (codec_data);
            omx_mpeg2dec->codec_data = buffer;
            gst_buffer_ref(buffer);
        }
    }
    omx_mpeg2dec->first_packet = TRUE;
    /* Input port configuration. */
       {
           param.nPortIndex = 0;
           OMX_GetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
           param.format.video.nFrameWidth = width;
           param.format.video.nFrameHeight = height;
           OMX_SetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
       }
    /* Output port configuration. */
       {
           param.nPortIndex = 1;
           OMX_GetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
           param.format.video.nFrameWidth = width;
           param.format.video.nFrameHeight = height;
           OMX_SetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
       }
    gst_object_unref(GST_ELEMENT(omx_mpeg2dec));
    return gst_pad_set_caps (pad, caps);
}

static GstFlowReturn gst_omx_mpeg2dec_pad_chain (GstPad *pad, GstBuffer *buf)
{
        GstOmxBaseFilter *omx_base;
        GstOmxMpeg2Dec *omx_mpeg2dec;
        GstFlowReturn result = GST_FLOW_OK;
        guint8 *data;
        guint ii = 0;
        GstBuffer *tmp = NULL;
        guint mpeg_header = 0xffffffff;
        guint seq_found = 0;
        guint pic_found = 0;
        GOmxCore *gomx = NULL;
        GstOmxBaseVideoDec *self;
        guint error;

        omx_base = GST_OMX_BASE_FILTER (GST_PAD_PARENT (pad));
        omx_mpeg2dec = GST_OMX_MPEG2DEC(gst_pad_get_parent (pad));
        self = GST_OMX_BASE_VIDEODEC (omx_base);
        gomx = omx_base->gomx;

        self->src_caps_change_if_needed(omx_base, buf);


        if( omx_mpeg2dec->base_chain_func && !omx_mpeg2dec->first_packet)
        {
            if (omx_base->currplayback_rate < 0 || omx_base->currplayback_rate > 2)
            {
                if(!self->only_i_frame)
                {
                    error = gstomx_set_decoder_property_only_key_frame_decode (gomx->omx_handle, OMX_TRUE);

                    if (error)
                    {
                        g_print("\n gstomx: error setting gstomx_set_decoder_property_only_key_frame_decode\n");
                    }
                    else
                    {
                        self->only_i_frame = TRUE;
                    }
                }

                    result = omx_mpeg2dec->base_chain_func(pad, buf);
            }

            else
            {
                if (self->only_i_frame)
                {
                    error = gstomx_set_decoder_property_only_key_frame_decode (gomx->omx_handle, OMX_FALSE);

                    if (error)
                    {
                        g_print("\n gstomx: error setting gstomx_set_decoder_property_only_key_frame_decode\n");
                    }
                    else
                    {
                        self->only_i_frame = FALSE;
                    }
                }

                result = omx_mpeg2dec->base_chain_func(pad, buf);
            }
        }

        else if( omx_mpeg2dec->base_chain_func )
        {
                if(omx_mpeg2dec->codec_data)
                {
                    tmp = gst_buffer_join(omx_mpeg2dec->codec_data,buf);
                    omx_mpeg2dec->first_mpeg_buffer = tmp;
                    omx_mpeg2dec->codec_data = NULL;
                }
                else
                {
                    if(omx_mpeg2dec->first_mpeg_buffer)
                        tmp = gst_buffer_join(omx_mpeg2dec->first_mpeg_buffer,buf);
                    else
                        tmp = buf;

                    omx_mpeg2dec->first_mpeg_buffer = tmp;
                }
                gst_buffer_copy_metadata(tmp,buf,GST_BUFFER_COPY_FLAGS | GST_BUFFER_COPY_TIMESTAMPS);
                data = GST_BUFFER_DATA(omx_mpeg2dec->first_mpeg_buffer);
                while(ii<GST_BUFFER_SIZE(omx_mpeg2dec->first_mpeg_buffer))
                {
                    mpeg_header = mpeg_header << 8 | data[ii];
                    ii++;
                    if(seq_found && mpeg_header == 0x00000100) //picture header
                    {
                        pic_found = 1;
                        break;
                    }

                    if (mpeg_header == 0x000001B3) //sequence header
                    {
                        //get width , height and frame rate
                        int width = 0, height = 0;
                        int frame_rate = 0;
                        int aspect_ratio_information;
                        OMX_PARAM_PORTDEFINITIONTYPE param;
                        seq_found = 1;

                        if ((ii + 3) < GST_BUFFER_SIZE(omx_mpeg2dec->first_mpeg_buffer))
                        {
                            width  = (data[ii] << 4) | (data[ii + 1] >> 4);
                            height = (((data[ii + 1] & 0xf) << 8) | data[ii + 2]);
                            aspect_ratio_information = (data[ii + 3] >> 4) & 0xf;
                            frame_rate =  (data[ii + 3] & 0xf);
                            mpeg_header = (mpeg_header << 24) | (data[ii] << 16) | (data[ii + 1] << 8) | data[ii + 2];
                            if (frame_rate <= 8)
                            {
                                self->framerate_num = frameratetab_num[frame_rate];
                                self->framerate_denom = frameratetab_denom[frame_rate];
                            }
                            switch (aspect_ratio_information)
                            {
                                case 4:
                                    self->pixel_aspect_ratio_num   = 221 * height;
                                    self->pixel_aspect_ratio_denom = 100 * width;
                                    break;
                                case 3:
                                    self->pixel_aspect_ratio_num   = 16 * height;
                                    self->pixel_aspect_ratio_denom =  9 * width;
                                    break;
                                case 2:
                                    self->pixel_aspect_ratio_num   = 4 * height;
                                    self->pixel_aspect_ratio_denom = 3 * width;
                                    break;
                                default:
                                    self->pixel_aspect_ratio_num   = 1;
                                    self->pixel_aspect_ratio_denom = 1;
                                    break;
                            }
                            if(width > FRAME_WIDTH || height > FRAME_HEIGHT)
                            {
                                GST_ELEMENT_ERROR(omx_mpeg2dec,STREAM,DECODE,("Unsupported Media"),("Video Resolution greater than %d X %d", FRAME_WIDTH, FRAME_HEIGHT));
                                buf = NULL;
                                result = GST_FLOW_OK;
                                goto ret;
                            }

                            /* reconfigure the ports with actual width/height, if caps can't get right one */
                            memset (&param, 0, sizeof (param));
                            param.nSize = sizeof (OMX_PARAM_PORTDEFINITIONTYPE);
                            param.nVersion.s.nVersionMajor = 1;
                            param.nVersion.s.nVersionMinor = 1;

                            param.nPortIndex = 1;
                            OMX_GetParameter (omx_base->gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
                            param.format.video.nFrameWidth = width;
                            param.format.video.nFrameHeight = height;
                            OMX_SetParameter (omx_base->gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
                        }
                    }
                }
                if(pic_found)
                {
                    result = omx_mpeg2dec->base_chain_func(pad, omx_mpeg2dec->first_mpeg_buffer);
                    omx_mpeg2dec->first_packet = FALSE;
                }
                buf = NULL;
        }
        else
        {
            printf("No PadChain Function \n");
        }

    ret:
        gst_object_unref(GST_ELEMENT(omx_mpeg2dec));
        return result;

}

static void
type_base_init (gpointer g_class)
{
    GstElementClass *element_class;

    element_class = GST_ELEMENT_CLASS (g_class);

    gst_element_class_set_details_simple (element_class,
                                          "OpenMAX IL MPEG2 video decoder",
                                          "Codec/Decoder/Video",
                                          "Decodes video in MPEG2 format with OpenMAX IL",
                                          "Felipe Contreras");

    {
        GstPadTemplate *template;

        template = gst_pad_template_new ("sink", GST_PAD_SINK,
                                         GST_PAD_ALWAYS,
                                         generate_sink_template ());

        gst_element_class_add_pad_template (element_class, template);
    }
}

static void
type_class_init (gpointer g_class,
                 gpointer class_data)
{
    parent_class = g_type_class_ref (GST_OMX_BASE_VIDEODEC_TYPE);
}

static void
type_instance_init (GTypeInstance *instance,
                    gpointer g_class)
{
    GstOmxBaseVideoDec *omx_base;
    GstOmxBaseFilter *omx_base_filter;
    GstOmxMpeg2Dec *omx_mpeg2dec;
    omx_base = GST_OMX_BASE_VIDEODEC (instance);
    omx_base_filter = GST_OMX_BASE_FILTER (instance);
    omx_mpeg2dec = GST_OMX_MPEG2DEC(instance);
    omx_base->compression_format = OMX_VIDEO_CodingAVC;
    gst_pad_set_setcaps_function (omx_base_filter->sinkpad, sink_setcaps);
    /* initialize h decoder specific data */
    omx_mpeg2dec->codec_data = NULL;
    omx_mpeg2dec->base_chain_func = NULL;
    omx_mpeg2dec->first_packet = FALSE;
    omx_mpeg2dec->first_mpeg_buffer = NULL;
    omx_base->only_i_frame = FALSE;
    /* replace base chain func */
    omx_mpeg2dec->base_chain_func = GST_PAD_CHAINFUNC(omx_base_filter->sinkpad);
    gst_pad_set_chain_function (omx_base_filter->sinkpad, gst_omx_mpeg2dec_pad_chain);

}

GType
gst_omx_mpeg2dec_get_type (void)
{
    static GType type = 0;

    if (G_UNLIKELY (type == 0))
    {
        GTypeInfo *type_info;

        type_info = g_new0 (GTypeInfo, 1);
        type_info->class_size = sizeof (GstOmxMpeg2DecClass);
        type_info->base_init = type_base_init;
        type_info->class_init = type_class_init;
        type_info->instance_size = sizeof (GstOmxMpeg2Dec);
        type_info->instance_init = type_instance_init;

        type = g_type_register_static (GST_OMX_BASE_VIDEODEC_TYPE, "GstOmxMpeg2Dec", type_info, 0);

        g_free (type_info);
    }

    return type;
}
