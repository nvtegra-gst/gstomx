/*
 * Copyright (c) 2009-2011, NVIDIA CORPORATION.  All rights reserved.
 *
 * Based on code copyright/by:
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
#include "gst/gst.h"
#include "gstbin_videosink.h"
#include <gst/interfaces/xoverlay.h>
#include <string.h> /* for memset, strcmp */
#include "config.h"

#define GST_TYPE_BIN_FILTER_TYPE (gst_bin_filter_get_type())
#define GST_TYPE_BIN_RENDER_TARGET_TYPE (gst_bin_render_target_type())

static GType gst_bin_filter_get_type (void);
static GType gst_bin_render_target_type (void);

static int dumpEnabled = 0;
GstElement *mixerelement = NULL;
GstElement *glelement = NULL;
GstElement *overlayelement = NULL;
GstElement *colorcontrolelement = NULL;

static GstElementClass *parent_class;
char dumpfilename[256];

static void createvideosinkpipeline(GstBinVideoSink *self);
static void destroyvideosinkpipeline(GstBinVideoSink *self);


#define MIXER_RENDER_TARGET      0
#define EGLIMAGE_RENDER_TARGET   1
#define OVERLAY_RENDER_TARGET    2

#define DEFAULT_RENDER_TARGET OVERLAY_RENDER_TARGET

enum
{
    ARG_0,
    ARG_X_SCALE,
    ARG_Y_SCALE,
    ARG_ROTATION,
    PROP_DISPLAY,
    ARG_FORCE_ASPECT_RATIO,
    ARG_CONTRAST,
    ARG_BRIGHTNESS,
    ARG_HUE,
    ARG_SATURATION,
    ARG_FRAMEDUMP,
    ARG_STARTFRAME,
    ARG_ENDFRAME,
    ARG_STEP,
    ARG_VIDEODUMPFILE,
    ARG_VIDEO_DISABLE,
    ARG_OUTPUT_YUV,
    ARG_FRAME_TYPE,
    ARG_DEINTERLACE_TYPE,
    ARG_ASYNC,
    ARG_SYNC,
    ARG_RENDER_TARGET,
    PROP_HANDLE_EVENTS,
};

#define DEFAULT_PROP_CONTRAST   1.0
#define DEFAULT_PROP_BRIGHTNESS 0.0
#define DEFAULT_PROP_HUE        0.0
#define DEFAULT_PROP_SATURATION 1.0

static void
type_base_init (gpointer g_class)
{
    GstElementClass *element_class;

    element_class = GST_ELEMENT_CLASS (g_class);

    gst_element_class_set_details_simple (element_class, 
                                              "OpenMAX IL videosink element",
                                              "Video/Sink",
                                              "Renders video",
                                              "vijaya bhaskar");

}

static void
set_property (GObject *object,
              guint prop_id,
              const GValue *value,
              GParamSpec *pspec)
{
    GstBinVideoSink *self;
    self = GST_BIN_VIDEOSINK (object);

    switch (prop_id)
    {
        case PROP_HANDLE_EVENTS:
            self->handle_events = g_value_get_boolean (value);
            break;
        case ARG_X_SCALE:
            self->x_scale = g_value_get_uint (value);
            if (G_IS_OBJECT (glelement))
            {
                g_object_set(G_OBJECT(glelement),"x-scale", self->x_scale, NULL);
            }
            break;
        case ARG_Y_SCALE:
            self->y_scale = g_value_get_uint (value);
            if (G_IS_OBJECT (glelement))
            {
                g_object_set(G_OBJECT(glelement),"y-scale", self->y_scale, NULL);
            }
            break;
        case ARG_ROTATION:
            self->rotation = g_value_get_uint (value);
            if (G_IS_OBJECT (glelement))
            {
                g_object_set(G_OBJECT(glelement),"rotation", self->rotation, NULL);
            }
            break;
        case ARG_HUE:
            self->hue = g_value_get_double (value);
            if (G_IS_OBJECT(colorcontrolelement))
            {
                g_object_set(G_OBJECT(colorcontrolelement),"hue", self->hue, NULL);
            }
            break;
        case ARG_CONTRAST:
            self->contrast = g_value_get_double (value);
            if (G_IS_OBJECT(colorcontrolelement))
            {
                g_object_set(G_OBJECT(colorcontrolelement),"contrast", self->contrast, NULL);
            }
            break;
        case ARG_BRIGHTNESS:
            self->brightness = g_value_get_double (value);
            if (G_IS_OBJECT(colorcontrolelement))
            {
                g_object_set(G_OBJECT(colorcontrolelement),"brightness", self->brightness, NULL);
            }
            break;
        case ARG_SATURATION:
            self->saturation = g_value_get_double (value);
            if (G_IS_OBJECT(colorcontrolelement))
            {
                g_object_set(G_OBJECT(colorcontrolelement),"saturation", self->saturation, NULL);
            }
            break;
        case ARG_FORCE_ASPECT_RATIO:
            self->keep_aspect = g_value_get_boolean (value);
            if (G_IS_OBJECT (glelement))
            {
                g_object_set(G_OBJECT(glelement),"force-aspect-ratio", self->keep_aspect, NULL);
            }
            else if (G_IS_OBJECT (overlayelement))
            {
                g_object_set(G_OBJECT(overlayelement),"force-aspect-ratio", self->keep_aspect, NULL);
            }
            break;
        case ARG_FRAMEDUMP:
            dumpEnabled = 0;
            self->enable_framedump = g_value_get_boolean (value);
            if (G_IS_OBJECT (glelement))
            {
                g_object_set(G_OBJECT(glelement),"enable_framedump", self->enable_framedump, NULL);
            }
            break;
        case ARG_STARTFRAME:
            self->start_frame = g_value_get_uint (value);
            if (G_IS_OBJECT (glelement))
            {
                g_object_set(G_OBJECT(glelement),"start", self->start_frame, NULL);
            }
            break;
        case ARG_ENDFRAME:
            self->end_frame = g_value_get_uint (value);
            if (G_IS_OBJECT (glelement))
            {
                g_object_set(G_OBJECT(glelement),"end", self->end_frame, NULL);
            }
            break;
        case ARG_STEP:
            self->step = g_value_get_uint (value);
            if (G_IS_OBJECT (glelement))
            {
                g_object_set(G_OBJECT(glelement),"step", self->step, NULL);
            }
            break;
       case ARG_VIDEODUMPFILE:
            self->videodumpfile_name = g_strdup (g_value_get_string (value));
            if (G_IS_OBJECT (glelement))
            {
                g_object_set(G_OBJECT(glelement),"videodumpfile", self->videodumpfile_name, NULL);
            }
            strcpy(dumpfilename,self->videodumpfile_name);
            break;
        case ARG_VIDEO_DISABLE:
            self->bvideodisable = g_value_get_boolean (value);
            if (G_IS_OBJECT (glelement))
            {
                g_object_set(G_OBJECT(glelement),"disablevideo", self->bvideodisable, NULL);
            }
            break;
        case ARG_OUTPUT_YUV:
            self->boutputyuv = g_value_get_boolean (value);
            if (G_IS_OBJECT (mixerelement))
            {
                g_object_set(G_OBJECT(mixerelement),"boutputyuv", self->boutputyuv, NULL);
            }
            break;
        case PROP_DISPLAY:
            self->display_name = g_strdup (g_value_get_string (value));
            if (G_IS_OBJECT (glelement))
            {
                g_object_set(G_OBJECT(glelement),"display", self->enable_framedump, NULL);
            }
            break;
        case ARG_DEINTERLACE_TYPE:
        {
            self->deinterlacetype = g_value_get_enum(value);
            if (G_IS_OBJECT (mixerelement))
            {
                g_object_set(G_OBJECT(mixerelement),"deint", self->deinterlacetype, NULL);
            }
        }
        break;
        case ARG_RENDER_TARGET:
        {
             destroyvideosinkpipeline(self);
             self->rendertarget = g_value_get_enum(value);
             createvideosinkpipeline(self);
        }
        break;
        case ARG_ASYNC:
        {
            if (self->rendertarget == OVERLAY_RENDER_TARGET)
            {
                if (G_IS_OBJECT (overlayelement))
                {
                    g_object_set(G_OBJECT(overlayelement), "async", g_value_get_boolean(value), NULL);
                }
            }
            else 
            {
                if (G_IS_OBJECT (glelement))
                {
                    g_object_set(G_OBJECT(glelement), "async", g_value_get_boolean(value), NULL);
                }
            }
        }
        break;
        case ARG_SYNC:
        {
            if (self->rendertarget == OVERLAY_RENDER_TARGET)
            {
                if (G_IS_OBJECT (overlayelement))
                {
                   g_object_set(G_OBJECT(overlayelement), "sync", g_value_get_boolean(value), NULL);
                }
            }
            else 
            {
                if (G_IS_OBJECT (glelement))
                {
                    g_object_set(G_OBJECT(glelement), "sync", g_value_get_boolean(value), NULL);
                }
            }
        }
        break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
get_property (GObject *object,
              guint prop_id,
              GValue *value,
              GParamSpec *pspec)
{
    GstBinVideoSink *self;

    self = GST_BIN_VIDEOSINK (object);
    switch (prop_id)
    {
        case PROP_HANDLE_EVENTS:
            self->handle_events = TRUE;
            g_value_set_boolean (value, self->handle_events);
            break;
        case ARG_X_SCALE:
            if (G_IS_OBJECT (glelement))
            {
                g_object_get(G_OBJECT(glelement),"x-scale", &self->x_scale, NULL);
                g_value_set_uint (value, self->x_scale);
            }
            break;
        case ARG_Y_SCALE:
            if (G_IS_OBJECT (glelement))
            {
                g_object_get(G_OBJECT(glelement),"y-scale", &self->y_scale, NULL);
                g_value_set_uint (value, self->y_scale);
            }
            break;
        case ARG_ROTATION:
            if (G_IS_OBJECT (glelement))
            {
                g_object_get(G_OBJECT(glelement),"rotation", &self->rotation, NULL);
                g_value_set_uint (value, self->rotation);
            }
            break;
        case ARG_HUE:
            if (G_IS_OBJECT(colorcontrolelement))
            {
                g_object_get(G_OBJECT(colorcontrolelement),"hue", &self->hue, NULL);
                g_value_set_double (value, self->hue);
            }
            break;
        case ARG_CONTRAST:
            if (G_IS_OBJECT(colorcontrolelement))
            {
                g_object_get(G_OBJECT(colorcontrolelement),"contrast", &self->contrast, NULL);
                g_value_set_double (value, self->contrast);
            }
            break;
        case ARG_BRIGHTNESS:
            if (G_IS_OBJECT(colorcontrolelement))
            {
                g_object_get(G_OBJECT(colorcontrolelement),"brightness", &self->brightness, NULL);
                g_value_set_double (value, self->brightness);
            }
            break;
        case ARG_SATURATION:
            if (G_IS_OBJECT(colorcontrolelement))
            {
                g_object_get(G_OBJECT(colorcontrolelement),"saturation", &self->saturation, NULL);
                g_value_set_double (value, self->saturation);
            }
            break;
        case ARG_FORCE_ASPECT_RATIO:
            if (G_IS_OBJECT (glelement))
            {
                g_object_get(G_OBJECT(glelement),"force-aspect-ratio", &self->keep_aspect, NULL);
                g_value_set_boolean (value, self->keep_aspect);
            }
            break;
        case ARG_FRAMEDUMP:
            if (G_IS_OBJECT (glelement))
            {
                g_object_get(G_OBJECT(glelement),"enable_framedump", &self->enable_framedump, NULL);
                g_value_set_boolean (value,self->enable_framedump);
            }
            break;
         case ARG_STARTFRAME:
            if (G_IS_OBJECT (glelement))
            {
                g_object_get(G_OBJECT(glelement),"start", &self->start_frame, NULL);
                g_value_set_uint (value,self->start_frame);
            }
            break;
        case ARG_ENDFRAME:
            if (G_IS_OBJECT (glelement))
            {
                g_object_get(G_OBJECT(glelement),"start", &self->end_frame, NULL);
                g_value_set_uint (value,self->end_frame);
            }
            break;
        case ARG_VIDEODUMPFILE:
            if (G_IS_OBJECT (glelement))
            {
                g_object_get(G_OBJECT(glelement),"videodumpfile", &self->videodumpfile_name, NULL);
                g_value_set_string (value, self->videodumpfile_name);
            }
            break;
        case ARG_STEP:
            if (G_IS_OBJECT (glelement))
            {
                g_object_get(G_OBJECT(glelement),"step", &self->step, NULL);
                g_value_set_uint (value,self->step);
            }
            break;
        case ARG_VIDEO_DISABLE:
            if (G_IS_OBJECT (glelement))
            {
                g_object_get(G_OBJECT(glelement),"disablevideo", &self->bvideodisable, NULL);
                g_value_set_boolean (value,self->bvideodisable);
            }
            break;
        case ARG_OUTPUT_YUV:
            if (G_IS_OBJECT (mixerelement))
            {
                g_object_get(G_OBJECT(mixerelement),"boutputyuv", &self->boutputyuv, NULL);
                g_value_set_boolean (value,self->boutputyuv);
            }
            break;
        case PROP_DISPLAY:
            if (G_IS_OBJECT (glelement))
            {
                g_object_get(G_OBJECT(glelement),"display", &self->display_name, NULL);
                g_value_set_string (value, self->display_name);
            }
            break;
        case ARG_DEINTERLACE_TYPE:
            if (G_IS_OBJECT (mixerelement))
            {
                g_object_get(G_OBJECT(mixerelement),"deint", &self->deinterlacetype, NULL);
                g_value_set_enum(value, self->deinterlacetype);
            }
            break;
        case ARG_RENDER_TARGET:
        {
            g_value_set_enum(value, self->rendertarget);
        }
        break;
        case ARG_ASYNC:
        {
            if (G_IS_OBJECT (glelement))
            {
                gboolean async;
                g_object_get(G_OBJECT(glelement),"async", &async, NULL);
                g_value_set_boolean (value, async);
            }
        }
        break;
        case ARG_SYNC:
        {
            if (G_IS_OBJECT (glelement))
            {
                gboolean sync;
                g_object_get(G_OBJECT(glelement),"sync", &sync, NULL);
                g_value_set_boolean (value, sync);
            }
        }
        break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
type_class_init (gpointer g_class,
                 gpointer class_data)
{
    GObjectClass *gobject_class;
//    GstElementClass *gstelement_class;

    gobject_class = (GObjectClass *) g_class;
//    gstelement_class = (GstElementClass *) g_class;
   
    parent_class = g_type_class_ref (GST_TYPE_BIN);
 
    gobject_class->set_property = set_property;
    gobject_class->get_property = get_property;

    g_object_class_install_property (gobject_class, ARG_X_SCALE,
                                     g_param_spec_uint ("x-scale", "X Scale",
                                                        "How much to scale the image in the X axis (100 means nothing)",
                                                        0, G_MAXUINT, 100, G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class, ARG_Y_SCALE,
                                     g_param_spec_uint ("y-scale", "Y Scale",
                                                        "How much to scale the image in the Y axis (100 means nothing)",
                                                        0, G_MAXUINT, 100, G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class, ARG_ROTATION,
                                     g_param_spec_uint ("rotation", "Rotation",
                                                        "Rotation angle",
                                                        0, G_MAXUINT, 360, G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class, PROP_DISPLAY,
                                       g_param_spec_string ("display", "Display", "X Display name",
                                                             NULL, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, ARG_CONTRAST,
                                       g_param_spec_double ("contrast", "Contrast", "The contrast of the video",
                                                          0.0, 2.0 , DEFAULT_PROP_CONTRAST, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, ARG_BRIGHTNESS,
                                       g_param_spec_double ("brightness", "Brightness",
                                                         "The brightness of the video", -1, 1, DEFAULT_PROP_BRIGHTNESS,
                                                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
    g_object_class_install_property (gobject_class, ARG_HUE,
                                       g_param_spec_double ("hue", "Hue", "The hue of the video", -1 , 1, DEFAULT_PROP_HUE,
                                                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, ARG_SATURATION,
                                       g_param_spec_double ("saturation", "Saturation",
                                                         "The saturation of the video", 0.0 , 2.0 , DEFAULT_PROP_SATURATION,
                                                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, ARG_FORCE_ASPECT_RATIO,
                                                          g_param_spec_boolean ("force-aspect-ratio", "Force aspect ratio",
                                                          "When enabled, scaling will respect original aspect ratio", FALSE,
                                                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
     
    g_object_class_install_property (gobject_class, ARG_FRAMEDUMP,
                                       g_param_spec_boolean ("enable-framedump", "Enable Frame Dump", "When enabled certain frames will be dumped", TRUE,
                                                          G_PARAM_READWRITE));

   g_object_class_install_property (gobject_class, ARG_STARTFRAME,
                                       g_param_spec_uint ("start", "Start Frame Number", "Start Frame Number for dumping", 0, G_MAXUINT, 30, 
                                                          G_PARAM_READWRITE));

   g_object_class_install_property (gobject_class, ARG_ENDFRAME,
                                       g_param_spec_uint ("end", "End Frame Number", "End Frame Number for dumping", 0, G_MAXUINT, 30,
                                                          G_PARAM_READWRITE));

   g_object_class_install_property (gobject_class, ARG_STEP,
                                       g_param_spec_uint ("step", "Step", "Step to increment By", 0, G_MAXUINT, 30, 
                                                          G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class, ARG_VIDEODUMPFILE,
                                       g_param_spec_string ("videodumpfile", "Videodumpfile", "Video dump file",
                                                             NULL, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, PROP_HANDLE_EVENTS,
                                      g_param_spec_boolean ("handle-events", "Handle XEvents",
                                                            "When enabled, XEvents will be selected and handled", TRUE,
                                                            G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, ARG_VIDEO_DISABLE,
                                       g_param_spec_boolean ("disablevideo", "disable video display", "Disables the Video Disiplay", FALSE,
                                                          G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_OUTPUT_YUV,
                                       g_param_spec_boolean ("boutputyuv", "mixer outputs yuv", "mixer outputs yuv", FALSE,
                                                          G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class, ARG_ASYNC,
                                       g_param_spec_boolean ("async", "enable/disable async", "enable/disable in basesink async", TRUE, G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class, ARG_SYNC,
                                       g_param_spec_boolean ("sync", "enable/disable avsync", "enable/disable avsync in basesink async", TRUE, G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class, ARG_DEINTERLACE_TYPE,
                                     g_param_spec_enum ("deint", "deinterlace mode",
                                                        "performs interlacing in BOB or Avanced1 Mode",
                                                        GST_TYPE_BIN_FILTER_TYPE, DEINTERLACE_TYPE_BOB, G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class, ARG_RENDER_TARGET,
                                     g_param_spec_enum ("rendertarget", "render target",
                                                        "renders to particular target",
                                                        GST_TYPE_BIN_RENDER_TARGET_TYPE, DEFAULT_RENDER_TARGET, G_PARAM_READWRITE));

}

static void destroyvideosinkpipeline(GstBinVideoSink *self)
{
    GstBin *bin = GST_BIN(self);
    
    gst_pad_set_active (self->sinkgpad, FALSE);
    gst_element_remove_pad (GST_ELEMENT (bin), self->sinkgpad);

    if ((self->rendertarget == MIXER_RENDER_TARGET) && G_IS_OBJECT (glelement) && G_IS_OBJECT (mixerelement))
    {
        gst_element_unlink(mixerelement, glelement);
        gst_bin_remove_many (GST_BIN(self), mixerelement, glelement, NULL);
    }
    else if ((self->rendertarget == OVERLAY_RENDER_TARGET) && G_IS_OBJECT (mixerelement) && G_IS_OBJECT (overlayelement))
    {
        gst_element_unlink(mixerelement, overlayelement);
        gst_bin_remove_many (GST_BIN(self), mixerelement, overlayelement, NULL);
    }
    else if (self->rendertarget == EGLIMAGE_RENDER_TARGET)
    {
        #ifdef USE_EGLIMAGE
        if (G_IS_OBJECT (glelement))
        {
            gst_bin_remove (GST_BIN(self), glelement);
        }
        #endif
    }

    self->sinkgpad = NULL;
    colorcontrolelement = NULL;
}

static void createvideosinkpipeline(GstBinVideoSink *self)
{
    GstPad *pad = NULL;
    GstBin *bin = GST_BIN(self);

    if (self->rendertarget == MIXER_RENDER_TARGET)
    {
        mixerelement = gst_element_factory_make ("nv_omx_videomixer", "mixer-filter");
        glelement    = gst_element_factory_make ("nv_gl_eglimagesink", "gl-sink");

        if (!G_IS_OBJECT (mixerelement) || !G_IS_OBJECT (glelement))
            return;

        gst_bin_add_many(bin, mixerelement, glelement, NULL);

        /* get the sinkpad */
        pad = gst_element_get_static_pad (mixerelement, "sink");

        /* ghost the sink pad to ourself */
        self->sinkgpad = gst_ghost_pad_new ("sink", pad);
        gst_pad_set_active (self->sinkgpad, TRUE);
        gst_element_add_pad (GST_ELEMENT (bin), self->sinkgpad);
        gst_element_no_more_pads(GST_ELEMENT(bin));
        gst_object_unref (pad);
        gst_element_link(mixerelement, glelement);
        colorcontrolelement = mixerelement;
    }
    else if (self->rendertarget == OVERLAY_RENDER_TARGET)
    {
        mixerelement = gst_element_factory_make ("nv_omx_videomixer", "mixer-filter");
        overlayelement = gst_element_factory_make ("nv_omx_overlaysink", "overlay-sink");

        if (!G_IS_OBJECT (mixerelement) || !G_IS_OBJECT (overlayelement))
            return;

        gst_bin_add_many(bin, mixerelement, overlayelement, NULL);

        /* get the sinkpad */
        pad = gst_element_get_static_pad (mixerelement, "sink");

        /* ghost the sink pad to ourself */
        self->sinkgpad = gst_ghost_pad_new ("sink", pad);
        gst_pad_set_active (self->sinkgpad, TRUE);
        gst_element_add_pad (GST_ELEMENT (bin), self->sinkgpad);
        gst_element_no_more_pads(GST_ELEMENT(bin));
        gst_object_unref (pad);
        gst_element_link(mixerelement, overlayelement);
        colorcontrolelement = overlayelement;
    }
    else if (self->rendertarget == EGLIMAGE_RENDER_TARGET)
    {
        #ifdef USE_EGLIMAGE
        glelement    = gst_element_factory_make ("nv_gl_eglimagesink", "gl-sink");

        if (! G_IS_OBJECT(glelement))
            return;

        gst_bin_add_many(bin, glelement, NULL);

        /* get the sinkpad */
        pad = gst_element_get_static_pad (glelement, "sink");

        /* ghost the sink pad to ourself */
        self->sinkgpad = gst_ghost_pad_new ("sink", pad);
        gst_pad_set_active (self->sinkgpad, TRUE);
        gst_element_add_pad (GST_ELEMENT (bin), self->sinkgpad);
        gst_element_no_more_pads(GST_ELEMENT(bin));
        gst_object_unref (pad);
        colorcontrolelement = glelement;
        #endif
    }

    return;
}

static void
type_instance_init (GTypeInstance *instance,
                    gpointer g_class)
{
    GstBinVideoSink *self;
//    GstElementClass *element_class;

//    element_class = GST_ELEMENT_CLASS (g_class);

    self = GST_BIN_VIDEOSINK (instance);

    GST_LOG_OBJECT (self, "begin");

    self->deinterlacetype = DEINTERLACE_TYPE_BOB;
    self->frametype       = FRAME_TYPE_INTERLACED;
    self->rendertarget    = DEFAULT_RENDER_TARGET;

    if (g_getenv ("RENDER_TARGET"))
    {
        if (!strcmp("xwindow",g_getenv ("RENDER_TARGET")))
            self->rendertarget    = MIXER_RENDER_TARGET;
        if (!strcmp("overlay",g_getenv ("RENDER_TARGET")))
            self->rendertarget    = OVERLAY_RENDER_TARGET;
    }

    createvideosinkpipeline(self);
}

#ifdef USE_EGLIMAGE
static void
#ifndef GST_DISABLE_OLD_SETXWINDOW
omx_videosink_set_xwindow_id (GstXOverlay * overlay, XID xwindow_id)
#else
omx_videosink_set_window_handle (GstXOverlay * overlay, guintptr xwindow_id)
#endif
{
    GstBinVideoSink *self = GST_BIN_VIDEOSINK (overlay);

    if (self->rendertarget == OVERLAY_RENDER_TARGET)
    {
        if (G_IS_OBJECT (overlayelement))
        {
            g_object_set(overlayelement, "xwindow", xwindow_id, NULL);
        }
    }
    else {
            if (G_IS_OBJECT (glelement))
            {
#ifndef GST_DISABLE_OLD_SETXWINDOW
                gst_x_overlay_set_xwindow_id(GST_X_OVERLAY(glelement),xwindow_id);
#else
                gst_x_overlay_set_window_handle(GST_X_OVERLAY(glelement),xwindow_id);
#endif
            }
    }
}

static void
omx_videosink_expose (GstXOverlay * overlay)
{
    /* TODO */ 
    /* Nothing to do as of now */
}

static void
omx_videosink_set_event_handling (GstXOverlay * overlay,
    gboolean handle_events)
{
    GstBinVideoSink *self;

    self = GST_BIN_VIDEOSINK (overlay);
    self->handle_events = handle_events; 
}
#endif

static void
omx_videosink_xoverlay_init (GstXOverlayClass * iface)
{
#ifdef USE_EGLIMAGE
#ifndef GST_DISABLE_OLD_SETXWINDOW
  iface->set_xwindow_id = omx_videosink_set_xwindow_id;
#else
  iface->set_window_handle = omx_videosink_set_window_handle;
#endif
  iface->expose = omx_videosink_expose;
  iface->handle_events = omx_videosink_set_event_handling;
#endif
}

static gboolean
interface_supported (GstImplementsInterface *iface,
                     GType type)
{
    g_assert (type == GST_TYPE_X_OVERLAY);
    return TRUE;
}

static void
interface_init (GstImplementsInterfaceClass *klass)
{
    klass->supported = interface_supported;
}

GType
gst_omx_bin_videosink_get_type (void)
{
    static GType type = 0;
    static const GInterfaceInfo overlay_info = {
      (GInterfaceInitFunc) omx_videosink_xoverlay_init,
      NULL,
      NULL,
    };

    if (G_UNLIKELY (type == 0))
    {
        GTypeInfo *type_info;
        GInterfaceInfo *iface_info;

        type_info = g_new0 (GTypeInfo, 1);
        type_info->class_size = sizeof (GstBinVideoSinkClass);
        type_info->class_init = type_class_init;
        type_info->instance_init = type_instance_init;
        type_info->instance_size = sizeof (GstBinVideoSink);
        type_info->base_init = type_base_init;

        type = g_type_register_static (GST_TYPE_BIN, "GstBinVideoSink", type_info, 0);
        g_free (type_info);
        iface_info = g_new0 (GInterfaceInfo, 1);
        iface_info->interface_init = (GInterfaceInitFunc) interface_init;

        g_type_add_interface_static (type, GST_TYPE_IMPLEMENTS_INTERFACE, iface_info);
        g_free (iface_info);
        g_type_add_interface_static (type, GST_TYPE_X_OVERLAY,&overlay_info);
    }

    return type;
}

static GType
gst_bin_filter_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static GEnumValue vals[] =
        {
            {DEINTERLACE_TYPE_BOB,        "Bob filter",             "Deinterlace with Bob Filter"},
            {DEINTERLACE_TYPE_ADVANCED1,   "Advanced1 filter",        "Deinterlace with Advanced1 filter"},
            {0, NULL, NULL},
        };

        type = g_enum_register_static ("GstOmxVideoSinkFilterMode", vals);
    }

    return type;
}

static GType
gst_bin_render_target_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static GEnumValue vals[] =
        {
            {MIXER_RENDER_TARGET,       "Mixer Render Target",         "decoder->mixer->eglimagesink"},
            {EGLIMAGE_RENDER_TARGET,    "EglImage Render Target",      "decoder->EglSurface"},
            {OVERLAY_RENDER_TARGET,     "Overlay Render Target",       "decoder->mixer->overlaysink"},
            {0, NULL, NULL},
        };

        type = g_enum_register_static ("GstOmxVideoSinkRenderTarget", vals);
    }
    return type;
}
