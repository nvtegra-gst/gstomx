/*
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * Based on code copyright/by:
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "config.h"
#include "gstomx.h"
#include "gstomx_x11eglutils.h"


/* Maximum number of attributes passed to eglCreateWindowSurface */
#define MAX_ATTRIB 1

/* forward declarations */
static void disown_thread_context(GSTGL_DISPLAY_DATA_PTR);
static void own_thread_context(GSTGL_DISPLAY_DATA_PTR);

static GMutex *gl_mutex;
static int FrameCnt=0;
static gboolean eglInitialized = FALSE;
extern char dumpfilename[256];

#define PROGRAM_VIDEO 0
#define PROGRAM_BORDER 1
#define NUM_PROGRAMS 2
static GLuint programObjs[NUM_PROGRAMS];

/* ERROR_GOTO_FAIL - if <val> is true, print <string> and goto fail */
#define ERROR_GOTO_FAIL(val, string) \
    do { \
        if (val) {\
            printf(string); \
            goto fail; \
        } \
    } while (0)

/* extension functions */
static PFNEGLCREATEIMAGEKHRPROC eglCreateImageKHR;
static PFNEGLDESTROYIMAGEKHRPROC eglDestroyImageKHR;
static PFNEGLCREATESYNCKHRPROC eglCreateSyncKHR;
static PFNEGLDESTROYSYNCKHRPROC eglDestroySyncKHR;
static PFNEGLCLIENTWAITSYNCKHRPROC eglClientWaitSyncKHR;
static PFNEGLGETSYNCATTRIBKHRPROC eglGetSyncAttribKHR;

/* vertex shader program */
static const char s_VertexShader[] =
    "attribute vec4 position;\n"
    "attribute vec2 tcoord;\n"
    "varying vec2 vtcoord;\n"
    "void main() {\n"
    "  gl_Position = position;\n"
    "  vtcoord = tcoord;\n"
    "}\n";

/* fragment shader program */
static const char s_FragmentShader[] =
    "precision mediump float;\n"
    "varying vec2 vtcoord;\n"
    "uniform sampler2D tex;\n"
    "void main() {\n"
    "  gl_FragColor = texture2D(tex, vtcoord);\n"
    "}\n";

/* fragment shader program for pillars and letterbox */
static const char s_BorderFragmentShader[] =
    "precision mediump float;\n"
    "void main() {\n"
    "  gl_FragColor = vec4(0,0,0,1);\n"
    "}\n";

/* 3D vertex geometry  */
static const GLfloat s_DefaultVerts[] =
{
  -1, -1, 0, 1,
   1, -1, 0, 1,
  -1,  1, 0, 1,
   1,  1, 0, 1
};
static GLfloat s_Verts[16];

/* 3D vertex geometry for a border around the video that extends
 * to the edges of the screen. It satisfies both pillar and letterbox
 * scenarios.  Initialized to have zero thickness. */
static const GLfloat s_DefaultBorderVerts[] =
{
  -1, -1, 0, 1,
  -1, -1, 0, 1,
  -1,  1, 0, 1,
  -1,  1, 0, 1,
   1,  1, 0, 1,
   1,  1, 0, 1,
   1, -1, 0, 1,
   1, -1, 0, 1,
  -1, -1, 0, 1,
  -1, -1, 0, 1
};
static GLfloat s_BorderVerts[40];

/* 3D tex coords */
static const GLfloat s_TexCoords[] =
{
  0, 0,
  1, 0,
  0, 1,
  1, 1
};

/* 3D tex coords */
static const GLfloat s_TexCoordsInv[] =
{
  0, 1,
  1, 1,
  0, 0,
  1, 0
};

void disown_thread_context(GSTGL_DISPLAY_DATA_PTR gfx_display)
{
  EGLBoolean eglStatus;
  if (gfx_display->display != EGL_NO_DISPLAY) {
    eglStatus = eglMakeCurrent(gfx_display->display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
    if (!eglStatus)
      printf("Error clearing current surfaces/context\n");
  }
}

void own_thread_context(GSTGL_DISPLAY_DATA_PTR gfx_display)
{
  EGLBoolean eglStatus;
  if (gfx_display->display != EGL_NO_DISPLAY) {
    eglStatus = eglMakeCurrent(gfx_display->display, gfx_display->surface, gfx_display->surface, gfx_display->context);
    if (!eglStatus)
      printf("Error Getting current surfaces/context %d\n", eglStatus);
  }
}

void nvomx_init_eglimagepointers(void)
{
  /* Get extension functions */
  eglCreateImageKHR = (PFNEGLCREATEIMAGEKHRPROC)eglGetProcAddress("eglCreateImageKHR");
  ERROR_GOTO_FAIL(!eglCreateImageKHR,"ERROR getting proc addr of eglCreateImageKHR\n");

  eglDestroyImageKHR = (PFNEGLDESTROYIMAGEKHRPROC)eglGetProcAddress("eglDestroyImageKHR");
  ERROR_GOTO_FAIL(!eglDestroyImageKHR,"ERROR getting proc addr of eglDestroyImageKHR\n");

  eglCreateSyncKHR = (PFNEGLCREATESYNCKHRPROC)eglGetProcAddress("eglCreateSyncKHR");
  ERROR_GOTO_FAIL(!eglCreateSyncKHR, "ERROR getting proc addr of eglCreateSyncKHR\n");

  eglDestroySyncKHR = (PFNEGLDESTROYSYNCKHRPROC)eglGetProcAddress("eglDestroySyncKHR");
  ERROR_GOTO_FAIL(!eglDestroySyncKHR, "ERROR getting proc addr of eglDestroySyncKHR\n");

  eglClientWaitSyncKHR = (PFNEGLCLIENTWAITSYNCKHRPROC)eglGetProcAddress("eglClientWaitSyncKHR");
  ERROR_GOTO_FAIL(!eglClientWaitSyncKHR, "ERROR getting proc addr of eglClientWaitSyncKHR\n");

  eglGetSyncAttribKHR = (PFNEGLGETSYNCATTRIBKHRPROC)eglGetProcAddress("eglGetSyncAttribKHR");
  ERROR_GOTO_FAIL(!eglGetSyncAttribKHR, "ERROR getting proc addr of eglGetSyncAttribKHR\n");

  return;

fail:
  printf("Function:%s failed \n",__FUNCTION__);
  return;
}

/* Take precompiled shader binaries and builds a shader program */
GLuint load_shader_code(const char* vertSource, const char* fragSource)
{
/* Binary shaders not supportable for non-ES OpenGL */
#ifdef GL_ES_VERSION_2_0
  GLuint prog;
  GLuint vertShader;
  GLuint fragShader;

  /* Create the program */
  prog = glCreateProgram();

  /* Create the GL shader objects */
  vertShader = glCreateShader(GL_VERTEX_SHADER);
  fragShader = glCreateShader(GL_FRAGMENT_SHADER);

  glShaderSource(vertShader, 1, &vertSource, NULL);
  glCompileShader(vertShader);

  /* Set fragment shader source and compile shader*/
  glShaderSource(fragShader, 1, &fragSource, NULL);
  glCompileShader(fragShader);

  /* Attach the shaders to the program */
  glAttachShader(prog, vertShader);
  glAttachShader(prog, fragShader);

  /* Delete the shaders */
  glDeleteShader(vertShader);
  glDeleteShader(fragShader);

  /* Link and validate the shader program */
  glLinkProgram(prog);

  return prog;
#else  // GL_ES_VERSION_2_0
  return 0;
#endif // GL_ES_VERSION_2_0
}

/* SetupGL - Setup OpenGL, including default geometry and shaders. */
int nvomx_setupgl(void)
{
  GLuint texSampler;

  /* Initialize the 3D Vertex Geometry to the default Values */
  memcpy(s_Verts, s_DefaultVerts, sizeof(s_Verts));
  memcpy(s_BorderVerts, s_DefaultBorderVerts, sizeof(s_BorderVerts));

  programObjs[PROGRAM_VIDEO] = load_shader_code(s_VertexShader,
                                                s_FragmentShader);
  programObjs[PROGRAM_BORDER] = load_shader_code(s_VertexShader,
                                                 s_BorderFragmentShader);
  if (!programObjs[PROGRAM_VIDEO] || !programObjs[PROGRAM_BORDER]) return 0;

  /* Bind texture samplers */
  glUseProgram(programObjs[PROGRAM_VIDEO]);
  texSampler = glGetUniformLocation(programObjs[PROGRAM_VIDEO], "tex");
  glUniform1i(texSampler, 0);

  glDisable(GL_DEPTH_TEST);
  return 1;
}

/* Setup geometry state for main video */
void gstgl_reconfigure_geometry_video(gboolean invertTexture)
{
  if (invertTexture) { // inverted video
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, s_Verts);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, s_TexCoordsInv);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
  } else { // regular non-inverted video
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, s_Verts);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, s_TexCoords);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
  }
  return;
}

/* Setup geometry state for drawing the pillars or letterbox bars */
void gstgl_reconfigure_geometry_border(void)
{
  glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, s_BorderVerts);
  glEnableVertexAttribArray(0);
  glDisableVertexAttribArray(1);
  return;
}

static int capture_frame(int width , int height , int framenum)
{
  int i,j;
  unsigned char *buffer , *cur , *temp;
  int bytes_per_row,dst;
  char img_filename[512];
  FILE *fp;

  sprintf(img_filename,"%s_%05d.ppm",dumpfilename,framenum);
  buffer = (unsigned char *) g_malloc(width * height *4);
  temp =   (unsigned char *) g_malloc(width * height *3);

  if (!buffer || !temp) {
     printf("Could not allocate memory for screenshot\n");
     return 0;
  }

  fp = fopen(img_filename,"wb");
  if (!fp) {
   printf("File %s cannot be opened\n",img_filename);
   return 0;
  }

  bytes_per_row = width * 4;
  glReadPixels(0,0,width,height,GL_RGBA,GL_UNSIGNED_BYTE,(void *)buffer);
  cur = buffer + (bytes_per_row * (height + 1));
  fprintf(fp,"P6 %d %d 255\n",width,height);
  dst = 0;
  for (j=0;j < height ; j++) {
   cur -=2 * bytes_per_row;
   for ( i =0 ; i <width; i++, cur +=4) {
     temp[dst++]= cur[0];
     temp[dst++]= cur[1];
     temp[dst++]= cur[2];
    }
  }
  printf("Dumping Frame number %d\n",framenum);
  fwrite(temp , 1, height*width*3,fp);
  fflush(fp);
  free(buffer);
  free(temp);
  fclose(fp);
  return 1;
}

void gstgl_apply_aspect_ratio_if_needed(GSTGL_DISPLAY_DATA_PTR gfx_display, int srcWidth , int srcHeight , float pixel_aspect_ratio, gboolean apply_aspect_ratio)
{
  float srcAR,dstAR, zoomRatio, xFactor, yFactor;
  int scaledHeight, scaledWidth;
  int dispWidth  = gfx_display->width;
  int dispHeight = gfx_display->height;

  /* Reinitialize the 3D Vertex Geometry to the original Values */
  memcpy(s_Verts, s_DefaultVerts, sizeof(s_Verts));
  memcpy(s_BorderVerts, s_DefaultBorderVerts, sizeof(s_BorderVerts));

  if (!apply_aspect_ratio)
  {
    return;
  }

  srcAR = (float) (pixel_aspect_ratio * (float)srcWidth/srcHeight);
  dstAR = (float)dispWidth/dispHeight;

  if (srcAR > dstAR) {
    zoomRatio = (float)dstAR/srcAR;
    scaledHeight = (int)((float)dispHeight * zoomRatio);
    scaledWidth = dispWidth;
  } else if (srcAR < dstAR) {
    zoomRatio = (float)srcAR/dstAR;
    scaledHeight = dispHeight;
    scaledWidth = (int)((float)dispWidth * zoomRatio);
  } else {
    scaledHeight = dispHeight;
    scaledWidth  = dispWidth;
  }

  xFactor = (float)scaledWidth/dispWidth;
  yFactor = (float)scaledHeight/dispHeight;

  /* Video geometry */
  s_Verts[0] *= xFactor; s_Verts[1] *= yFactor;
  s_Verts[4] *= xFactor; s_Verts[5] *= yFactor;
  s_Verts[8] *= xFactor; s_Verts[9] *= yFactor;
  s_Verts[12]*= xFactor; s_Verts[13]*= yFactor;

  /* Border geometry (pillars or letterbox) */
  s_BorderVerts[4] =  s_Verts[0];  s_BorderVerts[5]  = s_Verts[1];
  s_BorderVerts[12] = s_Verts[8];  s_BorderVerts[13] = s_Verts[9];
  s_BorderVerts[20] = s_Verts[12]; s_BorderVerts[21] = s_Verts[13];
  s_BorderVerts[28] = s_Verts[4];  s_BorderVerts[29] = s_Verts[5];
  s_BorderVerts[36] = s_Verts[0];  s_BorderVerts[37] = s_Verts[1];
}

static void clearBackGround()
{
  glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT);
  glFinish();
}

/* Callback to resize window */
static void resizeCB(int width, int height)
{
  glViewport(0, 0, width, height);
  clearBackGround();
}

void nvomx_x11egl_setwindowid(GSTGL_DISPLAY_DATA_PTR gfx_display, XID windowid)
{
  g_mutex_lock(gl_mutex);
  gfx_display->nativeWindow = windowid;
  g_mutex_unlock(gl_mutex);
}

void gstgl_clearscreen(GSTGL_DISPLAY_DATA_PTR gfx_display)
{
  own_thread_context(gfx_display);
  clearBackGround();
  eglSwapBuffers(gfx_display->display,gfx_display->surface);
  disown_thread_context(gfx_display);
}

/* Shutdown, Free all EGL and native window system resources */
void nvomx_x11egl_destroy(GSTGL_DISPLAY_DATA_PTR gfx_display)
{
  EGLBoolean eglStatus;
  g_mutex_lock(gl_mutex);
  FrameCnt = 0;

  if (!eglInitialized) {
    g_mutex_unlock(gl_mutex);
    return;
  }

  /* Clear the screen  */
  own_thread_context(gfx_display);
  clearBackGround(gfx_display);
  eglSwapBuffers(gfx_display->display,gfx_display->surface);

  /* Clear rendering context */
  if (gfx_display->display != EGL_NO_DISPLAY) {
    eglStatus = eglMakeCurrent(gfx_display->display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
    if (!eglStatus)
      printf("Error clearing current surfaces/context\n");
  }

  /* Destroy the EGL context */
  if (gfx_display->context != EGL_NO_CONTEXT) {
    eglStatus = eglDestroyContext(gfx_display->display, gfx_display->context);
    if (!eglStatus)
      printf("Error destroying EGL context\n");
    gfx_display->context = EGL_NO_CONTEXT;
  }

  /* Destroy the EGL surface */
  if (gfx_display->surface != EGL_NO_SURFACE) {
    eglStatus = eglDestroySurface(gfx_display->display, gfx_display->surface);
    if (!eglStatus)
      printf("Error destroying EGL surface\n");
    gfx_display->surface = EGL_NO_SURFACE;
  }

  /* Terminate EGL */
  if (gfx_display->display != EGL_NO_DISPLAY) {
    eglStatus = eglTerminate(gfx_display->display);
    if (!eglStatus)
      printf("Error terminating EGL\n");
    gfx_display->display = EGL_NO_DISPLAY;
  }

  /* Close the native window */
  if (gfx_display->platformspecific && gfx_display->nativeWindow) {
    XDestroyWindow((Display *)gfx_display->platformspecific->XDisplay,
      (Window) gfx_display->nativeWindow);
    XCloseDisplay ((Display *)gfx_display->platformspecific->XDisplay);
    gfx_display->nativeWindow = (NativeWindowType)0;
    g_free (gfx_display->platformspecific);
  }
  eglInitialized = FALSE;
  g_mutex_unlock(gl_mutex);
}

void egl_init(void)
{
  gl_mutex = g_mutex_new();
}

void egl_destroy(void)
{
  g_mutex_free(gl_mutex);
  gl_mutex = NULL;
}

/* Initialize native window system and EGL */
OMX_ERRORTYPE nvomx_x11egl_initialize(GSTGL_DISPLAY_DATA_PTR gfx_display)
{
  /* NOTE: This array is modified by the code below. The indices used
   * are hardcoded numbers, so be careful if you change the order of
   * the values */
  static EGLint configAttrs[] = {
    EGL_RED_SIZE,        1,
    EGL_GREEN_SIZE,      1,
    EGL_BLUE_SIZE,       1,
    EGL_DEPTH_SIZE,     16,
    EGL_RENDERABLE_TYPE,
    EGL_NONE,
#ifdef EGL_NV_coverage_sample
    EGL_COVERAGE_BUFFERS_NV,  0,
    EGL_COVERAGE_SAMPLES_NV,  0,
#endif
    EGL_NONE
  };

  EGLint contextAttrs[] = { EGL_NONE, 0, EGL_NONE };
  EGLenum    eglApi;
  EGLint     configCount;
  EGLint     eglApiBit  = EGL_OPENGL_ES2_BIT;
  EGLint     windowAttrs[2*MAX_ATTRIB + 1];
  EGLint     windowIndex = 0;
  EGLint     windowSize[2]  = { 0, 0 };
  EGLConfig* configList = NULL;
  EGLBoolean eglStatus;
  Atom       wm_destroy_window;
  XSizeHints size_hints;
  XWindowAttributes attr;

  g_mutex_lock(gl_mutex);

  if (!eglInitialized) {
    nvomx_init_eglimagepointers();
  } else {
    g_mutex_unlock(gl_mutex);
    return OMX_ErrorNone;
  }

  switch (eglApiBit) {
    case EGL_OPENGL_ES2_BIT:
      eglApi = EGL_OPENGL_ES_API;
      break;

    case EGL_OPENVG_BIT:
      eglApi = EGL_OPENVG_API;
      break;

    default:
      printf("Invalid/unsupported API requested\n");
      goto fail;
  }

  gfx_display->platformspecific = (GstGl_PlatformState*)g_malloc(sizeof(GstGl_PlatformState));
  if (!gfx_display->platformspecific) {
      printf("Could not allocate platform specific storage memory\n");
      goto fail;
  }

  /* Open X display */
  gfx_display->platformspecific->XDisplay = XOpenDisplay(NULL);
  if (gfx_display->platformspecific->XDisplay == NULL) {
    printf("X failed to open display\n");
    goto fail;
  }

  /* Obtain the EGL display */
  gfx_display->display = eglGetDisplay((EGLNativeDisplayType) gfx_display->platformspecific->XDisplay);
  if (gfx_display->display == EGL_NO_DISPLAY) {
    printf("EGL failed to obtain display\n");
    goto fail;
  }

  /* Initialize EGL */
  eglStatus = eglInitialize(gfx_display->display, 0, 0);
  if (!eglStatus) {
    printf("EGL failed to initialize\n");
    goto fail;
  }

  /* Put API-specific values into the config and context attributes.
   * This assumes that the EGL_RENDERABLE_TYPE enum is the fourth one
   * in the configAttrs array and that the contextAttrs array is
   * empty but has room for an EGL_CONTEXT_CLIENT_VERSION enum.
   * Also, while we're at it, bind the client API. */

  configAttrs[9] = eglApiBit;
  switch (eglApiBit) {
    case EGL_OPENGL_ES2_BIT:
      contextAttrs[0] = EGL_CONTEXT_CLIENT_VERSION;
      contextAttrs[1] = 2;
      break;

    default:
      break;
  }
  eglBindAPI(eglApi);

  /* Find out how many configurations suit our needs */
  eglStatus = eglChooseConfig(gfx_display->display, configAttrs, NULL, 0, &configCount);
  if (!eglStatus || !configCount) {
    printf("EGL failed to return any matching configurations\n");
    goto fail;
  }

  /* Allocate room for the list of matching configurations */
  configList = (EGLConfig*)g_malloc(configCount * sizeof(EGLConfig));
  if (!configList) {
    printf("malloc failure obtaining configuration list\n");
    goto fail;
  }

  /* Obtain the configuration list from EGL */
  eglStatus = eglChooseConfig(gfx_display->display, configAttrs, configList, configCount, &configCount);
  if (!eglStatus || !configCount) {
    printf("EGL failed to populate configuration list\n");
    goto fail;
  }

  /* Select an EGL configuration that matches the native window */
  gfx_display->config = configList[0];

  /* Set buffering options
   * Set window size. If no specified, default to 800x480 */

  if(!gfx_display->nativeWindow) {

    gfx_display->platformspecific->XScreen = DefaultScreen(gfx_display->platformspecific->XDisplay);

    if(gfx_display->fullscreen_window){
         windowSize[0] = DisplayWidth(gfx_display->platformspecific->XDisplay,gfx_display->platformspecific->XScreen);
         windowSize[1] = DisplayHeight(gfx_display->platformspecific->XDisplay,gfx_display->platformspecific->XScreen);
      }
     else{
       windowSize[0] = 800;
       windowSize[1] = 480;
      }

    /* Create a native window */
    gfx_display->nativeWindow =
      (NativeWindowType) XCreateSimpleWindow(gfx_display->platformspecific->XDisplay,
        RootWindow(gfx_display->platformspecific->XDisplay,
        gfx_display->platformspecific->XScreen), 0, 0, windowSize[0], windowSize[1], 0,
        BlackPixel(gfx_display->platformspecific->XDisplay, gfx_display->platformspecific->XScreen),
        WhitePixel(gfx_display->platformspecific->XDisplay, gfx_display->platformspecific->XScreen));

    if (!gfx_display->nativeWindow) {
      printf("Error creating native window\n");
      goto fail;
    }

    size_hints.flags = PPosition | PSize | PMinSize;
    size_hints.x = 0;
    size_hints.y = 0;
    size_hints.min_width  = (windowSize[0] < 320) ? windowSize[0] : 320;
    size_hints.min_height = (windowSize[1] < 240) ? windowSize[1] : 240;
    size_hints.width  = windowSize[0];
    size_hints.height = windowSize[1];

    XSetStandardProperties(gfx_display->platformspecific->XDisplay, (Window) gfx_display->nativeWindow,
      "omx","omx" , None, NULL, 0, &size_hints);

    XSetWindowBackgroundPixmap(gfx_display->platformspecific->XDisplay, (Window) gfx_display->nativeWindow, None);

    wm_destroy_window = XInternAtom(gfx_display->platformspecific->XDisplay, "WM_DELETE_WINDOW", False);

    XSetWMProtocols(gfx_display->platformspecific->XDisplay, (Window) gfx_display->nativeWindow,
      &wm_destroy_window, True);

    /* Make sure the MapNotify event goes into the queue */
    XSelectInput(gfx_display->platformspecific->XDisplay, (Window) gfx_display->nativeWindow, StructureNotifyMask);

    XMapWindow(gfx_display->platformspecific->XDisplay, (Window) gfx_display->nativeWindow);
    XGetWindowAttributes (gfx_display->platformspecific->XDisplay, (Window) gfx_display->nativeWindow, &attr);
  } else {
    /* Window Handle is given by the application Obtain the EGL display */
    XGetWindowAttributes (gfx_display->platformspecific->XDisplay, (Window) gfx_display->nativeWindow, &attr);
    XSelectInput (gfx_display->platformspecific->XDisplay, (Window) gfx_display->nativeWindow , ExposureMask |
      StructureNotifyMask | PointerMotionMask | KeyPressMask | KeyReleaseMask);
  }

  /* Create an EGL window surface for the native window */
  windowAttrs[windowIndex++] = EGL_NONE;
  gfx_display->surface = eglCreateWindowSurface(gfx_display->display, gfx_display->config, gfx_display->nativeWindow, windowAttrs);
  if (!gfx_display->surface) {
    printf("EGL couldn't create window\n");
    goto fail;
  }

  /* Create an EGL context */
  gfx_display->context = eglCreateContext(gfx_display->display, gfx_display->config, NULL, contextAttrs);
  if (!gfx_display->context) {
    printf("EGL couldn't create context\n");
    goto fail;
  }

  /* Make the context and surface current for rendering */
  eglStatus = eglMakeCurrent(gfx_display->display, gfx_display->surface, gfx_display->surface, gfx_display->context);
  if (!eglStatus) {
    printf("EGL couldn't make context/surface current\n");
    goto fail;
  }

  /* Query the EGL surface width and height */
  eglStatus =  eglQuerySurface(gfx_display->display, gfx_display->surface, EGL_WIDTH,  &gfx_display->width) &&
    eglQuerySurface(gfx_display->display, gfx_display->surface, EGL_HEIGHT, &gfx_display->height);
  if (!eglStatus) {
    printf("EGL couldn't get window width/height %d %d\n",gfx_display->width,gfx_display->height);
    goto fail;
  }

  glGetIntegerv(GL_MAX_TEXTURE_SIZE, &(gfx_display->maxTextureSize));

  /* free the configuration list and return success */
  free(configList);
  nvomx_setupgl();
  disown_thread_context(gfx_display);
  eglInitialized = TRUE;
  g_mutex_unlock(gl_mutex);

  return  OMX_ErrorNone;

fail:

  if (configList) free(configList);
  g_mutex_unlock(gl_mutex);
  nvomx_x11egl_destroy(gfx_display);
  return OMX_ErrorInsufficientResources;
}

/*
 * SetupEglImages - Create textures, then create eglImages from those textures,
 * then tell OpenMAX to use those eglImages.  Requires that SetupOMX has already
 * been called.
 */
OMX_ERRORTYPE nvomx_allocate_eglimages(void *display_data , nvomx_eglbuffer **eglbuffer, int width, int height)
{
  OMX_ERRORTYPE err = OMX_ErrorNone;
  EGLint attrib = EGL_NONE;
  nvomx_eglbuffer *buffer;
  GstGl_DisplayData *gfx_display = (GstGl_DisplayData*) display_data;

  /* Use default width & height */
  if (!width || !height) {
    width =800;
    height=480;
  }

  if (width > gfx_display->maxTextureSize)
      width = gfx_display->maxTextureSize;

  if (height > gfx_display->maxTextureSize)
      height = gfx_display->maxTextureSize;

  g_mutex_lock(gl_mutex);

  own_thread_context(gfx_display);
  glActiveTexture(GL_TEXTURE0);

  buffer = (nvomx_eglbuffer*) g_malloc(sizeof(nvomx_eglbuffer));
  memset (buffer, 0, sizeof(nvomx_eglbuffer));

  glGenTextures(1, &(buffer->hTex));
  glBindTexture(GL_TEXTURE_2D, buffer->hTex);

  /* create space for buffer with a texture */
  glTexImage2D(
    GL_TEXTURE_2D,    // target
    0,                // level
    GL_RGBA,          // internal format
    width,            // height
    height,           // width
    0,                // border
    GL_RGBA,          // format
    GL_UNSIGNED_BYTE, // type
    NULL);            // pixels -- will be provided later

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  /* create EGLImage from texture */
  buffer->hEglImg = eglCreateImageKHR (gfx_display->display, gfx_display->context,
    EGL_GL_TEXTURE_2D_KHR, (EGLClientBuffer)(buffer->hTex), &attrib);
  ERROR_GOTO_FAIL (!buffer->hEglImg, "ERROR creating EglImage\n");

  *eglbuffer = buffer;

  clearBackGround();
  disown_thread_context(gfx_display);
  g_mutex_unlock(gl_mutex);
  return OMX_ErrorNone;

fail:
  if (!err)
    err = OMX_ErrorUndefined;
  g_mutex_unlock(gl_mutex);
  return err;
}

/*
 * ShutdownEglImage - release resources created with SetupEglImage.
 * StopPlayback must be called before this.
 */
void nvomx_destroy_eglimages(void *display_data, nvomx_eglbuffer *eglbuffer)
{
  GstGl_DisplayData *gfx_display = (GstGl_DisplayData*) display_data;
  g_mutex_lock(gl_mutex);

  if (!eglInitialized) {
    g_mutex_unlock(gl_mutex);
    return;
  }

  own_thread_context(gfx_display);

  if (eglbuffer->hEglImg) {
    eglDestroyImageKHR(gfx_display->display, eglbuffer->hEglImg);
    eglbuffer->hEglImg = NULL;
  }

  glDeleteTextures(1, &(eglbuffer->hTex));
  g_free(eglbuffer);

  disown_thread_context(gfx_display);
  g_mutex_unlock(gl_mutex);
}

void nvomx_render_eglimage(GSTGL_DISPLAY_DATA_PTR gfx_display, nvomx_eglbuffer *buf , int dumpEnabled ,int start_frame, int end_frame, int step )
{
    int ret = 0;
    EGLSyncKHR sync;
    own_thread_context(gfx_display);

    // Draw letterbox or pillars to clear background
    gstgl_reconfigure_geometry_border();
    glUseProgram(programObjs[PROGRAM_BORDER]);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 10);

    // Draw video
    gstgl_reconfigure_geometry_video(buf->bimageflip);
    glUseProgram(programObjs[PROGRAM_VIDEO]);
    glBindTexture(GL_TEXTURE_2D, buf->hTex);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    sync = eglCreateSyncKHR(gfx_display->display, EGL_SYNC_FENCE_KHR, NULL);
    if (sync == EGL_NO_SYNC_KHR)
        printf("Error %x creating eglSync object on display %p\n", eglGetError(), gfx_display->display);
    // if dumpEnabled is set then dump frames starting from start_frame until end_frame in the increment of step. Default is just one frame (30'th frame)
    if(dumpEnabled == 1)
    {
     if(FrameCnt >= start_frame && FrameCnt <= end_frame && ((FrameCnt-start_frame)%step) == 0) {
        ret = capture_frame(gfx_display->width,gfx_display->height,FrameCnt);
        if (ret != 1) {
             dumpEnabled = 0;
             printf("File Dump Failed\n");
          }
       }
    }

    eglSwapBuffers(gfx_display->display,gfx_display->surface);

    eglClientWaitSyncKHR(gfx_display->display, sync,
            EGL_SYNC_FLUSH_COMMANDS_BIT_KHR,
            EGL_FOREVER_KHR);
    eglDestroySyncKHR(gfx_display->display, sync);


    // Below glClear is required to clear the remaining Frame Buffer to avoid flicker
    // Here we are doing it thrice to handle the case of triple-buffered flipqueue
    if(gfx_display->clearCount++<3) {
        glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
    }
    FrameCnt++;
    nvomx_checkevents(gfx_display);
    disown_thread_context(gfx_display);
}

void handle_dimensions_change(GSTGL_DISPLAY_DATA_PTR gfx_display, int Width, int Height)
{
    EGLBoolean eglStatus;
    XWindowAttributes attr;
    XWindowChanges values;
    if(gfx_display->width == Width && gfx_display->height==Height)
        return;

    own_thread_context(gfx_display);
    XGetWindowAttributes (gfx_display->platformspecific->XDisplay, (Window) gfx_display->nativeWindow, &attr);

    //We resize our view port to the ones provided by the top window
    values.width = Width;
    values.height = Height;

    XConfigureWindow(gfx_display->platformspecific->XDisplay,(Window) gfx_display->nativeWindow,CWWidth|CWHeight,&values);

    resizeCB(values.width ,values.height);

    // Query the EGL surface width and height
    eglStatus =  eglQuerySurface(gfx_display->display, gfx_display->surface,
                                 EGL_WIDTH,  &gfx_display->width)
                  && eglQuerySurface(gfx_display->display, gfx_display->surface,
                                 EGL_HEIGHT, &gfx_display->height);
    if (!eglStatus) {
           printf("EGL couldn't get window width/height \n");
    }
    disown_thread_context(gfx_display);
}

/* As of now handle only width/height change */
void nvomx_checkevents(GSTGL_DISPLAY_DATA_PTR gfx_display)
{
  XEvent event;
  while (XPending(gfx_display->platformspecific->XDisplay)) {
    XNextEvent(gfx_display->platformspecific->XDisplay, &event);
    switch (event.type) {
      case ConfigureNotify:
      if ((event.xconfigure.width  != gfx_display->width) ||
          (event.xconfigure.height != gfx_display->height)) {
        resizeCB(event.xconfigure.width, event.xconfigure.height);
        gfx_display->width = event.xconfigure.width;
        gfx_display->height = event.xconfigure.height;
      }
      break;
    }
  }
}
