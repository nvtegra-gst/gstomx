/*
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * NVIDIA Corporation and its licensors retain all intellectual property
 * and proprietary rights in and to this software, related documentation
 * and any modifications thereto.  Any use, reproduction, disclosure or
 * distribution of this software and related documentation without an express
 * license agreement from NVIDIA Corporation is strictly prohibited.
 *
 * version: 0.2
 */

#include "gstomx-camera2.h"
#include "gstomx-camera2-priv.h"
#ifdef GST_ENABLE_PHOTOGRAPHY
#include "gstomx-camera2-photography.h"
#endif
#include "gstomx.h"
#include "gstomx_nvutils.h"

#define IMAGE_CAPTURE_READY       (1 << 0)
#define IMAGE_CAPTURE_AFTERPAUSE  (1 << 1)

enum
{
  PROP_0,
  PROP_MODE,
  PROP_ZOOM,
  PROP_MAX_ZOOM,
  PROP_READY_FOR_CAPTURE,
  PROP_PAUSE_AFTER_CAPTURE,
  PROP_VIEWFINDER,
  PROP_SENSOR,
  PROP_FLASH,
#ifdef GST_ENABLE_PHOTOGRAPHY
  PROP_NV_FLASH,
#endif
  PROP_WHITE_BALANCE,
#ifdef GST_ENABLE_PHOTOGRAPHY
  PROP_NV_WHITE_BALANCE,
#endif
  PROP_ISO_SPEED,
  PROP_EXPOSURE,
  PROP_METERING,
  PROP_FLICKER_REDUCTION,
  PROP_COLOR_TONE,
#ifdef GST_ENABLE_PHOTOGRAPHY
  PROP_NV_COLOR_TONE,
#endif
  PROP_BRIGHTNESS,
  PROP_CONTRAST,
  PROP_SATURATION,
  PROP_HUE,
  PROP_AUTO_FOCUS_TIMEOUT,
  PROP_EV_COMPENSATION,
  PROP_ENABLE_EDGE_ENHANCEMENT,
  PROP_EDGE_ENHANCEMENT,
  PROP_FOCUS_MODE,
#ifdef GST_ENABLE_PHOTOGRAPHY
  PROP_NV_FOCUS_MODE,
#endif
  PROP_APERTURE,
  PROP_STEREO_MODE,
  PROP_NOISE_REDUCTION,
  PROP_SCENE_MODE,
#ifdef GST_ENABLE_PHOTOGRAPHY
  PROP_NV_SCENE_MODE,
#endif
  PROP_CAPABILITIES,
  PROP_IMAGE_CAPTURE_SUPPORTED_CAPS,
  PROP_PREVIEW_SUPPORTED_CAPS,
  PROP_ROTATION_VIEWFINDER,
  PROP_ROTATION_CAPTURE,
  PROP_AUTO_FRAMERATE,
#ifdef GST_KPI_MEASURE
  PROP_ENABLE_KPI_MEASURE,
#endif
  PROP_LAST
};

enum
{
  /* actions */
  SIGNAL_START_CAPTURE,
  SIGNAL_STOP_CAPTURE,
  SIGNAL_START_AUTO_FOCUS,
  SIGNAL_STOP_AUTO_FOCUS,
  LAST_SIGNAL
};


#define STATIC_VF_CAPS \
  "video/x-nv-yuv, " \
    "width = (int) [ 176, MAX ], " \
    "height = (int) [ 144, MAX ], " \
    "format = (fourcc) I420, " \
    "framerate = (fraction) 30/1;" \
  "video/x-nvrm-yuv, " \
    "width = (int) [ 176, MAX ], " \
    "height = (int) [ 144, MAX ], " \
    "format = (fourcc) I420, " \
    "framerate = (fraction) 30/1;" \
  "video/x-raw-yuv, " \
    "width = (int) [ 176, MAX ], " \
    "height = (int) [ 144, MAX ], " \
    "format = (fourcc) { I420, NVB1 }, " \
    "framerate = (fraction) 30/1;"


#define STATIC_CAPTURE_CAPS \
  "video/x-nv-yuv, " \
    "width = (int) [ 176, MAX ], " \
    "height = (int) [ 144, MAX ], " \
    "format = (fourcc) I420, " \
    "framerate = (fraction) 30/1;" \
  "video/x-nvrm-yuv, " \
    "width = (int) [ 176, MAX ], " \
    "height = (int) [ 144, MAX ], " \
    "format = (fourcc) I420, " \
    "framerate = (fraction) 30/1;" \
  "video/x-raw-yuv, " \
    "width = (int) [ 176, MAX ], " \
    "height = (int) [ 144, MAX ], " \
    "format = (fourcc) { I420, NVB1 }, " \
    "framerate = (fraction) 30/1;"

#define AUTO_FOCUS_AF  (1 << 0)
#define AUTO_FOCUS_AWB (1 << 1)
#define AUTO_FOCUS_AE  (1 << 2)

static guint gst_omx_camera_signals [LAST_SIGNAL] = { 0 };

static GstStaticPadTemplate vf_template = GST_STATIC_PAD_TEMPLATE ("vfsrc",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (STATIC_VF_CAPS));

static GstStaticPadTemplate img_template = GST_STATIC_PAD_TEMPLATE ("imgsrc",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (STATIC_CAPTURE_CAPS));

static GstStaticPadTemplate vid_template = GST_STATIC_PAD_TEMPLATE ("vidsrc",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (STATIC_CAPTURE_CAPS));

static GstElementClass *parent_class = NULL;

static void gst_omx_camera_base_init (gpointer g_class);
static void gst_omx_camera_class_init (GstOmxCamera2Class * klass);
static void gst_omx_camera_init (GstOmxCamera2 * src, gpointer g_class);
static void gst_omx_camera_finalize (GObject * object);
static void gst_omx_camera_init_interfaces (GType type);

static void gst_omx_camera_omxevent_handler (GstElement* element, OMX_EVENTTYPE eEvent,
    OMX_U32 data_1, OMX_U32 data_2, OMX_PTR event_data);
static gboolean gst_omx_camera_event_handler (GstPad * pad, GstEvent * event);

static gboolean gst_omx_camera_negotiate (GstOmxCamera2 * omxcamera, GstPad* pad);
static void gst_omx_camera_init_params (GstOmxCamera2 * omxcamera);
static void gst_omx_camera_start_capture (GstOmxCamera2 * omxcamera);
static void gst_omx_camera_stop_capture (GstOmxCamera2 * omxcamera);
static void gst_omx_camera_start_auto_focus (GstOmxCamera2* src);
static void gst_omx_camera_stop_auto_focus (GstOmxCamera2* src);

GType
gst_omx_camera2_get_type (void)
{
  static volatile gsize omx_camera_type = 0;

  if (g_once_init_enter (&omx_camera_type)) {
    GType _type;
    static const GTypeInfo omx_camera_info = {
      sizeof (GstOmxCamera2Class),
      (GBaseInitFunc) gst_omx_camera_base_init,
      NULL,
      (GClassInitFunc) gst_omx_camera_class_init,
      NULL,
      NULL,
      sizeof (GstOmxCamera2),
      0,
      (GInstanceInitFunc) gst_omx_camera_init,
    };

    _type = g_type_register_static (GST_TYPE_ELEMENT,
        "GstOmxCamera2", &omx_camera_info, 0);

    gst_omx_camera_init_interfaces (_type);

    g_once_init_leave (&omx_camera_type, _type);
    gst_nvomx_buffer_class_ref ();
  }
  return omx_camera_type;
}


static gboolean gst_omx_camera_activate_push (GstPad * pad, gboolean active);
static void gst_omx_camera_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_omx_camera_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);
static gboolean gst_omx_camera_send_event (GstElement * elem, GstEvent * event);
static GstStateChangeReturn gst_omx_camera_change_state (GstElement * element,
    GstStateChange transition);
static gboolean gst_omx_camera_setcaps (GstPad * pad, GstCaps * caps);
static void  gst_omx_camera_loop (GstPad* pad);


static void
gst_omx_camera_base_init (gpointer g_class)
{
  GstElementClass *element_class = GST_ELEMENT_CLASS (g_class);

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&vf_template));
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&img_template));
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&vid_template));

  gst_element_class_set_details_simple (element_class, "Nvidia OMX Camera2",
      "Video/Capture",
      "Nvidia OMX Camera2 for Nvidia OMX",
      "Nvidia Corporation <Cotigao>");
}


static void
gst_omx_camera_class_init (GstOmxCamera2Class * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;

  gobject_class = G_OBJECT_CLASS (klass);
  gstelement_class = GST_ELEMENT_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  gobject_class->finalize = gst_omx_camera_finalize;
  gobject_class->set_property = gst_omx_camera_set_property;
  gobject_class->get_property = gst_omx_camera_get_property;

  g_object_class_install_property (gobject_class, PROP_MODE,
      g_param_spec_enum ("mode", "Mode",
        "The capture mode (still image capture or video recording)",
        GST_TYPE_NV_CAMERA_MODE, GST_NV_MODE_IMAGE,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_ZOOM,
      g_param_spec_float (GST_PHOTOGRAPHY_PROP_ZOOM,
          "Zoom",
          "Digital zoom factor (e.g. 1.5 means 1.5x, only in OMX_StateExecuting)",
          1.0, 8.0, 1.0, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_MAX_ZOOM,
      g_param_spec_float ("max-zoom", "Maximum zoom level (note: may change "
          "depending on resolution/implementation)",
          "Digital zoom factor (e.g. 1.5 means 1.5x)", 1.0, G_MAXFLOAT,
          8.0, G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_READY_FOR_CAPTURE,
      g_param_spec_boolean ("ready-for-capture", "Ready for capture",
          "Informs this element is ready for starting another capture",
          TRUE, G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_PAUSE_AFTER_CAPTURE,
      g_param_spec_boolean ("pause-after-capture", "Pause After Capture",
          "Pauses after an image is captured by posting " \
          "GST_PHOTOGRAPHY_NV_PAUSED_AFTER_CAPTURE message on the bus",
          FALSE, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_VIEWFINDER,
      g_param_spec_boolean ("viewfinder", "Enable/Disable Viewfinder",
          "Enable/Disable Viewfinder (only in OMX_StateExecuting)",
          FALSE, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_SENSOR,
      g_param_spec_enum ("sensor",
        "Sensor Identifier",
        "Sensor Identifier (can be set only in NULL State above all other props)",
        GST_TYPE_NV_SENSOR,
        GST_NV_SENSOR_PRIMARY,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_FLASH,
      g_param_spec_enum (GST_PHOTOGRAPHY_PROP_FLASH_MODE,
        "Flash mode property",
        "Flash mode defines how the flash light should be used",
        GST_TYPE_FLASH_MODE,
        GST_PHOTOGRAPHY_FLASH_MODE_OFF,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

#ifdef GST_ENABLE_PHOTOGRAPHY
  g_object_class_install_property (gobject_class, PROP_NV_FLASH,
      g_param_spec_enum (GST_PHOTOGRAPHY_PROP_FLASH_MODE"-nv",
        "Flash mode property",
        "Flash mode defines how the flash light should be used (NvidiaOMX Extended)",
        GST_TYPE_NV_FLASH_MODE,
        GST_PHOTOGRAPHY_NV_FLASH_MODE_TORCH,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
#endif

  g_object_class_install_property (gobject_class, PROP_WHITE_BALANCE,
      g_param_spec_enum (GST_PHOTOGRAPHY_PROP_WB_MODE,
        "White balance mode property",
        "White balance affects the color temperature of the photo",
        GST_TYPE_WHITE_BALANCE_MODE,
        GST_PHOTOGRAPHY_WB_MODE_AUTO,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

#ifdef GST_ENABLE_PHOTOGRAPHY
  g_object_class_install_property (gobject_class, PROP_NV_WHITE_BALANCE,
      g_param_spec_enum (GST_PHOTOGRAPHY_PROP_WB_MODE"-nv",
        "White balance mode property",
        "White balance affects the color temperature of the photo (NvidiaOMX Extended)",
        GST_TYPE_NV_WHITE_BALANCE_MODE,
        GST_PHOTOGRAPHY_NV_WB_MODE_SHADE,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
#endif

  g_object_class_install_property (gobject_class, PROP_EXPOSURE,
      g_param_spec_uint (GST_PHOTOGRAPHY_PROP_EXPOSURE,
        "Exposure Time in ms",
        "Exposure (Shutter Speed) time in milliseconds, 0=auto",
        0, G_MAXUINT32, 0, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_ISO_SPEED,
      g_param_spec_uint (GST_PHOTOGRAPHY_PROP_ISO_SPEED,
        "ISO speed property",
        "ISO speed defines the light sensitivity (0=auto)", 0, 800, 0,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_METERING,
      g_param_spec_enum ("metering-mode",
        "Metering mode",
        "Metering Mode",
        GST_TYPE_NV_METERING_MODE,
        GST_NV_METERING_MODE_AVG,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_FLICKER_REDUCTION,
      g_param_spec_enum (GST_PHOTOGRAPHY_PROP_FLICKER_MODE,
        "Flicker reduction mode property",
        "Flicker reduction mode defines a line frequency for flickering prevention",
        GST_TYPE_FLICKER_REDUCTION_MODE,
        GST_PHOTOGRAPHY_FLICKER_REDUCTION_AUTO,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_COLOR_TONE,
      g_param_spec_enum (GST_PHOTOGRAPHY_PROP_COLOUR_TONE,
        "Colour tone mode property",
        "Colour tone setting changes colour shading in the photo",
        GST_TYPE_COLOUR_TONE_MODE,
        GST_PHOTOGRAPHY_COLOUR_TONE_MODE_NORMAL,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

#ifdef GST_ENABLE_PHOTOGRAPHY
  g_object_class_install_property (gobject_class, PROP_NV_COLOR_TONE,
      g_param_spec_enum (GST_PHOTOGRAPHY_PROP_COLOUR_TONE"-nv",
        "Colour tone mode property",
        "Colour tone setting changes colour shading in the photo (NvidiaOMX Extended)",
        GST_TYPE_NV_COLOUR_TONE_MODE,
        GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_NOISE,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
#endif

   g_object_class_install_property (gobject_class, PROP_BRIGHTNESS,
      g_param_spec_int ("brightness", "Brightness",
        "Brightness", 0, 100, 0,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_CONTRAST,
      g_param_spec_int ("contrast", "Contrast",
        "Contrast", 0, 100, 0,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_SATURATION,
      g_param_spec_int ("saturation", "Saturation",
        "Saturation", -100, 100, 0,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_HUE,
      g_param_spec_int ("hue", "Hue",
        "Hue", 0, 100, 0,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_AUTO_FOCUS_TIMEOUT,
      g_param_spec_int ("af-timeout", "Af Timeout",
        "Auto focus timeout value in milliseconds", 0, G_MAXINT, 3500,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_EV_COMPENSATION,
      g_param_spec_float (GST_PHOTOGRAPHY_PROP_EV_COMP,
        "EV compensation property",
        "EV compensation affects the brightness of the image",
        -2.5, 2.5, 0,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_ENABLE_EDGE_ENHANCEMENT,
      g_param_spec_boolean ("enable-edge-enhancement", "Enable Edge Enhancement",
        "Enable Edge Enhancement Control", FALSE,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_AUTO_FRAMERATE,
      g_param_spec_uint ("framerate", "Frame Rate",
        "Capture Frame Rate ", 5, 30, 30,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_EDGE_ENHANCEMENT,
      g_param_spec_float ("edge-enhancement", "Edge Enhancement",
        "Edge Enhancement Control",
        -1.0, 1.0, 0.0,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_FOCUS_MODE,
      g_param_spec_enum (GST_PHOTOGRAPHY_PROP_FOCUS_MODE,
          "Focus mode property",
          "Focus mode defines the range of focal lengths to use in autofocus search",
          GST_TYPE_FOCUS_MODE,
          GST_PHOTOGRAPHY_FOCUS_MODE_AUTO,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

#ifdef GST_ENABLE_PHOTOGRAPHY
  g_object_class_install_property (gobject_class, PROP_NV_FOCUS_MODE,
      g_param_spec_enum (GST_PHOTOGRAPHY_PROP_FOCUS_MODE"-nv",
          "Focus mode property",
          "Focus mode defines the range of focal lengths to use in autofocus search (NvidiaOMX Extended)",
          GST_TYPE_NV_FOCUS_MODE,
          GST_PHOTOGRAPHY_FOCUS_MODE_AUTO,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
#endif

  g_object_class_install_property (gobject_class, PROP_APERTURE,
      g_param_spec_uint (GST_PHOTOGRAPHY_PROP_APERTURE,
          "Aperture property",
          "Aperture defines the size of lens opening  (0 = auto) (**not implemented**)",
          0, G_MAXUINT8, 0, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_STEREO_MODE,
      g_param_spec_enum ("stereo-mode",
        "Stereo mode property",
        "Stereo Mode",
        GST_TYPE_NV_STEREO_MODE,
        GST_NV_STEREO_MODE_OFF,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_NOISE_REDUCTION,
      g_param_spec_flags (GST_PHOTOGRAPHY_PROP_NOISE_REDUCTION,
          "Noise Reduction settings",
          "Which noise reduction modes are enabled (0 = disabled)",
          GST_TYPE_PHOTOGRAPHY_NOISE_REDUCTION,
          0, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_SCENE_MODE,
      g_param_spec_enum (GST_PHOTOGRAPHY_PROP_SCENE_MODE,
          "Scene mode property",
          "Scene mode works as a preset for different photo shooting mode settings",
          GST_TYPE_SCENE_MODE,
          GST_PHOTOGRAPHY_SCENE_MODE_AUTO,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

#ifdef GST_ENABLE_PHOTOGRAPHY
  g_object_class_install_property (gobject_class, PROP_NV_SCENE_MODE,
      g_param_spec_enum (GST_PHOTOGRAPHY_PROP_SCENE_MODE"-nv",
          "Scene mode property",
          "Scene mode works as a preset for different photo shooting mode settings (NvidiaOMX Extended)",
          GST_TYPE_NV_SCENE_MODE,
          GST_PHOTOGRAPHY_NV_SCENE_MODE_ACTION,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
#endif

  g_object_class_install_property (gobject_class, PROP_CAPABILITIES,
      g_param_spec_ulong (GST_PHOTOGRAPHY_PROP_CAPABILITIES,
          "Photo capabilities bitmask",
          "Tells the photo capabilities of the device",
          0, G_MAXULONG, 0, G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_IMAGE_CAPTURE_SUPPORTED_CAPS,
      g_param_spec_boxed (GST_PHOTOGRAPHY_PROP_IMAGE_CAPTURE_SUPPORTED_CAPS,
          "Image capture supported caps",
          "Caps describing supported image capture formats", GST_TYPE_CAPS,
          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_PREVIEW_SUPPORTED_CAPS,
      g_param_spec_boxed (GST_PHOTOGRAPHY_PROP_IMAGE_PREVIEW_SUPPORTED_CAPS,
          "Image preview supported caps",
          "Caps describing supported image preview formats", GST_TYPE_CAPS,
          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_ROTATION_VIEWFINDER,
      g_param_spec_enum ("rotate-viewfinder",
        "Rotate ViewFinder Property",
        "Rotates ViewFinder by 0, 90, 180, 270 degrees",
        GST_TYPE_NV_ROTATION,
        GST_NV_ROTATION_0,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_ROTATION_CAPTURE,
      g_param_spec_enum ("rotate-capture",
        "Rotate Capture Property",
        "Rotates Capture by 0, 90, 180, 270 degrees",
        GST_TYPE_NV_ROTATION,
        GST_NV_ROTATION_0,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

#ifdef GST_KPI_MEASURE
  g_object_class_install_property (gobject_class, PROP_ENABLE_KPI_MEASURE,
      g_param_spec_boolean ("enable-kpi-measure", "Enable kpi measurement",
        "Enable kpi measurement Control", FALSE,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
#endif

  /* Signals */
  gst_omx_camera_signals[SIGNAL_START_CAPTURE] =
      g_signal_new ("start-capture",
      G_TYPE_FROM_CLASS (klass),
      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
      G_STRUCT_OFFSET (GstOmxCamera2Class, start_capture),
      NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

  gst_omx_camera_signals[SIGNAL_STOP_CAPTURE] =
      g_signal_new ("stop-capture",
      G_TYPE_FROM_CLASS (klass),
      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
      G_STRUCT_OFFSET (GstOmxCamera2Class, stop_capture),
      NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

  gst_omx_camera_signals[SIGNAL_START_AUTO_FOCUS] =
      g_signal_new ("start-auto-focus",
      G_TYPE_FROM_CLASS (klass),
      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
      G_STRUCT_OFFSET (GstOmxCamera2Class, start_auto_focus),
      NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

  gst_omx_camera_signals[SIGNAL_STOP_AUTO_FOCUS] =
      g_signal_new ("stop-auto-focus",
      G_TYPE_FROM_CLASS (klass),
      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
      G_STRUCT_OFFSET (GstOmxCamera2Class, stop_auto_focus),
      NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

  gstelement_class->change_state = gst_omx_camera_change_state;
  gstelement_class->send_event = gst_omx_camera_send_event;

  klass->start_capture = gst_omx_camera_start_capture;
  klass->stop_capture = gst_omx_camera_stop_capture;
  klass->start_auto_focus = gst_omx_camera_start_auto_focus;
  klass->stop_auto_focus = gst_omx_camera_stop_auto_focus;
}



static void
gst_omx_camera_init (GstOmxCamera2 * self, gpointer g_class)
{
  GOmxPort *port;

  self->vf_pad = gst_pad_new_from_static_template (&vf_template, "vfsrc");
  gst_pad_set_activatepush_function (self->vf_pad, gst_omx_camera_activate_push);
  gst_pad_set_setcaps_function (self->vf_pad, gst_omx_camera_setcaps);
  gst_pad_set_event_function (self->vf_pad, gst_omx_camera_event_handler);
  gst_element_add_pad (GST_ELEMENT (self), self->vf_pad);

  self->img_pad = gst_pad_new_from_static_template (&img_template, "imgsrc");
  gst_pad_set_activatepush_function (self->img_pad, gst_omx_camera_activate_push);
  gst_pad_set_setcaps_function (self->img_pad, gst_omx_camera_setcaps);
  gst_pad_set_event_function (self->img_pad, gst_omx_camera_event_handler);
  gst_element_add_pad (GST_ELEMENT (self), self->img_pad);

  self->vid_pad = gst_pad_new_from_static_template (&vid_template, "vidsrc");
  gst_pad_set_activatepush_function (self->vid_pad, gst_omx_camera_activate_push);
  gst_pad_set_setcaps_function (self->vid_pad, gst_omx_camera_setcaps);
  gst_pad_set_event_function (self->vid_pad, gst_omx_camera_event_handler);
  gst_element_add_pad (GST_ELEMENT (self), self->vid_pad);

  self->gomx = gstomx_core_new (self, G_TYPE_FROM_CLASS (GST_OMX_CAMERA2_GET_CLASS (self)));
  self->gomx->ehandler = gst_omx_camera_omxevent_handler;

  port = g_omx_core_new_port (self->gomx, GST_OMX_CAMERA_PREVIEW_PORT);
  gst_pad_set_element_private (self->vf_pad, port);

  port = g_omx_core_new_port (self->gomx, GST_OMX_CAMERA_STILL_PORT);
  gst_pad_set_element_private (self->img_pad, port);
  port = g_omx_core_new_port (self->gomx, GST_OMX_CAMERA_VIDEO_PORT);
  gst_pad_set_element_private (self->vid_pad, port);

  self->capture_lock = g_mutex_new ();
  self->eos_cond = g_cond_new ();
  self->image_capture_cond = g_cond_new ();

  gst_omx_camera_init_params (self);
}


static void
gst_omx_camera_finalize (GObject * object)
{
  GstOmxCamera2 *src;

  src = GST_OMX_CAMERA2 (object);

  g_omx_core_ports_free (src->gomx);
  g_omx_core_free (src->gomx);
  src->gomx = NULL;

  g_mutex_free (src->capture_lock);
  g_cond_free (src->eos_cond);
  g_cond_free (src->image_capture_cond);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}


static gboolean
gst_omx_camera_interface_supported (GstImplementsInterface * iface, GType type)
{
#ifdef GST_ENABLE_PHOTOGRAPHY
  if (type == GST_TYPE_PHOTOGRAPHY)
    return TRUE;
  else
#endif
    return FALSE;
}


static void
gst_omx_camera_interface_init (GstImplementsInterfaceClass * klass)
{
  klass->supported = gst_omx_camera_interface_supported;
}


#ifdef GST_ENABLE_PHOTOGRAPHY
static void
gst_omx_camera_photography_init (GstPhotographyInterface* photo)
{
  photo->get_ev_compensation = gst_omx_pcamera_get_ev_compensation;
  photo->get_iso_speed = gst_omx_pcamera_get_iso_speed;
  photo->get_aperture = gst_omx_pcamera_get_aperture;
  photo->get_exposure = gst_omx_pcamera_get_exposure;
  photo->get_white_balance_mode = gst_omx_pcamera_get_white_balance_mode;
  photo->get_colour_tone_mode = gst_omx_pcamera_get_colour_tone_mode;
  photo->get_scene_mode = gst_omx_pcamera_get_scene_mode;
  photo->get_flash_mode = gst_omx_pcamera_get_flash_mode;
  photo->get_noise_reduction = gst_omx_pcamera_get_noise_reduction;
  photo->get_zoom = gst_omx_pcamera_get_zoom;
  photo->get_flicker_mode = gst_omx_pcamera_get_flicker_mode;
  photo->get_focus_mode = gst_omx_pcamera_get_focus_mode;

  photo->set_ev_compensation = gst_omx_pcamera_set_ev_compensation;
  photo->set_iso_speed = gst_omx_pcamera_set_iso_speed;
  photo->set_aperture = gst_omx_pcamera_set_aperture;
  photo->set_exposure = gst_omx_pcamera_set_exposure;
  photo->set_white_balance_mode = gst_omx_pcamera_set_white_balance_mode;
  photo->set_colour_tone_mode = gst_omx_pcamera_set_colour_tone_mode;
  photo->set_scene_mode = gst_omx_pcamera_set_scene_mode;
  photo->set_flash_mode = gst_omx_pcamera_set_flash_mode;
  photo->set_noise_reduction = gst_omx_pcamera_set_noise_reduction;
  photo->set_zoom = gst_omx_pcamera_set_zoom;
  photo->set_flicker_mode = gst_omx_pcamera_set_flicker_mode;
  photo->set_focus_mode = gst_omx_pcamera_set_focus_mode;

  photo->prepare_for_capture  = gst_omx_pcamera_prepare_for_capture;
  photo->get_capabilities = gst_omx_pcamera_get_capabilities;
  photo->get_config = gst_omx_pcamera_get_config;
  photo->set_config = gst_omx_pcamera_set_config;
  photo->set_autofocus = gst_omx_pcamera_set_autofocus;
}


gboolean
gst_omx_camera_prepare_for_capture (GstOmxCamera2* src, GstPhotoCapturePrepared func,
    GstCaps *capture_caps, gpointer user_data)

{
  gboolean result = FALSE;

  g_mutex_lock (src->capture_lock);

  if (src->capturing == NvGstCapturingStopped &&
      capture_caps && !gst_caps_is_empty (capture_caps) &&
      !gst_caps_is_any (capture_caps)) {
    GstNvCameraMode mode;

    gst_omx_camera_get_mode (src, &mode);

    if (mode == GST_NV_MODE_IMAGE) {
      GstCaps* caps = gst_pad_get_allowed_caps (src->img_pad);

      if (caps) {
        if (gst_caps_can_intersect (caps, capture_caps))
          result = gst_pad_set_caps (src->img_pad, capture_caps);

        gst_caps_unref (caps);
      }

    } else if (mode == GST_NV_MODE_VIDEO) {
      GstCaps* caps = gst_pad_get_allowed_caps (src->vid_pad);

      if (caps) {
        if (gst_caps_can_intersect (caps, capture_caps))
          result = gst_pad_set_caps (src->vid_pad, capture_caps);

        gst_caps_unref (caps);
      }
    }
  }

  g_mutex_unlock (src->capture_lock);

  /* FIXME: should this be called from a diff thread?? */
  if (result)
    func (user_data, capture_caps);
  else {
    g_print ("could not prepare camera for capture\n");
    func (user_data, NULL);
  }

  return result;
}
#endif


static void
gst_omx_camera_init_interfaces (GType type)
{
  static const GInterfaceInfo iface_info = {
    (GInterfaceInitFunc) gst_omx_camera_interface_init,
    NULL,
    NULL,
  };

#ifdef GST_ENABLE_PHOTOGRAPHY
  static const GInterfaceInfo photography_info = {
    (GInterfaceInitFunc) gst_omx_camera_photography_init,
    NULL,
    NULL,
  };
#endif

  g_type_add_interface_static (type,
      GST_TYPE_IMPLEMENTS_INTERFACE, &iface_info);

#ifdef GST_ENABLE_PHOTOGRAPHY
  g_type_add_interface_static (type, GST_TYPE_PHOTOGRAPHY, &photography_info);
#endif
}



static gboolean
gst_omx_camera_send_event (GstElement * element, GstEvent * event)
{
//  GstOmxCamera2 *src;
  gboolean result = FALSE;

//  src = GST_OMX_CAMERA2 (element);

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_EOS:
    {
      /* TODO */
      break;
    }

    default:
      break;
  }

  if (event)
    gst_event_unref (event);

  return result;
}


static gboolean
gst_omx_camera_event_handler (GstPad * pad, GstEvent * event)
{
  GstOmxCamera2 *src = GST_OMX_CAMERA2 (gst_pad_get_parent (pad));
  gboolean result = TRUE;;

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_CUSTOM_BOTH: {
        if (gst_event_has_name (event, "renegotiate")) {
          result = gst_omx_camera_negotiate (src, pad);
        }
      }
      break;

    default:
      break;
  }

  gst_event_unref (event);
  gst_object_unref (src);

  return result;
}


static void
gst_omx_camera_change_mode (GstOmxCamera2* src, GstNvCameraMode new)
{
  GOmxCore *gomx = src->gomx;
  GstNvCameraMode old;

  gst_omx_camera_get_mode (src->gomx->omx_handle, &old);

  if (old != new) {
    if (gomx->omx_state == OMX_StateExecuting) {
//      GOmxPort* cap_port, *vf_port;
      GOmxPort* port;

#ifdef GST_KPI_MEASURE
      if (src->enable_kpi_measure)
        src->kpitimer.mctimer = g_timer_new ();
#endif
      if (src->viewfinder_on) {
        gst_omx_camera_set_viewfinder (src->gomx->omx_handle, FALSE);
        g_omx_core_wait_for_done (src->gomx);
        src->viewfinder_on = FALSE;
      }

      if (src->capturing)
        gst_omx_camera_stop_capture (src);

//      vf_port = (GOmxPort*) gst_pad_get_element_private (src->vf_pad);
//      cap_port = (GOmxPort*) gst_pad_get_element_private (src->img_pad);

      gst_pad_set_active (src->vf_pad, FALSE);
      gst_pad_set_active (src->img_pad, FALSE);
      gst_pad_set_active (src->vid_pad, FALSE);

      gst_pad_push_event (src->vf_pad, gst_event_new_flush_start ());
//      gst_pad_push_event (src->img_pad, gst_event_new_flush_start ());
//      gst_pad_push_event (src->vid_pad, gst_event_new_flush_start ());

      gst_pad_push_event (src->vf_pad, gst_event_new_flush_stop ());
//      gst_pad_push_event (src->img_pad, gst_event_new_flush_stop ());
//      gst_pad_push_event (src->vid_pad, gst_event_new_flush_stop ());

      g_omx_core_stop (gomx);
      g_omx_core_finish (gomx);

      gst_pad_set_caps (src->vf_pad, NULL);
      gst_pad_set_caps (src->img_pad, NULL);
      gst_pad_set_caps (src->vid_pad, NULL);

      gst_omx_camera_set_mode (src->gomx->omx_handle, new);
      gst_omx_camera_set_continuous_autofocus (src->gomx->omx_handle, TRUE);

      gst_pad_set_active (src->vf_pad, TRUE);
      if (new == GST_NV_MODE_IMAGE) {
        port = gst_pad_get_element_private (src->img_pad);
        g_omx_port_set_active (port, TRUE, FALSE);
        port->omx_allocate = TRUE;
        port = gst_pad_get_element_private (src->vid_pad);
        g_omx_port_set_active (port, FALSE, FALSE);
        gst_pad_set_active (src->img_pad, TRUE);
      } else {
        port = gst_pad_get_element_private (src->vid_pad);
        g_omx_port_set_active (port, TRUE, FALSE);
        port->omx_allocate = TRUE;
        port = gst_pad_get_element_private (src->img_pad);
        g_omx_port_set_active (port, FALSE, FALSE);
        gst_pad_set_active (src->vid_pad, TRUE);
      }

      g_omx_core_prepare (gomx);
      g_omx_core_start (gomx);

      gst_omx_camera_set_viewfinder (src->gomx->omx_handle, TRUE);
      /* TODO: wait for ack */
      src->viewfinder_on = TRUE;

#ifdef GST_KPI_MEASURE
      if (src->kpitimer.mctimer) {
        g_timer_stop (src->kpitimer.mctimer);
        g_print ("**** MODE SWITCH TIME: %g secs\n", g_timer_elapsed (src->kpitimer.mctimer, NULL));
        g_timer_destroy (src->kpitimer.mctimer);

        src->kpitimer.mctimer = NULL;
      }
#endif
    } else {
      g_print ("setting mode %d in LOADED state\n", new);
      gst_omx_camera_set_mode (src->gomx->omx_handle, new);
    }
  } else {
    g_print ("same mode mode %d\n", old);
  }
}


static void
gst_omx_camera_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstOmxCamera2 *src;
  gboolean ret = TRUE;

  src = GST_OMX_CAMERA2 (object);

  switch (prop_id) {
    case PROP_MODE:
      gst_omx_camera_change_mode (src, g_value_get_enum (value));
    break;

    case PROP_ZOOM:
      if (src->gomx->omx_state == OMX_StateExecuting)
        gst_omx_camera_set_zoom (src->gomx->omx_handle, g_value_get_float (value));
    break;

    case PROP_PAUSE_AFTER_CAPTURE:
      gst_omx_camera_set_pause_after_capture (src->gomx->omx_handle,
        g_value_get_boolean (value));
    break;

    case PROP_VIEWFINDER:
      if (src->gomx->omx_state == OMX_StateExecuting) {
        gboolean sbool = g_value_get_boolean (value);

        if (src->viewfinder_on != sbool) {
          gst_omx_camera_set_viewfinder (src->gomx->omx_handle, sbool);

          if (!sbool)
            g_omx_core_wait_for_done (src->gomx);
          else {
            /* TODO: wait for ack */
#ifdef GST_KPI_MEASURE
            if (src->kpitimer.c2ptimer) {
              g_timer_stop (src->kpitimer.c2ptimer);
              g_print ("**** CAPTURE TO PREVIEW TIME: %g secs\n", g_timer_elapsed (src->kpitimer.c2ptimer, NULL));
              g_timer_destroy (src->kpitimer.c2ptimer);

              src->kpitimer.c2ptimer = NULL;
            }
#endif
          }

          src->viewfinder_on = sbool;
        }
      }
    break;

    case PROP_SENSOR:
      if (src->gomx->omx_state == OMX_StateLoaded)
        gst_omx_camera_set_sensor (src->gomx->omx_handle, g_value_get_enum (value));
    break;
#ifdef GST_ENABLE_PHOTOGRAPHY
    case PROP_NV_FLASH:
#endif
    case PROP_FLASH:
      // Query flash support
      ret = gst_omx_camera_get_flash_parameters (src->gomx->omx_handle, &src->hasFlashSupport);
      if (ret != TRUE || !src->hasFlashSupport)
      {
        g_print ("Flash not supported");
      } else {
        src->flash_mode = g_value_get_enum (value);
      }

    break;

#ifdef GST_ENABLE_PHOTOGRAPHY
    case PROP_NV_WHITE_BALANCE:
#endif
    case PROP_WHITE_BALANCE:
      gst_omx_camera_set_white_balance_mode (src->gomx->omx_handle, g_value_get_enum (value));
    break;

    case PROP_ISO_SPEED:
      gst_omx_camera_set_iso_speed (src->gomx->omx_handle, g_value_get_uint (value));
    break;

    case PROP_EXPOSURE:
      gst_omx_camera_set_exposure (src->gomx->omx_handle, g_value_get_uint (value));
    break;

    case PROP_METERING:
      gst_omx_camera_set_metering_mode (src->gomx->omx_handle, g_value_get_enum (value));
    break;

    case PROP_FLICKER_REDUCTION:
      gst_omx_camera_set_flicker_reduction_mode (src->gomx->omx_handle, g_value_get_enum (value));
    break;

#ifdef GST_ENABLE_PHOTOGRAPHY
    case PROP_NV_COLOR_TONE:
#endif
    case PROP_COLOR_TONE:
      gst_omx_camera_set_color_tone (src->gomx->omx_handle, g_value_get_enum (value));
    break;

    case PROP_BRIGHTNESS:
      gst_omx_camera_set_brightness (src->gomx->omx_handle, g_value_get_int (value));
    break;

    case PROP_CONTRAST:
      gst_omx_camera_set_contrast (src->gomx->omx_handle, g_value_get_int (value));
    break;

    case PROP_SATURATION:
      gst_omx_camera_set_saturation (src->gomx->omx_handle, g_value_get_int (value));
    break;

    case PROP_HUE:
      gst_omx_camera_set_hue (src->gomx->omx_handle, g_value_get_int (value));
    break;

    case PROP_AUTO_FOCUS_TIMEOUT:
      src->auto_focus_timeout = g_value_get_int (value);
    break;

    case PROP_EV_COMPENSATION:
      gst_omx_camera_set_ev_compensation (src->gomx->omx_handle, g_value_get_float (value));
    break;

    case PROP_ENABLE_EDGE_ENHANCEMENT:
      gst_omx_camera_set_enable_edge_enhancement (src->gomx->omx_handle, g_value_get_boolean (value));
    break;

    case PROP_EDGE_ENHANCEMENT:
      gst_omx_camera_set_edge_enhancement (src->gomx->omx_handle, g_value_get_float (value));
    break;

#ifdef GST_ENABLE_PHOTOGRAPHY
    case PROP_NV_FOCUS_MODE :
#endif
    case PROP_FOCUS_MODE:
      // Query focus support
      ret = gst_omx_camera_get_focuser_parameters (src->gomx->omx_handle, &src->minPosition,
                                                   &src->maxPosition, &src->macroPosition,
                                                   &src->infModeAF, &src->macroModeAF, &src->hasFocusSupport);
      if (ret != TRUE) {
        g_print ("Focus not supported by this sensor, setting supported focus modes to Fixed");
        src->focus_mode = GST_PHOTOGRAPHY_NV_FOCUS_MODE_FIXED;
      }
      else {
        src->focus_mode = g_value_get_enum (value);
      }
    break;

    case PROP_APERTURE:
    break;

    case PROP_STEREO_MODE:
      gst_omx_camera_set_stereo_mode (src->gomx->omx_handle, g_value_get_enum (value));
    break;

    case PROP_NOISE_REDUCTION:
      gst_omx_camera_set_noise_reduction (src->gomx->omx_handle, g_value_get_flags (value), 1, 105);
    break;

#ifdef GST_ENABLE_PHOTOGRAPHY
    case PROP_NV_SCENE_MODE:
#endif
    case PROP_SCENE_MODE: {
      if (gst_omx_camera_set_scene_mode (src->gomx->omx_handle, g_value_get_enum (value)))
        src->scene_mode = g_value_get_enum (value);
    }
    break;

    case PROP_ROTATION_VIEWFINDER:
      gst_omx_camera_set_rotation (src->gomx->omx_handle, 0, g_value_get_enum (value));
    break;

    case PROP_ROTATION_CAPTURE:
      gst_omx_camera_set_rotation (src->gomx->omx_handle, 1, g_value_get_enum (value));
    break;

    case PROP_AUTO_FRAMERATE:
      gst_omx_camera_set_auto_framerate(src->gomx->omx_handle, g_value_get_uint(value));
    break;

#ifdef GST_KPI_MEASURE
    case PROP_ENABLE_KPI_MEASURE:
      src->enable_kpi_measure = g_value_get_boolean (value);
    break;
#endif

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    break;
  }
}



static void
gst_omx_camera_get_property (GObject * object, guint prop_id, GValue * value,
    GParamSpec * pspec)
{
  GstOmxCamera2 *src;

  src = GST_OMX_CAMERA2 (object);

  switch (prop_id) {
    case PROP_MODE: {
      GstNvCameraMode mode;
      gst_omx_camera_get_mode (src->gomx->omx_handle, &mode);
      g_value_set_enum (value, mode);
    }
    break;

    case PROP_ZOOM: {
      gfloat zoom;
      gst_omx_camera_get_zoom (src->gomx->omx_handle, &zoom);
      g_value_set_float (value, zoom);
    }
    break;

    case PROP_MAX_ZOOM: {
      g_value_set_float (value, 8.0);
    }
    break;

    case PROP_READY_FOR_CAPTURE: {
      g_value_set_boolean (value, src->capturing > NvGstCapturingStopped ? FALSE : TRUE);
    }
    break;

    case PROP_PAUSE_AFTER_CAPTURE: {
      gboolean ppc;
      gst_omx_camera_get_pause_after_capture (src->gomx->omx_handle, &ppc);
      g_value_set_boolean (value, ppc);
    }
    break;

    case PROP_VIEWFINDER: {
    //  gboolean sbool;
    //  gst_omx_camera_get_viewfinder (src->gomx->omx_handle, &sbool);
      g_value_set_boolean (value, src->viewfinder_on);
    }
    break;

    case PROP_SENSOR: {
      GstNvSensor sen;
      gst_omx_camera_get_sensor (src->gomx->omx_handle, &sen);
      g_value_set_enum (value, sen);
    }
    break;

#ifdef GST_ENABLE_PHOTOGRAPHY
    case PROP_NV_FLASH:
#endif
    case PROP_FLASH:
      g_value_set_enum (value, src->flash_mode);
    break;

#ifdef GST_ENABLE_PHOTOGRAPHY
    case PROP_NV_WHITE_BALANCE:
#endif
    case PROP_WHITE_BALANCE: {
      GstWhiteBalanceMode wb;
      gst_omx_camera_get_white_balance_mode (src->gomx->omx_handle, &wb);
      g_value_set_enum (value, wb);
    }
    break;

    case PROP_ISO_SPEED: {
      guint iso;
      gst_omx_camera_get_iso_speed (src->gomx->omx_handle, &iso);
      g_value_set_uint (value, iso);
    }
    break;

    case PROP_EXPOSURE: {
      guint32 exposure;
      gst_omx_camera_get_exposure (src->gomx->omx_handle, &exposure);
      g_value_set_uint (value, exposure);
    }
    break;

    case PROP_METERING: {
      GstNvMeteringMode mt;
      gst_omx_camera_get_metering_mode (src->gomx->omx_handle, &mt);
      g_value_set_enum (value, mt);
    }
    break;

    case PROP_FLICKER_REDUCTION: {
      GstFlickerReductionMode fr;
      gst_omx_camera_get_flicker_reduction_mode (src->gomx->omx_handle, &fr);
      g_value_set_enum (value, fr);
    }
    break;

#ifdef GST_ENABLE_PHOTOGRAPHY
    case PROP_NV_COLOR_TONE:
#endif
    case PROP_COLOR_TONE: {
      GstColourToneMode ct;
      gst_omx_camera_get_color_tone (src->gomx->omx_handle, &ct);
      g_value_set_enum (value, ct);
    }
    break;

    case PROP_BRIGHTNESS: {
      gint bt;
      gst_omx_camera_get_brightness (src->gomx->omx_handle, &bt);
      g_value_set_int (value, bt);
    }
    break;

    case PROP_CONTRAST: {
      gint ct;
      gst_omx_camera_get_contrast (src->gomx->omx_handle, &ct);
      g_value_set_int (value, ct);
    }
    break;

    case PROP_SATURATION: {
      gint st;
      gst_omx_camera_get_saturation (src->gomx->omx_handle, &st);
      g_value_set_int (value, st);
    }
    break;

    case PROP_HUE: {
      gint hue;
      gst_omx_camera_get_hue (src->gomx->omx_handle, &hue);
      g_value_set_int (value, hue);
    }
    break;

    case PROP_AUTO_FOCUS_TIMEOUT:
      g_value_set_int (value, src->auto_focus_timeout);
    break;

    case PROP_EV_COMPENSATION: {
      gfloat ev;
      gst_omx_camera_get_ev_compensation (src->gomx->omx_handle, &ev);
      g_value_set_float (value, ev);
    }
    break;

    case PROP_ENABLE_EDGE_ENHANCEMENT: {
      gboolean ed;
      gst_omx_camera_get_enable_edge_enhancement (src->gomx->omx_handle, &ed);
      g_value_set_boolean (value, ed);
    }
    break;

    case PROP_EDGE_ENHANCEMENT: {
      gfloat ed;
      gst_omx_camera_get_edge_enhancement (src->gomx->omx_handle, &ed);
      g_value_set_float (value, ed);
    }
    break;

#ifdef GST_ENABLE_PHOTOGRAPHY
    case PROP_NV_FOCUS_MODE :
#endif
    case PROP_FOCUS_MODE:
      g_value_set_enum (value, src->focus_mode);
    break;

    case PROP_APERTURE:
    break;

    case PROP_STEREO_MODE: {
      GstNvStereoMode stm;
      gst_omx_camera_get_stereo_mode (src->gomx->omx_handle, &stm);
      g_value_set_enum (value, stm);
    }
    break;

    case PROP_NOISE_REDUCTION: {
      GstPhotographyNoiseReduction nr = g_value_get_flags (value);
      gst_omx_camera_get_noise_reduction (src->gomx->omx_handle, &nr, NULL, NULL);
      g_value_set_flags (value, nr);
    }
    break;

#ifdef GST_ENABLE_PHOTOGRAPHY
    case PROP_NV_SCENE_MODE:
#endif
    case PROP_SCENE_MODE:
      g_value_set_enum (value, src->scene_mode);
    break;

    case PROP_CAPABILITIES:
      g_value_set_ulong (value, src->capabilities);
    break;

    case PROP_IMAGE_CAPTURE_SUPPORTED_CAPS:
      gst_value_set_caps (value, gst_pad_get_pad_template_caps (src->img_pad));
    break;

    case PROP_PREVIEW_SUPPORTED_CAPS:
      gst_value_set_caps (value, gst_pad_get_pad_template_caps (src->vf_pad));
    break;

    case PROP_ROTATION_VIEWFINDER: {
      GstNvRotation rt;
      gst_omx_camera_get_rotation (src->gomx->omx_handle, 0, &rt);
      g_value_set_enum (value, rt);
    }
    break;

    case PROP_ROTATION_CAPTURE: {
      GstNvRotation rt;
      gst_omx_camera_get_rotation (src->gomx->omx_handle, 1, &rt);
      g_value_set_enum (value, rt);
    }
    break;

    case PROP_AUTO_FRAMERATE: {
      guint frate;
      gst_omx_camera_get_auto_framerate(src->gomx->omx_handle, &frate);
      g_value_set_uint (value, frate);
    }
    break;

#ifdef GST_KPI_MEASURE
    case PROP_ENABLE_KPI_MEASURE:
      g_value_set_boolean (value, src->enable_kpi_measure);
    break;
#endif

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    break;
  }
}



static gboolean
gst_omx_camera_negotiate (GstOmxCamera2 * omxcamera, GstPad* pad)
{
  GstCaps *thiscaps;
  GstCaps *caps = NULL;
  GstCaps *peercaps = NULL;
  gboolean result = FALSE;

  thiscaps = gst_pad_get_caps_reffed (pad);

  if (thiscaps == NULL || gst_caps_is_any (thiscaps))
    goto no_nego_needed;

  if (G_UNLIKELY (gst_caps_is_empty (thiscaps)))
    goto no_caps;

  peercaps = gst_pad_peer_get_caps_reffed (pad);

  g_print ("peer caps: %s\n", gst_caps_to_string (peercaps));
  g_print ("my caps: %s\n", gst_caps_to_string (thiscaps));

  if (peercaps) {
    caps =
      gst_caps_intersect (peercaps, thiscaps);
    gst_caps_unref (peercaps);
  } else {
    caps = gst_caps_copy (thiscaps);
  }
  gst_caps_unref (thiscaps);
  if (caps) {
    gst_caps_truncate (caps);

    if (!gst_caps_is_empty (caps)) {
      gst_pad_fixate_caps (pad, caps);

      if (gst_caps_is_any (caps)) {
        result = TRUE;
      } else if (gst_caps_is_fixed (caps)) {
        result = gst_pad_set_caps (pad, caps);
      } else {
        g_print ("caps not fixed %s\n", gst_caps_to_string (caps));
      }
    } else {
      g_print ("caps is empty\n");
    }
    gst_caps_unref (caps);
  } else {
    g_print ("no caps\n");
  }

  return result;

no_nego_needed:
  {
    if (thiscaps)
      gst_caps_unref (thiscaps);
    return TRUE;
  }
no_caps:
  {
    GST_ELEMENT_ERROR (omxcamera, STREAM, FORMAT,
        ("No supported formats found"),
        ("This element did not produce valid caps"));
    if (thiscaps)
      gst_caps_unref (thiscaps);
    return TRUE;
  }
}



static gboolean
gst_omx_camera_activate_push (GstPad * pad, gboolean active)
{
  GstOmxCamera2 *src;
  GOmxPort* port = gst_pad_get_element_private (pad);
  gboolean result = TRUE, is_active = TRUE;
//  GOmxCore *core;
  GstNvCameraMode mode;

  src = GST_OMX_CAMERA2 (GST_OBJECT_PARENT (pad));
//  core = src->gomx;

  if (pad == src->vid_pad) {
    gst_omx_camera_get_mode (src->gomx->omx_handle, &mode);
    if (mode != GST_NV_MODE_VIDEO)
      is_active = FALSE;

  } else if (pad == src->img_pad) {
    gst_omx_camera_get_mode (src->gomx->omx_handle, &mode);
    if (mode != GST_NV_MODE_IMAGE)
      is_active = FALSE;
  }

  if (is_active) {
    if (active) {
      if ((result = gst_omx_camera_negotiate (src, pad))) {
        g_omx_port_resume (port);
        result = gst_pad_start_task (pad, (GstTaskFunction) gst_omx_camera_loop, (gpointer)pad);
      }
    } else {
      g_omx_port_pause (port);
      g_print ("-----i stopped task\n");
      result = gst_pad_stop_task (pad);
      g_print ("-----istopped task, done, %s\n", GST_PAD_NAME (pad));
    }
  } else {

  }

  return result;
}


static void
gst_omx_camera_omxevent_handler (GstElement* element, OMX_EVENTTYPE eEvent,
    OMX_U32 data_1, OMX_U32 data_2, OMX_PTR event_data)
{
  GstOmxCamera2* src = GST_OMX_CAMERA2 (element);
  gint no_post_af = 1;
//  g_print ("##################################### %p %p:%p\n", (void*) eEvent, (void*)data_1,  (void*)data_2);

  switch (eEvent) {
    case OMX_EventDynamicResourcesAvailable: {
      switch (data_1) {
        case NVX_EventCamera_AutoFocusAchieved:
          g_print ("NVX CAMERA2: NVX_EventCamera_AutoFocusAchieved:\n");
#ifdef GST_KPI_MEASURE
          if (src->kpitimer.aftimer) {
            g_timer_stop (src->kpitimer.aftimer);
            g_print ("**** AUTO FOCUS TIME: %g secs\n", g_timer_elapsed (src->kpitimer.aftimer, NULL));
            g_timer_destroy (src->kpitimer.aftimer);

            src->kpitimer.aftimer = NULL;
          }
#endif
          src->auto_focus_param &= ~AUTO_FOCUS_AF;
          no_post_af = src->auto_focus_param;
        break;

        case NVX_EventCamera_AutoFocusTimedOut:
          g_print ("NVX CAMERA2: NVX_EventCamera_AutoFocusTimedOut:\n");
          src->auto_focus_param &= ~AUTO_FOCUS_AF;
          no_post_af = src->auto_focus_param;
        break;

        case NVX_EventCamera_AutoExposureAchieved:
          g_print ("NVX CAMERA2: NVX_EventCamera_AutoExposureAchieved:\n");
          src->auto_focus_param &= ~AUTO_FOCUS_AE;
          no_post_af = src->auto_focus_param;
        break;

        case NVX_EventCamera_AutoExposureTimedOut:
          g_print ("NVX CAMERA2: NVX_EventCamera_AutoExposureTimedOut:\n");
          src->auto_focus_param &= ~AUTO_FOCUS_AE;
          no_post_af = src->auto_focus_param;
        break;

        case NVX_EventCamera_AutoWhiteBalanceAchieved:
          g_print ("NVX CAMERA2: NVX_EventCamera_AutoWhiteBalanceAchieved:\n");
          src->auto_focus_param &= ~AUTO_FOCUS_AWB;
          no_post_af = src->auto_focus_param;
        break;

        case NVX_EventCamera_AutoWhiteBalanceTimedOut:
          g_print ("NVX CAMERA2: NVX_EventCamera_AutoWhiteBalanceTimedOut:\n");
          src->auto_focus_param &= ~AUTO_FOCUS_AWB;
          no_post_af = src->auto_focus_param;
        break;

        case NVX_EventCamera_StillCaptureReady:
          g_print ("NVX CAMERA2: NVX_EventCamera_StillCaptureReady:\n");
          g_mutex_lock (src->capture_lock);
          src->image_capture_phase |= IMAGE_CAPTURE_READY;
          g_cond_signal (src->image_capture_cond);
          g_mutex_unlock (src->capture_lock);
        break;

        case NVX_EventCamera_PreviewPausedAfterStillCapture: {
          gboolean ppc;
          gst_omx_camera_get_pause_after_capture (src->gomx->omx_handle, &ppc);
          g_print ("NVX CAMERA2: NVX_EventCamera_PreviewPausedAfterStillCaptured: %d\n", ppc);

          if (ppc) {
            g_mutex_lock (src->capture_lock);
            src->image_capture_phase |= IMAGE_CAPTURE_AFTERPAUSE;
            g_cond_signal (src->image_capture_cond);
            g_mutex_unlock (src->capture_lock);
          }
        }
        break;

        default:
        break;
      }
    }
    break;

    case OMX_EventPortSettingsChanged: {
      //g_print ("NVX CAMERA2: >>>>>>>>>>>. PORT SETTINGS %p CHANGED OF %d\n", (void*)data_2, (int)data_1);
      if (data_2 == OMX_IndexParamPortDefinition) {
        /* TODO: configure new caps */
        //g_print ("NVX CAMERA2: >>>>>>>>>>>. PORT DEFINITION CHANGED OF %d\n", (int)data_1);
      }
    }
    break;

    default:
    break;
  }

  if (!no_post_af) {
    GstStructure* str = gst_structure_empty_new (GST_PHOTOGRAPHY_AUTOFOCUS_DONE);
    src->focus_status = GST_PHOTOGRAPHY_FOCUS_STATUS_SUCCESS;
    gst_element_post_message (element,
        gst_message_new_element (GST_OBJECT (element), str));
  }
}


static gboolean
gst_omx_camera_setcaps (GstPad* pad, GstCaps* caps)
{
  GstOmxCamera2* src = GST_OMX_CAMERA2 (GST_OBJECT_PARENT (pad));
  GOmxCore *gomx = src->gomx;
  GOmxPort* port = (GOmxPort*) gst_pad_get_element_private (pad);
  GstStructure* str = gst_caps_get_structure (caps, 0);
  gint width, height;
  gboolean ret = TRUE;
  OMX_PARAM_PORTDEFINITIONTYPE param;

  if (gomx->omx_state == OMX_StateExecuting) {
    if (pad == src->vf_pad) {

      if (src->viewfinder_on) {
        gst_omx_camera_set_viewfinder (gomx->omx_handle, FALSE);
        g_omx_core_wait_for_done (src->gomx);
        src->viewfinder_on = FALSE;
      }

    } else if (src->capturing) {
      gst_omx_camera_stop_capture (src);
    }

    g_omx_port_pause (port);
    ret = gst_pad_pause_task (pad);
    /* Task Paused */

    g_print ("pushing fsstart %d\n", gst_pad_push_event (pad, gst_event_new_flush_start ()));
    g_print ("pushin fsstop %d\n", gst_pad_push_event (pad, gst_event_new_flush_stop ()));

    g_omx_port_set_active (port, FALSE, TRUE);
  }

  if(!gst_structure_get_int (str, "width", &width))
    width = src->r_width;
  else
    src->r_width = width;

  if (!gst_structure_get_int (str, "height", &height))
    height = src->r_height;
  else
    src->r_height = height;

  G_OMX_INIT_PARAM (param);
  param.nPortIndex = port->port_index;
  OMX_GetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
  param.format.video.nFrameWidth = width;
  param.format.video.nFrameHeight = height;
  //  param.format.video.eCompressionFormat = OMX_VIDEO_CodingUnused;
  //  param.format.video.eColorFormat = OMX_COLOR_FormatYUV420PackedPlanar;
  //  param.nBufferSize = (height * width * 3) / 2;
  OMX_SetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);

  if (strcmp (gst_structure_get_name (str), "video/x-nvrm-yuv") == 0) {
    gstomx_use_nvrmsurf_extension (gomx->omx_handle);
    port->buffer_type = BUFFER_TYPE_NVRMBUFFER;

  } else if (strcmp (gst_structure_get_name (str), "video/x-nv-yuv") == 0) {
    gstomx_use_nvbuffer_extension (gomx->omx_handle, port->port_index);
    port->buffer_type = BUFFER_TYPE_NVBUFFER;

  } else {
    guint32 fourcc = 0;

    if (gst_structure_get_fourcc (str, "format", &fourcc) &&
        fourcc == GST_MAKE_FOURCC ('N', 'V', 'B', '1')) {
      gstomx_use_nvbuffer_extension (gomx->omx_handle, port->port_index);
      port->buffer_type = BUFFER_TYPE_NVBUFFER;
    } else
      port->buffer_type = BUFFER_TYPE_RAW;
  }

  if (ret) {
    guint size = 256;

    if (port->buffer_type == BUFFER_TYPE_RAW) {
      size = GST_ROUND_UP_2 (width) * GST_ROUND_UP_2 (height);
      size = ((size << 1) + size) >> 1;
    }

    g_omx_port_setup_full (port, 0, size);
    if (gomx->omx_state == OMX_StateExecuting) {
      g_omx_port_enable (port);
      ret = gst_pad_start_task (pad, (GstTaskFunction) gst_omx_camera_loop, (gpointer)pad);
      if (ret && pad == src->vf_pad) {
        gst_omx_camera_set_viewfinder (gomx->omx_handle, TRUE);
        /* TODO: wait for ack */
        src->viewfinder_on = TRUE;
      }
    }
  }

  g_print ("%d: Configured %s with %dx%d\n", ret, GST_PAD_NAME (pad), width, height);

  return ret;
}


static void
gst_omx_camera_loop (GstPad* pad)
{
  GstOmxCamera2* src = GST_OMX_CAMERA2 (GST_OBJECT_PARENT (pad));
  GOmxPort* port = gst_pad_get_element_private (pad);
  OMX_BUFFERHEADERTYPE* omx_buffer = NULL;
  GstFlowReturn ret = GST_FLOW_OK;
  GstBuffer* buf = NULL;
  GstClockTime timestamp;
  gint eos = FALSE;

  omx_buffer = g_omx_port_request_buffer (port);

  if (G_UNLIKELY (!omx_buffer)) {
    goto pause;
  }

  eos = omx_buffer->nFlags & OMX_BUFFERFLAG_EOS;
  if (G_LIKELY (omx_buffer->nFilledLen > 0)) {
    GstClock *clock;

    buf = (GstBuffer*) gst_nvomx_buffer_new (omx_buffer, port);

    if (port->buffer_type == BUFFER_TYPE_NVBUFFER) {
      omx_buffer->nFlags |= OMX_BUFFERFLAG_NV_BUFFER;
    }
    omx_buffer->nFlags |= OMX_BUFFERFLAG_RETAIN_OMX_TS;

    GST_OBJECT_LOCK (src);

    if ((clock = GST_ELEMENT_CLOCK (src))) {
      timestamp = GST_ELEMENT (src)->base_time;
      gst_object_ref (clock);

    } else {
      timestamp = GST_CLOCK_TIME_NONE;
    }

    GST_OBJECT_UNLOCK (src);

    if (G_LIKELY (clock)) {
      timestamp = gst_clock_get_time (clock) - timestamp;
      gst_object_unref (clock);

      if (GST_CLOCK_TIME_IS_VALID (src->duration)) {
        if (timestamp > src->duration)
          timestamp -= src->duration;
        else
          timestamp = 0;
      }
    }

    GST_BUFFER_TIMESTAMP (buf) = timestamp;
    GST_BUFFER_DURATION (buf) = src->duration;
    omx_buffer->nTimeStamp =
        gst_util_uint64_scale_int (timestamp, OMX_TICKS_PER_SECOND, GST_SECOND);

    gst_buffer_set_caps (buf, GST_PAD_CAPS (pad));

    if (G_UNLIKELY (port->segment_sent == FALSE)) {
      if (GST_CLOCK_TIME_IS_VALID (timestamp)) {
        gst_pad_push_event (pad, gst_event_new_new_segment (FALSE, 1.0,
              GST_FORMAT_TIME, timestamp, GST_CLOCK_TIME_NONE, 0));
        port->segment_sent = TRUE;
      }
    }

    ret = gst_pad_push (pad, buf);
    if (G_UNLIKELY (ret != GST_FLOW_OK)) {
      GST_INFO_OBJECT (src, "pausing after gst_pad_push() = %s",
          gst_flow_get_name (ret));
      goto pause;
    }

#ifdef GST_KPI_MEASURE
    if (port->port_index == 1){
      if (src->kpitimer.c2jtimer) {
        g_timer_stop (src->kpitimer.c2jtimer);
        g_print ("**** CAPTURE TO JPEG TIME: %g secs\n", g_timer_elapsed (src->kpitimer.c2jtimer, NULL));
        g_timer_destroy (src->kpitimer.c2jtimer);

        src->kpitimer.c2jtimer = NULL;
      }
    }
#endif
  } else {
    g_omx_port_release_buffer (port, omx_buffer);
  }

  if (G_UNLIKELY (eos && pad != src->vf_pad && src->capturing)) {
    g_mutex_lock (src->capture_lock);
    if (pad == src->vid_pad) {
      gst_pad_push_event (pad, gst_event_new_eos ());
    }
    src->eos_handled = TRUE;
    g_cond_signal (src->eos_cond);
    g_mutex_unlock (src->capture_lock);
    g_print ("@@@@@@@@@@@@@@@@@@@@@@ EOS handled\n");
  }

done:
  return;

pause:
  {
    const gchar *reason = gst_flow_get_name (ret);
    gst_pad_pause_task (pad);

    if (ret == GST_FLOW_UNEXPECTED) {
      GstEvent* event = gst_event_new_eos ();
      gst_pad_push_event (pad, event);

    } else if (ret != GST_FLOW_OK) {
      GST_ELEMENT_ERROR (src, STREAM, FAILED,
          ("Internal data flow error."),
          ("streaming task paused, reason %s (%d)", reason, ret));
    }
    goto done;
  }
}


static void
gst_omx_camera_start_auto_focus (GstOmxCamera2* src)
{
  GstWhiteBalanceMode wb;
  guint exp;
  gboolean ret = TRUE;
  gboolean af = FALSE, awb = FALSE, ae = FALSE;
  gboolean converged = FALSE;

  g_mutex_lock (src->capture_lock);

#ifdef GST_KPI_MEASURE
  if (src->enable_kpi_measure)
    src->kpitimer.aftimer = g_timer_new ();
#endif
  gst_omx_camera_get_white_balance_mode (src->gomx->omx_handle, &wb);
  gst_omx_camera_get_exposure (src->gomx->omx_handle, &exp);

  if (wb != (GstWhiteBalanceMode) GST_PHOTOGRAPHY_NV_WB_MODE_OFF &&
        wb != GST_PHOTOGRAPHY_WB_MODE_AUTO) {
    awb = TRUE;
    src->auto_focus_param |= AUTO_FOCUS_AWB;
  }

  if (!exp) {
    ae = TRUE;
    src->auto_focus_param |= AUTO_FOCUS_AE;
  }

  src->focus_status = GST_PHOTOGRAPHY_FOCUS_STATUS_RUNNING;

  switch ((gint)src->focus_mode)
  {
    case GST_PHOTOGRAPHY_FOCUS_MODE_AUTO:
      af = TRUE;
      src->auto_focus_param |= AUTO_FOCUS_AF;

      gst_omx_camera_set_auto_focus (src->gomx->omx_handle, TRUE, af, awb, ae,
                                     src->auto_focus_timeout, AFFullRange);
    break;

    case GST_PHOTOGRAPHY_NV_FOCUS_MODE_CONTINUOUS_PICTURE:
      if(!gst_omx_camera_queryCAFState (src->gomx->omx_handle, &converged))
         g_print ("queryCAFState Failed \n");

      if (converged)
      {
        ret = gst_omx_camera_programCAFLock(src->gomx->omx_handle, TRUE);
        if (ret != TRUE)
          g_print ("programCAFLock Failed");
      }
      else
      {
        gst_omx_camera_set_auto_focus (src->gomx->omx_handle, TRUE, af, awb, ae,
                                       src->auto_focus_timeout, AFFullRange);
      }
    break;

    case GST_PHOTOGRAPHY_NV_FOCUS_MODE_FIXED:
      g_print ("Focus mode is Fixed, so do nothing\n");
    break;

    case GST_PHOTOGRAPHY_FOCUS_MODE_INFINITY:
      if (src->infModeAF == FALSE)
      {
        gst_omx_camera_set_focus_pos (src->gomx->omx_handle, src->minPosition);
      }
      else
      {
        gst_omx_camera_set_auto_focus (src->gomx->omx_handle, TRUE, af, awb, ae,
                                       src->auto_focus_timeout, AFInfMode);
      }
    break;

    case GST_PHOTOGRAPHY_FOCUS_MODE_MACRO:
      if (src->macroModeAF == FALSE)
      {
        gst_omx_camera_set_focus_pos (src->gomx->omx_handle, src->macroPosition);
      }
      else
      {
        gst_omx_camera_set_auto_focus (src->gomx->omx_handle, TRUE, af, awb, ae,
                                       src->auto_focus_timeout, AFMacroMode);
      }
    break;

    default:
          g_print ("Unsupported focus mode\n");
    break;
  }
  g_mutex_unlock (src->capture_lock);
}


static void
gst_omx_camera_stop_auto_focus (GstOmxCamera2* src)
{
  g_mutex_lock (src->capture_lock);

  gst_omx_camera_set_auto_focus (src->gomx->omx_handle, FALSE,
      FALSE, FALSE, FALSE, -1, AFNone);
  src->focus_status = GST_PHOTOGRAPHY_FOCUS_STATUS_NONE;

  g_mutex_unlock (src->capture_lock);
}


static void
gst_omx_camera_start_capture (GstOmxCamera2* src)
{
  GstNvCameraMode mode;
  GOmxPort* port;
  gboolean ret = TRUE;
  gboolean converged;

#ifdef GST_KPI_MEASURE
  if (src->enable_kpi_measure)
  {
    struct timeval tv;
    struct timezone tz;
    struct tm *tm;
    gettimeofday(&tv, &tz);
    tm = localtime(&tv.tv_sec);
    g_print("**** CAPTURE TIME: %d:%02d:%02d %ld \n",
             tm->tm_hour, tm->tm_min, tm->tm_sec, tv.tv_usec);
    src->kpitimer.c2ptimer = g_timer_new ();
    src->kpitimer.c2jtimer = g_timer_new ();
  }
#endif
  if (src->capturing == NvGstCapturingStarted) {
    g_print ("already in capturing mode\n");
    return;
  }

  g_mutex_lock (src->capture_lock);

  if (src->capturing == NvGstCapturingStarted) {
    g_print ("already in capturing mode\n");
    g_mutex_unlock (src->capture_lock);
    return;
  }

  if ((gint)src->focus_mode == GST_PHOTOGRAPHY_NV_FOCUS_MODE_CONTINUOUS_VIDEO)
  {
    if(!gst_omx_camera_queryCAFState (src->gomx->omx_handle, &converged))
       g_print ("queryCAFState Failed \n");

    if (converged)
    {
      ret = gst_omx_camera_programCAFLock(src->gomx->omx_handle, TRUE);
      if (ret != TRUE)
        g_print ("programCAFLock Failed");
    }
    else
    {
      gst_omx_camera_set_auto_focus (src->gomx->omx_handle, TRUE, TRUE, TRUE, TRUE,
                                     src->auto_focus_timeout, AFFullRange);
    }
  }

  if (src->hasFlashSupport)
  {
    gst_omx_camera_set_flash_mode (src->gomx->omx_handle, src->flash_mode);
  }

  if (src->capturing == NvGstCapturingPaused) {
    src->capturing = NvGstCapturingStarted;
    gst_omx_camera_set_capture_paused (src->gomx->omx_handle, FALSE);

    g_mutex_unlock (src->capture_lock);

  } else {
    gst_omx_camera_get_mode (src->gomx->omx_handle, &mode);
    if (mode == GST_NV_MODE_IMAGE) {
      port = gst_pad_get_element_private (src->img_pad);
      port->segment_sent = TRUE;
    } else {
      port = gst_pad_get_element_private (src->vid_pad);
      port->segment_sent = FALSE;
    }

    src->capturing = NvGstCapturingStarted;
    g_object_notify (G_OBJECT (src), "ready-for-capture");
    gst_omx_camera_set_capture (src->gomx->omx_handle, TRUE);

    g_mutex_unlock (src->capture_lock);

    if (mode == GST_NV_MODE_IMAGE) {
      gst_omx_camera_stop_capture (src);
    }
  }
}


static void
gst_omx_camera_pause_capture (GstOmxCamera2* src)
{
  if (src->capturing == NvGstCapturingPaused) {
    g_print ("already in paused capturing mode\n");
    return;
  }

  g_mutex_lock (src->capture_lock);

  if (src->capturing == NvGstCapturingPaused) {
    g_print ("already in paused capturing mode\n");
    g_mutex_unlock (src->capture_lock);
    return;
  }

  gst_omx_camera_set_capture_paused (src->gomx->omx_handle, TRUE);

  g_mutex_unlock (src->capture_lock);
}


static void
gst_omx_camera_stop_capture (GstOmxCamera2* src)
{
  GstNvCameraMode mode;

  if (src->capturing == NvGstCapturingStopped) {
    g_print ("not in capturing mode\n");
    return;
  }

  g_mutex_lock (src->capture_lock);

  if (src->capturing == NvGstCapturingStopped) {
    g_print ("not in capturing mode\n");
    g_mutex_unlock (src->capture_lock);
    return;
  }

  if (src->capturing != NvGstCapturingStopped) {
    gboolean ppc = FALSE;

    if ((gint)src->focus_mode == GST_PHOTOGRAPHY_NV_FOCUS_MODE_CONTINUOUS_VIDEO)
    {
      gst_omx_camera_set_continuous_autofocus (src->gomx->omx_handle, TRUE);
    }

    gst_omx_camera_get_mode (src->gomx->omx_handle, &mode);

    if (mode == GST_NV_MODE_IMAGE) {
      gint pause_after_capture = 0;
      gst_omx_camera_get_pause_after_capture (src->gomx->omx_handle, &ppc);
      if (ppc)
        pause_after_capture = IMAGE_CAPTURE_AFTERPAUSE;

      while (src->image_capture_phase != (IMAGE_CAPTURE_READY | pause_after_capture))
        g_cond_wait (src->image_capture_cond, src->capture_lock);

      g_omx_core_wait_for_done (src->gomx);

    } else {
      gst_omx_camera_set_capture (src->gomx->omx_handle, FALSE);
      g_omx_core_wait_for_done (src->gomx);
    }

    while (!src->eos_handled)
      g_cond_wait (src->eos_cond, src->capture_lock);
    src->eos_handled = FALSE;

    if (mode == GST_NV_MODE_IMAGE) {
      src->image_capture_phase = 0;
      src->viewfinder_on = !ppc;

      if (ppc) {
        GstStructure* str = gst_structure_empty_new (GST_PHOTOGRAPHY_NV_PAUSED_AFTER_CAPTURE);
        gst_element_post_message (GST_ELEMENT_CAST (src),
            gst_message_new_element (GST_OBJECT (src), str));
      }
    }
  }

  src->capturing = NvGstCapturingStopped;
  g_object_notify (G_OBJECT (src), "ready-for-capture");

  g_mutex_unlock (src->capture_lock);
}


static void
gst_omx_camera_init_params (GstOmxCamera2* src)
{
  src->duration = 33000000;
  src->r_width = 176;
  src->r_height = 144;
  src->auto_focus_timeout = 3500;
  src->capabilities = (GST_PHOTOGRAPHY_CAPS_EV_COMP | GST_PHOTOGRAPHY_CAPS_ISO_SPEED
      | GST_PHOTOGRAPHY_CAPS_TONE | GST_PHOTOGRAPHY_CAPS_WB_MODE
      | GST_PHOTOGRAPHY_CAPS_FLASH | GST_PHOTOGRAPHY_CAPS_ZOOM
      | GST_PHOTOGRAPHY_CAPS_EXPOSURE | GST_PHOTOGRAPHY_CAPS_FLICKER_REDUCTION
      | GST_PHOTOGRAPHY_CAPS_NOISE_REDUCTION | GST_PHOTOGRAPHY_CAPS_SCENE);
}



static GstStateChangeReturn
gst_omx_camera_change_state (GstElement * element, GstStateChange transition)
{
  GstOmxCamera2 *src;
  GOmxCore *core;
  GstStateChangeReturn result = GST_STATE_CHANGE_SUCCESS;
  gboolean is_preroll = FALSE;
  GstNvCameraMode mode;

  src = GST_OMX_CAMERA2 (element);
  core = src->gomx;

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY: {
      GOmxPort* port;

      port = gst_pad_get_element_private (src->vf_pad);
      g_omx_port_set_active (port, TRUE, FALSE);
      port->omx_allocate = TRUE;

      gst_omx_camera_get_mode (src->gomx->omx_handle, &mode);
      if (mode == GST_NV_MODE_IMAGE) {
        port = gst_pad_get_element_private (src->img_pad);
        g_omx_port_set_active (port, TRUE, FALSE);
        port->omx_allocate = TRUE;
        port = gst_pad_get_element_private (src->vid_pad);
        g_omx_port_set_active (port, FALSE, FALSE);

      } else {
        port = gst_pad_get_element_private (src->vid_pad);
        g_omx_port_set_active (port, TRUE, FALSE);
        port->omx_allocate = TRUE;
        port = gst_pad_get_element_private (src->img_pad);
        g_omx_port_set_active (port, FALSE, FALSE);
      }
      gst_omx_camera_set_continuous_autofocus (src->gomx->omx_handle, TRUE);
    }
    break;

    case GST_STATE_CHANGE_READY_TO_PAUSED: {
      is_preroll = TRUE;
      src->capturing = NvGstCapturingStopped;
      src->start_capturing = src->start_with_capture;
    }
    break;

    case GST_STATE_CHANGE_PLAYING_TO_PAUSED: {
      if (core->omx_state == OMX_StateExecuting) {
        if (src->viewfinder_on) {
          gst_omx_camera_set_viewfinder (src->gomx->omx_handle, FALSE);
          g_omx_core_wait_for_done (src->gomx);
          src->viewfinder_on = FALSE;
        }

        if (src->capturing == NvGstCapturingStarted) {
          gst_omx_camera_pause_capture (src);
        }
      }
    }
    break;

    case GST_STATE_CHANGE_PAUSED_TO_READY: {
      if (core->omx_state == OMX_StateExecuting)
        if (src->capturing)
          gst_omx_camera_stop_capture (src);
    }
    break;

    default:
    break;
  }

  if ((result =
        GST_ELEMENT_CLASS (parent_class)->change_state (element,
          transition)) == GST_STATE_CHANGE_FAILURE)
    goto failure;

  switch (transition) {
    case GST_STATE_CHANGE_READY_TO_PAUSED: {

      gst_omx_camera_get_mode (src->gomx->omx_handle, &mode);

      if (mode == GST_NV_MODE_IMAGE) {
        gst_pad_set_active (src->vid_pad, FALSE);
      } else {
        gst_pad_set_active (src->img_pad, FALSE);
      }

      g_omx_core_prepare (core);
      g_omx_core_start (core);
    }
    break;

    case GST_STATE_CHANGE_PAUSED_TO_PLAYING: {
      if (core->omx_state == OMX_StateExecuting) {
        if (!src->viewfinder_on) {
          gst_omx_camera_set_viewfinder (src->gomx->omx_handle, TRUE);
          /* TODO: wait for ack */
          src->viewfinder_on = TRUE;
        }

        if (src->start_capturing) {
          src->start_capturing = FALSE;
          gst_omx_camera_start_capture (src);
        } else if (src->capturing == NvGstCapturingPaused) {
          gst_omx_camera_start_capture (src);
        }
      }
    }
    break;

    case GST_STATE_CHANGE_PAUSED_TO_READY: {
      if (core->omx_state == OMX_StateExecuting) {
        g_omx_core_stop (core);
        g_omx_core_finish (core);
        src->capturing = NvGstCapturingStopped;
      }
    }
    break;

    case GST_STATE_CHANGE_READY_TO_NULL: {
    }
    break;

    default:
    break;
  }


  if (result == GST_STATE_CHANGE_SUCCESS && is_preroll)
    result = GST_STATE_CHANGE_NO_PREROLL;

  return result;

  /* ERRORS */
failure:
  {
    return result;
  }
}
