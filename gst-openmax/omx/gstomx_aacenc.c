/*
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "gstomx_aacenc.h"
#include "gstomx_base_filter.h"
#include "gstomx.h"

#include <string.h> /* for memset */

enum
{
  ARG_0,
  ARG_BITRATE,
  ARG_PROFILE,
};

const guint32 AACSamplingRate[SAMPLE_RATE_TABLE_LENGTH] = {96000, 88200, 64000, 48000, 44100, 32000, 24000, 22050, 16000, 12000, 11025, 8000, 7350, 0 };

#define DEFAULT_BITRATE 64000
#define DEFAULT_PROFILE OMX_AUDIO_AACObjectLC
#define DEFAULT_OUTPUT_FORMAT OMX_AUDIO_AACStreamFormatMP4ADTS

static GstOmxBaseFilterClass *parent_class;

void initialize_ADTS_header(GstOmxAacEnc*);
static void setup_ports (GstOmxBaseFilter* self);
GstBuffer * generate_MP4_codec_data(GstOmxAacEnc*);

#define GST_TYPE_OMX_AACENC_PROFILE (gst_omx_aacenc_profile_get_type ())
static GType
gst_omx_aacenc_profile_get_type (void)
{
  static GType gst_omx_aacenc_profile_type = 0;

  if (!gst_omx_aacenc_profile_type) {
    static GEnumValue gst_omx_aacenc_profile[] = {
      {OMX_AUDIO_AACObjectLC, "Low Complexity", "LC"},
      {0, NULL, NULL},
    };

    gst_omx_aacenc_profile_type = g_enum_register_static ("GstOmxAacencProfile",
        gst_omx_aacenc_profile);
  }

  return gst_omx_aacenc_profile_type;
}

static void
setup_ports (GstOmxBaseFilter * self)
{

//  GstOmxAacEnc *aacenc;
  //GValue *tmp_value;
  //int i=0, index;

//  aacenc = GST_OMX_AACENC (self);

  /* Input port configuration. */
  g_omx_port_setup (self->in_port);
  gst_pad_set_element_private (self->sinkpad, self->in_port);

  /* Output port configuration. */
  g_omx_port_setup (self->out_port);
  gst_pad_set_element_private (self->srcpad, self->out_port);

  /* Avoid Memcpy'ies for Audio */
  /* Video Playback is via egl */
  /* share_input_buffer is needed because, timestamp interpolation will not take place */
  /* and hence WMA audio dropouts will be avoided. */
  self->share_output_buffer = TRUE;
  self->share_input_buffer = TRUE;

  self->in_port->is_audio_port  = TRUE;
  self->out_port->is_audio_port = TRUE;

  /* @todo: read from config file: */
  if (g_getenv ("OMX_ALLOCATE_ON")) {
    GST_DEBUG_OBJECT (self, "OMX_ALLOCATE_ON");
    self->in_port->omx_allocate = TRUE;
    self->out_port->omx_allocate = TRUE;
    self->share_input_buffer = FALSE;
    self->share_output_buffer = FALSE;
  } else if (g_getenv ("OMX_SHARE_HACK_ON")) {
    GST_DEBUG_OBJECT (self, "OMX_SHARE_HACK_ON");
    self->share_input_buffer = TRUE;
    self->share_output_buffer = TRUE;
  } else if (g_getenv ("OMX_SHARE_HACK_OFF")) {
    GST_DEBUG_OBJECT (self, "OMX_SHARE_HACK_OFF");
    self->share_input_buffer = FALSE;
    self->share_output_buffer = FALSE;
  } else {
    GST_DEBUG_OBJECT (self, "default sharing and allocation");
  }

  GST_DEBUG_OBJECT (self, "omx_allocate: in: %d, out: %d",
      self->in_port->omx_allocate, self->out_port->omx_allocate);
  GST_DEBUG_OBJECT (self, "share_buffer: in: %d, out: %d",
      self->share_input_buffer, self->share_output_buffer);
}

static GstCaps *
generate_src_template (void)
{
  GstCaps *caps;

  GstStructure *struc;

  caps = gst_caps_new_empty ();

  struc = gst_structure_new ("audio/mpeg",
      "mpegversion", G_TYPE_INT, 4,
      "rate", GST_TYPE_INT_RANGE, 8000, 96000,
      "channels", GST_TYPE_INT_RANGE, 1, 6, NULL);

  {
    GValue list;
    GValue val;

    list.g_type = val.g_type = 0;

    g_value_init (&list, GST_TYPE_LIST);
    g_value_init (&val, G_TYPE_INT);

    g_value_set_int (&val, 2);
    gst_value_list_append_value (&list, &val);

    g_value_set_int (&val, 4);
    gst_value_list_append_value (&list, &val);

    gst_structure_set_value (struc, "mpegversion", &list);

    g_value_unset (&val);
    g_value_unset (&list);
  }

  gst_caps_append_structure (caps, struc);

  return caps;
}

static GstCaps *
generate_sink_template (void)
{
  GstCaps *caps;

  caps = gst_caps_new_simple ("audio/x-raw-int",
      "endianness", G_TYPE_INT, G_BYTE_ORDER,
      "width", G_TYPE_INT, 16,
      "depth", G_TYPE_INT, 16,
      "rate", GST_TYPE_INT_RANGE, 8000, 96000,
      "signed", G_TYPE_BOOLEAN, TRUE,
      "channels", GST_TYPE_INT_RANGE, 1, 6, NULL);

  return caps;
}

static void
type_base_init (gpointer g_class)
{
  GstElementClass *element_class;

  element_class = GST_ELEMENT_CLASS (g_class);

  gst_element_class_set_details_simple (element_class,
      "OpenMAX IL AAC audio encoder",
      "Codec/Encoder/Audio",
      "Encodes audio in AAC format with OpenMAX IL", "Felipe Contreras");

  {
    GstPadTemplate *template;

    template = gst_pad_template_new ("src", GST_PAD_SRC,
        GST_PAD_ALWAYS, generate_src_template ());

    gst_element_class_add_pad_template (element_class, template);
  }

  {
    GstPadTemplate *template;

    template = gst_pad_template_new ("sink", GST_PAD_SINK,
        GST_PAD_ALWAYS, generate_sink_template ());

    gst_element_class_add_pad_template (element_class, template);
  }
}

static void
set_property (GObject * obj,
    guint prop_id, const GValue * value, GParamSpec * pspec)
{
  GstOmxAacEnc *self;

  self = GST_OMX_AACENC (obj);

  switch (prop_id) {
    case ARG_BITRATE:
      self->bitrate = g_value_get_uint (value);
      break;
    case ARG_PROFILE:
      self->profile = g_value_get_enum (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, prop_id, pspec);
      break;
  }
}

static void
get_property (GObject * obj, guint prop_id, GValue * value, GParamSpec * pspec)
{
  GstOmxAacEnc *self;

  self = GST_OMX_AACENC (obj);

  switch (prop_id) {
    case ARG_BITRATE:
            /** @todo propagate this to OpenMAX when processing. */
      g_value_set_uint (value, self->bitrate);
      break;
    case ARG_PROFILE:
      g_value_set_enum (value, self->profile);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, prop_id, pspec);
      break;
  }
}

static void
type_class_init (gpointer g_class, gpointer class_data)
{
  GObjectClass *gobject_class;

  gobject_class = G_OBJECT_CLASS (g_class);

  parent_class = g_type_class_ref (GST_OMX_BASE_FILTER_TYPE);

  /* Properties stuff */
  {
    gobject_class->set_property = set_property;
    gobject_class->get_property = get_property;

    g_object_class_install_property (gobject_class, ARG_BITRATE,
        g_param_spec_uint ("bitrate", "Bit-rate",
            "Encoding bit-rate",
            0, G_MAXUINT, DEFAULT_BITRATE,
            G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
    g_object_class_install_property (gobject_class, ARG_PROFILE,
        g_param_spec_enum ("profile", "Enocding profile",
            "OMX_AUDIO_AACPROFILETYPE of output",
            GST_TYPE_OMX_AACENC_PROFILE,
            DEFAULT_PROFILE, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  }
}
void
initialize_ADTS_header(GstOmxAacEnc *self)
{
  self->uFrameSync = 0x0FFF;       //Syncword 0xFFF
  self->uID = 0;                   //MPEG4 AAC
  self->uLayerIndex = 0;           //1st Layer
  self->uProtectionAbsent = 1;     //Protection absent
  self->uProfileObjectType = 1;    //LC Profile
  self->uPrivateBit = 0;           //Not private
  self->uChannelConfig = 2;        //Defualt is Stereo
  self->uSamplingRateIndex = 4;    //Default is 44100
  self->uOriginalBit = 0;          //Not Original Content
  self->uHomeBit = 0;              //Home
  self->uCopyIdentifyBit = 0;      //No Copyright
  self->uCopyIdentifyStart = 0;    //No Copyright
  self->uAdtsBufFullness = 0;      //Will be set in the SetBufFullness function
  self->uNumDataBlocks = 0;        //Raw Data Blocks

  for(self->uSamplingRateIndex = 0; self->uSamplingRateIndex < 13; self->uSamplingRateIndex++) {
    if(self->samplingrate == AACSamplingRate[self->uSamplingRateIndex])
      break;
  }

  self->uAdtsBufFullness = (self->bitrate/self->samplingrate)*1024;
  self->uAdtsBufFullness = (6144*self->channels) - self->uAdtsBufFullness;
  self->uAdtsBufFullness = self->uAdtsBufFullness/(32*self->channels);

  self->pHeader[0] = (guint8)(self->uFrameSync >> 4);
  self->pHeader[1] = (guint8)(self->uFrameSync << 4);
  self->pHeader[1] |= self->uID << 3;
  self->pHeader[1] |= self->uLayerIndex << 1;
  self->pHeader[1] |= self->uProtectionAbsent;
  self->pHeader[2] = self->uProfileObjectType<< 6;
  self->pHeader[2] |= self->uSamplingRateIndex << 2;
  self->pHeader[2] |= self->uPrivateBit << 1;
  self->pHeader[2] |= self->uChannelConfig >> 2;
  self->pHeader[3] = self->uChannelConfig << 6;
  self->pHeader[3] |= self->uOriginalBit << 5;
  self->pHeader[3] |= self->uHomeBit << 4;
  self->pHeader[3] |= self->uCopyIdentifyBit << 3;
  self->pHeader[3] |= self->uCopyIdentifyStart << 2;

  self->pHeader[5] |= (guint8)(self->uAdtsBufFullness >> 6);
  self->pHeader[6] = (guint8)(self->uNumDataBlocks << 2);
  self->pHeader[6] |= self->uNumDataBlocks;

}


static gboolean
sink_setcaps (GstPad * pad, GstCaps * caps)
{
  GstStructure *structure;
  GstOmxBaseFilter *omx_base;
  GstOmxAacEnc *self;
  GOmxCore *gomx;

  gint rate = 44100;
  gint channels = 2;

  omx_base = GST_OMX_BASE_FILTER (GST_PAD_PARENT (pad));
  gomx = (GOmxCore *) omx_base->gomx;
  self = GST_OMX_AACENC (omx_base);

  GST_INFO_OBJECT (omx_base, "setcaps (sink): %" GST_PTR_FORMAT, caps);

  g_return_val_if_fail (gst_caps_get_size (caps) == 1, FALSE);

  structure = gst_caps_get_structure (caps, 0);

  if(structure)
  {
    gst_structure_get_int (structure, "rate", &rate);
    self->samplingrate = rate;
    gst_structure_get_int (structure, "channels", &channels);
    self->channels = channels;
  }
  else
  {
    self->samplingrate = rate;
    self->channels = channels;
  }
  /* Input port configuration. */
  {
    OMX_AUDIO_PARAM_PCMMODETYPE param;
    memset (&param, 0, sizeof (param));
    param.nSize = sizeof (OMX_AUDIO_PARAM_PCMMODETYPE);
    param.nVersion.s.nVersionMajor = 1;
    param.nVersion.s.nVersionMinor = 1;

    param.nPortIndex = 0;
    OMX_GetParameter (gomx->omx_handle, OMX_IndexParamAudioPcm, &param);
    param.nSamplingRate = self->samplingrate;
    param.eNumData = OMX_NumericalDataSigned;
    param.eEndian = OMX_EndianLittle;
    param.nBitPerSample = 16;
    param.bInterleaved = OMX_TRUE;
    param.nChannels = self->channels;
    OMX_SetParameter (gomx->omx_handle, OMX_IndexParamAudioPcm, &param);
  }

  {
    GstCaps *src_caps;
    GstBuffer *codec_data;
    GstCaps *peercaps = NULL;
    GstStructure *peerstruct = NULL;

    if (gst_pad_is_linked(omx_base->srcpad)) {

      peercaps = gst_pad_get_allowed_caps (omx_base->srcpad);

      if (peercaps && peercaps->structs->len > 0) {

        peerstruct = gst_caps_get_structure(peercaps, 0);

        if(!g_strcmp0(gst_structure_get_string(peerstruct, "stream-format"), "raw")) {

          src_caps = gst_caps_new_simple ("audio/mpeg",
              "mpegversion", G_TYPE_INT, 4,
              "stream-format", G_TYPE_STRING, "raw",
              "rate", G_TYPE_INT, rate,
              "channels", G_TYPE_INT, channels,
              NULL);
          self->output_format = OMX_AUDIO_AACStreamFormatRAW;

        } else if(!g_strcmp0(gst_structure_get_string(peerstruct, "stream-format"), "adts")) {
          src_caps = gst_caps_new_simple ("audio/mpeg",
              "mpegversion", G_TYPE_INT, 4,
              "stream-format", G_TYPE_STRING, "adts",
              "rate", G_TYPE_INT, rate,
              "channels", G_TYPE_INT, channels,
              NULL);
          self->output_format = OMX_AUDIO_AACStreamFormatMP4ADTS;

        } else {
          src_caps = gst_caps_new_simple ("audio/mpeg",
              "mpegversion", G_TYPE_INT, 4,
              "stream-format", G_TYPE_STRING, "raw",
              "rate", G_TYPE_INT, rate,
              "channels", G_TYPE_INT, channels,
              NULL);
          self->output_format = OMX_AUDIO_AACStreamFormatRAW;
        }
      } else {
        GST_ERROR_OBJECT (omx_base, "gst_pad_get_allowed_caps FAILED");
      }
    } else {
      GST_ERROR_OBJECT (omx_base, "src pad not linked");
    }
    GST_INFO_OBJECT (omx_base, "src caps are: %" GST_PTR_FORMAT, src_caps);
    codec_data = generate_MP4_codec_data(self);

    if(codec_data)
      gst_caps_set_simple (src_caps,
          "codec_data", GST_TYPE_BUFFER, codec_data, NULL);
    gst_pad_set_caps (omx_base->srcpad, src_caps);
    gst_caps_unref (src_caps);
    gst_buffer_unref(codec_data);
    gst_caps_unref (peercaps);
  }

  return gst_pad_set_caps (pad, caps);
}

static void
omx_setup (GstOmxBaseFilter * omx_base)
{
  GstOmxAacEnc *self;
  GOmxCore *gomx;

  self = GST_OMX_AACENC (omx_base);
  gomx = (GOmxCore *) omx_base->gomx;

  GST_INFO_OBJECT (omx_base, "begin");

  {
    OMX_AUDIO_PARAM_AACPROFILETYPE param;

    memset (&param, 0, sizeof (param));
    param.nSize = sizeof (OMX_AUDIO_PARAM_AACPROFILETYPE);
    param.nVersion.s.nVersionMajor = 1;
    param.nVersion.s.nVersionMinor = 1;

    /* Output port configuration. */
    {
      param.nPortIndex = 1;
      OMX_GetParameter (gomx->omx_handle, OMX_IndexParamAudioAac, &param);

      GST_DEBUG_OBJECT (omx_base, "setting bitrate: %i", self->bitrate);
      param.nBitRate = self->bitrate;

      GST_DEBUG_OBJECT (omx_base, "setting profile: %i", self->profile);
      param.eAACProfile = self->profile;

      GST_DEBUG_OBJECT (omx_base, "setting output format: %i",
          self->output_format);
      param.eAACStreamFormat = self->output_format;

      param.nChannels = self->channels;
      param.nSampleRate = self->samplingrate;

      if(self->channels == 2)
        param.eChannelMode = OMX_AUDIO_ChannelModeStereo;
      else
        param.eChannelMode = OMX_AUDIO_ChannelModeMono;

      OMX_SetParameter (gomx->omx_handle, OMX_IndexParamAudioAac, &param);

      initialize_ADTS_header(self);
    }
  }

  /* some workarounds. */
#if 0
  {
    OMX_AUDIO_PARAM_PCMMODETYPE param;

    memset (&param, 0, sizeof (param));
    param.nSize = sizeof (OMX_AUDIO_PARAM_PCMMODETYPE);
    param.nVersion.s.nVersionMajor = 1;
    param.nVersion.s.nVersionMinor = 1;

    param.nPortIndex = omx_base->in_port->port_index;
    OMX_GetParameter (omx_base->gomx->omx_handle, OMX_IndexParamAudioPcm,
        &param);

    rate = param.nSamplingRate;
    channels = param.nChannels;
  }

  {
    OMX_AUDIO_PARAM_AACPROFILETYPE param;

    memset (&param, 0, sizeof (param));
    param.nSize = sizeof (OMX_AUDIO_PARAM_AACPROFILETYPE);
    param.nVersion.s.nVersionMajor = 1;
    param.nVersion.s.nVersionMinor = 1;

    param.nPortIndex = omx_base->out_port->port_index;
    OMX_GetParameter (omx_base->gomx->omx_handle, OMX_IndexParamAudioAac,
        &param);

    param.nSampleRate = rate;
    param.nChannels = channels;

    OMX_SetParameter (omx_base->gomx->omx_handle, OMX_IndexParamAudioAac,
        &param);
  }
#endif

  GST_INFO_OBJECT (omx_base, "end");
}


GstBuffer *
generate_MP4_codec_data(GstOmxAacEnc *aacenc)
{
    GstBuffer *codec_data;

    /* Refer to gst-plugins-bad-0.10.20/gst/qtmux/gstqtmux.c */
    codec_data = gst_buffer_new_and_alloc(5) ;
       codec_data->data[0] = (unsigned char) (aacenc->profile & 0x1f) << 3;

    for(aacenc->uSamplingRateIndex = 0; aacenc->uSamplingRateIndex < 13; aacenc->uSamplingRateIndex++) {
           if(aacenc->samplingrate == AACSamplingRate[aacenc->uSamplingRateIndex])
               break;
       }
       codec_data->data[0] |= (aacenc->uSamplingRateIndex & 0xe)>>1;
       codec_data->data[1] = (aacenc->uSamplingRateIndex & 1)<<7;
       codec_data->data[1] |= (AACSamplingRate[aacenc->uSamplingRateIndex]>>17)&0x7f;
       codec_data->data[2] = (AACSamplingRate[aacenc->uSamplingRateIndex]>>9)&0xff;
       codec_data->data[3] = (AACSamplingRate[aacenc->uSamplingRateIndex]>>1)&0xff;
       codec_data->data[4] = (AACSamplingRate[aacenc->uSamplingRateIndex]&0x1)<<7;

    return codec_data;
}

GstBuffer *
insert_ADTS_header (GstOmxBaseFilter *self, GstBuffer *buf, GstFlowReturn *ret);

GstBuffer *
insert_ADTS_header (GstOmxBaseFilter *self, GstBuffer *buf, GstFlowReturn *ret)
{
  GstOmxAacEnc *aacenc;
  GstBuffer *new_buf = NULL;

  aacenc = GST_OMX_AACENC (self);

  if(aacenc->uSamplingRateIndex == 13)
  {
    *ret = GST_FLOW_NOT_SUPPORTED;
    return buf;
  }

  aacenc->uAACFrameLength = (aacenc->uProtectionAbsent == 1 ? 7 : 9) + GST_BUFFER_SIZE(buf);
  aacenc->uAACFrameLength &= 0x1FFF;

  aacenc->pHeader[3] &= 0xFC;
  aacenc->pHeader[3] |= (guint8)(aacenc->uAACFrameLength >> 11);
  aacenc->pHeader[4] =  (guint8)(aacenc->uAACFrameLength >> 3);
  aacenc->pHeader[5] &= 0x1F;
  aacenc->pHeader[5] |= (guint8)(aacenc->uAACFrameLength << 5);

  new_buf = gst_buffer_new_and_alloc((aacenc->uProtectionAbsent == 1 ? 7 : 9)*sizeof(guint8));

  memcpy(&new_buf->data[0], &aacenc->pHeader[0],(aacenc->uProtectionAbsent == 1 ? 7 : 9)*sizeof(guint8));

  GST_BUFFER_TIMESTAMP (new_buf) = GST_BUFFER_TIMESTAMP (buf);

  return gst_buffer_join(new_buf, buf);
}

static inline GstBuffer *
handle_output_buffer_AACenc (GstOmxBaseFilter *self, GstBuffer *buf, GstFlowReturn *ret)
{
  GstOmxAacEnc *aacenc;

  aacenc = GST_OMX_AACENC (self);

  if((aacenc->output_format == OMX_AUDIO_AACStreamFormatMP4ADTS) || (aacenc->output_format == OMX_AUDIO_AACStreamFormatMP2ADTS))
    return insert_ADTS_header(self, buf, ret);
  else
    return buf;
}

static void
type_instance_init (GTypeInstance * instance, gpointer g_class)
{
  GstOmxBaseFilter *omx_base;
  GstOmxAacEnc *self;

  omx_base = GST_OMX_BASE_FILTER (instance);
  self = GST_OMX_AACENC (instance);

  omx_base->omx_setup = omx_setup;
  omx_base->handle_output_buffer = &handle_output_buffer_AACenc;

  self->samplingrate = 44100;
  self->channels = 2;
  gst_pad_set_setcaps_function (omx_base->sinkpad, sink_setcaps);

  self->bitrate = DEFAULT_BITRATE;
  self->profile = DEFAULT_PROFILE;
  self->output_format = DEFAULT_OUTPUT_FORMAT;

  omx_base->setup_ports = &setup_ports;
  omx_base->update_src_caps = FALSE;
}

GType
gst_omx_aacenc_get_type (void)
{
  static GType type = 0;

  if (G_UNLIKELY (type == 0))
  {
    GTypeInfo *type_info;

    type_info = g_new0 (GTypeInfo, 1);
    type_info->class_size = sizeof (GstOmxAacEncClass);
    type_info->base_init = type_base_init;
    type_info->class_init = type_class_init;
    type_info->instance_size = sizeof (GstOmxAacEnc);
    type_info->instance_init = type_instance_init;

    type = g_type_register_static (GST_OMX_BASE_FILTER_TYPE, "GstOmxAacEnc", type_info, 0);

    g_free (type_info);
  }

  return type;
}
