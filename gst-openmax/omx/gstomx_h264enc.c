/*
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "gstomx_h264enc.h"
#include "gstomx.h"

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-h264, "
        "framerate = (fraction) [0/1, MAX], "
        "width = (int) [ 176, MAX ], " "height = (int) [ 144, MAX ], "
        "stream-format = (string) { byte-stream, avc }, "
        "alignment = (string) { au }")
    );

GSTOMX_BOILERPLATE (GstOmxH264Enc, gst_omx_h264enc, GstOmxBaseVideoEnc,
    GST_OMX_BASE_VIDEOENC_TYPE);

static void
type_base_init (gpointer g_class)
{
  GstElementClass *element_class;

  element_class = GST_ELEMENT_CLASS (g_class);

  gst_element_class_set_details_simple (element_class,
      "OpenMAX IL H.264/AVC video encoder",
      "Codec/Encoder/Video",
      "Encodes video in H.264/AVC format with OpenMAX IL", "Felipe Contreras");

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&src_factory));
}


static void
type_class_init (gpointer g_class, gpointer class_data)
{
}


static inline GstBuffer *
handle_output_buffer_h264enc (GstOmxBaseFilter *self, GstBuffer *buf, GstFlowReturn *ret)
{
  GstOmxH264Enc *h264enc;
  GstBuffer* buffer;

  h264enc = GST_OMX_H264ENC (self);

  buffer = gst_buffer_join (h264enc->codec_buf, buf);

  h264enc->codec_buf = NULL;
  self->handle_output_buffer = NULL;

  return buffer;
}


static void
handle_codec_data (GstBuffer *buf, GstFlowReturn *ret_codec_data, GstOmxBaseFilter *omx_base_filter,GstBuffer **buf_ret)
{
    GstOmxH264Enc* h264enc = GST_OMX_H264ENC (omx_base_filter);

    if (h264enc->out_format == H264_AVC) {
      GstBuffer *new_buffer;
      OMX_U32 length_to_allocate,offset;
      OMX_U8 *pts_ptr;
      OMX_U8 data[6];
      OMX_U32 *codec_data = (OMX_U32*) GST_BUFFER_DATA (buf);
      //read how  many bytes is the sps data
      OMX_U32 sps_length = (codec_data[0]>>24);

      // Need additional 7 bytes of data to indicate no of sps,pps
      //and how many bytes indicate nal_length,profile etc also we
      //are going to strip down 2 bytes each for the sps and pps
      //length hence reducing the size by 4

      length_to_allocate = GST_BUFFER_SIZE(buf) + ADDITIONAL_LENGTH ;

#ifdef DEBUG_LEVEL1
      g_print("********************got length_to_allocate = %d ***********************\n",(int)length_to_allocate);
#endif
      new_buffer = gst_buffer_new_and_alloc (length_to_allocate);

      data[0] = 0x01;
      data[1] = 0x42; // 66 Baseline profile
      data[2] = 0x40; // constrained Baseline
      data[3] = 0x15; // level2.1
      data[4] = 0x03; // nal length = 4 bytes
      data[5] = 0x01; // 1SPS

#ifdef DEBUG_LEVEL1
      g_print("********************got SPS length = %d ***********************\n",(int)sps_length);

      for(i=0 ;i< (GST_BUFFER_SIZE(buf) >> 2);i++)
        g_print("********************codec_data[i] = %x ***********************\n",(int)codec_data[i]);

#endif

      // append data of 6 bytes and then the sps length and sps data
      memcpy (GST_BUFFER_DATA (new_buffer),data, HEADER_DATA_LENGTH);

      // Strip down the length indicating the NAL data size from 4 bytes to 2 bytes.
      //This is because the down stream h264parse/qtmux are expecting this to be only 2 bytes

      memcpy ((GST_BUFFER_DATA (new_buffer) + HEADER_DATA_LENGTH),(GST_BUFFER_DATA(buf)+ 2),
          DOWNSTREAM_NAL_LENGTH_INDICATOR);

      // Now copy the SPS data
      memcpy ((GST_BUFFER_DATA (new_buffer) + HEADER_DATA_LENGTH + 2),
          (GST_BUFFER_DATA(buf)+ ENCODER_NAL_LENGTH_INDICATOR),
          sps_length);

      offset = HEADER_DATA_LENGTH + DOWNSTREAM_NAL_LENGTH_INDICATOR + sps_length;

      pts_ptr = GST_BUFFER_DATA (new_buffer) + offset;
      pts_ptr[0]= 1 ; // 1 pps

      offset +=1;

      // Strip down the length indicating the NAL data size from 4 bytes to 2 bytes.
      //This is because the down stream h264parse/qtmux are expecting this to be only 2 bytes
      memcpy ((GST_BUFFER_DATA (new_buffer) + offset),
          (GST_BUFFER_DATA(buf)+ (ENCODER_NAL_LENGTH_INDICATOR+sps_length)+2),
          DOWNSTREAM_NAL_LENGTH_INDICATOR);

      offset+=2;
      // Now copy pps data
      memcpy((GST_BUFFER_DATA(new_buffer) + offset),
          (GST_BUFFER_DATA(buf)+ ((ENCODER_NAL_LENGTH_INDICATOR <<1)+sps_length)),
          (GST_BUFFER_SIZE(buf)-((ENCODER_NAL_LENGTH_INDICATOR << 1)+sps_length)));

      *buf_ret = new_buffer;

      *ret_codec_data = GST_FLOW_OK;

    } else {
       h264enc->codec_buf = gst_buffer_ref (buf);

      *ret_codec_data = GST_FLOW_RESEND;

      omx_base_filter->handle_output_buffer = &handle_output_buffer_h264enc;
    }
}

static void
settings_changed_cb (GOmxCore * core)
{
  GstOmxH264Enc* h264enc;
  GstOmxBaseVideoEnc *omx_base;
  GstOmxBaseFilter *omx_base_filter;

  guint width;
  guint height;

  omx_base_filter = core->object;
  omx_base = GST_OMX_BASE_VIDEOENC (omx_base_filter);
  h264enc = GST_OMX_H264ENC (omx_base_filter);

  GST_DEBUG_OBJECT (omx_base, "settings changed");

  {
    OMX_PARAM_PORTDEFINITIONTYPE param;

    G_OMX_INIT_PARAM (param);

    param.nPortIndex = omx_base_filter->out_port->port_index;
    OMX_GetParameter (core->omx_handle, OMX_IndexParamPortDefinition, &param);

    width = param.format.video.nFrameWidth;
    height = param.format.video.nFrameHeight;
  }

  {
    GstCaps *new_caps;

    if (h264enc->out_format == H264_AVC)
      new_caps = gst_caps_new_simple ("video/x-h264",
          "width", G_TYPE_INT, width,
          "height", G_TYPE_INT, height,
          "framerate", GST_TYPE_FRACTION, omx_base->framerate_num, omx_base->framerate_denom,
          "pixel-aspect-ratio", GST_TYPE_FRACTION, 1, 1,
          "stream-format", G_TYPE_STRING, "avc",
          "alignment", G_TYPE_STRING, "au", NULL);

    else
      new_caps = gst_caps_new_simple ("video/x-h264",
          "width", G_TYPE_INT, width,
          "height", G_TYPE_INT, height,
          "framerate", GST_TYPE_FRACTION, omx_base->framerate_num, omx_base->framerate_denom,
          "pixel-aspect-ratio", GST_TYPE_FRACTION, 1, 1,
          "stream-format", G_TYPE_STRING, "byte-stream",
          "alignment", G_TYPE_STRING, "au", NULL);


    GST_INFO_OBJECT (omx_base, "caps are: %" GST_PTR_FORMAT, new_caps);
    gst_pad_set_caps (omx_base_filter->srcpad, new_caps);
  }
}

static void
ifi_setup (GstOmxBaseFilter * omx_base)
{
  GstOmxBaseVideoEnc *self;
  GOmxCore *gomx;
  OMX_VIDEO_CONFIG_AVCINTRAPERIOD oIntraPeriod;

  self = GST_OMX_BASE_VIDEOENC (omx_base);
  gomx = (GOmxCore *) omx_base->gomx;

  G_OMX_INIT_PARAM(oIntraPeriod);
  oIntraPeriod.nPortIndex = omx_base->in_port->port_index;
  if (self->iframeinterval != 0)
  {
    oIntraPeriod.nIDRPeriod = self->iframeinterval;
    oIntraPeriod.nPFrames = self->iframeinterval - 1;
  }
  OMX_SetConfig(gomx->omx_handle, OMX_IndexConfigVideoAVCIntraPeriod, &oIntraPeriod);
}

static void
h264enc_configure_output_format (GstOmxBaseFilter * omx_base)
{
//    GstOmxBaseVideoEnc *self;
    GOmxCore *gomx;
    GstOmxH264Enc* h264enc;

//    OMX_ERRORTYPE ret_val;
    GstCaps *icaps = NULL;
    const gchar *format = NULL;

    OMX_VIDEO_CONFIG_NALSIZE OMXNalSize;

//    self = GST_OMX_BASE_VIDEOENC (omx_base);
    h264enc = GST_OMX_H264ENC (omx_base);
    gomx = (GOmxCore *) omx_base->gomx;

    icaps = gst_pad_get_allowed_caps (omx_base->srcpad);
    if (icaps) {
        GstStructure *structure = gst_caps_get_structure (icaps, 0);
        const GValue* val = gst_structure_get_value(structure, "stream-format");

        if (val && gst_value_is_fixed (val)) {
            format =  g_value_get_string (val);
        }
    }

    if(!format || !strcmp (format,"avc"))
    {
        G_OMX_INIT_PARAM(OMXNalSize);
        OMX_GetConfig(gomx->omx_handle, OMX_IndexConfigVideoNalSize, &OMXNalSize);

        OMXNalSize.nNaluBytes= 4;
        //output port of Encoder
        if(omx_base->out_port)
            OMXNalSize.nPortIndex = omx_base->out_port->port_index;

//        ret_val = OMX_SetConfig(gomx->omx_handle, OMX_IndexConfigVideoNalSize, &OMXNalSize);

#ifdef DEBUG_LEVEL1
        OMXNalSize.nNaluBytes= 0;

        OMX_GetConfig(gomx->omx_handle, OMX_IndexConfigVideoNalSize, &OMXNalSize);

        if(OMXNalSize.nNaluBytes==4)
        {
            g_print("******************************Configured successfully H264Encoder "
                    "in RTP mode from gst-omx***************************\n");
        }
#endif
        h264enc->out_format = H264_AVC;
    }
    else
    {
        h264enc->out_format = H264_BTS;
    }

    if (icaps)
        gst_caps_unref (icaps);
}


static void
type_instance_init (GTypeInstance * instance, gpointer g_class)
{
  GstOmxBaseFilter *omx_base_filter;
  GstOmxBaseVideoEnc *omx_base;

  omx_base_filter = GST_OMX_BASE_FILTER (instance);
  omx_base = GST_OMX_BASE_VIDEOENC (instance);

  omx_base->compression_format = OMX_VIDEO_CodingAVC;
  omx_base->ifi_setup = ifi_setup;

  omx_base_filter->gomx->settings_changed_cb = settings_changed_cb;

  omx_base_filter->handle_codec_data = handle_codec_data;

  omx_base->configure_output_format = h264enc_configure_output_format;

}
