/*
 * Copyright (c) 2011 NVIDIA Corporation.  All rights reserved.
 *
 * NVIDIA Corporation and its licensors retain all intellectual property
 * and proprietary rights in and to this software, related documentation
 * and any modifications thereto.  Any use, reproduction, disclosure or
 * distribution of this software and related documentation without an express
 * license agreement from NVIDIA Corporation is strictly prohibited.
 *
 * version: 0.2
 */

#ifndef __GST_OMX_CAMERA2_PHOTOGRAPHY_H__
#define __GST_OMX_CAMERA2_PHOTOGRAPHY_H__

#include "gstnvcameradefs.h"

G_BEGIN_DECLS

gboolean gst_omx_pcamera_get_ev_compensation (GstPhotography *photo, gfloat *ev_comp);

gboolean gst_omx_pcamera_set_ev_compensation (GstPhotography *photo, gfloat ev_comp);

gboolean gst_omx_pcamera_get_iso_speed (GstPhotography *photo, guint* iso_speed);

gboolean gst_omx_pcamera_set_iso_speed (GstPhotography *photo, guint iso_speed);

gboolean gst_omx_pcamera_get_aperture (GstPhotography *photo, guint* aperture);

gboolean gst_omx_pcamera_set_aperture (GstPhotography *photo, guint aperture);

gboolean gst_omx_pcamera_get_exposure (GstPhotography *photo, guint32 *exposure);

gboolean gst_omx_pcamera_set_exposure (GstPhotography *photo, guint32 exposure);

gboolean gst_omx_pcamera_get_white_balance_mode (GstPhotography *photo, GstWhiteBalanceMode *wb_mode);

gboolean gst_omx_pcamera_set_white_balance_mode (GstPhotography *photo, GstWhiteBalanceMode wb_mode);

gboolean gst_omx_pcamera_get_colour_tone_mode (GstPhotography *photo, GstColourToneMode *tone_mode);

gboolean gst_omx_pcamera_set_colour_tone_mode (GstPhotography *photo, GstColourToneMode tone_mode);

gboolean gst_omx_pcamera_get_scene_mode (GstPhotography *photo, GstSceneMode *scene_mode);

gboolean gst_omx_pcamera_set_scene_mode (GstPhotography *photo, GstSceneMode scene_mode);

gboolean gst_omx_pcamera_get_flash_mode (GstPhotography *photo, GstFlashMode *flash_mode);

gboolean gst_omx_pcamera_set_flash_mode (GstPhotography *photo, GstFlashMode flash_mode);

gboolean gst_omx_pcamera_get_noise_reduction (GstPhotography *photo, GstPhotographyNoiseReduction* noise_reduction);

gboolean gst_omx_pcamera_set_noise_reduction (GstPhotography *photo, GstPhotographyNoiseReduction noise_reduction);

gboolean gst_omx_pcamera_get_flicker_mode (GstPhotography *photo, GstFlickerReductionMode* flicker_mode);

gboolean gst_omx_pcamera_set_flicker_mode (GstPhotography *photo, GstFlickerReductionMode flicker_mode);

gboolean gst_omx_pcamera_get_focus_mode (GstPhotography *photo, GstFocusMode* focus_mode);

gboolean gst_omx_pcamera_set_focus_mode (GstPhotography *photo, GstFocusMode focus_mode);

gboolean gst_omx_pcamera_get_zoom (GstPhotography *photo, gfloat* zoom);

gboolean gst_omx_pcamera_set_zoom (GstPhotography *photo, gfloat zoom);

GstPhotoCaps gst_omx_pcamera_get_capabilities (GstPhotography *photo);

gboolean gst_omx_pcamera_prepare_for_capture (GstPhotography *photo,
    GstPhotoCapturePrepared func, GstCaps *capture_caps, gpointer user_data);

void gst_omx_pcamera_set_autofocus (GstPhotography *photo, gboolean on);

gboolean gst_omx_pcamera_set_config (GstPhotography *photo, GstPhotoSettings *config);

gboolean gst_omx_pcamera_get_config (GstPhotography *photo, GstPhotoSettings *config);

G_END_DECLS

#endif
