/*
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * Based on code copyright/by:
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef GSTGL_VIDEOSINK_H
#define GSTGL_VIDEOSINK_H

#include <gst/gst.h>
#include <gst/video/gstvideosink.h>
#include <gstomx_x11eglutils.h>
#include <gstomx_util.h>
G_BEGIN_DECLS

#define GST_GL_VIDEOSINK(obj) (GstGlVideoSink *) (obj)
#define GST_GL_VIDEOSINK_TYPE (gst_gl_videosink_get_type ())

typedef struct GstGlVideoSink GstGlVideoSink;
typedef struct GstGlVideoSinkClass GstGlVideoSinkClass;


struct GstGlVideoSink
{

    GstVideoSink element;
    GstPad *sinkpad;

    guint x_scale;
    guint y_scale;
    guint rotation;

    gchar *display_name;
    gboolean keep_aspect;
    gint hue;
    gint contrast;
    gint brightness;
    gint saturation;
    guint adaptor_no;
    gboolean enable_framedump;
    gboolean handle_events;
    gboolean bvideodisable;
    guint start_frame;
    guint end_frame;
    guint step;
    char *videodumpfile_name;

    /* Framerate numerator and denominator */
    gint fps_n;
    gint fps_d;

    GstGl_DisplayData gfx_display_object;
    gboolean initialized;

    gint pixel_aspect_ratio_num;
    gint pixel_aspect_ratio_denom;

    gint source_width;
    gint source_height;
    GOmxPropertyChangeFlags property_change_flags;
};

struct GstGlVideoSinkClass
{
    GstVideoSinkClass parent_class;
};

GType gst_gl_videosink_get_type (void);

G_END_DECLS

#endif /* GSTGL_VIDEOSINK_H */
