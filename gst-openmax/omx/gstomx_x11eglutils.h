/*
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * Based on code copyright/by:
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef _GSTOMX_X11EGL_UTILS_H_
#define _GSTOMX_X11EGL_UTILS_H_
#include "config.h"
#include "gstomx.h"
#include <OMX_Core.h>
#include <OMX_Component.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <string.h> /* for memset, strcmp */

typedef struct GstGl_PlatformState_
{
    Display* XDisplay;
    int XScreen;
}GstGl_PlatformState;


typedef struct GstGl_DisplayData_ {
    NativeDisplayType nativeDisplay;
    NativeWindowType  nativeWindow;
    EGLDisplay        display;
    EGLSurface        surface;
    EGLConfig         config;
    EGLContext        context;
    EGLint            width;
    EGLint            height;
    EGLint            buffering;
    EGLint            maxTextureSize;
    EGLenum           api;          // EGL_OPENGL_ES_API, EGL_OPENVG_API, ...
    GstGl_PlatformState* platformspecific;
    EGLint clearCount;
    gboolean fullscreen_window;
} GstGl_DisplayData;

typedef GstGl_DisplayData * GSTGL_DISPLAY_DATA_PTR;

typedef struct nvomx_eglbuffer nvomx_eglbuffer;

struct nvomx_eglbuffer
{
  GLuint hTex;
  EGLImageKHR hEglImg;
  /*This is set, if image is fed in flipped flash */
  gboolean bimageflip;
};

void nvomx_x11egl_setwindowid(GSTGL_DISPLAY_DATA_PTR gfx_display, XID windowid);
void nvomx_x11egl_destroy(GSTGL_DISPLAY_DATA_PTR gfx_display);
OMX_ERRORTYPE nvomx_x11egl_initialize(GSTGL_DISPLAY_DATA_PTR gfx_display);
OMX_ERRORTYPE nvomx_allocate_eglimages (void *display_data,  nvomx_eglbuffer **eglbuffer, int width, int height);
void nvomx_destroy_eglimages(void *display_data, nvomx_eglbuffer *eglbuffer);
void egl_init(void);
void egl_destroy(void);

GLuint load_shader_code(const char* vertSource, const char* fragSource);
int nvomx_setupgl(void);
void nvomx_init_eglimagepointers(void);
void handle_dimensions_change(GSTGL_DISPLAY_DATA_PTR gfx_display, int width, int height);
void nvomx_checkevents(GSTGL_DISPLAY_DATA_PTR gfx_display);
void waitForEglFence(void *eglsync);
void getEglFence(void *eglsync);
void nvomx_render_eglimage(GSTGL_DISPLAY_DATA_PTR gfx_display, nvomx_eglbuffer *buf , int dumpEnabled, int start_frame, int end_frame, int step);
void gstgl_clearscreen(GSTGL_DISPLAY_DATA_PTR gfx_display);
void gstgl_reconfigure_geometry_video(gboolean invertTexture);
void gstgl_reconfigure_geometry_border(void);
void gstgl_apply_aspect_ratio_if_needed(GSTGL_DISPLAY_DATA_PTR gfx_display, int srcWidth, int srcHeight, float pixel_aspect_ratio, gboolean apply_aspect_ratio);
#endif
