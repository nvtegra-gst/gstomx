/*
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * Based on code copyright/by:
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
#include "gstomx_nvutils.h"
#include "gstomx_util.h"
#include <string.h>

static GstBufferClass *nvomx_buffer_parent_class = NULL;

OMX_ERRORTYPE gstomx_use_nvbuffer_extension(OMX_HANDLETYPE omx_handle, guint index)
{
  NVX_PARAM_USENVBUFFER param;
  OMX_INDEXTYPE eIndex;
  OMX_ERRORTYPE eError = OMX_ErrorNone;

  eError = OMX_GetExtensionIndex(omx_handle, (char *)NVX_INDEX_CONFIG_USENVBUFFER, &eIndex);

  if(eError == OMX_ErrorNone) {
    param.nSize = sizeof(param);
    param.nVersion.s.nVersionMajor = 1;
    param.nVersion.s.nVersionMinor = 0;
    param.nVersion.s.nRevision = 0;
    param.nVersion.s.nStep = 0;
    param.nPortIndex = index;
    param.bUseNvBuffer = OMX_TRUE;
    OMX_SetParameter(omx_handle, eIndex, &param);
  }
  else
     g_error("Couldnt use the Vendor Extension %s \n",(char *)NVX_INDEX_CONFIG_USENVBUFFER);

  return eError;
}

OMX_ERRORTYPE gstomx_use_nvrmsurf_extension(OMX_HANDLETYPE omx_handle)
{
  OMX_CONFIG_BOOLEANTYPE param;
  OMX_INDEXTYPE eIndex;
  OMX_ERRORTYPE eError = OMX_ErrorNone;

  memset (&param, 0, sizeof (param));
  param.nSize = sizeof (OMX_CONFIG_BOOLEANTYPE);
  param.nVersion.s.nVersionMajor = 1;
  param.nVersion.s.nVersionMinor = 1;
  param.nVersion.s.nRevision = 0;
  param.nVersion.s.nStep = 0;

  eError = OMX_GetExtensionIndex(omx_handle, NVX_INDEX_PARAM_EMBEDRMSURACE, &eIndex);
  if(eError == OMX_ErrorNone) {
    param.bEnabled = OMX_TRUE;
    OMX_SetParameter(omx_handle, eIndex, &param);
  }
  else
     g_error("Couldnt use the Vendor Extension %s \n",(char *)NVX_INDEX_PARAM_EMBEDRMSURACE);

  return eError;
}

OMX_ERRORTYPE gstomx_use_deinterlace_extension(OMX_HANDLETYPE omx_handle, guint deinterlace_type)
{
  NVX_PARAM_DEINTERLACE param;
  OMX_INDEXTYPE eIndex;
  OMX_ERRORTYPE eError = OMX_ErrorNone;

  memset (&param, 0, sizeof (param));
  param.nSize = sizeof (param);
  param.nVersion.s.nVersionMajor = 1;
  param.nVersion.s.nVersionMinor = 1;
  param.nVersion.s.nRevision = 0;
  param.nVersion.s.nStep = 0;
  param.nPortIndex = GOMX_PORT_INPUT;

  switch (deinterlace_type)
  {
     case DEINTERLACE_TYPE_NONE:
           param.DeinterlaceMethod = OMX_DeintMethod_NoDeinterlacing;
     break;
     case DEINTERLACE_TYPE_BOB:
           param.DeinterlaceMethod = OMX_DeintMethod_BobAtFieldRate;
     break;
     case DEINTERLACE_TYPE_ADVANCED1:
           param.DeinterlaceMethod  = OMX_DeintMethod_Advanced1AtFieldRate;
     break;
     default: break;
  }

  eError = OMX_GetExtensionIndex(omx_handle, (char *)NVX_INDEX_PARAM_DEINTERLACING, &eIndex);
  if(eError == OMX_ErrorNone) {
    OMX_SetParameter(omx_handle, eIndex, &param);
  }
  else
     g_error("Couldnt use the Vendor Extension %s \n",(char *)NVX_INDEX_PARAM_DEINTERLACING);
  return eError;


}

OMX_ERRORTYPE
gstomx_use_aspectratio_extension(OMX_HANDLETYPE omx_handle, gboolean keep_aspect,
                                                          guint pixel_aspect_ratio_num, guint pixel_aspect_ratio_denom)
{
  NVX_CONFIG_KEEPASPECT  keep_aspect_params;
  NVX_CONFIG_FORCEASPECT force_aspect_params;
  OMX_INDEXTYPE eIndex;
  OMX_ERRORTYPE eError = OMX_ErrorNone;

  memset (&force_aspect_params, 0, sizeof (NVX_CONFIG_FORCEASPECT));

  force_aspect_params.nSize = sizeof (NVX_CONFIG_FORCEASPECT);
  force_aspect_params.nVersion.s.nVersionMajor = 1;
  force_aspect_params.nVersion.s.nVersionMinor = 0;
  force_aspect_params.nVersion.s.nRevision = 0;
  force_aspect_params.nNumerator   = pixel_aspect_ratio_num;
  force_aspect_params.nDenominator = pixel_aspect_ratio_denom;

  eError = OMX_GetExtensionIndex(omx_handle, (char *)NVX_INDEX_CONFIG_FORCEASPECT, &eIndex);
  if(eError == OMX_ErrorNone) {
    OMX_SetConfig(omx_handle, eIndex, &force_aspect_params);
  }
  else
     g_error("Couldnt use the Vendor Extension %s \n",(char *)NVX_INDEX_CONFIG_FORCEASPECT);

  memset (&keep_aspect_params, 0, sizeof (NVX_CONFIG_KEEPASPECT));

  keep_aspect_params.nSize = sizeof (NVX_CONFIG_KEEPASPECT);
  keep_aspect_params.nVersion.s.nVersionMajor = 1;
  keep_aspect_params.nVersion.s.nVersionMinor = 0;
  keep_aspect_params.nVersion.s.nRevision = 0;
  keep_aspect_params.bKeepAspect = keep_aspect;

  eError = OMX_GetExtensionIndex(omx_handle, (char *)NVX_INDEX_CONFIG_KEEPASPECT, &eIndex);
  if (eError == OMX_ErrorNone) {
    OMX_SetConfig(omx_handle, eIndex, &keep_aspect_params);
  }
  else
     g_error("Couldnt use the Vendor Extension %s \n",(char *)NVX_INDEX_CONFIG_KEEPASPECT);

  return eError;
}

OMX_ERRORTYPE
gstomx_set_video_encoder_property (OMX_HANDLETYPE omx_handle)
{
  OMX_INDEXTYPE eIndex;
  OMX_ERRORTYPE eError = OMX_ErrorNone;
  NVX_PARAM_VIDENCPROPERTY oEncodeProp;

  G_OMX_INIT_PARAM(oEncodeProp);

  eError = OMX_GetExtensionIndex(omx_handle, NVX_INDEX_PARAM_VIDEO_ENCODE_PROPERTY, &eIndex);
  if (eError == OMX_ErrorNone) {
     oEncodeProp.eApplicationType = NVX_VIDEO_Application_Camcorder;
     oEncodeProp.bSvcEncodeEnable = OMX_FALSE;
     oEncodeProp.eErrorResiliencyLevel = NVX_VIDEO_ErrorResiliency_None;

     OMX_SetParameter(omx_handle, eIndex, &oEncodeProp);
  }
  return eError;
}

OMX_ERRORTYPE
gstomx_set_video_encoder_ratecontrolmode (OMX_HANDLETYPE omx_handle, guint rcmode)
{
  OMX_INDEXTYPE eIndex;
  OMX_ERRORTYPE eError = OMX_ErrorNone;
  NVX_PARAM_RATECONTROLMODE oRCMode;

  G_OMX_INIT_PARAM(oRCMode);
  oRCMode.nPortIndex = 0;
  eError = OMX_GetExtensionIndex(omx_handle, NVX_INDEX_PARAM_RATECONTROLMODE, &eIndex);
  if (eError == OMX_ErrorNone) {
      switch (rcmode)
      {
        case NVX_VIDEO_RateControlMode_CBR:
                oRCMode.eRateCtrlMode = NVX_VIDEO_RateControlMode_CBR;
                break;
        case NVX_VIDEO_RateControlMode_VBR:
                oRCMode.eRateCtrlMode = NVX_VIDEO_RateControlMode_VBR;
                break;
        default:
                oRCMode.eRateCtrlMode = NVX_VIDEO_RateControlMode_VBR;
                break;
      }

      OMX_SetParameter(omx_handle, eIndex, &oRCMode);
  }
  return eError;
}

OMX_ERRORTYPE
gstomx_set_video_encoder_temporaltradeoff (OMX_HANDLETYPE omx_handle)
{
  OMX_INDEXTYPE eIndex;
  OMX_ERRORTYPE eError = OMX_ErrorNone;
  NVX_CONFIG_TEMPORALTRADEOFF oTemporalTradeOff;

  G_OMX_INIT_PARAM(oTemporalTradeOff);

  eError = OMX_GetExtensionIndex(omx_handle, NVX_INDEX_CONFIG_VIDEO_ENCODE_TEMPORALTRADEOFF, &eIndex);
  if (eError == OMX_ErrorNone) {
     oTemporalTradeOff.TemporalTradeOffLevel = 0;
     OMX_SetConfig(omx_handle, eIndex, &oTemporalTradeOff);
  }
  return eError;
}

OMX_ERRORTYPE
gstomx_set_FilterTimestamp (OMX_HANDLETYPE omx_handle)
{
  OMX_INDEXTYPE eIndex;
  OMX_ERRORTYPE eError = OMX_ErrorNone;

  NVX_PARAM_FILTER_TIMESTAMPS oFilterTimestamps;

  G_OMX_INIT_PARAM(oFilterTimestamps);

  eError = OMX_GetExtensionIndex(omx_handle, NVX_INDEX_PARAM_FILTER_TIMESTAMPS, &eIndex);
  if (eError == OMX_ErrorNone) {
    G_OMX_INIT_PARAM(oFilterTimestamps);
    eError = OMX_GetParameter(omx_handle, eIndex, &oFilterTimestamps);
    if (eError == OMX_ErrorNone)
    {
      oFilterTimestamps.bFilterTimestamps = OMX_TRUE;
      eError = OMX_SetParameter(omx_handle, eIndex, &oFilterTimestamps);
    }
  }
  return eError;
}

OMX_ERRORTYPE
gstomx_set_decoder_property (OMX_HANDLETYPE omx_handle)
{
    OMX_INDEXTYPE eIndex;
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    OMX_CONFIG_BOOLEANTYPE full_frame_data;
    G_OMX_INIT_PARAM (full_frame_data);
    eError = OMX_GetExtensionIndex (omx_handle, "OMX.Nvidia.index.param.vdecfullframedata", &eIndex);
    if (eError == OMX_ErrorNone) {
       full_frame_data.bEnabled  = OMX_FALSE;
       OMX_SetParameter (omx_handle, eIndex, &full_frame_data);
    }
    return eError;
}

OMX_ERRORTYPE
gstomx_set_audiodecoder_max_channels_supported (OMX_HANDLETYPE omx_handle, OMX_U32 num_channels)
{
  OMX_INDEXTYPE eIndex;
  OMX_ERRORTYPE eError = OMX_ErrorNone;
  OMX_PARAM_U32TYPE param;
  eError = OMX_GetExtensionIndex (omx_handle, "OMX.Nvidia.index.config.maxoutchannels", &eIndex);
  if (eError == OMX_ErrorNone && num_channels) {
    G_OMX_INIT_PARAM(param);
    param.nU32 = num_channels;
    OMX_SetConfig (omx_handle, eIndex, &param);
  }
  return eError;
}


OMX_ERRORTYPE
gstomx_set_decoder_property_only_key_frame_decode (OMX_HANDLETYPE omx_handle, OMX_BOOL set_prop)
{
    OMX_INDEXTYPE eIndex;
    OMX_ERRORTYPE eError = OMX_ErrorNone;
    OMX_CONFIG_BOOLEANTYPE only_key_frame;
    G_OMX_INIT_PARAM (only_key_frame);
    eError = OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_DECODE_IFRAMES, &eIndex);
    if (eError == OMX_ErrorNone) {
        if(set_prop)
            only_key_frame.bEnabled  = OMX_TRUE;
        else
            only_key_frame.bEnabled  = OMX_FALSE;
       OMX_SetConfig (omx_handle, eIndex, &only_key_frame);
    }
    return eError;
}

gint
NvSFxFloat2Fixed (gfloat f)
{
    /*
     * There is no portable, C standard compliant code to reinterprete a float
     * as an integer. Employing a union is the safest way in my experience and
     * this 'type punning' is specifically supported by gcc.
     */
    union {
        float f;
        int i;
    } cvt;
    int res;
    int exp;
    cvt.f = f;
    exp = ((unsigned)cvt.i >> 23) & 0xff;
    /* if absolute value of number less that 2^-17, converts to 0 */
    if (exp < (IEEE_SGL_EXPO_BIAS - 17)) {
        return 0;
    }
    /* if number >= 2^15, clamp to -2^15 or 2^15-1/65536 */
    else if (exp >= (IEEE_SGL_EXPO_BIAS + 15)) {
        return (cvt.i >> 31) ? 0x80000000 : 0x7FFFFFFF;
    } else {
        /* normalise mantissa and add hidden bit */
        res = (cvt.i << 8) | 0x80000000;
        /* shift result into position based on magnitude */
        res = ((unsigned int)res) >> (IEEE_SGL_EXPO_BIAS + 14 - exp);
        /*
         * Round to fixed-point. Overflow during rounding is impossible for
         * float source. Because there are only 24 mantissa bits, the lsbs
         * of res for operands close to 2^15 are zero.
         */
        res = ((unsigned int)(res + 1)) >> 1;
        /* apply sign bit */
        return (cvt.i >> 31) ? -res : res;
    }
}

gfloat
NvSFxFixed2Float (gint x)
{
    union {
        float f;
        unsigned int i;
    } cvt;
    unsigned int u, t;
    int log2 = 0;

    if (!x) {
        return 0.0f;
    }
    cvt.i = (x < 0) ? 0xC7000000 : 0x47000000;
    t = u = (x < 0) ? -x : x;
    if (t & 0xffff0000) {
        t >>= 16;
        log2 |= 16;
    }
    if (t & 0xff00) {
        t >>= 8;
        log2 |=  8;
    }
    if (t & 0xf0) {
        t >>= 4;
        log2 |=  4;
    }
    if (t & 0xC) {
        t >>= 2;
        log2 |=  2;
    }
    if (t & 0x2) {
        log2 |=  1;
    }
    /* compute exponent of result */
    cvt.i -= (31 - log2) << 23;
    /* normalize mantissa */
    t = (u << (31 - log2));
    /* remove mantissa hidden bit and round mantissa */
    if ((t & 0xFF) == 0x80) {
        u = (t >> 8) & 1;
    } else {
        u = (t >> 7) & 1;
    }
    cvt.i += (((t << 1) >> 9) + u);
    return cvt.f;
}


/********** NVOMX_BUFFER *************/

void
gst_nvomx_buffer_class_ref (void)
{
  g_type_class_ref (gst_nvomx_buffer_get_type ());
}


GstNvOmxBuffer*
gst_nvomx_buffer_new (OMX_BUFFERHEADERTYPE* buf, GOmxPort* port)
{
  GstNvOmxBuffer* nvbuf = (GstNvOmxBuffer *) gst_mini_object_new (GST_TYPE_NVOMX_BUFFER);

  nvbuf->omxport = port;
  nvbuf->omxbuf = buf;

  gst_buffer_set_data (GST_BUFFER_CAST (nvbuf), buf->pBuffer, buf->nFilledLen);

  return nvbuf;
}


static void
gst_nvomx_buffer_finalize (GstNvOmxBuffer * nvbuf)
{
  nvbuf->omxbuf->nFilledLen = 0;
  nvbuf->omxbuf->nFlags = 0;

  g_omx_port_release_buffer (nvbuf->omxport, nvbuf->omxbuf);

  GST_MINI_OBJECT_CLASS (nvomx_buffer_parent_class)->finalize (GST_MINI_OBJECT
      (nvbuf));

  return;
}


static void
gst_nvomx_buffer_init (GstNvOmxBuffer * nvbuf, gpointer g_class)
{

}


static void
gst_nvomx_buffer_class_init (gpointer g_class, gpointer class_data)
{
  GstMiniObjectClass *mini_object_class = GST_MINI_OBJECT_CLASS (g_class);

  nvomx_buffer_parent_class = g_type_class_peek_parent (g_class);

  mini_object_class->finalize = (GstMiniObjectFinalizeFunction)
      gst_nvomx_buffer_finalize;
}


GType
gst_nvomx_buffer_get_type (void)
{
  static GType _gst_nvomx_buffer_type;

  if (G_UNLIKELY (_gst_nvomx_buffer_type == 0)) {
    static const GTypeInfo nvomx_buffer_info = {
      sizeof (GstBufferClass),
      NULL,
      NULL,
      gst_nvomx_buffer_class_init,
      NULL,
      NULL,
      sizeof (GstNvOmxBuffer),
      0,
      (GInstanceInitFunc) gst_nvomx_buffer_init,
      NULL
    };
    _gst_nvomx_buffer_type = g_type_register_static (GST_TYPE_BUFFER,
        "GstNvOmxBuffer", &nvomx_buffer_info, 0);
  }
  return _gst_nvomx_buffer_type;
}

