/*
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 * Copyright (C) 2009 Texas Instruments, Inc.
 *
 * Author: Rob Clark <rob@ti.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "gstomx_base_audiodec.h"
#include "gstomx.h"
#include <gst/audio/multichannel.h>

GSTOMX_BOILERPLATE (GstOmxBaseAudioDec, gst_omx_base_audiodec, GstOmxBaseFilter,
    GST_OMX_BASE_FILTER_TYPE);

static void
type_base_init (gpointer g_class)
{
}

static void
type_class_init (gpointer g_class, gpointer class_data)
{
}

static void
settings_changed_cb (GOmxCore * core)
{
  GstOmxBaseFilter *omx_base;
  GstOmxBaseAudioDec* dec;
  guint i;
  GstAudioChannelPosition *pos = NULL;

  omx_base = core->object;
  dec = GST_OMX_BASE_AUDIODEC (omx_base);

  GST_DEBUG_OBJECT (omx_base, "settings changed");

  {
    OMX_AUDIO_PARAM_PCMMODETYPE param;

    G_OMX_INIT_PARAM (param);

    param.nPortIndex = omx_base->out_port->port_index;
    OMX_GetParameter (omx_base->gomx->omx_handle, OMX_IndexParamAudioPcm,
        &param);

    dec->rate = param.nSamplingRate;
    dec->channels = param.nChannels;
    if (dec->rate == 0) {
            /** @todo: this shouldn't happen. */
      GST_WARNING_OBJECT (omx_base, "Bad samplerate");
      dec->rate = 44100;
    }
    if (dec->channels > 2)
    {
        pos = g_new (GstAudioChannelPosition, dec->channels);

        for (i=0; i<dec->channels; i++) {
              switch( param.eChannelMapping[i])
              {
              case OMX_AUDIO_ChannelNone :
                  pos[i] = GST_AUDIO_CHANNEL_POSITION_NONE;
                  break;
              case OMX_AUDIO_ChannelLF :
                  pos[i] = GST_AUDIO_CHANNEL_POSITION_FRONT_LEFT;
                  break;
              case OMX_AUDIO_ChannelRF :
                  pos[i] = GST_AUDIO_CHANNEL_POSITION_FRONT_RIGHT;
                  break;
              case OMX_AUDIO_ChannelCF :
                  pos[i] = GST_AUDIO_CHANNEL_POSITION_FRONT_CENTER;
                  break;
              case OMX_AUDIO_ChannelLS :
                  pos[i] = GST_AUDIO_CHANNEL_POSITION_SIDE_LEFT;
                  break;
              case OMX_AUDIO_ChannelRS :
                  pos[i] = GST_AUDIO_CHANNEL_POSITION_SIDE_RIGHT;
                  break;
              case OMX_AUDIO_ChannelLFE :
                  pos[i] = GST_AUDIO_CHANNEL_POSITION_LFE;
                  break;
              case OMX_AUDIO_ChannelCS :
                  pos[i] = GST_AUDIO_CHANNEL_POSITION_REAR_CENTER;
                  break;
              case OMX_AUDIO_ChannelLR :
                 pos[i] = GST_AUDIO_CHANNEL_POSITION_REAR_LEFT;
                 break;
             case OMX_AUDIO_ChannelRR :
                 pos[i] = GST_AUDIO_CHANNEL_POSITION_REAR_RIGHT;
                 break;
             default:
                 break;
           }
       }
     }
  }

  {
    GstCaps *new_caps;

    new_caps = gst_caps_new_simple ("audio/x-raw-int",
        "width", G_TYPE_INT, 16,
        "depth", G_TYPE_INT, 16,
        "rate", G_TYPE_INT, dec->rate,
        "signed", G_TYPE_BOOLEAN, TRUE,
        "endianness", G_TYPE_INT, G_BYTE_ORDER,
        "channels", G_TYPE_INT, dec->channels, NULL);

    if (pos) {
        #ifdef AUTOMOTIVE
         gst_audio_set_channel_positions (gst_caps_get_structure (new_caps, 0), pos);
        #endif
         g_free(pos);
     }
    GST_INFO_OBJECT (omx_base, "caps are: %" GST_PTR_FORMAT, new_caps);
    gst_pad_set_caps (omx_base->srcpad, new_caps);
    gst_caps_unref(new_caps);
  }

  dec->sample_size = 2 * dec->channels;
}


static inline GstBuffer *
handle_output_buffer (GstOmxBaseFilter *self, GstBuffer *buf, GstFlowReturn *ret)
{
  GstOmxBaseAudioDec* dec = GST_OMX_BASE_AUDIODEC (self);
  gint samples = GST_BUFFER_SIZE (buf) / dec->sample_size;

  GST_BUFFER_DURATION (buf) = gst_util_uint64_scale_int (samples, GST_SECOND, dec->rate);

  return buf;
}

static void
type_instance_init (GTypeInstance * instance, gpointer g_class)
{
  GstOmxBaseFilter *omx_base;

  omx_base = GST_OMX_BASE_FILTER (instance);

  GST_DEBUG_OBJECT (omx_base, "start");

  omx_base->gomx->settings_changed_cb = settings_changed_cb;
  omx_base->handle_output_buffer = &handle_output_buffer;
}
