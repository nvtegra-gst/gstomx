/*
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * NVIDIA Corporation and its licensors retain all intellectual property
 * and proprietary rights in and to this software, related documentation
 * and any modifications thereto.  Any use, reproduction, disclosure or
 * distribution of this software and related documentation without an express
 * license agreement from NVIDIA Corporation is strictly prohibited.
 *
 * version: 0.2
 */


#include "gstomx-camera2-priv.h"
#include <string.h>

#define G_OMX_INIT_PARAM(param) G_STMT_START {     \
        memset (&(param), 0, sizeof ((param)));    \
        (param).nSize = sizeof (param);            \
        (param).nVersion.s.nVersionMajor = 1;      \
        (param).nVersion.s.nVersionMinor = 1;      \
    } G_STMT_END


GType
gst_nv_camera_mode_get_type (void)
{
  static volatile gsize mode_type = 0;
  static const GEnumValue mode[] = {
    {GST_NV_MODE_IMAGE, "GST_NV_MODE_IMAGE", "mode-image"},
    {GST_NV_MODE_VIDEO, "GST_NV_MODE_VIDEO", "mode-video"},
    {0, NULL, NULL}
  };

  if (g_once_init_enter (&mode_type)) {
    GType tmp = g_enum_register_static ("GstNvCameraMode", mode);
    g_once_init_leave (&mode_type, tmp);
  }

  return (GType) mode_type;
}


GType
gst_nv_metering_mode_get_type (void)
{
  static volatile gsize metering_type = 0;
  static const GEnumValue metering[] = {
    {GST_NV_METERING_MODE_AVG, "GST_NV_METERING_MODE_AVG", "average"},
    {GST_NV_METERING_MODE_SPOT, "GST_NV_METERING_MODE_SPOT", "spot"},
    {GST_NV_METERING_MODE_MATRIX, "GST_NV_METERING_MODE_MATRIX", "matrix"},
    {0, NULL, NULL}
  };

  if (g_once_init_enter (&metering_type)) {
    GType tmp = g_enum_register_static ("GstNvMeteringMode", metering);
    g_once_init_leave (&metering_type, tmp);
  }

  return (GType) metering_type;
}


GType
gst_nv_stereo_mode_get_type (void)
{
  static volatile gsize stereo_mode_type = 0;
  static const GEnumValue stereo_mode[] = {
    {GST_NV_STEREO_MODE_OFF, "GST_NV_STEREO_MODE_OFF", "off"},
    {GST_NV_STEREO_MODE_LEFT_ONLY, "GST_NV_STEREO_MODE_LEFT_ONLY", "left only"},
    {GST_NV_STEREO_MODE_RIGHT_ONLY, "GST_NV_STEREO_MODE_RIGHT_ONLY", "right only"},
    {GST_NV_STEREO_MODE_FULL, "GST_NV_STEREO_MODE_FULL", "full stereo"},
    {0, NULL, NULL}
  };

  if (g_once_init_enter (&stereo_mode_type)) {
    GType tmp = g_enum_register_static ("GstNvStereoMode", stereo_mode);
    g_once_init_leave (&stereo_mode_type, tmp);
  }

  return (GType) stereo_mode_type;
}


GType
gst_nv_sensor_get_type (void)
{
  static volatile gsize sensor_type = 0;
  static const GEnumValue sensor[] = {
    {GST_NV_SENSOR_PRIMARY, "GST_NV_SENSOR_PRIMARY", "primary"},
    {GST_NV_SENSOR_SECONDARY, "GST_NV_SENSOR_SECONDARY", "secondary"},
    {GST_NV_SENSOR_STEREO, "GST_NV_SENSOR_STEREO", "stereo"},
    {0, NULL, NULL}
  };

  if (g_once_init_enter (&sensor_type)) {
    GType tmp = g_enum_register_static ("GstNvSensor", sensor);
    g_once_init_leave (&sensor_type, tmp);
  }

  return (GType) sensor_type;
}


GType
gst_nv_rotation_get_type (void)
{
  static volatile gsize rotation_type = 0;
  static const GEnumValue rotation[] = {
    {GST_NV_ROTATION_0, "GST_NV_ROTATION_0", "0"},
    {GST_NV_ROTATION_90, "GST_NV_ROTATION_90", "90"},
    {GST_NV_ROTATION_180, "GST_NV_ROTATION_180", "180"},
    {GST_NV_ROTATION_270, "GST_NV_ROTATION_270", "270"},
    {0, NULL, NULL}
  };

  if (g_once_init_enter (&rotation_type)) {
    GType tmp = g_enum_register_static ("GstNvRotation", rotation);
    g_once_init_leave (&rotation_type, tmp);
  }

  return (GType) rotation_type;
}


#ifndef GST_ENABLE_PHOTOGRAPHY
GType
gst_flash_mode_get_type (void)
{
  static volatile gsize flash_type = 0;
  static const GEnumValue flash[] = {
    {GST_PHOTOGRAPHY_FLASH_MODE_AUTO, "GST_PHOTOGRAPHY_FLASH_MODE_AUTO", "auto"},
    {GST_PHOTOGRAPHY_FLASH_MODE_OFF, "GST_PHOTOGRAPHY_FLASH_MODE_OFF", "off"},
    {GST_PHOTOGRAPHY_FLASH_MODE_ON, "GST_PHOTOGRAPHY_FLASH_MODE_ON", "on"},
    {GST_PHOTOGRAPHY_FLASH_MODE_FILL_IN, "GST_PHOTOGRAPHY_FLASH_MODE_FILL_IN", "fillin"},
    {GST_PHOTOGRAPHY_FLASH_MODE_RED_EYE, "GST_PHOTOGRAPHY_FLASH_MODE_RED_EYE", "redeye"},
    {GST_PHOTOGRAPHY_NV_FLASH_MODE_TORCH, "GST_PHOTOGRAPHY_NV_FLASH_MODE_TORCH", "torch"},
    {0, NULL, NULL}
  };

  if (g_once_init_enter (&flash_type)) {
    GType tmp = g_enum_register_static ("GstFlashMode", flash);
    g_once_init_leave (&flash_type, tmp);
  }

  return (GType) flash_type;
}


GType
gst_photography_noise_reduction_get_type (void)
{
  static volatile gsize noise_reduction_type = 0;
  static const GFlagsValue noise_reduction[] = {
      {GST_PHOTOGRAPHY_NOISE_REDUCTION_BAYER, "GST_PHOTOGRAPHY_NOISE_REDUCTION_BAYER (not supported)", "bayer" },
      {GST_PHOTOGRAPHY_NOISE_REDUCTION_YCC, "GST_PHOTOGRAPHY_NOISE_REDUCTION_YCC", "ycc" },
      {GST_PHOTOGRAPHY_NOISE_REDUCTION_TEMPORAL, "GST_PHOTOGRAPHY_NOISE_REDUCTION_TEMPORAL (not supported)", "temporal" },
      {GST_PHOTOGRAPHY_NOISE_REDUCTION_FIXED, "GST_PHOTOGRAPHY_NOISE_REDUCTION_FIXED (not supported)", "fixed" },
      {GST_PHOTOGRAPHY_NOISE_REDUCTION_EXTRA, "GST_PHOTOGRAPHY_NOISE_REDUCTION_EXTRA (not supported)", "extra" },
      { 0, NULL, NULL }
    };

  if (g_once_init_enter (&noise_reduction_type)) {
    GType tmp = g_flags_register_static ("GstPhotographyNoiseReduction", noise_reduction);
    g_once_init_leave (&noise_reduction_type, tmp);
  }
  return (GType) noise_reduction_type;
}


GType
gst_scene_mode_get_type (void)
{
  static volatile gsize scene_mode_type = 0;
  static const GEnumValue scene_mode[] = {
    {GST_PHOTOGRAPHY_SCENE_MODE_MANUAL, "GST_PHOTOGRAPHY_SCENE_MODE_MANUAL", "manual"},
    {GST_PHOTOGRAPHY_SCENE_MODE_CLOSEUP, "GST_PHOTOGRAPHY_SCENE_MODE_CLOSEUP (not supported)", "closeup"},
    {GST_PHOTOGRAPHY_SCENE_MODE_PORTRAIT, "GST_PHOTOGRAPHY_SCENE_MODE_PORTRAIT", "portrait"},
    {GST_PHOTOGRAPHY_SCENE_MODE_LANDSCAPE, "GST_PHOTOGRAPHY_SCENE_MODE_LANDSCAPE", "landscape"},
    {GST_PHOTOGRAPHY_SCENE_MODE_SPORT, "GST_PHOTOGRAPHY_SCENE_MODE_SPORT", "sport"},
    {GST_PHOTOGRAPHY_SCENE_MODE_NIGHT, "GST_PHOTOGRAPHY_SCENE_MODE_NIGHT", "night"},
    {GST_PHOTOGRAPHY_SCENE_MODE_AUTO, "GST_PHOTOGRAPHY_SCENE_MODE_AUTO", "auto"},
    {GST_PHOTOGRAPHY_NV_SCENE_MODE_ACTION, "GST_PHOTOGRAPHY_NV_SCENE_MODE_ACTION", "action"},
    {GST_PHOTOGRAPHY_NV_SCENE_MODE_BEACH, "GST_PHOTOGRAPHY_NV_SCENE_MODE_BEACH", "beach"},
    {GST_PHOTOGRAPHY_NV_SCENE_MODE_CANDLELIGHT, "GST_PHOTOGRAPHY_NV_SCENE_MODE_CANDLELIGHT", "candlelight"},
    {GST_PHOTOGRAPHY_NV_SCENE_MODE_FIREWORKS, "GST_PHOTOGRAPHY_NV_SCENE_MODE_FIREWORKS", "fireworks"},
    {GST_PHOTOGRAPHY_NV_SCENE_MODE_NIGHTPORTRAIT, "GST_PHOTOGRAPHY_NV_SCENE_MODE_NIGHTPORTRAIT", "nightportrait"},
    {GST_PHOTOGRAPHY_NV_SCENE_MODE_PARTY, "GST_PHOTOGRAPHY_NV_SCENE_MODE_PARTY", "party"},
    {GST_PHOTOGRAPHY_NV_SCENE_MODE_SNOW, "GST_PHOTOGRAPHY_NV_SCENE_MODE_SNOW", "snow"},
    {GST_PHOTOGRAPHY_NV_SCENE_MODE_SUNSET, "GST_PHOTOGRAPHY_NV_SCENE_MODE_SUNSET", "sunset"},
    {GST_PHOTOGRAPHY_NV_SCENE_MODE_THEATRE, "GST_PHOTOGRAPHY_NV_SCENE_MODE_THEATRE", "theatre"},
    {GST_PHOTOGRAPHY_NV_SCENE_MODE_BARCODE, "GST_PHOTOGRAPHY_NV_SCENE_MODE_BARCODE", "barcode"},
    {0, NULL, NULL}
  };

  if (g_once_init_enter (&scene_mode_type)) {
    GType tmp = g_enum_register_static ("GstSceneMode", scene_mode);
    g_once_init_leave (&scene_mode_type, tmp);
  }

  return (GType) scene_mode_type;
}


GType
gst_white_balance_mode_get_type (void)
{
  static volatile gsize white_balance_type = 0;
  static const GEnumValue white_balance[] = {
    {GST_PHOTOGRAPHY_WB_MODE_AUTO, "GST_PHOTOGRAPHY_WB_MODE_AUTO", "auto"},
    {GST_PHOTOGRAPHY_WB_MODE_DAYLIGHT, "GST_PHOTOGRAPHY_WB_MODE_DAYLIGHT", "daylight"},
    {GST_PHOTOGRAPHY_WB_MODE_CLOUDY, "GST_PHOTOGRAPHY_WB_MODE_CLOUDY", "cloudy"},
    {GST_PHOTOGRAPHY_WB_MODE_SUNSET, "GST_PHOTOGRAPHY_WB_MODE_SUNSET (not supported)", "sunset"},
    {GST_PHOTOGRAPHY_WB_MODE_TUNGSTEN, "GST_PHOTOGRAPHY_WB_MODE_TUNGSTEN", "tungsten"},
    {GST_PHOTOGRAPHY_WB_MODE_FLUORESCENT, "GST_PHOTOGRAPHY_WB_MODE_FLUORESCENT", "fluorescent"},
    {GST_PHOTOGRAPHY_NV_WB_MODE_SHADE, "GST_PHOTOGRAPHY_NV_WB_MODE_SHADE", "shade"},
    {GST_PHOTOGRAPHY_NV_WB_MODE_INCANDESCENT, "GST_PHOTOGRAPHY_NV_WB_MODE_INCANDESCENT", "incandescent"},
    {GST_PHOTOGRAPHY_NV_WB_MODE_FLASH, "GST_PHOTOGRAPHY_NV_WB_MODE_FLASH", "flash"},
    {GST_PHOTOGRAPHY_NV_WB_MODE_HORIZON, "GST_PHOTOGRAPHY_NV_WB_MODE_HORIZON", "horizon"},
    {GST_PHOTOGRAPHY_NV_WB_MODE_OFF, "GST_PHOTOGRAPHY_NV_WB_MODE_OFF", "off"},
    {0, NULL, NULL}
  };

  if (g_once_init_enter (&white_balance_type)) {
    GType tmp = g_enum_register_static ("GstWhiteBalanceMode", white_balance);
    g_once_init_leave (&white_balance_type, tmp);
  }

  return (GType) white_balance_type;
}


GType
gst_flicker_reduction_mode_get_type (void)
{
  static volatile gsize flicker_reduction_type = 0;
  static const GEnumValue flicker_reduction[] = {
    {GST_PHOTOGRAPHY_FLICKER_REDUCTION_OFF, "GST_PHOTOGRAPHY_FLICKER_REDUCTION_OFF", "off"},
    {GST_PHOTOGRAPHY_FLICKER_REDUCTION_50HZ, "GST_PHOTOGRAPHY_FLICKER_REDUCTION_50HZ", "50Hz"},
    {GST_PHOTOGRAPHY_FLICKER_REDUCTION_60HZ, "GST_PHOTOGRAPHY_FLICKER_REDUCTION_60HZ", "60Hz"},
    {GST_PHOTOGRAPHY_FLICKER_REDUCTION_AUTO, "GST_PHOTOGRAPHY_FLICKER_REDUCTION_AUTO", "auto"},
    {0, NULL, NULL}
  };

  if (g_once_init_enter (&flicker_reduction_type)) {
    GType tmp = g_enum_register_static ("GstFlickerReductionMode", flicker_reduction);
    g_once_init_leave (&flicker_reduction_type, tmp);
  }

  return (GType) flicker_reduction_type;
}


GType
gst_colour_tone_mode_get_type (void)
{
  static volatile gsize color_tone_type = 0;
  static const GEnumValue color_tone[] = {
    {GST_PHOTOGRAPHY_COLOUR_TONE_MODE_NORMAL, "GST_PHOTOGRAPHY_COLOUR_TONE_MODE_NORMAL", "normal"},
    {GST_PHOTOGRAPHY_COLOUR_TONE_MODE_SEPIA, "GST_PHOTOGRAPHY_COLOUR_TONE_MODE_SEPIA", "sepia"},
    {GST_PHOTOGRAPHY_COLOUR_TONE_MODE_NEGATIVE, "GST_PHOTOGRAPHY_COLOUR_TONE_MODE_NEGATIVE", "negative"},
    {GST_PHOTOGRAPHY_COLOUR_TONE_MODE_GRAYSCALE, "GST_PHOTOGRAPHY_COLOUR_TONE_MODE_GRAYSCALE (not supported)", "grayscale"},
    {GST_PHOTOGRAPHY_COLOUR_TONE_MODE_NATURAL, "GST_PHOTOGRAPHY_COLOUR_TONE_MODE_NATURAL (not supported)", "natural"},
    {GST_PHOTOGRAPHY_COLOUR_TONE_MODE_VIVID, "GST_PHOTOGRAPHY_COLOUR_TONE_MODE_VIVID (not supported)", "vivid"},
    {GST_PHOTOGRAPHY_COLOUR_TONE_MODE_COLORSWAP, "GST_PHOTOGRAPHY_COLOUR_TONE_MODE_COLORSWAP (not supported)", "colorswap"},
    {GST_PHOTOGRAPHY_COLOUR_TONE_MODE_SOLARIZE, "GST_PHOTOGRAPHY_COLOUR_TONE_MODE_SOLARIZE", "solarize"},
    {GST_PHOTOGRAPHY_COLOUR_TONE_MODE_OUT_OF_FOCUS, "GST_PHOTOGRAPHY_COLOUR_TONE_MODE_OUT_OF_FOCUS (not supported)", "outoffocus"},
    {GST_PHOTOGRAPHY_COLOUR_TONE_MODE_SKY_BLUE, "GST_PHOTOGRAPHY_COLOUR_TONE_MODE_SKY_BLUE (not supported)", "skyblue"},
    {GST_PHOTOGRAPHY_COLOUR_TONE_MODE_GRASS_GREEN, "GST_PHOTOGRAPHY_COLOUR_TONE_MODE_GRASS_GREEN (not supported)", "grassgreen"},
    {GST_PHOTOGRAPHY_COLOUR_TONE_MODE_SKIN_WHITEN, "GST_PHOTOGRAPHY_COLOUR_TONE_MODE_SKIN_WHITEN (not supported)", "skinwhiten"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_NOISE, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_NOISE", "noise"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_EMBOSS, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_EMBOSS", "emboss"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_SKETCH, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_SKETCH", "sketch"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_OIL_PAINT, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_OIL_PAINT", "oilpaint"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_HATCH, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_HATCH", "hatch"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_GPEN, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_GPEN", "gpen"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_ANTIALIAS, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_ANTIALIAS", "antialias"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_DERING, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_DERING", "dering"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_POSTERIZE, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_POSTERIZE", "posterize"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_BW, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_BW", "bw"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_MANUAL, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_MANUAL", "manual"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_AQUA, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_AQUA", "aqua"},
    {0, NULL, NULL}
  };

  if (g_once_init_enter (&color_tone_type)) {
    GType tmp = g_enum_register_static ("GstColourToneMode", color_tone);
    g_once_init_leave (&color_tone_type, tmp);
  }

  return (GType) color_tone_type;
}


GType
gst_focus_mode_get_type (void)
{
  static volatile gsize focus_mode_type = 0;
  static const GEnumValue focus_mode[] = {
    {GST_PHOTOGRAPHY_FOCUS_MODE_AUTO, "GST_PHOTOGRAPHY_FOCUS_MODE_AUTO", "auto" },
    {GST_PHOTOGRAPHY_FOCUS_MODE_MACRO, "GST_PHOTOGRAPHY_FOCUS_MODE_MACRO", "macro" },
    {GST_PHOTOGRAPHY_FOCUS_MODE_PORTRAIT, "GST_PHOTOGRAPHY_FOCUS_MODE_PORTRAIT (not supported)", "portrait" },
    {GST_PHOTOGRAPHY_FOCUS_MODE_INFINITY, "GST_PHOTOGRAPHY_FOCUS_MODE_INFINITY", "infinity" },
    {GST_PHOTOGRAPHY_FOCUS_MODE_HYPERFOCAL, "GST_PHOTOGRAPHY_FOCUS_MODE_HYPERFOCAL (not supported)", "hyperfocal" },
    {GST_PHOTOGRAPHY_FOCUS_MODE_EXTENDED, "GST_PHOTOGRAPHY_FOCUS_MODE_EXTENDED (not supported)", "extended" },
    {GST_PHOTOGRAPHY_FOCUS_MODE_CONTINUOUS_NORMAL, "GST_PHOTOGRAPHY_FOCUS_MODE_CONTINUOUS_NORMAL (not supported)", "continuous-normal" },
    {GST_PHOTOGRAPHY_FOCUS_MODE_CONTINUOUS_EXTENDED, "GST_PHOTOGRAPHY_FOCUS_MODE_CONTINUOUS_EXTENDED (not supported)", "continuous-extended" },
    {GST_PHOTOGRAPHY_NV_FOCUS_MODE_CONTINUOUS_PICTURE, "GST_PHOTOGRAPHY_NV_FOCUS_MODE_CONTINUOUS_PICTURE", "continuous-picture" },
    {GST_PHOTOGRAPHY_NV_FOCUS_MODE_CONTINUOUS_VIDEO, "GST_PHOTOGRAPHY_NV_FOCUS_MODE_CONTINUOUS_VIDEO", "continuous-video" },
    {GST_PHOTOGRAPHY_NV_FOCUS_MODE_FIXED, "GST_PHOTOGRAPHY_NV_FOCUS_MODE_FIXED", "fixed" },
    {0, NULL, NULL }
  };

  if (g_once_init_enter (&focus_mode_type)) {
    GType tmp = g_enum_register_static ("GstFocusMode", focus_mode);
    g_once_init_leave (&focus_mode_type, tmp);
  }

  return (GType) focus_mode_type;
}

#else

GType
gst_nv_focus_mode_get_type (void)
{
  static volatile gsize focus_mode_nv_type = 0;
  static const GEnumValue focus_mode_nv[] = {
    {GST_PHOTOGRAPHY_NV_FOCUS_MODE_CONTINUOUS_PICTURE, "GST_PHOTOGRAPHY_NV_FOCUS_MODE_CONTINUOUS_PICTURE", "continuous-picture" },
    {GST_PHOTOGRAPHY_NV_FOCUS_MODE_CONTINUOUS_VIDEO, "GST_PHOTOGRAPHY_NV_FOCUS_MODE_CONTINUOUS_VIDEO", "continuous-video" },
    {0, NULL, NULL }
  };

  if (g_once_init_enter (&focus_mode_nv_type)) {
    GType tmp = g_enum_register_static ("GstNVFocusMode", focus_mode_nv);
    g_once_init_leave (&focus_mode_nv_type, tmp);
  }

  return (GType) focus_mode_nv_type;
}

GType
gst_nv_flash_mode_get_type (void)
{
  static volatile gsize flash_nv_type = 0;
  static const GEnumValue flash_nv[] = {
    {GST_PHOTOGRAPHY_NV_FLASH_MODE_TORCH, "GST_PHOTOGRAPHY_NV_FLASH_MODE_TORCH", "torch"},
    {0, NULL, NULL}
  };

  if (g_once_init_enter (&flash_nv_type)) {
    GType tmp = g_enum_register_static ("GstNvFlashMode", flash_nv);
    g_once_init_leave (&flash_nv_type, tmp);
  }

  return (GType) flash_nv_type;
}


GType
gst_nv_white_balance_mode_get_type (void)
{
  static volatile gsize white_balance_nv_type = 0;
  static const GEnumValue white_balance_nv[] = {
    {GST_PHOTOGRAPHY_NV_WB_MODE_SHADE, "GST_PHOTOGRAPHY_NV_WB_MODE_SHADE", "shade"},
    {GST_PHOTOGRAPHY_NV_WB_MODE_INCANDESCENT, "GST_PHOTOGRAPHY_NV_WB_MODE_INCANDESCENT", "incandescent"},
    {GST_PHOTOGRAPHY_NV_WB_MODE_FLASH, "GST_PHOTOGRAPHY_NV_WB_MODE_FLASH", "flash"},
    {GST_PHOTOGRAPHY_NV_WB_MODE_HORIZON, "GST_PHOTOGRAPHY_NV_WB_MODE_HORIZON", "horizon"},
    {GST_PHOTOGRAPHY_NV_WB_MODE_OFF, "GST_PHOTOGRAPHY_NV_WB_MODE_OFF", "off"},
    {0, NULL, NULL}
  };

  if (g_once_init_enter (&white_balance_nv_type)) {
    GType tmp = g_enum_register_static ("GstNvWhiteBalanceMode", white_balance_nv);
    g_once_init_leave (&white_balance_nv_type, tmp);
  }

  return (GType) white_balance_nv_type;
}


GType
gst_nv_colour_tone_mode_get_type (void)
{
  static volatile gsize color_tone_nv_type = 0;
  static const GEnumValue color_tone_nv[] = {
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_NOISE, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_NOISE", "noise"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_EMBOSS, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_EMBOSS", "emboss"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_SKETCH, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_SKETCH", "sketch"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_OIL_PAINT, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_OIL_PAINT", "oilpaint"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_HATCH, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_HATCH", "hatch"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_GPEN, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_GPEN", "gpen"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_ANTIALIAS, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_ANTIALIAS", "antialias"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_DERING, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_DERING", "dering"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_POSTERIZE, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_POSTERIZE", "posterize"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_BW, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_BW", "bw"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_MANUAL, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_MANUAL", "manual"},
    {GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_AQUA, "GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_AQUA", "aqua"},
    {0, NULL, NULL}
  };

  if (g_once_init_enter (&color_tone_nv_type)) {
    GType tmp = g_enum_register_static ("GstNvColourToneMode", color_tone_nv);
    g_once_init_leave (&color_tone_nv_type, tmp);
  }

  return (GType) color_tone_nv_type;
}


GType
gst_nv_scene_mode_get_type (void)
{
  static volatile gsize scene_mode_nv_type = 0;
  static const GEnumValue scene_mode_nv[] = {
    {GST_PHOTOGRAPHY_NV_SCENE_MODE_ACTION, "GST_PHOTOGRAPHY_NV_SCENE_MODE_ACTION", "action"},
    {GST_PHOTOGRAPHY_NV_SCENE_MODE_BEACH, "GST_PHOTOGRAPHY_NV_SCENE_MODE_BEACH", "beach"},
    {GST_PHOTOGRAPHY_NV_SCENE_MODE_CANDLELIGHT, "GST_PHOTOGRAPHY_NV_SCENE_MODE_CANDLELIGHT", "candlelight"},
    {GST_PHOTOGRAPHY_NV_SCENE_MODE_FIREWORKS, "GST_PHOTOGRAPHY_NV_SCENE_MODE_FIREWORKS", "fireworks"},
    {GST_PHOTOGRAPHY_NV_SCENE_MODE_NIGHTPORTRAIT, "GST_PHOTOGRAPHY_NV_SCENE_MODE_NIGHTPORTRAIT", "nightportrait"},
    {GST_PHOTOGRAPHY_NV_SCENE_MODE_PARTY, "GST_PHOTOGRAPHY_NV_SCENE_MODE_PARTY", "party"},
    {GST_PHOTOGRAPHY_NV_SCENE_MODE_SNOW, "GST_PHOTOGRAPHY_NV_SCENE_MODE_SNOW", "snow"},
    {GST_PHOTOGRAPHY_NV_SCENE_MODE_SUNSET, "GST_PHOTOGRAPHY_NV_SCENE_MODE_SUNSET", "sunset"},
    {GST_PHOTOGRAPHY_NV_SCENE_MODE_THEATRE, "GST_PHOTOGRAPHY_NV_SCENE_MODE_THEATRE", "theatre"},
    {GST_PHOTOGRAPHY_NV_SCENE_MODE_BARCODE, "GST_PHOTOGRAPHY_NV_SCENE_MODE_BARCODE", "barcode"},
    {0, NULL, NULL}
  };

  if (g_once_init_enter (&scene_mode_nv_type)) {
    GType tmp = g_enum_register_static ("GstNvSceneMode", scene_mode_nv);
    g_once_init_leave (&scene_mode_nv_type, tmp);
  }

  return (GType) scene_mode_nv_type;
}

#endif


static OMX_IMAGE_FLASHCONTROLTYPE
gst_omx_camera_flash_gst_to_omx (GstFlashMode flash_mode)
{
  switch ((gint)flash_mode) {
    case GST_PHOTOGRAPHY_FLASH_MODE_AUTO:
      return OMX_IMAGE_FlashControlAuto;

    case GST_PHOTOGRAPHY_FLASH_MODE_OFF:
      return OMX_IMAGE_FlashControlOff;

    case GST_PHOTOGRAPHY_FLASH_MODE_ON:
      return OMX_IMAGE_FlashControlOn;

    case GST_PHOTOGRAPHY_FLASH_MODE_FILL_IN:
      return OMX_IMAGE_FlashControlFillin;

    case GST_PHOTOGRAPHY_FLASH_MODE_RED_EYE:
      return OMX_IMAGE_FlashControlRedEyeReduction;

    case  GST_PHOTOGRAPHY_NV_FLASH_MODE_TORCH:
      return OMX_IMAGE_FlashControlTorch;

    default:
      return -1;
  }
}


static GstFlashMode
gst_omx_camera_flash_omx_to_gst (OMX_IMAGE_FLASHCONTROLTYPE flash_mode)
{
  switch ((gint)flash_mode) {
    case OMX_IMAGE_FlashControlAuto:
      return GST_PHOTOGRAPHY_FLASH_MODE_AUTO;

    case OMX_IMAGE_FlashControlOff:
      return GST_PHOTOGRAPHY_FLASH_MODE_OFF;

    case OMX_IMAGE_FlashControlOn:
      return GST_PHOTOGRAPHY_FLASH_MODE_ON;

    case OMX_IMAGE_FlashControlFillin:
      return GST_PHOTOGRAPHY_FLASH_MODE_FILL_IN;

    case OMX_IMAGE_FlashControlRedEyeReduction:
      return GST_PHOTOGRAPHY_FLASH_MODE_RED_EYE;

    case OMX_IMAGE_FlashControlTorch:
      return GST_PHOTOGRAPHY_NV_FLASH_MODE_TORCH;

    default:
      return -1;
  }
}


static OMX_WHITEBALCONTROLTYPE
gst_omx_camera_white_balance_gst_to_omx (GstWhiteBalanceMode wb_mode)
{
  switch ((gint)wb_mode) {
    case GST_PHOTOGRAPHY_WB_MODE_AUTO:
      return OMX_WhiteBalControlAuto;

    case GST_PHOTOGRAPHY_WB_MODE_DAYLIGHT:
      return OMX_WhiteBalControlSunLight;

    case GST_PHOTOGRAPHY_WB_MODE_CLOUDY:
      return OMX_WhiteBalControlCloudy;

    case GST_PHOTOGRAPHY_WB_MODE_TUNGSTEN:
      return OMX_WhiteBalControlTungsten;

    case GST_PHOTOGRAPHY_WB_MODE_FLUORESCENT:
      return OMX_WhiteBalControlFluorescent;

    case GST_PHOTOGRAPHY_NV_WB_MODE_SHADE:
      return OMX_WhiteBalControlShade;

    case GST_PHOTOGRAPHY_NV_WB_MODE_INCANDESCENT:
      return OMX_WhiteBalControlIncandescent;

    case GST_PHOTOGRAPHY_NV_WB_MODE_FLASH:
      return OMX_WhiteBalControlFlash;

    case GST_PHOTOGRAPHY_NV_WB_MODE_HORIZON:
      return OMX_WhiteBalControlHorizon;

    case GST_PHOTOGRAPHY_NV_WB_MODE_OFF:
      return OMX_WhiteBalControlOff;

    default:
      return -1;
  }
}


static GstFlashMode
gst_omx_camera_white_balance_omx_to_gst (OMX_WHITEBALCONTROLTYPE wb_mode)
{
  switch ((gint)wb_mode) {
    case OMX_WhiteBalControlAuto:
      return GST_PHOTOGRAPHY_WB_MODE_AUTO;

    case OMX_WhiteBalControlSunLight:
      return GST_PHOTOGRAPHY_WB_MODE_DAYLIGHT;

    case OMX_WhiteBalControlCloudy:
      return GST_PHOTOGRAPHY_WB_MODE_CLOUDY;

    case OMX_WhiteBalControlTungsten:
      return GST_PHOTOGRAPHY_WB_MODE_TUNGSTEN;

    case OMX_WhiteBalControlFluorescent:
      return GST_PHOTOGRAPHY_WB_MODE_FLUORESCENT;

    case OMX_WhiteBalControlShade:
      return GST_PHOTOGRAPHY_NV_WB_MODE_SHADE;

    case OMX_WhiteBalControlFlash:
      return GST_PHOTOGRAPHY_NV_WB_MODE_FLASH;

    case OMX_WhiteBalControlHorizon:
      return GST_PHOTOGRAPHY_NV_WB_MODE_HORIZON;

    case OMX_WhiteBalControlOff:
      return GST_PHOTOGRAPHY_NV_WB_MODE_OFF;

    default:
      return -1;
  }
}


static OMX_METERINGTYPE
gst_omx_camera_metering_gst_to_omx (GstNvMeteringMode metering_mode)
{
  switch ((gint)metering_mode) {
    case GST_NV_METERING_MODE_AVG:
      return OMX_MeteringModeAverage;

    case GST_NV_METERING_MODE_SPOT:
      return OMX_MeteringModeSpot;

    case GST_NV_METERING_MODE_MATRIX:
      return OMX_MeteringModeMatrix;

    default:
      return -1;
  }
}


static GstNvMeteringMode
gst_omx_camera_metering_omx_to_gst (OMX_METERINGTYPE metering_mode)
{
  switch ((gint)metering_mode) {
    case OMX_MeteringModeAverage:
      return GST_NV_METERING_MODE_AVG;

    case OMX_MeteringModeSpot:
      return GST_NV_METERING_MODE_SPOT;

    case OMX_MeteringModeMatrix:
      return GST_NV_METERING_MODE_MATRIX;

    default:
      return -1;
  }
}


static OMX_IMAGEFILTERTYPE
gst_omx_camera_color_tone_gst_to_omx (GstColourToneMode color_tone_mode)
{
  switch ((gint)color_tone_mode) {
    case GST_PHOTOGRAPHY_COLOUR_TONE_MODE_NORMAL:
      return OMX_ImageFilterNone;

    case GST_PHOTOGRAPHY_COLOUR_TONE_MODE_SEPIA:
      return NVX_ImageFilterSepia;

    case GST_PHOTOGRAPHY_COLOUR_TONE_MODE_NEGATIVE:
      return OMX_ImageFilterNegative;

    case GST_PHOTOGRAPHY_COLOUR_TONE_MODE_SOLARIZE:
      return OMX_ImageFilterSolarize;

    case GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_NOISE:
      return OMX_ImageFilterNoise;

    case GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_EMBOSS:
      return OMX_ImageFilterEmboss;

    case GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_SKETCH:
      return OMX_ImageFilterSketch;

    case GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_OIL_PAINT:
      return OMX_ImageFilterOilPaint;

    case GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_HATCH:
      return OMX_ImageFilterHatch;

    case GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_GPEN:
      return OMX_ImageFilterGpen;

    case GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_ANTIALIAS:
      return OMX_ImageFilterAntialias;

    case GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_DERING:
      return OMX_ImageFilterDeRing;

    case GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_POSTERIZE:
      return NVX_ImageFilterPosterize;

    case GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_BW:
      return NVX_ImageFilterBW;

    case GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_MANUAL:
      return NVX_ImageFilterManual;

    case GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_AQUA:
      return NVX_ImageFilterAqua;

    default:
      return -1;
  }
}


static GstColourToneMode
gst_omx_camera_color_tone_omx_to_gst (OMX_IMAGEFILTERTYPE color_tone_mode)
{
  switch ((gint)color_tone_mode) {
    case OMX_ImageFilterNone:
      return GST_PHOTOGRAPHY_COLOUR_TONE_MODE_NORMAL;

    case NVX_ImageFilterSepia:
      return GST_PHOTOGRAPHY_COLOUR_TONE_MODE_SEPIA;

    case OMX_ImageFilterNegative:
      return GST_PHOTOGRAPHY_COLOUR_TONE_MODE_NEGATIVE;

    case OMX_ImageFilterSolarize:
      return GST_PHOTOGRAPHY_COLOUR_TONE_MODE_SOLARIZE;

    case OMX_ImageFilterNoise:
      return GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_NOISE;

    case OMX_ImageFilterEmboss:
      return GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_EMBOSS;

    case OMX_ImageFilterSketch:
      return GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_SKETCH;

    case OMX_ImageFilterOilPaint:
      return GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_OIL_PAINT;

    case OMX_ImageFilterHatch:
      return GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_HATCH;

    case OMX_ImageFilterGpen:
      return GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_GPEN;

    case OMX_ImageFilterAntialias:
      return GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_ANTIALIAS;

    case OMX_ImageFilterDeRing:
      return GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_DERING;

    case NVX_ImageFilterPosterize:
      return GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_POSTERIZE;

    case NVX_ImageFilterBW:
      return GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_BW;

    case NVX_ImageFilterManual:
      return GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_MANUAL;

    case NVX_ImageFilterAqua:
      return GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_AQUA;

    default:
      return -1;
  }
}



static ENvxFlickerType
gst_omx_camera_flicker_reduction_gst_to_omx (GstFlickerReductionMode fr_mode)
{
  switch ((gint)fr_mode) {
    case GST_PHOTOGRAPHY_FLICKER_REDUCTION_OFF:
      return NvxFlicker_Off;

    case GST_PHOTOGRAPHY_FLICKER_REDUCTION_50HZ:
      return NvxFlicker_50HZ;

    case GST_PHOTOGRAPHY_FLICKER_REDUCTION_60HZ:
      return NvxFlicker_60HZ;

    case GST_PHOTOGRAPHY_FLICKER_REDUCTION_AUTO:
      return NvxFlicker_Auto;

    default:
      return -1;
  }
}



static GstFlickerReductionMode
gst_omx_camera_flicker_reduction_omx_to_gst (ENvxFlickerType fr_mode)
{
  switch ((gint)fr_mode) {
    case NvxFlicker_Off:
      return GST_PHOTOGRAPHY_FLICKER_REDUCTION_OFF;

    case NvxFlicker_50HZ:
      return GST_PHOTOGRAPHY_FLICKER_REDUCTION_50HZ;

    case NvxFlicker_60HZ:
      return GST_PHOTOGRAPHY_FLICKER_REDUCTION_60HZ;

    case NvxFlicker_Auto:
      return GST_PHOTOGRAPHY_FLICKER_REDUCTION_AUTO;

    default:
      return -1;
  }
}



gboolean
gst_omx_camera_set_mode (OMX_HANDLETYPE omx_handle, GstNvCameraMode mode)
{
  OMX_PARAM_SENSORMODETYPE oSensorMode;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM (oSensorMode);
  OMX_GetParameter (omx_handle, OMX_IndexParamCommonSensorMode, &oSensorMode);
  oSensorMode.bOneShot = mode == GST_NV_MODE_IMAGE ? OMX_TRUE : OMX_FALSE;
  if(mode == GST_NV_MODE_VIDEO)
    oSensorMode.nPortIndex = GST_OMX_CAMERA_VIDEO_PORT;
  else
    oSensorMode.nPortIndex = GST_OMX_CAMERA_STILL_PORT;

  OMX_SetParameter(omx_handle, OMX_IndexParamCommonSensorMode, &oSensorMode);

  return ret;
}


gboolean
gst_omx_camera_get_mode (OMX_HANDLETYPE omx_handle, GstNvCameraMode* mode)
{
  OMX_PARAM_SENSORMODETYPE oSensorMode;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM (oSensorMode);
  oSensorMode.nPortIndex = 1;

  OMX_GetParameter (omx_handle, OMX_IndexParamCommonSensorMode, &oSensorMode);

  *mode = oSensorMode.bOneShot == OMX_TRUE ? GST_NV_MODE_IMAGE : GST_NV_MODE_VIDEO;

  return ret;
}


gboolean
gst_omx_camera_set_stereo_mode (OMX_HANDLETYPE omx_handle, GstNvStereoMode stm)
{
  OMX_INDEXTYPE index;
  NVX_CONFIG_STEREOCAPABLE oStereoCapable;
  NVX_PARAM_STEREOCAMERAMODE oStereoMode;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_STEREOCAPABLE, &index);

  G_OMX_INIT_PARAM(oStereoCapable);
  OMX_GetConfig(omx_handle, index, &oStereoCapable);

  if ((oStereoCapable.StereoCapable) && (stm != GST_NV_STEREO_MODE_OFF))
  {
    if (OMX_ErrorNone == OMX_GetExtensionIndex (omx_handle, NVX_INDEX_PARAM_STEREOCAMERAMODE, &index))
    {
      G_OMX_INIT_PARAM(oStereoMode);
      switch (stm)
      {
        case GST_NV_STEREO_MODE_LEFT_ONLY:
           oStereoMode.StereoCameraMode = NvxLeftOnly;
           break;
        case GST_NV_STEREO_MODE_RIGHT_ONLY:
           oStereoMode.StereoCameraMode = NvxRightOnly;
           break;
        case GST_NV_STEREO_MODE_FULL:
           oStereoMode.StereoCameraMode = NvxStereo;
           break;
        default:
           oStereoMode.StereoCameraMode = NvxLeftOnly;
           break;
      }
      OMX_SetParameter (omx_handle, index, &oStereoMode);
    }
  }

  return ret;
}



gboolean
gst_omx_camera_get_stereo_mode (OMX_HANDLETYPE omx_handle, GstNvStereoMode *stm)
{
  OMX_INDEXTYPE index;
  NVX_PARAM_STEREOCAMERAMODE oStereoMode;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_PARAM_STEREOCAMERAMODE, &index);

  G_OMX_INIT_PARAM(oStereoMode);
  OMX_GetParameter (omx_handle, index, &oStereoMode);
  switch (oStereoMode.StereoCameraMode)
  {
    case NvxLeftOnly:
       *stm = GST_NV_STEREO_MODE_LEFT_ONLY;
       break;
    case NvxRightOnly:
       *stm = GST_NV_STEREO_MODE_RIGHT_ONLY;
       break;
    case NvxStereo:
       *stm = GST_NV_STEREO_MODE_FULL;
       break;
    default:
       *stm = GST_NV_STEREO_MODE_OFF;
       break;
  }

  return ret;
}


gboolean
gst_omx_camera_set_noise_reduction (OMX_HANDLETYPE omx_handle, GstPhotographyNoiseReduction anr_mode,
    gulong lThreshold, gulong cThreshold)
{
  gboolean ret = TRUE;
  OMX_INDEXTYPE index;
  NVX_CONFIG_ADVANCED_NOISE_REDUCTION oanr;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_ADVANCED_NOISE_REDUCTION, &index);

  G_OMX_INIT_PARAM(oanr);
  OMX_GetConfig(omx_handle, index, &oanr);

  if (anr_mode & GST_PHOTOGRAPHY_NOISE_REDUCTION_YCC) {
    oanr.enable = OMX_TRUE;
    oanr.lumaIsoThreshold = lThreshold;
    oanr.chromaIsoThreshold = cThreshold;

  } else {
    oanr.enable = OMX_FALSE;
    oanr.lumaIsoThreshold = 0;
    oanr.chromaIsoThreshold = 0;
  }

  OMX_SetConfig(omx_handle, index, &oanr);

  return ret;
}


gboolean
gst_omx_camera_get_noise_reduction (OMX_HANDLETYPE omx_handle, GstPhotographyNoiseReduction* anr_mode,
    gulong* lThreshold, gulong* cThreshold)
{
  gboolean ret = TRUE;
  OMX_INDEXTYPE index;
  NVX_CONFIG_ADVANCED_NOISE_REDUCTION oanr;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_ADVANCED_NOISE_REDUCTION, &index);

  G_OMX_INIT_PARAM(oanr);
  OMX_GetConfig(omx_handle, index, &oanr);

  if (oanr.enable == OMX_TRUE) {
    *anr_mode |= GST_PHOTOGRAPHY_NOISE_REDUCTION_YCC;

    if (lThreshold)
      *lThreshold = oanr.lumaIsoThreshold;

    if (cThreshold)
      *cThreshold = oanr.chromaIsoThreshold;
  }

  return ret;
}


gboolean
gst_omx_camera_set_scene_mode (OMX_HANDLETYPE omx_handle, GstSceneMode sm)
{
  OMX_INDEXTYPE index, index2, index3;
  NVX_CONFIG_EXPOSURETIME_RANGE oExposureTimeRange;
  NVX_CONFIG_ISOSENSITIVITY_RANGE oISORange;
  NVX_CONFIG_CCTWHITEBALANCE_RANGE oWBCCTRange;
  gboolean ret = TRUE;

  gst_omx_camera_set_white_balance_mode (omx_handle, GST_PHOTOGRAPHY_WB_MODE_AUTO);

  gst_omx_camera_set_flash_mode (omx_handle, GST_PHOTOGRAPHY_FLASH_MODE_OFF);

  gst_omx_camera_set_noise_reduction (omx_handle, GST_PHOTOGRAPHY_NOISE_REDUCTION_YCC, 150, 105);

  gst_omx_camera_set_iso_speed (omx_handle, 0);

  gst_omx_camera_set_contrast (omx_handle, 0);

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_EXPOSURETIMERANGE, &index);

  G_OMX_INIT_PARAM (oExposureTimeRange);
  OMX_GetConfig (omx_handle, index, &oExposureTimeRange);

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_ISOSENSITIVITYRANGE, &index2);

  G_OMX_INIT_PARAM (oISORange);
  OMX_GetConfig (omx_handle, index2, &oISORange);

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_CCTWHITEBALANCERANGE, &index3);

  G_OMX_INIT_PARAM (oWBCCTRange);
  OMX_GetConfig (omx_handle, index3, &oWBCCTRange);

  switch ((gint)sm)
  {
    case GST_PHOTOGRAPHY_SCENE_MODE_AUTO:
    break;

    case GST_PHOTOGRAPHY_NV_SCENE_MODE_ACTION:
      gst_omx_camera_set_noise_reduction (omx_handle, 0, 0, 0);
      gst_omx_camera_set_flash_mode (omx_handle, GST_PHOTOGRAPHY_FLASH_MODE_OFF);

      oExposureTimeRange.low = 0;
      oExposureTimeRange.high = 33;
      OMX_SetConfig(omx_handle, index, &oExposureTimeRange);

      oISORange.low = 100;
      oISORange.high = 6400;
      OMX_SetConfig(omx_handle, index2, &oISORange);
    break;

    case GST_PHOTOGRAPHY_SCENE_MODE_PORTRAIT:
      gst_omx_camera_set_flash_mode (omx_handle, GST_PHOTOGRAPHY_FLASH_MODE_AUTO);

      oExposureTimeRange.low = 0;
      oExposureTimeRange.high = 1000;
      OMX_SetConfig(omx_handle, index, &oExposureTimeRange);

      oISORange.low = 100;
      oISORange.high = 1600;
      OMX_SetConfig(omx_handle, index2, &oISORange);
    break;

    case GST_PHOTOGRAPHY_SCENE_MODE_LANDSCAPE:
      gst_omx_camera_set_flash_mode (omx_handle, GST_PHOTOGRAPHY_FLASH_MODE_OFF);

      oExposureTimeRange.low = 0;
      oExposureTimeRange.high = 1000;
      OMX_SetConfig(omx_handle, index, &oExposureTimeRange);

      oISORange.low = 100;
      oISORange.high = 1600;
      OMX_SetConfig(omx_handle, index2, &oISORange);
    break;

    case GST_PHOTOGRAPHY_NV_SCENE_MODE_BEACH:
      gst_omx_camera_set_white_balance_mode (omx_handle, OMX_WhiteBalControlSunLight);
      gst_omx_camera_set_flash_mode (omx_handle, GST_PHOTOGRAPHY_FLASH_MODE_AUTO);
    break;

    case GST_PHOTOGRAPHY_NV_SCENE_MODE_CANDLELIGHT:
      gst_omx_camera_set_flash_mode (omx_handle, GST_PHOTOGRAPHY_FLASH_MODE_OFF);

      oExposureTimeRange.low = 0;
      oExposureTimeRange.high = 2000;
      OMX_SetConfig(omx_handle, index, &oExposureTimeRange);

      oISORange.low = 100;
      oISORange.high = 1600;
      OMX_SetConfig(omx_handle, index2, &oISORange);
    break;

    case GST_PHOTOGRAPHY_NV_SCENE_MODE_FIREWORKS:
      gst_omx_camera_set_flash_mode (omx_handle, GST_PHOTOGRAPHY_FLASH_MODE_OFF);
      gst_omx_camera_set_white_balance_mode (omx_handle, OMX_WhiteBalControlAuto);

      oExposureTimeRange.low = 0;
      oExposureTimeRange.high = 2000;
      OMX_SetConfig(omx_handle, index, &oExposureTimeRange);
    break;

    case GST_PHOTOGRAPHY_SCENE_MODE_NIGHT:
      gst_omx_camera_set_flash_mode (omx_handle, GST_PHOTOGRAPHY_FLASH_MODE_OFF);
      gst_omx_camera_set_noise_reduction (omx_handle, GST_PHOTOGRAPHY_NOISE_REDUCTION_YCC, 0, 0);

      oExposureTimeRange.low = 0;
      oExposureTimeRange.high = 2000;
      OMX_SetConfig(omx_handle, index, &oExposureTimeRange);
    break;

    case GST_PHOTOGRAPHY_NV_SCENE_MODE_NIGHTPORTRAIT:
      gst_omx_camera_set_flash_mode (omx_handle, OMX_IMAGE_FlashControlRedEyeReduction);
      gst_omx_camera_set_noise_reduction (omx_handle, GST_PHOTOGRAPHY_NOISE_REDUCTION_YCC, 0, 0);

      oExposureTimeRange.low = 0;
      oExposureTimeRange.high = 2000;
      OMX_SetConfig(omx_handle, index, &oExposureTimeRange);

      oISORange.low = 100;
      oISORange.high = 1600;
      OMX_SetConfig(omx_handle, index2, &oISORange);
    break;

    case GST_PHOTOGRAPHY_NV_SCENE_MODE_PARTY:
      gst_omx_camera_set_flash_mode (omx_handle, GST_PHOTOGRAPHY_FLASH_MODE_AUTO);

      oExposureTimeRange.low = 0;
      oExposureTimeRange.high = 2000;
      OMX_SetConfig(omx_handle, index, &oExposureTimeRange);

      oISORange.low = 100;
      oISORange.high = 1600;
      OMX_SetConfig(omx_handle, index2, &oISORange);
    break;

    case GST_PHOTOGRAPHY_NV_SCENE_MODE_SNOW:
      gst_omx_camera_set_flash_mode (omx_handle, GST_PHOTOGRAPHY_FLASH_MODE_AUTO);

      oWBCCTRange.low = 5500;
      oWBCCTRange.high = 8500;
      OMX_SetConfig(omx_handle, index3, &oWBCCTRange);
    break;

    case GST_PHOTOGRAPHY_SCENE_MODE_SPORT:
      gst_omx_camera_set_flash_mode (omx_handle, GST_PHOTOGRAPHY_FLASH_MODE_OFF);
      gst_omx_camera_set_noise_reduction (omx_handle, 0, 0, 0);

      oExposureTimeRange.low = 0;
      oExposureTimeRange.high = 33;
      OMX_SetConfig(omx_handle, index, &oExposureTimeRange);

      oISORange.low = 100;
      oISORange.high = 6400;
      OMX_SetConfig(omx_handle, index2, &oISORange);
    break;

    case GST_PHOTOGRAPHY_NV_SCENE_MODE_SUNSET:
      gst_omx_camera_set_flash_mode (omx_handle, GST_PHOTOGRAPHY_FLASH_MODE_OFF);

      oExposureTimeRange.low = 0;
      oExposureTimeRange.high = 2000;
      OMX_SetConfig(omx_handle, index, &oExposureTimeRange);
    break;

    case GST_PHOTOGRAPHY_NV_SCENE_MODE_THEATRE:
      gst_omx_camera_set_flash_mode (omx_handle, GST_PHOTOGRAPHY_FLASH_MODE_OFF);

      oISORange.low = 100;
      oISORange.high = 1600;
      OMX_SetConfig(omx_handle, index2, &oISORange);
    break;

    case GST_PHOTOGRAPHY_NV_SCENE_MODE_BARCODE:
      gst_omx_camera_set_flash_mode (omx_handle, GST_PHOTOGRAPHY_FLASH_MODE_OFF);
      gst_omx_camera_set_enable_edge_enhancement (omx_handle, TRUE);
      gst_omx_camera_set_edge_enhancement (omx_handle, 0.72);
      gst_omx_camera_set_contrast (omx_handle, 100);
    break;

    default:
    break;
  }

  return ret;
}


gboolean
gst_omx_camera_set_pause_after_capture (OMX_HANDLETYPE omx_handle, gboolean sbool)
{
  OMX_CONFIG_BOOLEANTYPE oPausePreviewAfterStillCapture;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM (oPausePreviewAfterStillCapture);

  OMX_GetConfig(omx_handle, OMX_IndexAutoPauseAfterCapture, &oPausePreviewAfterStillCapture);
  oPausePreviewAfterStillCapture.bEnabled = sbool;
  OMX_SetConfig(omx_handle, OMX_IndexAutoPauseAfterCapture, &oPausePreviewAfterStillCapture);

  return ret;
}


gboolean
gst_omx_camera_get_pause_after_capture (OMX_HANDLETYPE omx_handle, gboolean* sbool)
{
  OMX_CONFIG_BOOLEANTYPE oPausePreviewAfterStillCapture;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM (oPausePreviewAfterStillCapture);

  OMX_GetConfig(omx_handle, OMX_IndexAutoPauseAfterCapture, &oPausePreviewAfterStillCapture);
  *sbool = oPausePreviewAfterStillCapture.bEnabled;

  return ret;
}


gboolean
gst_omx_camera_set_capture (OMX_HANDLETYPE omx_handle, gboolean sbool)
{
  NVX_CONFIG_COMBINEDCAPTURE ocombcapture;
  OMX_INDEXTYPE index;
  GstNvCameraMode mode;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_COMBINEDCAPTURE, &index);
  G_OMX_INIT_PARAM (ocombcapture);
  gst_omx_camera_get_mode (omx_handle, &mode);
  if(mode == GST_NV_MODE_VIDEO)
    ocombcapture.nPortIndex = GST_OMX_CAMERA_VIDEO_PORT;
  else
    ocombcapture.nPortIndex = GST_OMX_CAMERA_STILL_PORT;

  if(sbool)
    ocombcapture.nImages = 1;
  else
    ocombcapture.nImages = 0;

  OMX_SetConfig(omx_handle, index, &ocombcapture);

  return ret;
}


gboolean
gst_omx_camera_get_capture (OMX_HANDLETYPE omx_handle, gboolean *sbool)
{
  OMX_CONFIG_BOOLEANTYPE cc;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM (cc);

  OMX_GetConfig (omx_handle, OMX_IndexConfigCapturing, &cc);
  *sbool = cc.bEnabled;

  return ret;
}


gboolean
gst_omx_camera_set_zoom (OMX_HANDLETYPE omx_handle, gfloat zoom)
{
  OMX_CONFIG_SCALEFACTORTYPE oScaleFactor;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM (oScaleFactor);

  OMX_GetConfig (omx_handle, OMX_IndexConfigCommonDigitalZoom, &oScaleFactor);

  oScaleFactor.xWidth =  NvSFxFloat2Fixed ((float)zoom);
  oScaleFactor.xHeight =  oScaleFactor.xWidth;

  OMX_SetConfig (omx_handle, OMX_IndexConfigCommonDigitalZoom, &oScaleFactor);

  return ret;
}


gboolean
gst_omx_camera_get_zoom (OMX_HANDLETYPE omx_handle, gfloat* zoom)
{
  OMX_CONFIG_SCALEFACTORTYPE oScaleFactor;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM (oScaleFactor);

  OMX_GetConfig (omx_handle, OMX_IndexConfigCommonDigitalZoom, &oScaleFactor);
  *zoom = NvSFxFixed2Float (oScaleFactor.xWidth);

  return ret;
}


gboolean
gst_omx_camera_set_flash_mode (OMX_HANDLETYPE omx_handle, GstFlashMode flash)
{
  OMX_IMAGE_PARAM_FLASHCONTROLTYPE oFlash;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM(oFlash);
  OMX_GetConfig(omx_handle, OMX_IndexConfigFlashControl, &oFlash);

  oFlash.eFlashControl = gst_omx_camera_flash_gst_to_omx (flash);
  OMX_SetConfig(omx_handle, OMX_IndexConfigFlashControl, &oFlash);

  return ret;
}


gboolean
gst_omx_camera_get_flash_mode (OMX_HANDLETYPE omx_handle, GstFlashMode *flash)
{
  OMX_IMAGE_PARAM_FLASHCONTROLTYPE oFlash;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM(oFlash);
  OMX_GetConfig(omx_handle, OMX_IndexConfigFlashControl, &oFlash);

  *flash = gst_omx_camera_flash_omx_to_gst (oFlash.eFlashControl);

  return ret;
}


gboolean
gst_omx_camera_set_white_balance_mode (OMX_HANDLETYPE omx_handle, GstWhiteBalanceMode wb)
{
  OMX_CONFIG_WHITEBALCONTROLTYPE  oWB;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM(oWB);
  OMX_GetConfig(omx_handle, OMX_IndexConfigCommonWhiteBalance, &oWB);

  oWB.eWhiteBalControl = gst_omx_camera_white_balance_gst_to_omx (wb);
  OMX_SetConfig(omx_handle, OMX_IndexConfigCommonWhiteBalance, &oWB);

  return ret;
}


gboolean
gst_omx_camera_get_white_balance_mode (OMX_HANDLETYPE omx_handle, GstWhiteBalanceMode* wb)
{
  OMX_CONFIG_WHITEBALCONTROLTYPE  oWB;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM (oWB);
  OMX_GetConfig(omx_handle, OMX_IndexConfigCommonWhiteBalance, &oWB);

  *wb = gst_omx_camera_white_balance_omx_to_gst (oWB.eWhiteBalControl);

  return ret;
}


gboolean
gst_omx_camera_set_iso_speed (OMX_HANDLETYPE omx_handle, guint iso)
{
  OMX_CONFIG_EXPOSUREVALUETYPE ExposureValue;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM (ExposureValue);

  OMX_GetConfig(omx_handle, OMX_IndexConfigCommonExposureValue, &ExposureValue);

  if (iso == 0) {
    ExposureValue.bAutoSensitivity = OMX_TRUE;
  } else {
    ExposureValue.bAutoSensitivity = OMX_FALSE;
    ExposureValue.nSensitivity = iso;
  }

  OMX_SetConfig(omx_handle, OMX_IndexConfigCommonExposureValue, &ExposureValue);

  return ret;
}


gboolean
gst_omx_camera_get_iso_speed (OMX_HANDLETYPE omx_handle, guint *iso)
{
  OMX_CONFIG_EXPOSUREVALUETYPE ExposureValue;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM(ExposureValue);
  OMX_GetConfig(omx_handle, OMX_IndexConfigCommonExposureValue, &ExposureValue);

  if (ExposureValue.bAutoSensitivity == OMX_TRUE)
    *iso = 0;
  else
    *iso = ExposureValue.nSensitivity;

  return ret;
}


gboolean
gst_omx_camera_set_exposure (OMX_HANDLETYPE omx_handle, guint32 exp)
{
  OMX_CONFIG_EXPOSUREVALUETYPE ExposureValue;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM (ExposureValue);

  OMX_GetConfig(omx_handle, OMX_IndexConfigCommonExposureValue, &ExposureValue);

  if (exp == 0) {
    ExposureValue.bAutoShutterSpeed = OMX_TRUE;
  } else {
    ExposureValue.bAutoShutterSpeed = OMX_FALSE;
    ExposureValue.nShutterSpeedMsec = exp;
  }

  OMX_SetConfig(omx_handle, OMX_IndexConfigCommonExposureValue, &ExposureValue);

  return ret;
}


gboolean
gst_omx_camera_get_exposure (OMX_HANDLETYPE omx_handle, guint32 *exp)
{
  OMX_CONFIG_EXPOSUREVALUETYPE ExposureValue;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM(ExposureValue);
  OMX_GetConfig(omx_handle, OMX_IndexConfigCommonExposureValue, &ExposureValue);

  if (ExposureValue.bAutoShutterSpeed == OMX_TRUE)
    *exp = 0;
  else
    *exp = ExposureValue.nShutterSpeedMsec;

  return ret;
}

gboolean
gst_omx_camera_set_auto_framerate(OMX_HANDLETYPE omx_handle, guint32 frate)
{
  NVX_CONFIG_AUTOFRAMERATE  oAutoFrameRate;
  OMX_INDEXTYPE index;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_AUTOFRAMERATE, &index);

  G_OMX_INIT_PARAM(oAutoFrameRate);
  OMX_GetConfig(omx_handle, index, &oAutoFrameRate);
  oAutoFrameRate.bEnabled = TRUE;
  oAutoFrameRate.low = NV_WHOLE_TO_FX(frate);
  oAutoFrameRate.high = NV_WHOLE_TO_FX(frate);

  OMX_SetConfig(omx_handle, index, &oAutoFrameRate);

  return ret;

}

gboolean
gst_omx_camera_get_auto_framerate(OMX_HANDLETYPE omx_handle, guint32* frate )
{
  NVX_CONFIG_AUTOFRAMERATE  oAutoFrameRate;
  OMX_INDEXTYPE index;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_AUTOFRAMERATE, &index);

  G_OMX_INIT_PARAM(oAutoFrameRate);
  OMX_GetConfig(omx_handle, index, &oAutoFrameRate);
  *frate = NV_FX_TO_WHOLE(oAutoFrameRate.low);

  return ret;
}

gboolean
gst_omx_camera_set_metering_mode (OMX_HANDLETYPE omx_handle, GstNvMeteringMode mt)
{
  OMX_CONFIG_EXPOSUREVALUETYPE ExposureValue;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM(ExposureValue);
  OMX_GetConfig(omx_handle, OMX_IndexConfigCommonExposureValue, &ExposureValue);

  ExposureValue.eMetering = gst_omx_camera_metering_gst_to_omx (mt);
  OMX_SetConfig(omx_handle, OMX_IndexConfigCommonExposureValue, &ExposureValue);

  return ret;
}



gboolean
gst_omx_camera_get_metering_mode (OMX_HANDLETYPE omx_handle, GstNvMeteringMode* mt)
{
  OMX_CONFIG_EXPOSUREVALUETYPE ExposureValue;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM (ExposureValue);
  OMX_GetConfig(omx_handle, OMX_IndexConfigCommonExposureValue, &ExposureValue);

  *mt = gst_omx_camera_metering_omx_to_gst (ExposureValue.eMetering);

  return ret;
}



gboolean
gst_omx_camera_set_color_tone (OMX_HANDLETYPE omx_handle, GstColourToneMode ct)
{
  OMX_CONFIG_IMAGEFILTERTYPE oImageFilter;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM(oImageFilter);
  OMX_GetConfig(omx_handle, OMX_IndexConfigCommonImageFilter, &oImageFilter);

  oImageFilter.eImageFilter = gst_omx_camera_color_tone_gst_to_omx (ct);
  OMX_SetConfig(omx_handle, OMX_IndexConfigCommonImageFilter, &oImageFilter);

  return ret;
}



gboolean
gst_omx_camera_get_color_tone (OMX_HANDLETYPE omx_handle, GstColourToneMode *ct)
{
  OMX_CONFIG_IMAGEFILTERTYPE oImageFilter;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM(oImageFilter);
  OMX_GetConfig(omx_handle, OMX_IndexConfigCommonImageFilter, &oImageFilter);

  *ct = gst_omx_camera_color_tone_omx_to_gst (oImageFilter.eImageFilter);

  return ret;
}


gboolean
gst_omx_camera_set_brightness (OMX_HANDLETYPE omx_handle, gint brightness)
{
  OMX_CONFIG_BRIGHTNESSTYPE  oBrightness;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM(oBrightness);
  OMX_GetConfig(omx_handle, OMX_IndexConfigCommonBrightness, &oBrightness);

  oBrightness.nBrightness = brightness;
  OMX_SetConfig(omx_handle, OMX_IndexConfigCommonBrightness, &oBrightness);

  return ret;
}


gboolean
gst_omx_camera_get_brightness (OMX_HANDLETYPE omx_handle, gint* brightness)
{
  OMX_CONFIG_BRIGHTNESSTYPE  oBrightness;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM(oBrightness);
  OMX_GetConfig(omx_handle, OMX_IndexConfigCommonBrightness, &oBrightness);

  *brightness = oBrightness.nBrightness;

  return ret;
}


gboolean
gst_omx_camera_set_contrast (OMX_HANDLETYPE omx_handle, gint contrast)
{
  OMX_CONFIG_CONTRASTTYPE  oContrast;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM(oContrast);
  OMX_GetConfig(omx_handle, OMX_IndexConfigCommonContrast, &oContrast);

  oContrast.nContrast = contrast;
  OMX_SetConfig(omx_handle, OMX_IndexConfigCommonContrast, &oContrast);

  return ret;
}


gboolean
gst_omx_camera_get_contrast (OMX_HANDLETYPE omx_handle, gint* contrast)
{
  OMX_CONFIG_CONTRASTTYPE  oContrast;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM(oContrast);
  OMX_GetConfig(omx_handle, OMX_IndexConfigCommonContrast, &oContrast);

  *contrast = oContrast.nContrast;

  return ret;
}


gboolean
gst_omx_camera_set_saturation (OMX_HANDLETYPE omx_handle, gint saturation)
{
  OMX_CONFIG_SATURATIONTYPE  oSaturation;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM(oSaturation);
  OMX_GetConfig(omx_handle, OMX_IndexConfigCommonSaturation, &oSaturation);

  oSaturation.nSaturation = saturation;
  OMX_SetConfig(omx_handle, OMX_IndexConfigCommonSaturation, &oSaturation);

  return ret;
}


gboolean
gst_omx_camera_get_saturation (OMX_HANDLETYPE omx_handle, gint *saturation)
{
  OMX_CONFIG_SATURATIONTYPE  oSaturation;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM(oSaturation);
  OMX_GetConfig(omx_handle, OMX_IndexConfigCommonSaturation, &oSaturation);

  *saturation = oSaturation.nSaturation;

  return ret;
}


gboolean
gst_omx_camera_set_ev_compensation (OMX_HANDLETYPE omx_handle, gfloat evCompensation)
{
  OMX_CONFIG_EXPOSUREVALUETYPE ExposureValue;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM(ExposureValue);

  OMX_GetConfig(omx_handle, OMX_IndexConfigCommonExposureValue, &ExposureValue);
  ExposureValue.xEVCompensation = NvSFxFloat2Fixed (evCompensation);
  OMX_SetConfig(omx_handle, OMX_IndexConfigCommonExposureValue, &ExposureValue);

  return ret;
}


gboolean
gst_omx_camera_get_ev_compensation (OMX_HANDLETYPE omx_handle, gfloat *evCompensation)
{
  OMX_CONFIG_EXPOSUREVALUETYPE ExposureValue;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM(ExposureValue);
  OMX_GetConfig(omx_handle, OMX_IndexConfigCommonExposureValue, &ExposureValue);

  *evCompensation = NvSFxFixed2Float (ExposureValue.xEVCompensation);

  return ret;
}


gboolean
gst_omx_camera_set_rotation (OMX_HANDLETYPE omx_handle, guint port, GstNvRotation rotate)
{
  OMX_CONFIG_ROTATIONTYPE  oRotation;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM(oRotation);
  oRotation.nPortIndex = port;
  OMX_GetConfig(omx_handle, OMX_IndexConfigCommonRotate, &oRotation);

  oRotation.nRotation = rotate;
  OMX_SetConfig(omx_handle, OMX_IndexConfigCommonRotate, &oRotation);

  return ret;
}


gboolean
gst_omx_camera_get_rotation (OMX_HANDLETYPE omx_handle, guint port, GstNvRotation* rotate)
{
  OMX_CONFIG_ROTATIONTYPE  oRotation;
  gboolean ret = TRUE;

  G_OMX_INIT_PARAM(oRotation);
  oRotation.nPortIndex = port;
  OMX_GetConfig(omx_handle, OMX_IndexConfigCommonRotate, &oRotation);

  *rotate = oRotation.nRotation;

  return ret;
}




/* nv specific */

gboolean
gst_omx_camera_set_continuous_autofocus (OMX_HANDLETYPE omx_handle, gboolean sbool)
{
  OMX_CONFIG_BOOLEANTYPE obool;
  OMX_INDEXTYPE index;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex(omx_handle, NVX_INDEX_CONFIG_CONTINUOUSAUTOFOCUS,
      &index);

  G_OMX_INIT_PARAM (obool);

  OMX_GetConfig (omx_handle, index, &obool);
  obool.bEnabled = sbool;
  OMX_SetConfig (omx_handle, index, &obool);

  return ret;
}


gboolean
gst_omx_camera_get_continuous_autofocus (OMX_HANDLETYPE omx_handle, gboolean* sbool)
{
  OMX_CONFIG_BOOLEANTYPE obool;
  OMX_INDEXTYPE index;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex(omx_handle, NVX_INDEX_CONFIG_CONTINUOUSAUTOFOCUS,
      &index);

  G_OMX_INIT_PARAM (obool);

  OMX_GetConfig (omx_handle, index, &obool);
  *sbool = obool.bEnabled;

  return ret;
}


gboolean
gst_omx_camera_set_sensor (OMX_HANDLETYPE omx_handle, GstNvSensor sensor)
{
  OMX_PARAM_U32TYPE oSensorIdParam;
  OMX_INDEXTYPE index;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex(omx_handle, NVX_INDEX_PARAM_SENSORID, &index);

  G_OMX_INIT_PARAM (oSensorIdParam);

  OMX_GetParameter(omx_handle, index, &oSensorIdParam);
  oSensorIdParam.nU32 = sensor;
  OMX_SetParameter(omx_handle, index, &oSensorIdParam);

  return ret;
}


gboolean
gst_omx_camera_get_sensor (OMX_HANDLETYPE omx_handle, GstNvSensor* sensor)
{
  OMX_PARAM_U32TYPE oSensorIdParam;
  OMX_INDEXTYPE index;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex(omx_handle, NVX_INDEX_PARAM_SENSORID, &index);

  G_OMX_INIT_PARAM (oSensorIdParam);

  OMX_GetParameter(omx_handle, index, &oSensorIdParam);
  *sensor = oSensorIdParam.nU32;

  return ret;
}


gboolean
gst_omx_camera_set_viewfinder (OMX_HANDLETYPE omx_handle, gboolean sbool)
{
  OMX_CONFIG_BOOLEANTYPE cc;
  OMX_INDEXTYPE index;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_PREVIEWENABLE, &index);

  G_OMX_INIT_PARAM (cc);

  OMX_GetConfig (omx_handle, index, &cc);
  cc.bEnabled = sbool;
  OMX_SetConfig (omx_handle, index, &cc);

  return ret;
}


gboolean
gst_omx_camera_get_viewfinder (OMX_HANDLETYPE omx_handle, gboolean* sbool)
{
  OMX_CONFIG_BOOLEANTYPE cc;
  OMX_INDEXTYPE index;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_PREVIEWENABLE, &index);

  G_OMX_INIT_PARAM (cc);

  OMX_GetConfig (omx_handle, index, &cc);
  *sbool = cc.bEnabled;

  return ret;
}


gboolean
gst_omx_camera_set_flicker_reduction_mode (OMX_HANDLETYPE omx_handle, GstFlickerReductionMode fr)
{
  OMX_INDEXTYPE index;
  NVX_CONFIG_FLICKER oFlicker;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_FLICKER, &index);

  G_OMX_INIT_PARAM(oFlicker);
  OMX_GetConfig(omx_handle, index, &oFlicker);

  oFlicker.eFlicker = gst_omx_camera_flicker_reduction_gst_to_omx (fr);
  OMX_SetConfig(omx_handle, index, &oFlicker);

  return ret;
}


gboolean
gst_omx_camera_get_flicker_reduction_mode (OMX_HANDLETYPE omx_handle, GstFlickerReductionMode *fr)
{
  OMX_INDEXTYPE index;
  NVX_CONFIG_FLICKER oFlicker;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_FLICKER, &index);

  G_OMX_INIT_PARAM(oFlicker);
  OMX_GetConfig(omx_handle, index, &oFlicker);

  *fr = gst_omx_camera_flicker_reduction_omx_to_gst (oFlicker.eFlicker);

  return ret;
}


gboolean
gst_omx_camera_set_hue (OMX_HANDLETYPE omx_handle, gint hue)
{
  OMX_INDEXTYPE index;
  NVX_CONFIG_HUETYPE  oHue;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_HUE, &index);

  G_OMX_INIT_PARAM(oHue);
  OMX_GetConfig(omx_handle, index, &oHue);

  oHue.nHue = hue;
  OMX_SetConfig(omx_handle, index, &oHue);

  return ret;
}


gboolean
gst_omx_camera_get_hue (OMX_HANDLETYPE omx_handle, gint *hue)
{
  OMX_INDEXTYPE index;
  NVX_CONFIG_HUETYPE  oHue;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_HUE, &index);

  G_OMX_INIT_PARAM(oHue);
  OMX_GetConfig (omx_handle, index, &oHue);

  *hue = oHue.nHue;

  return ret;
}


gboolean
gst_omx_camera_set_enable_edge_enhancement (OMX_HANDLETYPE omx_handle, gboolean sbool)
{
  OMX_INDEXTYPE index;
  NVX_CONFIG_EDGEENHANCEMENT oEdgeEnhancement;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_EDGEENHANCEMENT, &index);

  G_OMX_INIT_PARAM(oEdgeEnhancement);
  OMX_GetConfig(omx_handle, index, &oEdgeEnhancement);

  oEdgeEnhancement.bEnable = sbool;

  OMX_SetConfig(omx_handle, index, &oEdgeEnhancement);

  return ret;
}


gboolean
gst_omx_camera_get_enable_edge_enhancement (OMX_HANDLETYPE omx_handle, gboolean* sbool)
{
  OMX_INDEXTYPE index;
  NVX_CONFIG_EDGEENHANCEMENT oEdgeEnhancement;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_EDGEENHANCEMENT, &index);

  G_OMX_INIT_PARAM(oEdgeEnhancement);
  OMX_GetConfig(omx_handle, index, &oEdgeEnhancement);

  *sbool = oEdgeEnhancement.bEnable;

  return ret;
}


gboolean
gst_omx_camera_set_edge_enhancement (OMX_HANDLETYPE omx_handle, gfloat edgeEnhancement)
{
  OMX_INDEXTYPE index;
  NVX_CONFIG_EDGEENHANCEMENT oEdgeEnhancement;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_EDGEENHANCEMENT, &index);

  G_OMX_INIT_PARAM(oEdgeEnhancement);
  OMX_GetConfig(omx_handle, index, &oEdgeEnhancement);

  oEdgeEnhancement.strengthBias = edgeEnhancement;

  OMX_SetConfig(omx_handle, index, &oEdgeEnhancement);

  return ret;
}


gboolean
gst_omx_camera_get_edge_enhancement (OMX_HANDLETYPE omx_handle, gfloat *edgeEnhancement)
{
  OMX_INDEXTYPE index;
  NVX_CONFIG_EDGEENHANCEMENT oEdgeEnhancement;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_EDGEENHANCEMENT, &index);

  OMX_GetConfig(omx_handle, index, &oEdgeEnhancement);

  *edgeEnhancement = oEdgeEnhancement.strengthBias;

  return ret;
}


gboolean
gst_omx_camera_set_capture_paused (OMX_HANDLETYPE omx_handle, gboolean sbool)
{
  OMX_CONFIG_BOOLEANTYPE cc;
  OMX_INDEXTYPE index;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_CAPTUREPAUSE, &index);

  G_OMX_INIT_PARAM (cc);

  OMX_GetConfig (omx_handle, index, &cc);
  cc.bEnabled = sbool;
  OMX_SetConfig (omx_handle, index, &cc);

  return ret;
}


gboolean
gst_omx_camera_get_capture_paused (OMX_HANDLETYPE omx_handle, gboolean* sbool)
{
  OMX_CONFIG_BOOLEANTYPE cc;
  OMX_INDEXTYPE index;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_CAPTUREPAUSE, &index);

  G_OMX_INIT_PARAM (cc);

  OMX_GetConfig (omx_handle, index, &cc);
  *sbool = cc.bEnabled;

  return ret;
}


gboolean
gst_omx_camera_get_flash_parameters (OMX_HANDLETYPE omx_handle, gboolean* FlashSupport)
{
  OMX_INDEXTYPE index;
  gboolean ret = TRUE;
  OMX_ERRORTYPE omxErr = OMX_ErrorNone;
  NVX_CONFIG_FLASH_PARAMETERS flParams;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_FLASHPARAMETERS, &index);

  G_OMX_INIT_PARAM (flParams);

  omxErr = OMX_GetConfig (omx_handle, index, &flParams);
  if (omxErr != OMX_ErrorNone)
  {
    g_print("Failed to get Flash parameters\n");
    return FALSE;
  }

  *FlashSupport = flParams.bPresent;

  return ret;
}


gboolean
gst_omx_camera_get_focuser_parameters (OMX_HANDLETYPE omx_handle, glong* minpos, glong* maxpos,
                    glong* macropos, gboolean* infAF, gboolean* macroAF, gboolean* FocusSupport)
{
  OMX_INDEXTYPE index;
  gboolean ret = TRUE;
  OMX_ERRORTYPE omxErr = OMX_ErrorNone;
  NVX_CONFIG_FOCUSER_PARAMETERS fParams;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_FOCUSERPARAMETERS, &index);

  G_OMX_INIT_PARAM (fParams);

  omxErr = OMX_GetConfig (omx_handle, index, &fParams);
  if (omxErr != OMX_ErrorNone)
  {
    g_print("Failed to get Focuser parameters\n");
    return FALSE;
  }

  if (fParams.minPosition == fParams.maxPosition && !(fParams.sensorispAFsupport))
  {
    // Focus is not supported by this sensor
    g_print("Focus is not supported by this sensor\n");
    *FocusSupport = OMX_FALSE;
    return FALSE;
  }
  else
  {
    // Focus is supported by this sensor
    *FocusSupport = OMX_TRUE;
    *minpos = fParams.minPosition;
    *maxpos = fParams.maxPosition;
    *macropos = fParams.macro;
    *infAF = fParams.infModeAF;
    *macroAF = fParams.macroModeAF;
  }

  return ret;
}


gboolean
gst_omx_camera_set_focus_pos (OMX_HANDLETYPE omx_handle, glong focus_pos)
{
  gboolean ret = TRUE;
  OMX_IMAGE_CONFIG_FOCUSCONTROLTYPE fControl;

  G_OMX_INIT_PARAM (fControl);

  OMX_GetConfig (omx_handle, OMX_IndexConfigFocusControl, &fControl);

  fControl.eFocusControl = OMX_IMAGE_FocusControlOn;
  fControl.nFocusStepIndex = focus_pos;

  OMX_SetConfig (omx_handle, OMX_IndexConfigFocusControl, &fControl);

  return ret;
}


gboolean
gst_omx_camera_queryCAFState (OMX_HANDLETYPE omx_handle, gboolean* converged)
{
  OMX_INDEXTYPE index;
  gboolean ret = TRUE;
  OMX_ERRORTYPE omxErr = OMX_ErrorNone;
  NVX_CONFIG_CONTINUOUSAUTOFOCUS_STATE cafState;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_CONTINUOUSAUTOFOCUS_STATE, &index);

  G_OMX_INIT_PARAM (cafState);

  omxErr = OMX_GetConfig (omx_handle, index, &cafState);
  if (omxErr != OMX_ErrorNone)
  {
    g_print("Failed to quary CAF state\n");
    return FALSE;
  }

  *converged = cafState.bConverged;

  return ret;
}


gboolean
gst_omx_camera_programCAFLock (OMX_HANDLETYPE omx_handle, gboolean sbool)
{
  OMX_INDEXTYPE index;
  gboolean ret = TRUE;
  OMX_CONFIG_BOOLEANTYPE oBL;
  OMX_ERRORTYPE omxErr = OMX_ErrorNone;

  OMX_GetExtensionIndex (omx_handle, NVX_INDEX_CONFIG_CONTINUOUSAUTOFOCUS_PAUSE, &index);

  G_OMX_INIT_PARAM (oBL);

  oBL.bEnabled = sbool;
  omxErr = OMX_SetConfig (omx_handle, index, &oBL);
  if (omxErr != OMX_ErrorNone)
  {
    return FALSE;
  }

  return ret;
}


gboolean
gst_omx_camera_set_auto_focus (OMX_HANDLETYPE omx_handle, gboolean sbool,
    gboolean af, gboolean awb, gboolean ae, gint timeout, AlgSubType algSubType)
{
  NVX_CONFIG_CONVERGEANDLOCK oConvergeAndLock;
  OMX_INDEXTYPE eIndexConvergeAndLock;
  gboolean ret = TRUE;

  OMX_GetExtensionIndex(omx_handle, NVX_INDEX_CONFIG_CONVERGEANDLOCK,
      &eIndexConvergeAndLock);

  G_OMX_INIT_PARAM (oConvergeAndLock);

  OMX_GetConfig(omx_handle, eIndexConvergeAndLock, &oConvergeAndLock);

  oConvergeAndLock.bUnlock = !sbool;

  if (oConvergeAndLock.bUnlock == OMX_FALSE) {
    oConvergeAndLock.bAutoFocus = (af) ? OMX_TRUE : OMX_FALSE;
    oConvergeAndLock.bAutoExposure = (ae) ? OMX_TRUE : OMX_FALSE;
    oConvergeAndLock.bAutoWhiteBalance = (awb) ? OMX_TRUE : OMX_FALSE;
    oConvergeAndLock.nTimeOutMS = timeout;
    oConvergeAndLock.algSubType = algSubType;
  }

  OMX_SetConfig (omx_handle, eIndexConvergeAndLock, &oConvergeAndLock);

  return ret;
}

