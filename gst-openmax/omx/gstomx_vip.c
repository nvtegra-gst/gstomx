/*
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * Based on code copyright/by:
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
#include<string.h>//for memset
#include "gstomx_vip.h"
#include "gstomx_base_src.h"
#include "gstomx.h"
#include "OMX_Index.h"
#include "OMX_Image.h"
#include "config.h"
#include "gstomx_nvutils.h"

#define VIP_PORT 0

#define GST_TYPE_OMX_VIP_FORMAT_TYPE (gst_omx_vip_format_get_type())
#define GST_TYPE_OMX_VIP_FILTER_TYPE (gst_omx_vip_filter_get_type())
#define GST_TYPE_OMX_VIP_FRAME_TYPE  (gst_omx_vip_frame_get_type())

static GType gst_omx_vip_format_get_type (void);
static GType gst_omx_vip_filter_get_type (void);
static GType gst_omx_vip_frame_get_type (void);


enum
{
    ARG_0,
    ARG_FORMAT_TYPE,
    ARG_DEINTERLACE_TYPE,
    ARG_FRAME_TYPE
};

typedef enum
{
    FORMAT_TYPE_NTSC        = 0,
    FORMAT_TYPE_PAL          = 1
} eFormatType;

static GstBaseSrcClass *parent_class;
static eFormatType formattype = FORMAT_TYPE_NTSC;
static eDeinterlaceType deinterlacetype = DEINTERLACE_TYPE_BOB;
static eFrameType       frametype = FRAME_TYPE_INTERLACED;

static GstCaps *
generate_src_template (void)
{
    GstCaps *caps;
    GstStructure *struc;
    GstStructure *struc1;
    GstStructure *struc2;

    caps = gst_caps_new_empty ();

    struc = gst_structure_new ("video/x-nv-yuv",
                               "width", GST_TYPE_INT_RANGE, 16, 4096,
                               "height", GST_TYPE_INT_RANGE, 16, 4096,
                               "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1,
                               NULL);
    struc1 = gst_structure_new ("video/x-raw-gl",
                               "width", GST_TYPE_INT_RANGE, 16, 4096,
                               "height", GST_TYPE_INT_RANGE, 16, 4096,
                               "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1,
                               NULL);
    struc2 = gst_structure_new ("video/x-nvrm-yuv",
                               "width", GST_TYPE_INT_RANGE, 16, 4096,
                               "height", GST_TYPE_INT_RANGE, 16, 4096,
                               "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1,
                               NULL);

    {
        GValue list;
        GValue val;

        list.g_type = val.g_type = 0;

        g_value_init (&list, GST_TYPE_LIST);
        g_value_init (&val, GST_TYPE_FOURCC);

        gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('I', '4', '2', '2'));
        gst_value_list_append_value (&list, &val);
        gst_structure_set_value (struc, "format", &list);

        gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('R', 'G', 'B', 'A'));
        gst_value_list_append_value (&list, &val);
        gst_structure_set_value (struc1, "format", &list);

        gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('I', '4', '2', '0'));
        gst_value_list_append_value (&list, &val);
        gst_structure_set_value (struc2, "format", &list);

        g_value_unset (&val);
        g_value_unset (&list);
    }

    gst_caps_append_structure (caps, struc);
    gst_caps_append_structure (caps, struc1);
    gst_caps_append_structure (caps, struc2);
    return caps;
}

static void
type_base_init (gpointer g_class)
{
    GstElementClass *element_class;

    element_class = GST_ELEMENT_CLASS (g_class);

    gst_element_class_set_details_simple (element_class,
                                   "OpenMAX IL vip src element",
                                   "None",
                                   "Does nothing",
                                   "Felipe Contreras");

    {
        GstPadTemplate *template;

        template = gst_pad_template_new ("src", GST_PAD_SRC,
                                         GST_PAD_ALWAYS,
                                         generate_src_template ());

        gst_element_class_add_pad_template (element_class, template);
    }
}

static gboolean
setcaps (GstBaseSrc *gst_src,
         GstCaps *caps)
{
//  GstOmxBaseSrc *self;
  guint32 format = 0;

//  self = GST_OMX_BASE_SRC (gst_src);
  g_return_val_if_fail (gst_caps_get_size (caps) == 1, FALSE);

  {
    GstStructure *structure;

    structure = gst_caps_get_structure (caps, 0);

    if (!structure)
    {
      format = GST_MAKE_FOURCC ('I', '4', '2', '2');
      structure = gst_structure_new ("video/x-nv-yuv",
          "width", GST_TYPE_INT_RANGE, 16, 4096,
          "height", GST_TYPE_INT_RANGE, 16, 4096,
          "format", GST_TYPE_FOURCC, format,
          "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1,
          NULL);

      gst_caps_append_structure (caps, structure);
    }

    if (strcmp (gst_structure_get_name (structure), "video/x-nv-yuv") == 0 ||
        strcmp (gst_structure_get_name (structure), "video/x-raw-gl") == 0||
        strcmp (gst_structure_get_name (structure), "video/x-nvrm-yuv") == 0)
    {
      if (formattype == FORMAT_TYPE_NTSC)
      {
        gst_structure_set(structure, "width", G_TYPE_INT, 720, NULL);
        gst_structure_set(structure, "height", G_TYPE_INT, 480, NULL);
        gst_structure_set(structure, "framerate", GST_TYPE_FRACTION, 30, 1, NULL);
      }
      else
      {
        gst_structure_set(structure, "width", G_TYPE_INT, 720, NULL);
        gst_structure_set(structure, "height", G_TYPE_INT, 576, NULL);
        gst_structure_set(structure, "framerate", GST_TYPE_FRACTION, 25, 1, NULL);
      }
      gst_pad_set_caps(gst_src->srcpad, caps);
      return TRUE;
    }
  }

  return FALSE;
}

static void
settings_changed_cb (GOmxCore *core)
{
/*    GstOmxBaseSrc *omx_base;

    omx_base = core->object;
*/

    /** @todo properly set the capabilities */
}

static void
setup_ports (GstOmxBaseSrc *base_src)
{
    GOmxCore *core;
    OMX_PARAM_PORTDEFINITIONTYPE param;
    GstCaps *peercaps = NULL;
    GstStructure *peerstruct = NULL;
       GstPad *pad = (GST_BASE_SRC(base_src))->srcpad;

    core = base_src->gomx;

    memset (&param, 0, sizeof (param));
    param.nSize = sizeof (OMX_PARAM_PORTDEFINITIONTYPE);
    param.nVersion.s.nVersionMajor = 1;
    param.nVersion.s.nVersionMinor = 1;
    param.nPortIndex = VIP_PORT;

    OMX_GetParameter(core->omx_handle, OMX_IndexParamPortDefinition, &param);

    base_src->out_port =  g_omx_core_setup_port (core, &param);

    if (formattype == FORMAT_TYPE_NTSC)
    {
        param.format.video.nFrameWidth  = 720;
        param.format.video.nFrameHeight = 480;
    }
    else
    {
        param.format.video.nFrameWidth  = 720;
        param.format.video.nFrameHeight = 576;
    }
    param.format.video.eColorFormat = OMX_COLOR_FormatYUV420Planar;

    if (frametype == FRAME_TYPE_PROGRESSIVE)
    {
        deinterlacetype = DEINTERLACE_TYPE_NONE;
    }
#ifdef TEGRA
    gstomx_use_deinterlace_extension(core->omx_handle, deinterlacetype);
#endif
    if (gst_pad_is_linked(pad)) {
        peercaps = gst_pad_peer_get_caps (pad);
        if (peercaps && peercaps->structs->len > 0) {
            peerstruct = gst_caps_get_structure(peercaps, 0);
            if (!strcmp(gst_structure_get_name(peerstruct), "video/x-raw-gl")) {
#ifdef USE_EGLIMAGE
                GstQuery *query;
                GstStructure * querystruct;
                querystruct = gst_structure_new("query_struct","display_data",G_TYPE_POINTER,NULL,NULL);
                query = gst_query_new_application(GST_QUERY_CUSTOM, querystruct);
                gst_pad_peer_query(pad, query);
                gst_structure_get(querystruct, "display_data", G_TYPE_POINTER ,&base_src->gomx->display_data, NULL);
                base_src->out_port->buffer_type = BUFFER_TYPE_EGLIMAGE;
                gst_query_unref(query);
                param.format.video.eColorFormat = OMX_COLOR_Format32bitARGB8888;
#else
        g_error("EGL Image Display Info could not be found \n");
#endif
           } else if (!strcmp(gst_structure_get_name(peerstruct), "video/x-nv-yuv")) {
#ifdef TEGRA
                base_src->out_port->buffer_type = BUFFER_TYPE_NVBUFFER;
#endif
          } else if(!strcmp(gst_structure_get_name(peerstruct), "video/x-nvrm-yuv")){
#ifdef TEGRA
                base_src->out_port->buffer_type = BUFFER_TYPE_NVRMBUFFER;
                gstomx_use_nvbuffer_extension(core->omx_handle, GOMX_PORT_INPUT);
                gstomx_use_nvrmsurf_extension(core->omx_handle);
#endif
          } else {
             g_error("Undefined caps \n");
          }
        }
    } else {
      base_src->out_port->buffer_type = BUFFER_TYPE_NVBUFFER;
    }

    OMX_SetParameter(core->omx_handle, OMX_IndexParamPortDefinition, &param);

    if (base_src->out_port->buffer_type == BUFFER_TYPE_NVBUFFER)
    {
#ifdef TEGRA
    gstomx_use_nvbuffer_extension(core->omx_handle, GOMX_PORT_INPUT);
#endif
    }
}

static void
set_property (GObject *obj,
              guint prop_id,
              const GValue *value,
              GParamSpec *pspec)
{
    switch (prop_id)
    {
        case ARG_FORMAT_TYPE:
        {
            formattype = g_value_get_enum(value);
        }
        break;
        case ARG_FRAME_TYPE:
        {
            frametype = g_value_get_enum(value);
        }
        break;
        case ARG_DEINTERLACE_TYPE:
        {
            deinterlacetype = g_value_get_enum(value);
        }
        break;
        default:
        break;
    }
}

static void
get_property (GObject *obj,
              guint prop_id,
              GValue *value,
              GParamSpec *pspec)
{
    switch (prop_id)
    {
        case ARG_FORMAT_TYPE:
        {
            g_value_set_enum(value, formattype);
        }
        break;
        case ARG_FRAME_TYPE:
        {
            g_value_set_enum(value, frametype);
        }
        break;
        case ARG_DEINTERLACE_TYPE:
        {
            g_value_set_enum(value, deinterlacetype);
        }
        break;
        default:
        break;
    }
}

static GstFlowReturn
create (GstBaseSrc *gst_base,
        guint64 offset,
        guint length,
        GstBuffer **ret_buf)
{
    GOmxCore *core;
    GstOmxBaseSrc *omx_base_src;
    GstFlowReturn ret = GST_FLOW_OK;
#ifdef USE_EGLIMAGE
    guint i;
#endif

    omx_base_src = GST_OMX_BASE_SRC (gst_base);

    core = omx_base_src->gomx;

    if (core->omx_state == OMX_StateLoaded)
    {
        setup_ports (omx_base_src);
        g_omx_core_prepare (core);

        if (omx_base_src->out_port->buffer_type == BUFFER_TYPE_EGLIMAGE)
        {
    #ifdef USE_EGLIMAGE
            for (i = 0; i < omx_base_src->out_port->num_buffers; i++)
            {
                omx_base_src->out_port->eglbuffers[i]->bimageflip = TRUE;
            }
    #endif
        }
    }

    if (core->omx_state == OMX_StateIdle)
    {
        g_omx_core_start (core);
    }

    ret = GST_BASE_SRC_CLASS (parent_class)->create(gst_base, offset, length, ret_buf);

    return ret;
}

static void
type_class_init (gpointer g_class,
                 gpointer class_data)
{
    GstBaseSrcClass *gst_base_src_class;
    GObjectClass *gobject_class;

    parent_class = g_type_class_ref (GST_OMX_BASE_SRC_TYPE);
    gst_base_src_class = GST_BASE_SRC_CLASS (g_class);
    gobject_class = G_OBJECT_CLASS (g_class);

    gst_base_src_class->set_caps = setcaps;
    gst_base_src_class->create = create;

    /* Properties stuff */
    {
        gobject_class->set_property = set_property;
        gobject_class->get_property = get_property;

        g_object_class_install_property (gobject_class, ARG_FORMAT_TYPE,
                                         g_param_spec_enum ("format", "vip capture format",
                                                            "captures in ntsc or pal mode",
                                                            GST_TYPE_OMX_VIP_FORMAT_TYPE, FORMAT_TYPE_NTSC, G_PARAM_READWRITE));

        g_object_class_install_property (gobject_class, ARG_FRAME_TYPE,
                                         g_param_spec_enum ("frame", "vip frame type",
                                                            "captures in progressive or interlaced frame",
                                                            GST_TYPE_OMX_VIP_FRAME_TYPE, FRAME_TYPE_INTERLACED, G_PARAM_READWRITE));

        g_object_class_install_property (gobject_class, ARG_DEINTERLACE_TYPE,
                                         g_param_spec_enum ("deint", "deinterlace mode",
                                                            "performs interlacing in BOB or Advanced1 Mode",
                                                            GST_TYPE_OMX_VIP_FILTER_TYPE, DEINTERLACE_TYPE_BOB, G_PARAM_READWRITE));
    }
}

static void
type_instance_init (GTypeInstance *instance,
                    gpointer g_class)
{
    GstOmxBaseSrc *omx_base;
//    GstOmxVip  *gstvip;


//    gstvip = GST_OMX_VIP(instance);

    omx_base = GST_OMX_BASE_SRC (instance);

    GST_DEBUG_OBJECT (omx_base, "begin");

    omx_base->setup_ports = setup_ports;

    omx_base->gomx->settings_changed_cb = settings_changed_cb;

    GST_DEBUG_OBJECT (omx_base, "end");
}

GType
gst_omx_vip_get_type (void)
{
    static GType type = 0;

    if (G_UNLIKELY (type == 0))
    {
        GTypeInfo *type_info;

        type_info = g_new0 (GTypeInfo, 1);
        type_info->class_size = sizeof (GstOmxVipClass);
        type_info->base_init = type_base_init;
        type_info->class_init = type_class_init;
        type_info->instance_size = sizeof (GstOmxVip);
        type_info->instance_init = type_instance_init;

        type = g_type_register_static (GST_OMX_BASE_SRC_TYPE, "GstOmxVip", type_info, 0);

        g_free (type_info);
    }

    return type;
}

static GType
gst_omx_vip_format_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static GEnumValue vals[] =
        {
            {FORMAT_TYPE_NTSC,        "NTSC FORMAT",       "Capures in NTSC format"},
            {FORMAT_TYPE_PAL,         "PAL FORMAT",        "Captures in PAL format"},
            {0, NULL, NULL},
        };

        type = g_enum_register_static ("GstOmxVipformattype", vals);
    }

    return type;
}

static GType
gst_omx_vip_filter_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static GEnumValue vals[] =
        {
            {DEINTERLACE_TYPE_BOB,        "Bob filter",             "Deinterlace with Bob Filter"},
            {DEINTERLACE_TYPE_ADVANCED1,   "Advanced1 filter",        "Deinterlace with Advanced1 filter"},
            {0, NULL, NULL},
        };

        type = g_enum_register_static ("GstOmxVipFilterMode", vals);
    }

    return type;
}

static GType
gst_omx_vip_frame_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static GEnumValue vals[] =
        {
            {FRAME_TYPE_PROGRESSIVE,  "Progressive Frame",  "Captures Progressive Frame"},
            {FRAME_TYPE_INTERLACED,   "Interlaced Frame",   "Captures Interlaced Frame"},
            {0, NULL, NULL},
        };

        type = g_enum_register_static ("GstOmxVipFrameType", vals);
    }

    return type;
}
