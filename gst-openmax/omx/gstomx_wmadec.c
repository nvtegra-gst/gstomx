/*
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * Based on code copyright/by:
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "gstomx_wmadec.h"
#include "gstomx.h"
#include "gstomx_nvutils.h"
#define WMA_MAX_CHANNELS_SUPPORTED 6

typedef struct {
  short int  wFormatTag;
  short int  nChannels;
  unsigned int  nSamplesPerSec;
  unsigned int  nAvgBytesPerSec;
  short int  nBlockAlign;
  short int  wBitsPerSample;
  int nChannelMask;
  short int wEncodeOpt;
  short int  wAdvancedEncodeOpt;
  int dwAdvancedEncodeOpt2;
  unsigned char hasDRM;
  int policyType;
  int meteringTime;
  int licenseConsumeTime;
  int drmContext;
  int coreContext;
}WAVEFORMATEX;

static GstCaps *
generate_src_template (void)
{
  GstCaps *caps;

  caps = gst_caps_new_simple ("audio/x-raw-int",
      "endianness", G_TYPE_INT, G_BYTE_ORDER,
      "width", G_TYPE_INT, 16,
      "depth", G_TYPE_INT, 16,
      "rate", GST_TYPE_INT_RANGE, 8000, 96000,
      "signed", G_TYPE_BOOLEAN, TRUE,
      "channels", GST_TYPE_INT_RANGE, 1, 6, NULL);

  return caps;
}
static GstCaps *
generate_sink_template_wmalossless (void)
{
  GstCaps *caps;
  GstStructure *struc;

  caps = gst_caps_new_empty ();

  struc = gst_structure_new ("audio/x-wma",
      "wmaversion",G_TYPE_INT,4,
      "rate", GST_TYPE_INT_RANGE, 8000, 96000,
      "channels", GST_TYPE_INT_RANGE, 1, 6, NULL);

  {
    GValue list;
    GValue val;

    list.g_type = val.g_type = 0;

    g_value_init (&list, GST_TYPE_LIST);
    g_value_init (&val, G_TYPE_INT);

    g_value_set_int (&val, 2);
    gst_value_list_append_value (&list, &val);

    g_value_set_int (&val, 4);
    gst_value_list_append_value (&list, &val);

    g_value_unset (&val);
    g_value_unset (&list);
  }

  gst_caps_append_structure (caps, struc);

  return caps;
}

static GstCaps *
generate_sink_template_wma (void)
{
  GstCaps *caps;
  GstStructure *struc;

  caps = gst_caps_new_empty ();

  struc = gst_structure_new ("audio/x-wma",
      "wmaversion",GST_TYPE_INT_RANGE,1,2,
      "rate", GST_TYPE_INT_RANGE, 8000, 96000,
      "channels", GST_TYPE_INT_RANGE, 1, 6, NULL);

  {
    GValue list;
    GValue val;

    list.g_type = val.g_type = 0;

    g_value_init (&list, GST_TYPE_LIST);
    g_value_init (&val, G_TYPE_INT);

    g_value_set_int (&val, 2);
    gst_value_list_append_value (&list, &val);

    g_value_set_int (&val, 4);
    gst_value_list_append_value (&list, &val);

    g_value_unset (&val);
    g_value_unset (&list);
  }

  gst_caps_append_structure (caps, struc);

  return caps;
}
static GstCaps *
generate_sink_template_wmapro (void)
{
  GstCaps *caps;
  GstStructure *struc;

  caps = gst_caps_new_empty ();

  struc = gst_structure_new ("audio/x-wma",
      "wmaversion",G_TYPE_INT,3,
      "rate", GST_TYPE_INT_RANGE, 8000, 96000,
      "channels", GST_TYPE_INT_RANGE, 1, 6, NULL);

  {
    GValue list;
    GValue val;

    list.g_type = val.g_type = 0;

    g_value_init (&list, GST_TYPE_LIST);
    g_value_init (&val, G_TYPE_INT);

    g_value_set_int (&val, 2);
    gst_value_list_append_value (&list, &val);

    g_value_set_int (&val, 4);
    gst_value_list_append_value (&list, &val);

    g_value_unset (&val);
    g_value_unset (&list);
  }

  gst_caps_append_structure (caps, struc);

  return caps;
}

static void
type_base_init_wma (gpointer g_class)
{
  GstElementClass *element_class;

  element_class = GST_ELEMENT_CLASS (g_class);

  gst_element_class_set_details_simple (element_class,
      "OpenMAX IL WMA audio decoder",
      "Codec/Decoder/Audio",
      "Decodes audio in WMA format with OpenMAX IL", "Felipe Contreras");

  {
    GstPadTemplate *template;

    template = gst_pad_template_new ("src", GST_PAD_SRC,
        GST_PAD_ALWAYS, generate_src_template ());

    gst_element_class_add_pad_template (element_class, template);
  }

  {
    GstPadTemplate *template;

    template = gst_pad_template_new ("sink", GST_PAD_SINK,
        GST_PAD_ALWAYS, generate_sink_template_wma ());

    gst_element_class_add_pad_template (element_class, template);
  }
}
static void
type_base_init_wmapro (gpointer g_class)
{
  GstElementClass *element_class;

  element_class = GST_ELEMENT_CLASS (g_class);

  gst_element_class_set_details_simple (element_class,
      "OpenMAX IL WMA audio decoder",
      "Codec/Decoder/Audio",
      "Decodes audio in WMA format with OpenMAX IL", "Felipe Contreras");

  {
    GstPadTemplate *template;

    template = gst_pad_template_new ("src", GST_PAD_SRC,
        GST_PAD_ALWAYS, generate_src_template ());

    gst_element_class_add_pad_template (element_class, template);
  }

  {
    GstPadTemplate *template;

    template = gst_pad_template_new ("sink", GST_PAD_SINK,
        GST_PAD_ALWAYS, generate_sink_template_wmapro ());

    gst_element_class_add_pad_template (element_class, template);
  }
}

static void
type_base_init_wmalossless (gpointer g_class)
{
  GstElementClass *element_class;

  element_class = GST_ELEMENT_CLASS (g_class);

  gst_element_class_set_details_simple (element_class,
      "OpenMAX IL WMA audio decoder",
      "Codec/Decoder/Audio",
      "Decodes audio in WMA format with OpenMAX IL",  "Felipe Contreras");

  {
    GstPadTemplate *template;

    template = gst_pad_template_new ("src", GST_PAD_SRC,
        GST_PAD_ALWAYS, generate_src_template ());

    gst_element_class_add_pad_template (element_class, template);
  }

  {
    GstPadTemplate *template;

    template = gst_pad_template_new ("sink", GST_PAD_SINK,
        GST_PAD_ALWAYS, generate_sink_template_wmalossless ());

    gst_element_class_add_pad_template (element_class, template);
  }
}

static void
type_class_init (gpointer g_class, gpointer class_data)
{

}

static gboolean
sink_setcaps (GstPad *pad, GstCaps * caps)
{
  GstStructure *structure;
  GstOmxBaseFilter *omx_base;
  GOmxCore *gomx;
  WAVEFORMATEX wma_header;
  omx_base = GST_OMX_BASE_FILTER (GST_PAD_PARENT (pad));
  gomx = (GOmxCore *) omx_base->gomx;

  GST_INFO_OBJECT (omx_base, "setcaps (sink): %" GST_PTR_FORMAT, caps);

  structure = gst_caps_get_structure (caps, 0);

  {
    const GValue *codec_data;
    GstBuffer *buffer;
    GstBuffer *final;
    codec_data = gst_structure_get_value (structure, "codec_data");
    if (codec_data) {
      int rate,channels,wmaversion,depth,bitrate,block_align;    
      gst_structure_get_int(structure,"channels", &channels);
      gst_structure_get_int(structure,"rate", &rate);
      gst_structure_get_int(structure,"wmaversion",&wmaversion);
      gst_structure_get_int(structure,"depth",&depth);
      gst_structure_get_int(structure,"bitrate",&bitrate);
      gst_structure_get_int(structure,"block_align",&block_align);
      buffer = gst_value_get_buffer (codec_data);

      memset(&wma_header,0,sizeof(WAVEFORMATEX));
      switch(wmaversion) {
          case 1 : wma_header.wFormatTag = 0x160;break;
          case 2 : wma_header.wFormatTag = 0x161;break;
          case 3 : wma_header.wFormatTag = 0x162;break;
          case 4 : wma_header.wFormatTag = 0x163;break;
          default :
              GST_ELEMENT_ERROR(gomx->object,STREAM,DECODE,(NULL),
                  ("Unsupported wmaversion :%d \n",wmaversion));
              return GST_FLOW_ERROR;
      }
      wma_header.nChannels = channels;
      wma_header.nSamplesPerSec = rate;
      wma_header.nAvgBytesPerSec = bitrate/8;
      wma_header.nBlockAlign = block_align;
      wma_header.wBitsPerSample = depth;
      wma_header.nChannelMask = (1<<channels) - 1;
      wma_header.wEncodeOpt = 0;
      wma_header.wAdvancedEncodeOpt = 0;
      wma_header.dwAdvancedEncodeOpt2 = 0;
      if(wmaversion == 1 && GST_BUFFER_SIZE(buffer)>= 4) {
          wma_header.wEncodeOpt = buffer->data[3]<<8 | buffer->data[2];
      } else if (wmaversion == 2 && GST_BUFFER_SIZE(buffer)>= 6) {
          wma_header.wEncodeOpt = buffer->data[5]<<8 | buffer->data[4];
      } else if (wmaversion >= 3 && GST_BUFFER_SIZE(buffer)>=18) {
          wma_header.wEncodeOpt = buffer->data[15]<<8 | buffer->data[14];
          wma_header.wAdvancedEncodeOpt = buffer->data[17]<<8 | buffer->data[16];
          wma_header.dwAdvancedEncodeOpt2 = buffer->data[13]<<24 | buffer->data[12]<<16 | \
                                            buffer->data[11]<<8 | buffer->data[10];
      } else {
          GST_ELEMENT_WARNING(gomx->object,STREAM,DECODE,(NULL),
              ("Received Invalid Codec Data with WMA"));
      }

      final = gst_buffer_new_and_alloc(sizeof(WAVEFORMATEX));
      memcpy(GST_BUFFER_DATA(final),&wma_header,sizeof(WAVEFORMATEX));
      omx_base->codec_data = final;
      gst_buffer_ref (final);
    }
  }

  return gst_pad_set_caps (pad, caps);
}

static void
omx_config (GstOmxBaseFilter * omx_base)
{
/*  GstOmxWmaDec *self;
  GOmxCore *gomx;

  self = GST_OMX_WMADEC (omx_base);
  gomx = (GOmxCore *) omx_base->gomx;
*/
#ifdef AUTOMOTIVE
  gstomx_set_audiodecoder_max_channels_supported(gomx->omx_handle,
                                      WMA_MAX_CHANNELS_SUPPORTED);
#endif

  return;
}

static void
type_instance_init (GTypeInstance *instance, gpointer g_class)
{
  GstOmxBaseFilter *omx_base;

  omx_base = GST_OMX_BASE_FILTER (instance);

  omx_base->omx_config = &omx_config;

  gst_pad_set_setcaps_function (omx_base->sinkpad, sink_setcaps);
}


GType
gst_omx_wmadec_get_type (void)
{
  static GType type = 0;

  if (G_UNLIKELY (type == 0)) {
    GTypeInfo *type_info;

    type_info = g_new0 (GTypeInfo, 1);
    type_info->class_size = sizeof (GstOmxWmaDecClass);
    type_info->base_init = type_base_init_wma;
    type_info->class_init = type_class_init;
    type_info->instance_size = sizeof (GstOmxWmaDec);
    type_info->instance_init = type_instance_init;

    type = g_type_register_static (GST_OMX_BASE_AUDIODEC_TYPE, "GstOmxWmaDec", type_info, 0);

    g_free (type_info);
  }

  return type;
}


GType
gst_omx_wmaprodec_get_type (void)
{
  static GType type = 0;

  if (G_UNLIKELY (type == 0)) {
    GTypeInfo *type_info;

    type_info = g_new0 (GTypeInfo, 1);
    type_info->class_size = sizeof (GstOmxWmaDecClass);
    type_info->base_init = type_base_init_wmapro;
    type_info->class_init = type_class_init;
    type_info->instance_size = sizeof (GstOmxWmaDec);
    type_info->instance_init = type_instance_init;

    type = g_type_register_static (GST_OMX_BASE_AUDIODEC_TYPE, "GstOmxWmaProDec", type_info, 0);

    g_free (type_info);
  }

  return type;
}

GType
gst_omx_wmalosslessdec_get_type (void)
{
  static GType type = 0;

  if (G_UNLIKELY (type == 0)) {
    GTypeInfo *type_info;

    type_info = g_new0 (GTypeInfo, 1);
    type_info->class_size = sizeof (GstOmxWmaDecClass);
    type_info->base_init = type_base_init_wmalossless;
    type_info->class_init = type_class_init;
    type_info->instance_size = sizeof (GstOmxWmaDec);
    type_info->instance_init = type_instance_init;

    type = g_type_register_static (GST_OMX_BASE_AUDIODEC_TYPE, "GstOmxWmaLossLessDec", type_info, 0);

    g_free (type_info);
  }

  return type;
}
