/*
 * Copyright (C) 2007-2009 Nokia Corporation.
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "gstomx_aacdec.h"
#include "gstomx.h"
#include "gstomx_nvutils.h"
#define AAC_MAX_CHANNELS_SUPPORTED 6

GSTOMX_BOILERPLATE (GstOmxAacDec, gst_omx_aacdec, GstOmxBaseAudioDec,
    GST_OMX_BASE_AUDIODEC_TYPE);

static GstFlowReturn gst_omx_aacdec_pad_chain (GstPad *pad, GstBuffer *buf);

static GstCaps *
generate_src_template (void)
{
  GstCaps *caps;

  caps = gst_caps_new_simple ("audio/x-raw-int",
      "endianness", G_TYPE_INT, G_BYTE_ORDER,
      "width", G_TYPE_INT, 16,
      "depth", G_TYPE_INT, 16,
      "rate", GST_TYPE_INT_RANGE, 8000, 96000,
      "signed", G_TYPE_BOOLEAN, TRUE,
      "channels", GST_TYPE_INT_RANGE, 1, 6, NULL);

  return caps;
}

static GstCaps *
generate_sink_template (void)
{
  GstCaps *caps;
  GstStructure *struc;

  caps = gst_caps_new_empty ();

  struc = gst_structure_new ("audio/mpeg",
      "mpegversion", G_TYPE_INT, 4,
      "rate", GST_TYPE_INT_RANGE, 8000, 96000,
      "framed",G_TYPE_BOOLEAN,TRUE,
      "channels", GST_TYPE_INT_RANGE, 1, 6, NULL);

  {
    GValue list;
    GValue val;

    list.g_type = val.g_type = 0;

    g_value_init (&list, GST_TYPE_LIST);
    g_value_init (&val, G_TYPE_INT);

    g_value_set_int (&val, 2);
    gst_value_list_append_value (&list, &val);

    g_value_set_int (&val, 4);
    gst_value_list_append_value (&list, &val);

    gst_structure_set_value (struc, "mpegversion", &list);

    g_value_unset (&val);
    g_value_unset (&list);
  }

  gst_caps_append_structure (caps, struc);

  return caps;
}

static void
type_base_init (gpointer g_class)
{
  GstElementClass *element_class;

  element_class = GST_ELEMENT_CLASS (g_class);

  gst_element_class_set_details_simple (element_class,
      "OpenMAX IL AAC audio decoder",
      "Codec/Decoder/Audio",
      "Decodes audio in AAC format with OpenMAX IL", "Felipe Contreras");

  {
    GstPadTemplate *template;

    template = gst_pad_template_new ("src", GST_PAD_SRC,
        GST_PAD_ALWAYS, generate_src_template ());

    gst_element_class_add_pad_template (element_class, template);
  }

  {
    GstPadTemplate *template;

    template = gst_pad_template_new ("sink", GST_PAD_SINK,
        GST_PAD_ALWAYS, generate_sink_template ());

    gst_element_class_add_pad_template (element_class, template);
  }
}

static void
type_class_init (gpointer g_class, gpointer class_data)
{
}

static gboolean
sink_setcaps (GstPad * pad, GstCaps * caps)
{
  GstStructure *structure;
  GstOmxBaseFilter *omx_base;

  omx_base = GST_OMX_BASE_FILTER (GST_PAD_PARENT (pad));

  GST_INFO_OBJECT (omx_base, "setcaps (sink): %" GST_PTR_FORMAT, caps);

  structure = gst_caps_get_structure (caps, 0);

  {
    const GValue *codec_data;
    GstBuffer *buffer;

    codec_data = gst_structure_get_value (structure, "codec_data");
    if (codec_data) {
      buffer = gst_value_get_buffer (codec_data);
      omx_base->codec_data = buffer;
      gst_buffer_ref (buffer);
    }
  }

  return gst_pad_set_caps (pad, caps);
}

static int sample_rates[] = { 96000,88200,64000,48000,44100,32000,24000,22050,16000,12000,11025,8000 };

static GstFlowReturn gst_omx_aacdec_pad_chain (GstPad *pad, GstBuffer *buf)
{
  GstOmxBaseFilter *omx_base;
  GstOmxAacDec *omx_aacdec;
  GstFlowReturn result = GST_FLOW_ERROR;
  int object_type, index, protection_absent;
//  int ID, object_type, index, protection_absent;
//  unsigned int frame_length;
  int skip_bytes = 0;
  GstBuffer *temp;
  omx_base = GST_OMX_BASE_FILTER (GST_PAD_PARENT (pad));
  omx_aacdec = GST_OMX_AACDEC(gst_pad_get_parent (pad));
  if (omx_aacdec->base_chain_func && omx_base->codec_data == NULL) {
    /* Parse and remove the ADTS header */
    if ((buf->data[0] = 0xff && (buf->data[1] & 0xf6) == 0xf0)) {
//      ID = buf->data[1] & 0x08;
      protection_absent = buf->data[1]&1;
      object_type   = (buf->data[2] & 0xc0)>>6;
      index = (buf->data[2] & 0x3c)>>2;
//      frame_length = ((buf->data[3]&3)<<11) | buf->data[4]<<3 | (buf->data[5]&0xe0 >> 5);
      if (protection_absent == 1) {
        skip_bytes += ADTS_PROTECTION_ON_SKIP_BYTES;
      }
      else {
        skip_bytes += ADTS_PROTECTION_OFF_SKIP_BYTES;
      }

      /* Program the first packet as per the mp4 syntax sent */
      if (omx_aacdec->first_packet) {
        temp = gst_buffer_new_and_alloc(5);
        temp->data[0] = (object_type & 0x1f) << 3;
        temp->data[0] |= (index & 0xe)>>1;
        temp->data[1] = (index & 1)<<7;
        temp->data[1] |= (sample_rates[index]>>17)&0x7f;
        temp->data[2] = (sample_rates[index]>>9)&0xff;
        temp->data[3] = (sample_rates[index]>>1)&0xff;
        temp->data[4] = (sample_rates[index]&0x1)<<7;
        omx_aacdec->first_packet = FALSE;
        GST_INFO_OBJECT(omx_aacdec, "Adts stream");
        omx_base->codec_data = temp;
      }
      {
        if(GST_BUFFER_SIZE(buf) < skip_bytes) {
          gst_buffer_unref(buf);
          return result;
        }
        temp = gst_buffer_new_and_alloc(GST_BUFFER_SIZE(buf)-skip_bytes);
        memcpy(GST_BUFFER_DATA(temp),GST_BUFFER_DATA(buf)+skip_bytes,GST_BUFFER_SIZE(buf)-skip_bytes);
        /* Copy everything */
        gst_buffer_copy_metadata(temp,buf,0x7);
      }
      result = omx_aacdec->base_chain_func(pad,temp);

      gst_buffer_unref(buf);

      if (omx_base->codec_data) {
        gst_buffer_unref (omx_base->codec_data);
        omx_base->codec_data = NULL;
      }

    } else {
      result = omx_aacdec->base_chain_func(pad,buf);
    }
  } else if(omx_aacdec->base_chain_func) {
    result = omx_aacdec->base_chain_func(pad,buf);
  }

  gst_object_unref(GST_ELEMENT(omx_aacdec));
  return result;
}

static void
omx_config (GstOmxBaseFilter * omx_base)
{
#ifdef AUTOMOTIVE
  GstOmxAacDec *self;
  GOmxCore *gomx;

  self = GST_OMX_AACDEC (omx_base);
  gomx = (GOmxCore *) omx_base->gomx;

  gstomx_set_audiodecoder_max_channels_supported(gomx->omx_handle,
                                                    AAC_MAX_CHANNELS_SUPPORTED);
#endif

  return;
}

static void
type_instance_init (GTypeInstance * instance, gpointer g_class)
{
  GstOmxBaseFilter *omx_base;
  GstOmxAacDec *omx_aacdec;

  omx_base = GST_OMX_BASE_FILTER (instance);
  omx_aacdec = GST_OMX_AACDEC(instance);

  omx_base->omx_config = &omx_config;

  omx_aacdec->base_chain_func = NULL;
  omx_aacdec->first_packet = TRUE;
  /* replace base chain func */
  omx_aacdec->base_chain_func = GST_PAD_CHAINFUNC(omx_base->sinkpad);
  gst_pad_set_chain_function (omx_base->sinkpad, gst_omx_aacdec_pad_chain);
  gst_pad_set_setcaps_function (omx_base->sinkpad, sink_setcaps);
}
