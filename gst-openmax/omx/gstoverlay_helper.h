/*
 * Copyright (c) 2009-2011, NVIDIA CORPORATION.  All rights reserved.
 *
 * Based on code copyright/by:
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef _NV_OVERLAY_HELPER_H
#define _NV_OVERLAY_HELPER_H

#include <X11/Xlib.h>

#ifdef __cplusplus
extern "C" {
#endif


typedef enum {
   NV_OVERLAY_WINDOW_TYPE_NOT_OVERLAY = 0,
   NV_OVERLAY_WINDOW_TYPE_OVERLAY = 1
} NvOverlayWindowType;

typedef struct {
   NvOverlayWindowType type;  /* is overlay or not */
   Bool src_rect_valid;       /* a src_rect has been specified */
   XRectangle src_rect;       /* valid only if src_rect_valid */
   Bool dst_rect_valid;       /* a dst_rect has been specified */
   XRectangle dst_rect;       /* valid only if dst_rect_valid */
   int rotation;              /* in degrees clockwise (0 or 180) */
   Bool brightness_valid;
   float brightness;          /* only if brightness_valid */
   Bool contrast_valid;
   float contrast;            /* only if contrast_valid */
   Bool saturation_valid;
   float saturation;          /* only if saturation_valid */
   Bool hue_valid;
   float hue;                 /* only if hue_valid */
} NvOverlayWindowInfo;

typedef struct _list_element {
  Window win;
  NvOverlayWindowInfo info; 
  struct _list_element *next;
} ListElement;

ListElement * FindElement(Window win);

int AddWindow(Window win);

void RemoveWindow(Window win);

Display *InitDisplay(void);

//int NvOverlayGetWindowInfo(Window win, NvOverlayWindowInfo *info);
NvOverlayWindowInfo* NvOverlayGetWindowInfo(Window win);


#ifdef __cplusplus
};     /* extern "C" */
#endif

#endif /* _NV_OVERLAY_HELPER_H */

