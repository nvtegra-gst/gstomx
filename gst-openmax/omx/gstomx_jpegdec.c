/*
 * Copyright (c) 2009-2011, NVIDIA CORPORATION.  All rights reserved.
 *
 * Based on code copyright/by:
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "gstomx_jpegdec.h"
#include "gstomx.h"

static GstOmxBaseVideoDecClass *parent_class;

static GstCaps *
generate_sink_template (void)
{
  GstCaps *caps;
  GstStructure *struc;

  caps = gst_caps_new_empty ();

  struc = gst_structure_new ("image/jpeg",
      "width", GST_TYPE_INT_RANGE, 16, 4096,
      "height", GST_TYPE_INT_RANGE, 16, 4096,
      "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1, NULL);

  gst_caps_append_structure (caps, struc);

  return caps;
}

static void
type_base_init (gpointer g_class)
{
  GstElementClass *element_class;

  element_class = GST_ELEMENT_CLASS (g_class);

  gst_element_class_set_details_simple (element_class,
      "OpenMAX IL JPEG decoder",
      "Codec/Decoder/Image",
      "Decodes video in JPEG format with OpenMAX IL", "Nitin Pai");

  {
    GstPadTemplate *template;

    template = gst_pad_template_new ("sink", GST_PAD_SINK,
        GST_PAD_ALWAYS, generate_sink_template ());

    gst_element_class_add_pad_template (element_class, template);
  }
}

static void
type_class_init (gpointer g_class, gpointer class_data)
{
  parent_class = g_type_class_ref (GST_OMX_BASE_VIDEODEC_TYPE);
}

static void
type_instance_init (GTypeInstance *instance, gpointer g_class)
{
  GstOmxBaseVideoDec *omx_base;

  omx_base = GST_OMX_BASE_VIDEODEC (instance);

  omx_base->compression_format = OMX_IMAGE_CodingJPEG;
}

GType 
gst_omx_jpegdec_get_type (void)
{
  static GType type = 0;

  if (G_UNLIKELY (type == 0)) {
    GTypeInfo *type_info;

    type_info = g_new0 (GTypeInfo, 1);
    type_info->class_size = sizeof (GstOmxJPEGDecClass);
    type_info->base_init = type_base_init;
    type_info->class_init = type_class_init;
    type_info->instance_size = sizeof (GstOmxJPEGDec);
    type_info->instance_init = type_instance_init;

    type = g_type_register_static (GST_OMX_BASE_VIDEODEC_TYPE, "GstOmxJPEGDec", type_info, 0);

    g_free (type_info);
  }

  return type;
}
