/*
 * Copyright (c) 2009-2011, NVIDIA CORPORATION.  All rights reserved.
 *
 * Based on code copyright/by:
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
#include<string.h>//for memset
#include "gstomx_camera.h"
#include "gstomx_base_src.h"
#include "gstomx.h"
#include "OMX_Index.h"
#include "OMX_Image.h"
#include "NVOMX_IndexExtensions.h"

/* from nvfxmath.h */
typedef int NvSFx;
/* Constants */
#define NV_SFX_ONE                ((NvSFx)0x00010000)
#define NV_SFX_HIGH               ((NvSFx)0x7FFFFFFF)

#define PREVIEW_PORT 0
#define CAPTURE_PORT 1
#define CLOCK_PORT   2

#define CAMERA_PORT get_camera_port()

static int get_camera_port();

#define GST_TYPE_OMX_CAMERA_MODE (gst_omx_camera_mode_get_type ())

static GType gst_omx_camera_mode_get_type (void);

enum
{
    ARG_0,
    ARG_CAMERA_MODE,
    ARG_MODE_WIDTH,
    ARG_MODE_HEIGHT,
    ARG_PREVIEW_TIME
};

enum
{
    MODE_PREVIEW        = 0,
    MODE_STILL          = 1,
};

static GstBaseSrcClass *parent_class;

typedef struct NvxCameraParam_ {
    int nWidthPreview;
    int nHeightPreview;
    int nWidthCapture;
    int nHeightCapture;
    OMX_BOOL enablePreview;
    OMX_BOOL enableCameraCapture;
    OMX_BOOL videoCapture;
    int captureTime;
    int previewTime;
    int stillCountBurst;
    int nDelayCapture;
    int nDelayHalfPress;
    int nImageQuality;

    int focusPosition;
    OMX_IMAGE_FOCUSCONTROLTYPE focusAuto;
    int exposureTimeMS;
    int iso;
    OMX_METERINGTYPE exposureMeter;
    OMX_S32 nContrast;
    OMX_BOOL bAutoFrameRate;
    OMX_S32 nAutoFrameRateLow;
    OMX_S32 nAutoFrameRateHigh;
    OMX_IMAGE_FLASHCONTROLTYPE   eFlashControl;
    ENvxFlickerType              eFlicker;

    OMX_IMAGEFILTERTYPE          eImageFilter;
    OMX_S32 xScaleFactor;
    OMX_S32 xScaleFactorMultiplier;
    OMX_BOOL EnableTestPattern;
    OMX_BOOL bPrecaptureConverge;
    OMX_S32 nRotationPreview;
    OMX_S32 nRotationCapture;
    OMX_BOOL bStab;
    OMX_S32  nBrightness;
    OMX_BOOL bUserSetBrightness;
    OMX_S32  nSaturation;
    OMX_S32  nHue;
    OMX_WHITEBALCONTROLTYPE eWhiteBalControl;

    NVX_F32 EdgeEnhancementStrength;
    NVX_RectF32 oCropPreview;
    NVX_RectF32 oCropCapture;

    OMX_U32 SensorId;
    OMX_U32 nBitRate;
    OMX_U32 nFrameRate;
    int useVidHdrRW;
    OMX_U32 nAppType;
    OMX_U32 nQualityLevel;
    OMX_U32 nErrorResilLevel;
    OMX_BOOL enableSvcVideoEncoding;
    int videoEncoderType;
    int videoEncodeLevel;
    OMX_U32 TemporalTradeOffLevel;

    OMX_BOOL enableAudio;
    OMX_AUDIO_CODINGTYPE eAudioCodingType;
    OMX_AUDIO_AACPROFILETYPE eAacProfileType;
    int audioTime;
    OMX_U32 nAudioSampleRate;
    OMX_U32 nAudioBitRate;
    OMX_U32 nChannels;
    OMX_BOOL bDrcEnabled;
    OMX_S32   ClippingTh;
    OMX_S32   LowerCompTh;
    OMX_S32   UpperCompTh;
    OMX_S32   NoiseGateTh;

    OMX_U32  nLoopsStillCapture;        // loops of single shot
    OMX_U64  nTSAppStart;               // timestamp right before init_graph
    OMX_U64  nTSAppCaptureStart;
    OMX_U64  nTSAppCaptureEnd;
    OMX_U64  nTSAppEnd;
    OMX_U64  nTotalShutterLag;
    OMX_U64  nTSAutoFocusAchieved;
    OMX_U64  nTSAutoExposureAchieved;
    OMX_U64  nTSAutoWhiteBalanceAchieved;
    OMX_BOOL bHalfPress;
    OMX_U32  nHalfPressFlag;
    OMX_U32  nHalfPressTimeOutMS;
    OMX_U32  nRawDumpCount;
    OMX_STRING FilenameRawDump;
    OMX_U8   eDeleteJPEGFiles;
    OMX_BOOL bAbortOnHalfPressFailure;
    OMX_U32  n3gpMaxFramesAudio;
    OMX_U32  n3gpMaxFramesVideo;
    OMX_U32  nSmoothZoomTimeMS;
    OMX_U32  nZoomAbortMS;
    OMX_S32  nRotateByMPE;
    OMX_AUDIO_PCMMODETYPE pcmMode;
} NvxCameraParam;

#define VIDEO_ENCODER_TYPE_H264 0

static NvxCameraParam g_cameraParam = {176, 144, 176, 144, OMX_FALSE, OMX_FALSE, OMX_FALSE, 0, 30, 1, 0, 0, 4,
                                       0, OMX_IMAGE_FocusControlOn, -1, -1, OMX_MeteringModeMatrix,
                                       50, OMX_FALSE, NV_SFX_ONE, NV_SFX_HIGH, OMX_IMAGE_FlashControlOff, NvxFlicker_Off,
                                       OMX_ImageFilterNone, NV_SFX_ONE, NV_SFX_ONE, OMX_FALSE, OMX_FALSE,
                                       0, 0, OMX_FALSE, 0, OMX_FALSE, 0, 0, OMX_WhiteBalControlAuto,
                                       0.0, {0}, {0},
                                       0, 512000, 30, 0, 0, 0, 0, OMX_FALSE, VIDEO_ENCODER_TYPE_H264, 255, 0,
                                       OMX_FALSE, OMX_AUDIO_CodingAAC, OMX_AUDIO_AACObjectLC, 0, 44100, 128000, 2,
                                       OMX_FALSE, 15000, 6000, 12000, 500,
                                       1, 0, OMX_AUDIO_PCMModeLinear};

static int get_camera_port(void);

static GstCaps *
generate_src_template (void)
{
    GstCaps *caps;
    caps = gst_caps_new_any ();
    return caps;
}

static void
type_base_init (gpointer g_class)
{
    GstElementClass *element_class;

    element_class = GST_ELEMENT_CLASS (g_class);

    gst_element_class_set_details_simple (element_class,
                                   "OpenMAX IL camera src element",
                                   "None",
                                   "Does nothing",
                                   "Felipe Contreras");

    {
        GstPadTemplate *template;

        template = gst_pad_template_new ("src", GST_PAD_SRC,
                                         GST_PAD_ALWAYS,
                                         generate_src_template ());

        gst_element_class_add_pad_template (element_class, template);
    }
}

static gboolean
setcaps (GstBaseSrc *gst_src,
         GstCaps *caps)
{
//    GstOmxBaseSrc *self;

//    self = GST_OMX_BASE_SRC (gst_src);

    //GST_INFO_OBJECT (self, "setcaps (src): %" GST_PTR_FORMAT, caps);

    g_return_val_if_fail (gst_caps_get_size (caps) == 1, FALSE);

    return TRUE;
}

static void
settings_changed_cb (GOmxCore *core)
{
/*    GstOmxBaseSrc *omx_base;

    omx_base = core->object;
*/

    /** @todo properly set the capabilities */
}

static void
setup_ports (GstOmxBaseSrc *base_src)
{
    GOmxCore *core;
    OMX_PARAM_PORTDEFINITIONTYPE param;
    OMX_PARAM_U32TYPE  oSensorIdParam;
    OMX_INDEXTYPE  eIndexSensorId;
//    OMX_ERRORTYPE  err;
    OMX_IMAGE_PARAM_FLASHCONTROLTYPE    oFlashControl;
    OMX_PARAM_SENSORMODETYPE     oSensorMode;

    core = base_src->gomx; 

    memset (&param, 0, sizeof (param));
    param.nSize = sizeof (OMX_PARAM_PORTDEFINITIONTYPE);
    param.nVersion.s.nVersionMajor = 1;
    param.nVersion.s.nVersionMinor = 1;
    param.nPortIndex = CAMERA_PORT;

    OMX_GetParameter(core->omx_handle, OMX_IndexParamPortDefinition, &param);

    //Disable Camera Clock Port too.
    OMX_SendCommand(core->omx_handle, OMX_CommandPortDisable, CLOCK_PORT, 0);

    if (CAMERA_PORT == PREVIEW_PORT)
    {
        param.format.video.nFrameWidth = g_cameraParam.nWidthPreview;
        param.format.video.nFrameHeight = g_cameraParam.nHeightPreview;
    }
    else
    {
        param.format.video.nFrameWidth = g_cameraParam.nWidthCapture;
        param.format.video.nFrameHeight = g_cameraParam.nHeightCapture;
    }

    base_src->out_port =  g_omx_core_setup_port (core, &param);

    OMX_SetParameter(core->omx_handle, OMX_IndexParamPortDefinition, &param);

    OMX_SendCommand(core->omx_handle, OMX_CommandPortEnable, CAMERA_PORT, 0);  // camera preview out

    memset (&oSensorIdParam, 0xDE, sizeof (oSensorIdParam));

    oSensorIdParam.nU32 = g_cameraParam.SensorId;

    OMX_GetExtensionIndex(core->omx_handle, NVX_INDEX_PARAM_SENSORID,
                                   &eIndexSensorId);

    OMX_SetParameter(core->omx_handle, eIndexSensorId, &oSensorIdParam);

    memset (&oSensorMode, 0xDE, sizeof (oSensorMode));
    oSensorMode.nPortIndex = CAPTURE_PORT;
    OMX_GetParameter(core->omx_handle, OMX_IndexParamCommonSensorMode, &oSensorMode);
    oSensorMode.bOneShot = !g_cameraParam.videoCapture;
    OMX_SetParameter(core->omx_handle, OMX_IndexParamCommonSensorMode, &oSensorMode);

    memset(&oFlashControl, 0xDE, sizeof(oFlashControl)); 
    oFlashControl.nPortIndex = CAPTURE_PORT;
    oFlashControl.eFlashControl = g_cameraParam.eFlashControl;
    OMX_SetParameter(core->omx_handle, OMX_IndexParamFlashControl, &oFlashControl);

    if (CAMERA_PORT == PREVIEW_PORT) // disable the other port
    {
        OMX_SendCommand(core->omx_handle, OMX_CommandPortDisable, CAPTURE_PORT, 0);
    }
    else
    {
        OMX_SendCommand(core->omx_handle, OMX_CommandPortDisable, PREVIEW_PORT, 0);
        //set the output port to camera port
    }

    base_src->out_port->buffer_type = BUFFER_TYPE_EGLIMAGE;
}

static int get_camera_port(void)
{
    if (g_cameraParam.enablePreview)
    {
        return PREVIEW_PORT;
    }

    if (g_cameraParam.enableCameraCapture)
    {
        return CAPTURE_PORT;
    }

    //If none of them are enabled, default it to preview more.
    g_cameraParam.enablePreview = OMX_TRUE;
    return PREVIEW_PORT;
}

static void
set_property (GObject *obj,
              guint prop_id,
              const GValue *value,
              GParamSpec *pspec)
{
    switch (prop_id)
    {
        case ARG_CAMERA_MODE:
        {
            switch (g_value_get_enum(value))
            {
                case MODE_PREVIEW:
                {
                    g_cameraParam.enablePreview = OMX_TRUE;
                }
                break;
                case MODE_STILL:
                {
                    g_cameraParam.enableCameraCapture = OMX_TRUE;
                }
                break;
                default:
                    G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, prop_id, pspec);
                    break;
            }
        }
        break;
        case ARG_MODE_WIDTH:
        {
            if (g_cameraParam.enablePreview)
            {
                g_cameraParam.nWidthPreview = g_value_get_uint (value);
            }

            if (g_cameraParam.enableCameraCapture)
            {
                g_cameraParam.nWidthCapture = g_value_get_uint (value);
            }
        }
        break;
        case ARG_MODE_HEIGHT:
        {
            if (g_cameraParam.enablePreview)
            {
                g_cameraParam.nHeightPreview = g_value_get_uint (value);
            }

            if (g_cameraParam.enableCameraCapture)
            {
                g_cameraParam.nHeightCapture = g_value_get_uint (value);
            }
        }
        break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, prop_id, pspec);
            break;
    }
}

static void
get_property (GObject *obj,
              guint prop_id,
              GValue *value,
              GParamSpec *pspec)
{
    switch (prop_id)
    {
        case ARG_CAMERA_MODE:
        {
            if (g_cameraParam.enablePreview)
            {
               g_value_set_enum(value, MODE_PREVIEW);
            }
            
            if (g_cameraParam.enableCameraCapture)
            {
               g_value_set_enum(value, MODE_STILL);
            }
        }
        break;
        case ARG_MODE_WIDTH:
        {
            if (g_cameraParam.enablePreview)
            {
                g_value_set_uint (value, g_cameraParam.nWidthPreview);
            }

            if (g_cameraParam.enableCameraCapture)
            {
                g_value_set_uint (value, g_cameraParam.nWidthCapture);
            }
        }
        break;
        case ARG_MODE_HEIGHT:
        {
            if (g_cameraParam.enablePreview)
            {
                g_value_set_uint (value, g_cameraParam.nHeightPreview);
            }

            if (g_cameraParam.enableCameraCapture)
            {
                g_value_set_uint (value, g_cameraParam.nHeightCapture);
            }
        }
        break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, prop_id, pspec);
            break;
    }
}

static GstFlowReturn
create (GstBaseSrc *gst_base,
        guint64 offset,
        guint length,
        GstBuffer **ret_buf)
{
    GOmxCore *core;
    GstOmxBaseSrc *omx_base_src;
    GstFlowReturn ret = GST_FLOW_OK;
  
    omx_base_src = GST_OMX_BASE_SRC (gst_base);

    core = omx_base_src->gomx;
   
    if (core->omx_state == OMX_StateLoaded)
    {
        setup_ports (omx_base_src);
        g_omx_core_prepare (core);
    }
   
    if (core->omx_state == OMX_StateIdle)
    {
        g_omx_core_start (core);
    }

    if ((core->omx_state == OMX_StateExecuting) && (CAMERA_PORT == PREVIEW_PORT))
    {
        OMX_CONFIG_BOOLEANTYPE     cc;
        OMX_INDEXTYPE eIndexPreviewEnable;
//        OMX_ERRORTYPE err;
  
 
        // start preview
        OMX_GetExtensionIndex(core->omx_handle, NVX_INDEX_CONFIG_PREVIEWENABLE,
                                       &eIndexPreviewEnable);
        cc.bEnabled = OMX_TRUE;
        OMX_SetConfig(core->omx_handle, eIndexPreviewEnable, &cc);
    }
    
    ret = GST_BASE_SRC_CLASS (parent_class)->create(gst_base, offset, length, ret_buf);

    return ret; 
}

static void
type_class_init (gpointer g_class,
                 gpointer class_data)
{
    GstBaseSrcClass *gst_base_src_class;
    GObjectClass *gobject_class;

    parent_class = g_type_class_ref (GST_OMX_BASE_SRC_TYPE);
    gst_base_src_class = GST_BASE_SRC_CLASS (g_class);
    gobject_class = G_OBJECT_CLASS (g_class);

    gst_base_src_class->set_caps = setcaps;
    gst_base_src_class->create = create;

    /* Properties stuff */
    {
        gobject_class->set_property = set_property;
        gobject_class->get_property = get_property;

        g_object_class_install_property (gobject_class, ARG_CAMERA_MODE,
                                         g_param_spec_enum ("cameramode", "camera mode",
                                                            "enables preview or capture mode",
                                                            GST_TYPE_OMX_CAMERA_MODE, MODE_PREVIEW, G_PARAM_READWRITE));
        
        g_object_class_install_property (gobject_class, ARG_MODE_WIDTH,
                                         g_param_spec_uint  ("width", "modewidth",
                                                             "preview or capture image width",
                                                             0, 1920, 720, G_PARAM_READWRITE));
        
        g_object_class_install_property (gobject_class, ARG_MODE_HEIGHT,
                                         g_param_spec_uint  ("height", "modeheight",
                                                             "preview or capture image height",
                                                             0, 1080, 480, G_PARAM_READWRITE));

#define FOREVER 0xFFFFFFFF

        g_object_class_install_property (gobject_class, ARG_PREVIEW_TIME,
                                         g_param_spec_uint  ("previewtime", "previewtime",
                                                             "time till preview is enabled",
                                                             0,FOREVER,FOREVER,G_PARAM_READWRITE));
    }
}

static void
type_instance_init (GTypeInstance *instance,
                    gpointer g_class)
{
    GstOmxBaseSrc *omx_base;
//    GstOmxCamera  *gstcamera;

//    gstcamera = GST_OMX_CAMERA(instance);

    omx_base = GST_OMX_BASE_SRC (instance);

    GST_DEBUG_OBJECT (omx_base, "begin");

    omx_base->setup_ports = setup_ports;

    omx_base->gomx->settings_changed_cb = settings_changed_cb;

    GST_DEBUG_OBJECT (omx_base, "end");
}

GType
gst_omx_camera_get_type (void)
{
    static GType type = 0;

    if (G_UNLIKELY (type == 0))
    {
        GTypeInfo *type_info;

        type_info = g_new0 (GTypeInfo, 1);
        type_info->class_size = sizeof (GstOmxCameraClass);
        type_info->base_init = type_base_init;
        type_info->class_init = type_class_init;
        type_info->instance_size = sizeof (GstOmxCamera);
        type_info->instance_init = type_instance_init;

        type = g_type_register_static (GST_OMX_BASE_SRC_TYPE, "GstOmxCamera", type_info, 0);

        g_free (type_info);
    }

    return type;
}

static GType
gst_omx_camera_mode_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static GEnumValue vals[] =
        {
            {MODE_PREVIEW,        "Preview",                    "preview"},
            {MODE_STILL,          "Still Image Capture",        "stillimage"},
            {0, NULL, NULL},
        };

        type = g_enum_register_static ("GstOmxCameraMode", vals);
    }

    return type;
}
