/*
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * Based on code copyright/by:
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "config.h"
#include "gstomx.h"
#include "gstgl_videosink.h"
#include "gstomx_x11eglutils.h"
#include <gst/interfaces/xoverlay.h>
#include <string.h> /* for memset, strcmp */

static GstVideoSinkClass *parent_class = NULL;
static int start_frame=0, end_frame=0, step=1;
static gboolean gstgl_videosink_query (GstPad *pad, GstQuery * query);
enum
{
    ARG_0,
    ARG_X_SCALE,
    ARG_Y_SCALE,
    ARG_ROTATION,
    PROP_DISPLAY,
    ARG_FORCE_ASPECT_RATIO,
    ARG_CONTRAST,
    ARG_BRIGHTNESS,
    ARG_HUE,
    ARG_SATURATION,
    ARG_FRAMEDUMP,
    ARG_STARTFRAME,
    ARG_ENDFRAME,
    ARG_STEP,
    ARG_VIDEODUMPFILE,
    ARG_VIDEO_DISABLE,
    PROP_HANDLE_EVENTS,
    PROP_FORCE_FULLSCREEN
};

static GstCaps *
generate_sink_template (void)
{
    GstCaps *caps;
    GstStructure *struc;

    caps = gst_caps_new_empty ();
    struc = gst_structure_new ("video/x-raw-gl",
                               "width", GST_TYPE_INT_RANGE, 16, 4096,
                               "height", GST_TYPE_INT_RANGE, 16, 4096,
                               "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1,
                                NULL);

    {
        GValue list;
        GValue val;

        list.g_type = val.g_type = 0;

        g_value_init (&list, GST_TYPE_LIST);
        g_value_init (&val, GST_TYPE_FOURCC);

        gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('R', 'G', 'B', 'A'));
        gst_value_list_append_value (&list, &val);

        gst_structure_set_value (struc, "format", &list);

        g_value_unset (&val);
        g_value_unset (&list);
    }

    gst_caps_append_structure (caps, struc);

    return caps;
}

#if 0
static GstFlowReturn
preroll (GstBaseSink *gst_base,
        GstBuffer *buf)
{
    return GST_FLOW_OK;
}
#endif

static GstFlowReturn
render (GstBaseSink *gst_base,
        GstBuffer *buf)
{
    GstGlVideoSink *self;
    nvomx_eglbuffer eglbuf ;

    self = GST_GL_VIDEOSINK (gst_base);
    memcpy(&eglbuf,GST_BUFFER_DATA(buf),sizeof(nvomx_eglbuffer));

    if (self->property_change_flags & GOMX_PROPERTY_CHANGE_FLAGS_KEEPASPECT)
    {
        if (self->keep_aspect)
        {
           gstgl_apply_aspect_ratio_if_needed(&self->gfx_display_object, self->source_width,self->source_height, ((float)self->pixel_aspect_ratio_num / (float)self->pixel_aspect_ratio_denom), TRUE);
        }
        else
        {
           gstgl_apply_aspect_ratio_if_needed(&self->gfx_display_object, self->source_width,self->source_height, ((float)self->pixel_aspect_ratio_num/(float)self->pixel_aspect_ratio_denom), FALSE);
        }

        gstgl_reconfigure_geometry_video(FALSE);
        self->property_change_flags &= ~GOMX_PROPERTY_CHANGE_FLAGS_KEEPASPECT;
    }

    if (self->bvideodisable)
    {
        return GST_FLOW_OK;
    }
    else
    {
        nvomx_render_eglimage(&self->gfx_display_object, &eglbuf ,self->enable_framedump ,start_frame ,end_frame ,step);
        return GST_FLOW_OK;
    }
}

static void
type_instance_init (GTypeInstance *instance,
                    gpointer g_class)
{
    GstGlVideoSink *self = GST_GL_VIDEOSINK(instance);
    GstBaseSink *base = GST_BASE_SINK(self);
    self->gfx_display_object.fullscreen_window = TRUE;
    gst_pad_set_query_function(base->sinkpad, gstgl_videosink_query);

    self->keep_aspect = TRUE;
    self->property_change_flags = 0;
    self->pixel_aspect_ratio_num = 1;
    self->pixel_aspect_ratio_denom = 1;
    self->enable_framedump = FALSE;
}

static void
type_base_init (gpointer g_class)
{
    GstElementClass *element_class;

    element_class = GST_ELEMENT_CLASS (g_class);

    gst_element_class_set_details_simple (element_class,
                                          "OpenMAX IL videosink element",
                                          "Video/Sink",
                                          "Renders video",
                                          "Felipe Contreras");

    {
        GstPadTemplate *template;

        template = gst_pad_template_new ("sink", GST_PAD_SINK,
                                         GST_PAD_ALWAYS,
                                         generate_sink_template ());

        gst_element_class_add_pad_template (element_class, template);
    }
}

static gboolean
setcaps (GstBaseSink *gst_sink,
         GstCaps *caps)
{
    GstGlVideoSink *self;
    const GValue *fps;
    const GValue *pixel_aspect_ratio;
    self = GST_GL_VIDEOSINK (gst_sink);
    g_return_val_if_fail (gst_caps_get_size (caps) == 1, FALSE);
    {
        GstStructure *structure;
        structure = gst_caps_get_structure (caps, 0);

        gst_structure_get_int (structure, "width", &self->source_width);
        gst_structure_get_int (structure, "height", &self->source_height);
        fps = gst_structure_get_value (structure, "framerate");
        self->fps_n = gst_value_get_fraction_numerator (fps);
        self->fps_d = gst_value_get_fraction_denominator (fps);

        pixel_aspect_ratio = gst_structure_get_value (structure, "pixel-aspect-ratio");
        if (pixel_aspect_ratio)
        {
            self->pixel_aspect_ratio_num   = gst_value_get_fraction_numerator (pixel_aspect_ratio);
            self->pixel_aspect_ratio_denom = gst_value_get_fraction_denominator (pixel_aspect_ratio);
        }

        if (self->keep_aspect)
        {
            gstgl_apply_aspect_ratio_if_needed(&self->gfx_display_object, self->source_width,self->source_height, ((float)self->pixel_aspect_ratio_num/(float)self->pixel_aspect_ratio_denom), TRUE);
        }
        else
        {
            gstgl_apply_aspect_ratio_if_needed(&self->gfx_display_object, self->source_width,self->source_height, ((float)self->pixel_aspect_ratio_num/(float)self->pixel_aspect_ratio_denom), FALSE);
        }
    }
    return TRUE;
}

static void
set_property (GObject *object,
              guint prop_id,
              const GValue *value,
              GParamSpec *pspec)
{
    GstGlVideoSink *self;
    self = GST_GL_VIDEOSINK (object);


    switch (prop_id)
    {
        case PROP_HANDLE_EVENTS:
            self->handle_events = g_value_get_boolean (value);
            break;
        case ARG_X_SCALE:
            self->x_scale = g_value_get_uint (value);
            break;
        case ARG_Y_SCALE:
            self->y_scale = g_value_get_uint (value);
            break;
        case ARG_ROTATION:
            self->rotation = g_value_get_uint (value);
            break;
        case ARG_HUE:
              self->hue = g_value_get_int (value);
              break;
        case ARG_CONTRAST:
              self->contrast = g_value_get_int (value);
              break;
        case ARG_BRIGHTNESS:
              self->brightness = g_value_get_int (value);
              break;
        case ARG_SATURATION:
              self->saturation = g_value_get_int (value);
              break;
        case ARG_FORCE_ASPECT_RATIO:
              self->keep_aspect = g_value_get_boolean (value);
              self->property_change_flags |= GOMX_PROPERTY_CHANGE_FLAGS_KEEPASPECT;
              break;
        case ARG_FRAMEDUMP:
              self->enable_framedump = FALSE;
              self->enable_framedump = g_value_get_boolean (value);
              if(self->enable_framedump) {
                  printf("FrameDump is Enabled\n");
              } else
                  printf("FrameDump is Disabled\n");
              break;
        case ARG_STARTFRAME:
            self->start_frame = g_value_get_uint (value);
            start_frame = (int) self->start_frame;
            break;
        case ARG_ENDFRAME:
            self->end_frame = g_value_get_uint (value);
            end_frame = (int) self->end_frame;
            break;
        case ARG_STEP:
            self->step = g_value_get_uint (value);
            step = (int) self->step;
            break;
         case ARG_VIDEODUMPFILE:
              self->videodumpfile_name = g_strdup (g_value_get_string (value));
              break;
        case ARG_VIDEO_DISABLE:
            self->bvideodisable = g_value_get_boolean (value);
            break;
        case PROP_DISPLAY:
              self->display_name = g_strdup (g_value_get_string (value));
              break;
        case PROP_FORCE_FULLSCREEN:
              self->gfx_display_object.fullscreen_window = g_value_get_boolean (value);
              break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
get_property (GObject *object,
              guint prop_id,
              GValue *value,
              GParamSpec *pspec)
{
    GstGlVideoSink *self;

    self = GST_GL_VIDEOSINK (object);
    switch (prop_id)
    {
        case PROP_HANDLE_EVENTS:
            self->handle_events = TRUE;
            g_value_set_boolean (value, self->handle_events);
            break;
        case ARG_X_SCALE:
            g_value_set_uint (value, self->x_scale);
            break;
        case ARG_Y_SCALE:
            g_value_set_uint (value, self->y_scale);
            break;
        case ARG_ROTATION:
            g_value_set_uint (value, self->rotation);
            break;
        case ARG_HUE:
            g_value_set_int (value, self->hue);
            break;
        case ARG_CONTRAST:
            g_value_set_int (value, self->contrast);
            break;
        case ARG_BRIGHTNESS:
            g_value_set_int (value, self->brightness);
            break;
        case ARG_SATURATION:
            g_value_set_int (value, self->saturation);
            break;
        case ARG_FORCE_ASPECT_RATIO:
            g_value_set_boolean (value, self->keep_aspect);
            break;
        case ARG_FRAMEDUMP:
            g_value_set_boolean (value, self->enable_framedump);
            break;
         case ARG_STARTFRAME:
            g_value_set_uint (value,self->start_frame);
            break;
        case ARG_ENDFRAME:
            g_value_set_uint (value,self->end_frame);
            break;
        case ARG_STEP:
            g_value_set_uint (value,self->step);
            break;
        case ARG_VIDEODUMPFILE:
            g_value_set_string (value, self->videodumpfile_name);
            break;
        case ARG_VIDEO_DISABLE:
            g_value_set_boolean (value, self->bvideodisable);
            break;
        case PROP_DISPLAY:
            g_value_set_string (value, self->display_name);
            break;
        case PROP_FORCE_FULLSCREEN:
            g_value_set_boolean (value, self->gfx_display_object.fullscreen_window);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}


static gboolean
start (GstGlVideoSink *gst_base)
{
    OMX_ERRORTYPE err = OMX_ErrorNone;
    GstGlVideoSink *self = GST_GL_VIDEOSINK(gst_base);
    err = nvomx_x11egl_initialize(&self->gfx_display_object);
    if(err != OMX_ErrorNone) return FALSE;
    return TRUE;
}

static gboolean
stop (GstGlVideoSink *gst_base)
{
    GstGlVideoSink *self = GST_GL_VIDEOSINK(gst_base);
    nvomx_x11egl_destroy(&self->gfx_display_object);
    return TRUE;
}

static GstStateChangeReturn
change_state (GstElement * element, GstStateChange transition)
{

  GstGlVideoSink *self = GST_GL_VIDEOSINK(element);
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      break;
    case GST_STATE_CHANGE_READY_TO_PAUSED:
      if (self->initialized == FALSE) {
        gst_x_overlay_prepare_xwindow_id (GST_X_OVERLAY (element));
        if (!start(self)) {
          ret = GST_STATE_CHANGE_FAILURE;
          goto done;
        }
      }
      self->initialized = TRUE;
      break;
    case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
      break;
    default:
      break;
  }

 ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  switch (transition) {
    case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
      break;
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      self->fps_n = 0;
      self->fps_d = 1;
      GST_VIDEO_SINK_WIDTH (self) = 0;
      GST_VIDEO_SINK_HEIGHT (self) = 0;
      break;
    case GST_STATE_CHANGE_READY_TO_NULL:
       stop(self);
       self->initialized = FALSE;
      break;
    default:
      break;
  }
done:
  return ret;
}

static void
gst_glvideosink_get_times (GstBaseSink * bsink, GstBuffer * buf,
    GstClockTime * start, GstClockTime * end)
{
  GstGlVideoSink *self = GST_GL_VIDEOSINK(bsink);
  if (GST_BUFFER_TIMESTAMP_IS_VALID (buf)) {
    *start = GST_BUFFER_TIMESTAMP (buf);
    if (GST_BUFFER_DURATION_IS_VALID (buf)) {
      *end = *start + GST_BUFFER_DURATION (buf);
    } else {
      if (self->fps_n > 0) {
        *end = *start +
            gst_util_uint64_scale_int (GST_SECOND, self->fps_d,
            self->fps_n);
      }
    }
  }
}

static gboolean
handle_event (GstBaseSink *gst_base,
              GstEvent *event)
{
    GstGlVideoSink *self = GST_GL_VIDEOSINK(gst_base);
    GST_LOG_OBJECT (self, "begin");

    switch (GST_EVENT_TYPE (event))
    {
        case GST_EVENT_EOS:
             gstgl_clearscreen(&self->gfx_display_object);
             break;

        case GST_EVENT_FLUSH_START:
            break;

        case GST_EVENT_FLUSH_STOP:
            break;

        default:
            break;
    }
    GST_LOG_OBJECT (self, "end");
    if (GST_BASE_SINK_CLASS (parent_class)->event)
        return GST_BASE_SINK_CLASS (parent_class)->event (gst_base, event);
      else
        return TRUE;
}

static gboolean
gstgl_videosink_query (GstPad *pad, GstQuery * query)
{
  GstGlVideoSink *self = GST_GL_VIDEOSINK(GST_OBJECT_PARENT(pad));
  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_CUSTOM:
    {
      GstStructure *structure = gst_query_get_structure (query);
      gst_structure_set (structure, "display_data", G_TYPE_POINTER,
                          &self->gfx_display_object, NULL);
      break;
    }
    default:
      break;
  }
  return GST_FLOW_OK;
}


static void
type_class_init (gpointer g_class,
                 gpointer class_data)
{
    GObjectClass *gobject_class;
    GstBaseSinkClass *gst_base_sink_class;
    GstElementClass *gstelement_class;

    gobject_class = (GObjectClass *) g_class;
    gst_base_sink_class = GST_BASE_SINK_CLASS (g_class);
    gstelement_class = (GstElementClass *) g_class;
    parent_class = g_type_class_peek_parent (g_class);

    gst_base_sink_class->set_caps = setcaps;
    gstelement_class->change_state = change_state;

    gst_base_sink_class->render = render;
    gst_base_sink_class->preroll = render;
    gst_base_sink_class->event = handle_event;
    gst_base_sink_class->get_times = gst_glvideosink_get_times;

    gobject_class->set_property = set_property;
    gobject_class->get_property = get_property;

    g_object_class_install_property (gobject_class, ARG_X_SCALE,
                                     g_param_spec_uint ("x-scale", "X Scale",
                                                        "How much to scale the image in the X axis (100 means nothing)",
                                                        0, G_MAXUINT, 100, G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class, ARG_Y_SCALE,
                                     g_param_spec_uint ("y-scale", "Y Scale",
                                                        "How much to scale the image in the Y axis (100 means nothing)",
                                                        0, G_MAXUINT, 100, G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class, ARG_ROTATION,
                                     g_param_spec_uint ("rotation", "Rotation",
                                                        "Rotation angle",
                                                        0, G_MAXUINT, 360, G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class, PROP_DISPLAY,
                                       g_param_spec_string ("display", "Display", "X Display name",
                                                             NULL, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, ARG_CONTRAST,
                                       g_param_spec_int ("contrast", "Contrast", "The contrast of the video",
                                                          -1000, 1000, 0, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, ARG_BRIGHTNESS,
                                       g_param_spec_int ("brightness", "Brightness",
                                                         "The brightness of the video", -1000, 1000, 0,
                                                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
    g_object_class_install_property (gobject_class, ARG_HUE,
                                       g_param_spec_int ("hue", "Hue", "The hue of the video", -1000, 1000, 0,
                                                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, ARG_SATURATION,
                                       g_param_spec_int ("saturation", "Saturation",
                                                         "The saturation of the video", -1000, 1000, 0,
                                                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, ARG_FORCE_ASPECT_RATIO,
                                                          g_param_spec_boolean ("force-aspect-ratio", "Force aspect ratio",
                                                          "When enabled, scaling will respect original aspect ratio", FALSE,
                                                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, ARG_FRAMEDUMP,
                                       g_param_spec_boolean ("enable-framedump", "Enable Frame Dump", "When enabled certain frames will be dumped", TRUE,
                                                          G_PARAM_READWRITE));

   g_object_class_install_property (gobject_class, ARG_STARTFRAME,
                                       g_param_spec_uint ("start", "Start Frame Number", "Start Frame Number for dumping", 0, G_MAXUINT, 30,
                                                          G_PARAM_READWRITE));

   g_object_class_install_property (gobject_class, ARG_ENDFRAME,
                                       g_param_spec_uint ("end", "End Frame Number", "End Frame Number for dumping", 0, G_MAXUINT, 30,
                                                          G_PARAM_READWRITE));

   g_object_class_install_property (gobject_class, ARG_VIDEODUMPFILE,
                                       g_param_spec_string ("videodumpfile", "Videodumpfile", "Video dump file",
                                                             NULL, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

   g_object_class_install_property (gobject_class, ARG_STEP,
                                       g_param_spec_uint ("step", "Step", "Step to increment By", 0, G_MAXUINT, 30,
                                                          G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class, PROP_HANDLE_EVENTS,
                                      g_param_spec_boolean ("handle-events", "Handle XEvents",
                                                            "When enabled, XEvents will be selected and handled", TRUE,
                                                            G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, ARG_VIDEO_DISABLE,
                                       g_param_spec_boolean ("disablevideo", "disable video display", "Disables the Video Disiplay", FALSE,
                                                          G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, PROP_FORCE_FULLSCREEN,
                                       g_param_spec_boolean ("force-fullscreen", "Force fullscreen window",
                                                              "When enabled, fullscreen window will be created", FALSE,
                                                               G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));


}

static void
#ifndef GST_DISABLE_OLD_SETXWINDOW
omx_videosink_set_xwindow_id (GstXOverlay * overlay, XID xwindow_id)
#else
omx_videosink_set_window_handle (GstXOverlay * overlay, guintptr xwindow_id)
#endif
{
    GstGlVideoSink *self = GST_GL_VIDEOSINK (overlay);
    nvomx_x11egl_setwindowid(&self->gfx_display_object, xwindow_id);
}

static void
omx_videosink_expose (GstXOverlay * overlay)
{
    //TODO
    /* Nothing to do as of now */
}

static void
omx_videosink_set_event_handling (GstXOverlay * overlay,
    gboolean handle_events)
{
    GstGlVideoSink *self;

    self = GST_GL_VIDEOSINK (overlay);
    self->handle_events = handle_events;
}

static void
omx_videosink_xoverlay_init (GstXOverlayClass * iface)
{
#ifndef GST_DISABLE_OLD_SETXWINDOW
  iface->set_xwindow_id = omx_videosink_set_xwindow_id;
#else
  iface->set_window_handle = omx_videosink_set_window_handle;
#endif
  iface->expose = omx_videosink_expose;
  iface->handle_events = omx_videosink_set_event_handling;
}
static gboolean
interface_supported (GstImplementsInterface *iface,
                     GType type)
{
    g_assert (type == GST_TYPE_X_OVERLAY);
    return TRUE;
}

static void
interface_init (GstImplementsInterfaceClass *klass)
{
    klass->supported = interface_supported;
}


GType
gst_gl_videosink_get_type (void)
{
    static GType type = 0;
    static const GInterfaceInfo overlay_info = {
      (GInterfaceInitFunc) omx_videosink_xoverlay_init,
      NULL,
      NULL,
    };
    if (G_UNLIKELY (type == 0))
    {
        GTypeInfo *type_info;
        GInterfaceInfo *iface_info;

        type_info = g_new0 (GTypeInfo, 1);
        type_info->class_size = sizeof (GstGlVideoSinkClass);
        type_info->class_init = type_class_init;
        type_info->instance_size = sizeof (GstGlVideoSink);
        type_info->base_init = type_base_init;
        type_info->instance_init = type_instance_init;

        type = g_type_register_static (GST_TYPE_VIDEO_SINK, "GstGlVideoSink", type_info, 0);
        g_free (type_info);

        iface_info = g_new0 (GInterfaceInfo, 1);
        iface_info->interface_init = (GInterfaceInitFunc) interface_init;

        g_type_add_interface_static (type, GST_TYPE_IMPLEMENTS_INTERFACE, iface_info);
        g_free (iface_info);
        g_type_add_interface_static (type, GST_TYPE_X_OVERLAY,&overlay_info);


    }

    return type;
}
