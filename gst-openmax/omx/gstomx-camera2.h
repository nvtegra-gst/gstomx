/*
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * NVIDIA Corporation and its licensors retain all intellectual property
 * and proprietary rights in and to this software, related documentation
 * and any modifications thereto.  Any use, reproduction, disclosure or
 * distribution of this software and related documentation without an express
 * license agreement from NVIDIA Corporation is strictly prohibited.
 *
 * version: 0.2
 */

#ifndef __GST_OMX_CAMERA2_H__
#define __GST_OMX_CAMERA2_H__

#include <gst/gst.h>
#include "gstomx_util.h"
#include "gstnvcameradefs.h"
#include <async_queue.h>

G_BEGIN_DECLS

#define GST_TYPE_OMX_CAMERA2 \
  (gst_omx_camera2_get_type ())
#define GST_OMX_CAMERA2(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_OMX_CAMERA2, GstOmxCamera2))
#define GST_OMX_CAMERA2_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), GST_TYPE_OMX_CAMERA2, GstOmxCamera2Class))
#define GST_IS_OMX_CAMERA2(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_OMX_CAMERA2))
#define GST_IS_OMX_CAMERA2_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_TYPE_OMX_CAMERA2))
#define GST_OMX_CAMERA2_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_TYPE_OMX_CAMERA2, GstOmxCamera2Class))


typedef struct GstOmxCamera2 GstOmxCamera2;
typedef struct GstOmxCamera2Class GstOmxCamera2Class;


typedef enum
{
  NvGstCapturingStopped = 0,
  NvGstCapturingPaused,
  NvGstCapturingStarted
} NvGstCapturingState;

typedef struct
{
  GTimer *mctimer;
  GTimer *aftimer;
  GTimer *c2ptimer;
  GTimer *c2jtimer;
} NvGstKPITimers;

struct GstOmxCamera2
{
  GstElement element;

  GOmxCore *gomx;

  GstPad *vf_pad;
  GstPad *img_pad;
  GstPad *vid_pad;

  GstClockTime duration;
  GMutex* capture_lock;
  GCond* eos_cond;
  GCond* image_capture_cond;

  gint r_width;
  gint r_height;

  gboolean viewfinder_on;
  gboolean eos_handled;
  gboolean start_capturing;
  gboolean start_with_capture;
  NvGstCapturingState capturing;
  NvGstKPITimers kpitimer;

  gint auto_focus_param;
  gint auto_focus_timeout;
  glong  minPosition;
  glong  maxPosition;
  glong  macroPosition;
  glong  hyperfocalPosition;
  gboolean infModeAF;
  gboolean macroModeAF;
  gboolean hasFocusSupport;
  gboolean hasFlashSupport;

  gboolean enable_kpi_measure;

  gint image_capture_phase;
  GstFocusStatus focus_status;

  GstSceneMode scene_mode;
  GstFocusMode focus_mode;
  GstFlashMode flash_mode;
  GstPhotoCaps capabilities;
};

struct GstOmxCamera2Class
{
  GstElementClass parent_class;

  void (*start_capture) (GstOmxCamera2*);
  void (*stop_capture) (GstOmxCamera2*);
  void (*start_auto_focus) (GstOmxCamera2*);
  void (*stop_auto_focus) (GstOmxCamera2*);
};

GType gst_omx_camera2_get_type (void);

#ifdef GST_ENABLE_PHOTOGRAPHY
gboolean gst_omx_camera_prepare_for_capture (GstOmxCamera2* src, GstPhotoCapturePrepared func,
    GstCaps *capture_caps, gpointer user_data);
#endif

G_END_DECLS

#endif /* GST_OMX_CAMERA2_H */

