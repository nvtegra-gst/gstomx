/*
 * Copyright (c) 2009-2011, NVIDIA CORPORATION.  All rights reserved.
 *
 * Based on code copyright/by:
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef GSTOMX_JPEGDEC_H
#define GSTOMX_JPEGDEC_H

#include <gst/gst.h>

G_BEGIN_DECLS
#define GST_OMX_JPEGDEC(obj) (GstOmxJPEGDec *) (obj)
#define GST_OMX_JPEGDEC_TYPE (gst_omx_jpegdec_get_type ())
typedef struct GstOmxJPEGDec GstOmxJPEGDec;
typedef struct GstOmxJPEGDecClass GstOmxJPEGDecClass;

#include "gstomx_base_videodec.h"

struct GstOmxJPEGDec
{
  GstOmxBaseVideoDec omx_base;
};

struct GstOmxJPEGDecClass
{
  GstOmxBaseVideoDecClass parent_class;
};

GType gst_omx_jpegdec_get_type (void);

G_END_DECLS
#endif /* GSTOMX_JPEGDEC_H */
