/*
 * Copyright (C) 2007-2009 Nokia Corporation.
 * Copyright (c) 2011-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "gstomx_h264dec.h"
#include "gstomx.h"
#include "string.h"
#include "stdlib.h"

#define MAX_DELIMITER_BYTES 6

GSTOMX_BOILERPLATE (GstOmxH264Dec, gst_omx_h264dec, GstOmxBaseVideoDec,
    GST_OMX_BASE_VIDEODEC_TYPE);

static GstFlowReturn gst_omx_h264dec_pad_chain (GstPad *pad, GstBuffer *buf);

static GstCaps *
generate_sink_template (void)
{
  GstCaps *caps;
  GstStructure *struc;

  caps = gst_caps_new_empty ();

  struc = gst_structure_new ("video/x-h264",
      "width", GST_TYPE_INT_RANGE, 16, 4096,
      "height", GST_TYPE_INT_RANGE, 16, 4096,
      "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1, NULL);

  gst_caps_append_structure (caps, struc);

  return caps;
}

static gboolean
sink_setcaps (GstPad *pad,
              GstCaps *caps)
{
  GstStructure *structure;
  GstOmxBaseVideoDec *self;
  GstOmxH264Dec *omx_h264dec;
  GstOmxBaseFilter *omx_base;
  GOmxCore *gomx;
  OMX_PARAM_PORTDEFINITIONTYPE param;
  gint width = 0;
  gint height = 0;
  gdouble framerate_num,framerate_denom;
  self = GST_OMX_BASE_VIDEODEC (GST_PAD_PARENT (pad));
  omx_base = GST_OMX_BASE_FILTER (self);
  omx_h264dec = GST_OMX_H264DEC(gst_pad_get_parent (pad));

  gomx = (GOmxCore *) omx_base->gomx;
  g_return_val_if_fail (gst_caps_get_size (caps) == 1, FALSE);
  structure = gst_caps_get_structure (caps, 0);
  if(omx_h264dec->codec_data != NULL)
      gst_buffer_unref(omx_h264dec->codec_data);
  omx_h264dec->codec_data = NULL;
  omx_h264dec->length_size = 0;
  omx_h264dec->first_idr = 0;
  gst_structure_get_int (structure, "width", &width);
  gst_structure_get_int (structure, "height", &height);
  if (!width || !height) {
    width = 720;
    height = 480;
    GST_ELEMENT_WARNING(gomx->object,STREAM,DECODE,(NULL),
        ("Received Invalid Width/Height - using 720/480"));
  }

  {
    const GValue *framerate = NULL;
    framerate = gst_structure_get_value (structure, "framerate");
    if (framerate) {
      self->framerate_num = gst_value_get_fraction_numerator (framerate);
      self->framerate_denom = gst_value_get_fraction_denominator (framerate);
      framerate_num = (gdouble)self->framerate_num;
      framerate_denom = (gdouble)self->framerate_denom;

      omx_base->duration = (GstClockTime)((framerate_denom /framerate_num)*GST_SECOND );
    }
  }

  memset (&param, 0, sizeof (param));
  param.nSize = sizeof (OMX_PARAM_PORTDEFINITIONTYPE);
  param.nVersion.s.nVersionMajor = 1;
  param.nVersion.s.nVersionMinor = 1;
  {
    const GValue *codec_data;
    char *pOut = NULL;

    GstBuffer *buffer;
    GstBuffer *final;
    unsigned int total_size = 0;
    unsigned loop_count = 0;

    codec_data = gst_structure_get_value (structure, "codec_data");
parse_sps_pps:
    if (codec_data) {

      loop_count++;
      buffer = gst_value_get_buffer (codec_data);
      if((buffer->size > 6) && (buffer->data[0] == 1)) {
        /* retrieve sps and pps NAL units from extradata */
        unsigned short unit_size;
        unsigned char unit_nb, sps_done = 0, length_size;
        const char *extradata = (char *)buffer->data + 4;
        static const char  nalu_header[4] = {0, 0, 0, 1};
        /* retrieve length coded size */
        length_size = (*extradata++ & 0x3) + 1;
        if(length_size == 3)
          goto  done;
        omx_h264dec->length_size = length_size;
        /* retrieve sps and pps unit(s) */
        unit_nb = *extradata++ & 0x1f; /* number of sps unit(s) */
        if(!unit_nb) {
          unit_nb = *extradata++; /* number of pps unit(s) */
          sps_done++;
        }
        while (unit_nb--) {
          unit_size = (*extradata << 8) | *(extradata + 1);
          total_size += unit_size+4;
          if(extradata + 2 + unit_size > (char *)buffer->data + buffer->size) {
            if(pOut != NULL)
              free(pOut);

            goto done;
          }
          pOut = (char *)realloc(pOut, total_size);
          if(!pOut)
            goto done;
          memcpy(pOut + total_size - unit_size - 4, nalu_header, 4);
          memcpy(pOut + total_size - unit_size,     extradata + 2, unit_size);
          extradata += 2 + unit_size;

          if(!unit_nb && !sps_done++)
            unit_nb = *extradata++; /* number of pps unit(s) */
        }
      }
    }
    //See if the stream also has MVC data present
    codec_data = gst_structure_get_value (structure, "codec_data_mvc");
    if (loop_count==1)
    {

      if(codec_data == NULL)
      {
          // There is no MVC data in the stream so retain original data
#ifdef DEBUG_LEVEL1
          g_print("Found normal h264 data in function sink_setcaps loop_count = %d \n",loop_count);
          g_print("Allocated buffer with total size for non-MVC case = %d \n",total_size);
#endif
          final = gst_buffer_new_and_alloc(total_size);
          memcpy(final->data,pOut,total_size);
          free(pOut);

          omx_h264dec->codec_data = final;
          omx_h264dec->first_idr = 1;
          gst_buffer_ref(omx_h264dec->codec_data);

      }
      else if(codec_data )
      {
          // There is MVC data present in the stream and we have not yet parsed it
          // So repeat the loop
#ifdef DEBUG_LEVEL1
          g_print("Found MVC data in function sink_setcaps loop_count = %d \n",loop_count);
#endif
          omx_h264dec->h264_mvc_content_flag = TRUE;
          goto parse_sps_pps;
      }
    }
    else if((loop_count==2) && (omx_h264dec->h264_mvc_content_flag == TRUE))
    {
      // MVC data has been parsed and appended to the normal data so now
      // assign it to the final data
#ifdef DEBUG_LEVEL1
      gint i;
#endif


#ifdef DEBUG_LEVEL1
      g_print("Allocated buffer with total size for MVC case = %d,loop_count=%d \n",total_size,loop_count);
#endif

      final = gst_buffer_new_and_alloc(total_size);
      memcpy(final->data,pOut,total_size);
      free(pOut);

#ifdef DEBUG_LEVEL1
      for(i = 0; i< total_size; i++ )
        g_print("%x \n",(final->data[i]));
#endif

      omx_h264dec->codec_data = final;
      omx_h264dec->first_idr = 1;
      gst_buffer_ref(omx_h264dec->codec_data);
      self->stereo_mode_flag = TRUE;

    }
  }
done:

  /* Input port configuration. */
  {
    param.nPortIndex = 0;
    OMX_GetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
    param.format.video.nFrameWidth = width;
    param.format.video.nFrameHeight = height;

    OMX_SetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
  }
  /* Output port configuration. */
  {
    param.nPortIndex = 1;
    OMX_GetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
    param.format.video.nFrameWidth = width;
    param.format.video.nFrameHeight = height;
    if(omx_h264dec->h264_mvc_content_flag == TRUE)
    {
      param.nBufferCountMin = param.nBufferCountMin << 1;
      param.nBufferCountActual = param.nBufferCountActual << 1;
    }
    OMX_SetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
  }
  gst_object_unref(GST_ELEMENT(omx_h264dec));
  return gst_pad_set_caps (pad, caps);
}

static GstFlowReturn gst_omx_h264dec_pad_chain (GstPad *pad, GstBuffer *buf)
{
  GstOmxBaseFilter *omx_base;
  GstOmxH264Dec *omx_h264dec;
  GstOmxBaseVideoDec *omx_base_dec;
  GstFlowReturn result = GST_FLOW_OK;
  GOmxCore *gomx;
  int nal_header_size = 0;
  int index = 0;
  guint size;
  guint8 *data;
  guint8 *tmp;
  guint8 unit_type;
  guint  nal_size;
  gint   access_unit_delimiter_bytes = 0;

  omx_base = GST_OMX_BASE_FILTER (GST_PAD_PARENT (pad));
  omx_h264dec = GST_OMX_H264DEC(gst_pad_get_parent (pad));
  gomx = (GOmxCore *) omx_base->gomx;

  if (omx_base->currplayback_rate != omx_base->prevplayback_rate) {
    omx_h264dec->sync_to_idrframe = TRUE;
    omx_base->prevplayback_rate = omx_base->currplayback_rate;
  }

  omx_base_dec = (GstOmxBaseVideoDec*) omx_base;

  omx_base_dec->src_caps_change_if_needed(omx_base, buf);

  if(omx_h264dec->base_chain_func) {
    if (omx_h264dec->codec_data == NULL) {
      result = omx_h264dec->base_chain_func(pad,buf);
    }
    else if(buf != NULL) {
      GstBuffer *NalUnitbuf = NULL;
      GstBuffer *framebuffer = NULL;
      data = GST_BUFFER_DATA (buf);
      size = GST_BUFFER_SIZE (buf);
      index = 0;
      do {
        if (omx_h264dec->length_size == 1) {
          nal_size = data[0];
        } else if (omx_h264dec->length_size == 2) {
          nal_size = (*data << 8) | *(data + 1);
        } else {
          nal_size = (*data << 24) | *(data + 1)<<16 | *(data+2)<<8 | *(data +3);
        }

        if(nal_size >= (size - index)) {
          GST_ELEMENT_ERROR(gomx->object,STREAM,DECODE,(NULL),
              ("Received Nal_Size greater than the Input Buffer Size"));
          gst_buffer_unref(buf);
          buf = NULL;
          return GST_FLOW_ERROR;
        }

        data += omx_h264dec->length_size;
        unit_type = *data & 0x1f;
        nal_header_size = (index?3:4);

        if (omx_base->currplayback_rate < 0.0 || omx_base->currplayback_rate > 2.0) {
          access_unit_delimiter_bytes = MAX_DELIMITER_BYTES;
        }

        /* prepend only to the first type 5 NAL unit of an IDR picture */
        if (omx_h264dec->first_idr) {
          NalUnitbuf = gst_buffer_new_and_alloc(nal_size + nal_header_size +(GST_BUFFER_SIZE(omx_h264dec->codec_data)) +access_unit_delimiter_bytes);

          if(omx_h264dec->codec_data) {
#ifdef DEBUG_LEVEL1
            g_print("Inside the case first_idr =1 with unit_type = %u \n", unit_type);
#endif
            memcpy(GST_BUFFER_DATA(NalUnitbuf),GST_BUFFER_DATA(omx_h264dec->codec_data),GST_BUFFER_SIZE(omx_h264dec->codec_data));
            tmp = GST_BUFFER_DATA(NalUnitbuf)+ (GST_BUFFER_SIZE(omx_h264dec->codec_data));

          } else {
            g_print("no codec data!\n");
            return GST_FLOW_ERROR;
          }
          omx_h264dec->first_idr = 0;

        }
        else
        {
          NalUnitbuf = gst_buffer_new_and_alloc(nal_size + nal_header_size + access_unit_delimiter_bytes);
          tmp = GST_BUFFER_DATA(NalUnitbuf);
        }

        if(nal_header_size == 4)
        {
          tmp[3]=1; tmp[2]=tmp[1]=tmp[0]=0;
        }
        else
        {
          tmp[2]=1; tmp[1]=tmp[0]=0;
        }

        tmp += nal_header_size;
        memcpy(tmp,data,nal_size);

        GST_BUFFER_TIMESTAMP (NalUnitbuf) = GST_BUFFER_TIMESTAMP (buf);
        /* Drop all the buffers until it syncs to first IDR frame. However send sps and pps params. */
        if (omx_h264dec->sync_to_idrframe && unit_type < 5) {
          gst_buffer_unref(NalUnitbuf);
        } else {
          if ((omx_h264dec->sync_to_idrframe) || (omx_base->currplayback_rate < 0.0 || omx_base->currplayback_rate > 2.0)) {
            gst_buffer_ref(omx_h264dec->codec_data);
            result = omx_h264dec->base_chain_func(pad, omx_h264dec->codec_data);
            if (unit_type == 5) {
              omx_h264dec->sync_to_idrframe = FALSE;
            }
          }

          if (framebuffer) {
            GstBuffer *tmpbuffer = framebuffer;
            framebuffer = gst_buffer_merge(tmpbuffer, NalUnitbuf);
            gst_buffer_unref(tmpbuffer);
            gst_buffer_unref(NalUnitbuf);
          } else {
            framebuffer = NalUnitbuf;
          }
        }

        data += nal_size;
        index += nal_size + omx_h264dec->length_size;
      } while (index < size);

      if (framebuffer) {
        gst_buffer_copy_metadata(framebuffer,buf,GST_BUFFER_COPY_FLAGS | GST_BUFFER_COPY_TIMESTAMPS);
        result = omx_h264dec->base_chain_func(pad, framebuffer);
      }
      gst_buffer_unref(buf);
      buf = NULL;
    }
  } else {
    GST_ELEMENT_INFO(gomx->object,STREAM,DECODE,(NULL),
      ("No PadChain Function"));
  }
  gst_object_unref(GST_ELEMENT(omx_h264dec));
  return result;
}

static void
omx_config(GstOmxBaseFilter *omx_base)
{
  NVX_CONFIG_VIDEO_STEREOINFO StereoInfo;

  OMX_INDEXTYPE eIndex;
  GstOmxBaseVideoDec *omx_base_dec;

  G_OMX_INIT_PARAM(StereoInfo);
  omx_base_dec = (GstOmxBaseVideoDec*) omx_base;

  if(omx_base_dec->stereo_mode_flag == TRUE)
  {
    OMX_GetExtensionIndex(omx_base->gomx->omx_handle, NVX_INDEX_CONFIG_VIDEO_STEREOINFO, &eIndex);
    OMX_GetConfig(omx_base->gomx->omx_handle, eIndex , &StereoInfo);

    StereoInfo.stereoEnable = 1;
    StereoInfo.fpType = 4 ;         // Default = top-bottom
    StereoInfo.contentType = 0;     // unspecified

    OMX_SetConfig(omx_base->gomx->omx_handle, eIndex, &StereoInfo);



#ifdef DEBUG_LEVEL1
    OMX_GetConfig(omx_base->gomx->omx_handle, eIndex , &StereoInfo);


    g_print("After Configuration Stereo Enable = %u \n",(unsigned int)StereoInfo.stereoEnable);
    g_print("After Configuration fptype = %u \n",StereoInfo.fpType);
    g_print("After Configuration contentType = %u \n",StereoInfo.contentType);
#endif
  }

}

static void
type_base_init (gpointer g_class)
{
  GstElementClass *element_class;
  element_class = GST_ELEMENT_CLASS (g_class);

  gst_element_class_set_details_simple (element_class,
      "OpenMAX IL H.264/AVC video decoder",
      "Codec/Decoder/Video",
      "Decodes video in H.264/AVC format with OpenMAX IL", "Felipe Contreras");

  {
    GstPadTemplate *template;

    template = gst_pad_template_new ("sink", GST_PAD_SINK,
        GST_PAD_ALWAYS, generate_sink_template ());

    gst_element_class_add_pad_template (element_class, template);
  }
}

static void
type_class_init (gpointer g_class, gpointer class_data)
{
}

static void
type_instance_init (GTypeInstance * instance, gpointer g_class)
{
  GstOmxBaseVideoDec *omx_base;
  GstOmxBaseFilter *omx_base_filter;
  GstOmxH264Dec *omx_h264dec;
  omx_base = GST_OMX_BASE_VIDEODEC (instance);
  omx_base_filter = GST_OMX_BASE_FILTER (instance);
  omx_h264dec = GST_OMX_H264DEC(instance);
  omx_base->compression_format = OMX_VIDEO_CodingAVC;
  gst_pad_set_setcaps_function (omx_base_filter->sinkpad, sink_setcaps);
  omx_h264dec->base_chain_func = GST_PAD_CHAINFUNC(omx_base_filter->sinkpad);
  gst_pad_set_chain_function (omx_base_filter->sinkpad, gst_omx_h264dec_pad_chain);
  omx_base_filter->omx_config = &omx_config ;
  omx_h264dec->h264_mvc_content_flag = FALSE;
}
