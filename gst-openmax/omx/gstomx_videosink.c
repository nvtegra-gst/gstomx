/*
 * Copyright (C) 2007-2009 Nokia Corporation.
 * Copyright (c) 2009-2011, NVIDIA CORPORATION.  All rights reserved.
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
#include "config.h"
#include "gstomx_videosink.h"
#include "gstomx_base_sink.h"
#include "gstomx.h"

#include <string.h>             /* for strcmp */
#include <gst/interfaces/xoverlay.h>
#include <X11/Xlib.h>

GSTOMX_BOILERPLATE (GstOmxVideoSink, gst_omx_videosink, GstOmxBaseSink,
    GST_OMX_BASE_SINK_TYPE);

enum
{
  ARG_0,
  ARG_X_SCALE,
  ARG_Y_SCALE,
  ARG_ROTATION,
  PROP_DISPLAY,
  ARG_FORCE_ASPECT_RATIO,
  ARG_CONTRAST,
  ARG_BRIGHTNESS,
  ARG_HUE,
  ARG_SATURATION,
};


static GstOmxBaseSinkClass *parent_class = NULL;
static GstStateChangeReturn
  gst_omx_videosink_change_state (GstElement * element, GstStateChange transition);

static GstCaps *
generate_sink_template (void)
{
  GstCaps *caps;
  GstStructure *struc1;
  GstStructure *struc2;

  caps = gst_caps_new_empty ();

  struc1 = gst_structure_new ("video/x-nv-yuv",
       "width", GST_TYPE_INT_RANGE, 16, 4096,
       "height", GST_TYPE_INT_RANGE, 16, 4096,
       "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1, NULL);

  struc2 = gst_structure_new ("video/x-raw-yuv",
      "width", GST_TYPE_INT_RANGE, 16, 4096,
      "height", GST_TYPE_INT_RANGE, 16, 4096,
      "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1, NULL);

  {
    GValue list;
    GValue val;

    list.g_type = val.g_type = 0;

    g_value_init (&list, GST_TYPE_LIST);
    g_value_init (&val, GST_TYPE_FOURCC);

    gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('I', '4', '2', '0'));
    gst_value_list_append_value (&list, &val);

    gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('Y', 'U', 'Y', '2'));
    gst_value_list_append_value (&list, &val);

    gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('U', 'Y', 'V', 'Y'));
    gst_value_list_append_value (&list, &val);

    gst_structure_set_value (struc1, "format", &list);

    gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('N', 'V', 'B', '1'));
    gst_value_list_append_value (&list, &val);

    gst_structure_set_value (struc2, "format", &list);

    g_value_unset (&val);
    g_value_unset (&list);
  }

  gst_caps_append_structure (caps, struc1);
  gst_caps_append_structure (caps, struc2);

  return caps;
}

static void
type_base_init (gpointer g_class)
{
  GstElementClass *element_class;

  element_class = GST_ELEMENT_CLASS (g_class);

  gst_element_class_set_details_simple (element_class,
      "OpenMAX IL videosink element",
      "Video/Sink", "Renders video", "Felipe Contreras");

  {
    GstPadTemplate *template;

    template = gst_pad_template_new ("sink", GST_PAD_SINK,
        GST_PAD_ALWAYS, generate_sink_template ());

    gst_element_class_add_pad_template (element_class, template);
  }
}

static gboolean
setcaps (GstBaseSink * gst_sink, GstCaps * caps)
{
  GstOmxBaseSink *omx_base;
  GstOmxVideoSink *self;
  GOmxCore *gomx;
  GOmxPort *port;

  omx_base = GST_OMX_BASE_SINK (gst_sink);
  self = GST_OMX_VIDEOSINK (gst_sink);
  gomx = (GOmxCore *) omx_base->gomx;

  GST_INFO_OBJECT (omx_base, "setcaps (sink): %" GST_PTR_FORMAT, caps);

  g_return_val_if_fail (gst_caps_get_size (caps) == 1, FALSE);

  {
    GstStructure *structure;
    const GValue *framerate = NULL;
    gint width;
    gint height;
    OMX_COLOR_FORMATTYPE color_format = OMX_COLOR_FormatUnused;

    structure = gst_caps_get_structure (caps, 0);

    gst_structure_get_int (structure, "width", &width);
    gst_structure_get_int (structure, "height", &height);
    port = g_omx_core_get_port (gomx, 0);
    {
      guint32 fourcc;

      framerate = gst_structure_get_value (structure, "framerate");
      if (framerate) {
        self->fps_n = gst_value_get_fraction_numerator (framerate);
        self->fps_d = gst_value_get_fraction_denominator (framerate);
      }

      if (gst_structure_get_fourcc (structure, "format", &fourcc)) {
        switch (fourcc) {
          case GST_MAKE_FOURCC ('I', '4', '2', '0'):
          case GST_MAKE_FOURCC ('N', 'V', 'B', '1'):
            color_format = OMX_COLOR_FormatYUV420PackedPlanar;
            break;
          case GST_MAKE_FOURCC ('Y', 'U', 'Y', '2'):
            color_format = OMX_COLOR_FormatYCbYCr;
            break;
          case GST_MAKE_FOURCC ('U', 'Y', 'V', 'Y'):
            color_format = OMX_COLOR_FormatCbYCrY;
            break;
        }
      }
    }
    if (strcmp (gst_structure_get_name (structure), "video/x-nv-yuv") == 0) {
      port->buffer_type = BUFFER_TYPE_NVBUFFER;

    } else if (strcmp (gst_structure_get_name (structure), "video/x-raw-yuv") == 0) {
        guint32 format;
        gst_structure_get_fourcc (structure, "fourcc", &format);

        if (format != GST_MAKE_FOURCC ('N', 'V', 'B', '1'))
          port->buffer_type = BUFFER_TYPE_RAW;
        else
          port->buffer_type = BUFFER_TYPE_NVBUFFER;
    }

    {
      OMX_PARAM_PORTDEFINITIONTYPE param;

      G_OMX_INIT_PARAM (param);

      param.nPortIndex = omx_base->in_port->port_index;
      OMX_GetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);

      switch (color_format) {
        case OMX_COLOR_FormatYUV420PackedPlanar:
          param.nBufferSize = (width * height * 3)/2;
          break;
        case OMX_COLOR_FormatYCbYCr:
        case OMX_COLOR_FormatCbYCrY:
          param.nBufferSize = (width * height * 2);
          break;
        default:
          param.nBufferSize = (width * height * 3)/2;
          break;
      }

      param.format.video.nFrameWidth = width;
      param.format.video.nFrameHeight = height;
      param.format.video.eCompressionFormat = OMX_VIDEO_CodingUnused;
      param.format.video.eColorFormat = color_format;
      param.format.video.xFramerate = 30 << 16;
      param.nBufferSize = (width * height * 3)/2;
      if (framerate) {
        /* convert to Q.16 */
        param.format.video.xFramerate =
            (gst_value_get_fraction_numerator (framerate) << 16) /
            gst_value_get_fraction_denominator (framerate);
      }

      if (gomx->omx_state == OMX_StateExecuting || gomx->omx_state == OMX_StateIdle) {
        g_omx_port_disable(port);
      }

      OMX_SetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
      g_omx_port_setup(port);

      if (gomx->omx_state == OMX_StateExecuting || gomx->omx_state == OMX_StateIdle) {
        g_omx_port_enable(port);
      }
    }

    {
      OMX_CONFIG_ROTATIONTYPE config;

      G_OMX_INIT_PARAM (config);

      config.nPortIndex = omx_base->in_port->port_index;
      OMX_GetConfig (gomx->omx_handle, OMX_IndexConfigCommonScale, &config);

      config.nRotation = self->rotation;

      OMX_SetConfig (gomx->omx_handle, OMX_IndexConfigCommonRotate, &config);
    }

    {
      OMX_CONFIG_SCALEFACTORTYPE config;

      G_OMX_INIT_PARAM (config);

      config.nPortIndex = omx_base->in_port->port_index;
      OMX_GetConfig (gomx->omx_handle, OMX_IndexConfigCommonScale, &config);

      config.xWidth = self->x_scale;
      config.xHeight = self->y_scale;

      OMX_SetConfig (gomx->omx_handle, OMX_IndexConfigCommonScale, &config);
    }
  }

  return TRUE;
}


static void
get_times (GstBaseSink * bsink, GstBuffer * buf,
    GstClockTime * start, GstClockTime * end)
{
  GstOmxVideoSink *self;

  self = GST_OMX_VIDEOSINK (bsink);

  if (GST_BUFFER_TIMESTAMP_IS_VALID (buf)) {
    *start = GST_BUFFER_TIMESTAMP (buf);
    if (GST_BUFFER_DURATION_IS_VALID (buf)) {
      *end = *start + GST_BUFFER_DURATION (buf);
    } else {
      if (self->fps_n > 0) {
        *end = *start +
          gst_util_uint64_scale_int (GST_SECOND, self->fps_d,
              self->fps_n);
      }
    }
  }
}


static void
set_property (GObject * object,
    guint prop_id, const GValue * value, GParamSpec * pspec)
{
  GstOmxVideoSink *self;

  self = GST_OMX_VIDEOSINK (object);

  switch (prop_id) {
    case ARG_X_SCALE:
      self->x_scale = g_value_get_uint (value);
      break;
    case ARG_Y_SCALE:
      self->y_scale = g_value_get_uint (value);
      break;
    case ARG_ROTATION:
      self->rotation = g_value_get_uint (value);
      break;
    case ARG_HUE:
      self->hue = g_value_get_int (value);
      break;
    case ARG_CONTRAST:
      self->contrast = g_value_get_int (value);
      break;
    case ARG_BRIGHTNESS:
      self->brightness = g_value_get_int (value);
      break;
    case ARG_SATURATION:
      self->saturation = g_value_get_int (value);
      break;
    case ARG_FORCE_ASPECT_RATIO:
      self->keep_aspect = g_value_get_boolean (value);
      break;
    case PROP_DISPLAY:
      self->display_name = g_strdup (g_value_get_string (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec)
{
  GstOmxVideoSink *self;

  self = GST_OMX_VIDEOSINK (object);

  switch (prop_id) {
    case ARG_X_SCALE:
      g_value_set_uint (value, self->x_scale);
      break;
    case ARG_Y_SCALE:
      g_value_set_uint (value, self->y_scale);
      break;
    case ARG_ROTATION:
      g_value_set_uint (value, self->rotation);
      break;
    case ARG_HUE:
      g_value_set_int (value, self->hue);
      break;
    case ARG_CONTRAST:
      g_value_set_int (value, self->contrast);
      break;
    case ARG_BRIGHTNESS:
      g_value_set_int (value, self->brightness);
      break;
    case ARG_SATURATION:
      g_value_set_int (value, self->saturation);
      break;
    case ARG_FORCE_ASPECT_RATIO:
      g_value_set_boolean (value, self->keep_aspect);
      break;
    case PROP_DISPLAY:
      g_value_set_string (value, self->display_name);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}


static void
type_class_init (gpointer g_class, gpointer class_data)
{
  GObjectClass *gobject_class;
  GstBaseSinkClass *gst_base_sink_class;
  GstElementClass *gstelement_class;

  gobject_class = (GObjectClass *) g_class;
  gstelement_class = GST_ELEMENT_CLASS (g_class);
  gst_base_sink_class = GST_BASE_SINK_CLASS (g_class);
  parent_class = g_type_class_peek_parent (g_class);

  gst_base_sink_class->set_caps = setcaps;
  gst_base_sink_class->get_times = get_times;

  gstelement_class->change_state = gst_omx_videosink_change_state;

  gobject_class->set_property = set_property;
  gobject_class->get_property = get_property;

  g_object_class_install_property (gobject_class, ARG_X_SCALE,
      g_param_spec_uint ("x-scale", "X Scale",
          "How much to scale the image in the X axis (100 means nothing)",
          0, G_MAXUINT, 100, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, ARG_Y_SCALE,
      g_param_spec_uint ("y-scale", "Y Scale",
          "How much to scale the image in the Y axis (100 means nothing)",
          0, G_MAXUINT, 100, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, ARG_ROTATION,
      g_param_spec_uint ("rotation", "Rotation",
          "Rotation angle",
          0, G_MAXUINT, 360, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_DISPLAY,
      g_param_spec_string ("display", "Display", "X Display name",
          NULL, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, ARG_CONTRAST,
      g_param_spec_int ("contrast", "Contrast", "The contrast of the video",
          -1000, 1000, 0, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, ARG_BRIGHTNESS,
      g_param_spec_int ("brightness", "Brightness",
          "The brightness of the video", -1000, 1000, 0,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, ARG_HUE,
      g_param_spec_int ("hue", "Hue", "The hue of the video", -1000, 1000, 0,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, ARG_SATURATION,
      g_param_spec_int ("saturation", "Saturation",
          "The saturation of the video", -1000, 1000, 0,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, ARG_FORCE_ASPECT_RATIO,
      g_param_spec_boolean ("force-aspect-ratio", "Force aspect ratio",
          "When enabled, scaling will respect original aspect ratio", FALSE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
}

#if 0
static void
omx_videosink_set_xwindow_id (GstXOverlay * overlay, XID xwindow_id)
{
  GstOmxBaseSink *omx_base;
  omx_base = GST_OMX_BASE_SINK (overlay);
}

static void
omx_videosink_expose (GstXOverlay * overlay)
{
  //TODO
  /* Nothing to do as of now */
}

static void
omx_videosink_set_event_handling (GstXOverlay * overlay,
    gboolean handle_events)
{
  //TODO
  //Add event handling functionality
}

static void
omx_videosink_xoverlay_init (GstXOverlayClass * iface)
{
  iface->set_xwindow_id = omx_videosink_set_xwindow_id;
  iface->expose = omx_videosink_expose;
  iface->handle_events = omx_videosink_set_event_handling;
}
#endif

static void
type_instance_init (GTypeInstance * instance, gpointer g_class)
{
  GstOmxBaseSink *omx_base;
  GstOmxVideoSink *omx;

  omx = GST_OMX_VIDEOSINK (instance);
  omx_base = GST_OMX_BASE_SINK (instance);
  omx_base->audio_sink = FALSE;

  gst_base_sink_set_max_lateness (GST_BASE_SINK (omx_base), 20 * GST_MSECOND);
  gst_base_sink_set_qos_enabled (GST_BASE_SINK (omx_base), TRUE);

  omx->fps_n = 0;
  omx->fps_d = 0;

  GST_DEBUG_OBJECT (omx_base, "start");
}


static GstStateChangeReturn
gst_omx_videosink_change_state (GstElement * element, GstStateChange transition)
{
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;
  GstOmxVideoSink* self = GST_OMX_VIDEOSINK (element);

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  switch (transition) {
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      self->fps_n = 0;
      self->fps_d = 1;
      break;

    default:
      break;
  }

  return ret;
}
