/*
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * Based on code copyright/by:
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef GSTOMX_OVERLAYSINK_H
#define GSTOMX_OVERLAYSINK_H

#include <gst/gst.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <stdlib.h>
#include "config.h"
#include "gstoverlay_helper.h"

G_BEGIN_DECLS

#define INDEX_REDIRECT  0
#define INDEX_SRC_RECT  1
#define INDEX_DST_RECT  2
#define INDEX_ROTATION  3
#define INDEX_WM_DELETE_WINDOW 4
#define INDEX_WM_PROTOCOLS 5
#define NUM_INDICES (INDEX_WM_PROTOCOLS+1)

#define GST_OMX_OVERLAYSINK(obj) (GstOmxOverlaySink *) (obj)
#define GST_OMX_OVERLAYSINK_TYPE (gst_omx_overlaysink_get_type ())

typedef struct GstOmxOverlaySink GstOmxOverlaySink;
typedef struct GstOmxOverlaySinkClass GstOmxOverlaySinkClass;

#include "gstomx_base_sink.h"

struct GstOmxOverlaySink
{
    GstOmxBaseSink omx_base;
    guint x_scale;
    guint y_scale;
    guint rotation;

    gchar *display_name;
    gboolean keep_aspect;
    double hue;
    double contrast;
    double brightness;
    double saturation;

    double current_hue;
    double current_contrast;
    double current_brightness;
    double current_saturation;

    guint adaptor_no;

    Window client_window;
    Display *display;
    NvOverlayWindowType client_window_type;
    gboolean    overlay_engine_running;
    gboolean    handle_events;
    gboolean    using_external_xwindowid;
    GOmxPropertyChangeFlags    property_change_flags;

    /* Framerate numerator and denominator */
    gint fps_n;
    gint fps_d;
    gboolean is_frame_dropped;

    gint pixel_aspect_ratio_num;
    gint pixel_aspect_ratio_denom;
};

struct GstOmxOverlaySinkClass
{
    GstOmxBaseSinkClass parent_class;
};

GType gst_omx_overlaysink_get_type (void);

G_END_DECLS

#endif /* GSTOMX_OVERLAYSINK_H */
