/*
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * Based on code copyright/by:
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
#ifndef _GSTOMX_NV_UTILS_H_
#define _GSTOMX_NV_UTILS_H_

#include<gst/gst.h>
#include <OMX_Core.h>
#include <OMX_Component.h>
#include "config.h"
#ifdef TEGRA
#include "NVOMX_IndexExtensions.h"
#define IEEE_SGL_EXPO_BIAS 127
#endif

#include "gstomx_util.h"
#define STEREO_TOP_BOTTOM       0x05
#define STEREO_SIDE_BY_SIDE     0x06

/*** NVOMX_BUFFER ****/

#define GST_TYPE_NVOMX_BUFFER (gst_nvomx_buffer_get_type())
#define GST_IS_NVOMX_BUFFER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_NVOMX_BUFFER))
#define GST_NVOMX_BUFFER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_NVOMX_BUFFER, GstNvOmxBuffer))
#define GST_NVOMX_BUFFER_CAST(obj) ((GstNvOmxBuffer *)(obj))

#define NV_WHOLE_TO_FX(i)      ((i) << 16)
#define NV_FX_TO_WHOLE(i)      ((i) >> 16)

typedef struct _GstNvOmxBuffer GstNvOmxBuffer;
typedef struct _GstNvOmxBufferClass GstNvOmxBufferClass;

struct _GstNvOmxBuffer {
  GstBuffer buffer;

  OMX_BUFFERHEADERTYPE* omxbuf;
  GOmxPort* omxport;

  OMX_U32     colorformat;
  eFrameType  frametype;
  gint        datatype;
  OMX_U32     sourcewidth;
  OMX_U32     sourceheight;
};

GstNvOmxBuffer* gst_nvomx_buffer_new (OMX_BUFFERHEADERTYPE* buf, GOmxPort* port);
void gst_nvomx_buffer_class_ref (void);
GType gst_nvomx_buffer_get_type (void);

/*** NVOMX_BUFFER ****/

OMX_ERRORTYPE gstomx_use_nvbuffer_extension(OMX_HANDLETYPE handle, guint index);
OMX_ERRORTYPE gstomx_use_nvrmsurf_extension(OMX_HANDLETYPE handle);
OMX_ERRORTYPE gstomx_use_deinterlace_extension(OMX_HANDLETYPE handle, guint deinterlace_type);
OMX_ERRORTYPE gstomx_use_aspectratio_extension (OMX_HANDLETYPE omx_handle, gboolean keep_aspect, guint pixel_aspect_ratio_num, guint pixel_aspect_ratio_denom);
OMX_ERRORTYPE gstomx_set_decoder_property (OMX_HANDLETYPE omx_handle);
OMX_ERRORTYPE gstomx_set_video_encoder_property (OMX_HANDLETYPE omx_handle);
OMX_ERRORTYPE gstomx_set_video_encoder_ratecontrolmode (OMX_HANDLETYPE omx_handle, guint rcmode);
OMX_ERRORTYPE gstomx_set_video_encoder_temporaltradeoff (OMX_HANDLETYPE omx_handle);
OMX_ERRORTYPE gstomx_set_decoder_property_only_key_frame_decode (OMX_HANDLETYPE omx_handle, OMX_BOOL set_prop);
OMX_ERRORTYPE gstomx_set_audiodecoder_max_channels_supported (OMX_HANDLETYPE omx_handle, OMX_U32 num_channels);
OMX_ERRORTYPE gstomx_set_FilterTimestamp (OMX_HANDLETYPE omx_handle);
gint NvSFxFloat2Fixed (gfloat f);
gfloat NvSFxFixed2Float (gint x);

#endif
