/*
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * Based on code copyright/by:
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef GSTOMX_MIXER_H
#define GSTOMX_MIXER_H

#include "gstomx_util.h"
#include <gst/gst.h>

G_BEGIN_DECLS

#define GST_OMX_VIDEOMIXER(obj) (GstOmxVideoMixer *) (obj)
#define GST_OMX_VIDEOMIXER_TYPE (gst_omx_videomixer_get_type ())

typedef struct GstOmxVideoMixer GstOmxVideoMixer;
typedef struct GstOmxVideoMixerClass GstOmxVideoMixerClass;


struct GstOmxVideoMixer
{
    GstOmxBaseFilter omx_base;
    gint width;
    gint height;
    double hue;
    double contrast;
    double brightness;
    double saturation;
    
    eDeinterlaceType deinterlacetype;
    eFrameType       frametype;

    double current_contrast;
    double current_brightness;
    double current_saturation;
    double current_hue;

    gboolean bvideodisable;
    gboolean boutputyuv;

    GstPadChainFunction base_chain_func;

    gint framerate_num;
    gint framerate_denom;
    gint pixel_aspect_ratio_num;
    gint pixel_aspect_ratio_denom;
};

struct GstOmxVideoMixerClass
{
    GstOmxBaseFilterClass parent_class;
};

GType gst_omx_videomixer_get_type (void);

G_END_DECLS

#endif /* GSTOMX_MIXER_H */
