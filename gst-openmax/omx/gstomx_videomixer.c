/*
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * Based on code copyright/by:
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
#include "gstomx_base_filter.h"
#include "gstomx_videomixer.h"
#include "gstomx.h"

#ifdef TEGRA
#include "gstomx_nvutils.h"
#endif
#include <gst/video/gstvideosink.h>
#include <string.h>

#define GST_TYPE_OMX_VIDEOMIXER_DEINTERLACE_TYPE (gst_omx_videomixer_deinterlace_get_type())
static GType gst_omx_videomixer_deinterlace_get_type (void);
static GstFlowReturn gst_omx_videomixer_pad_chain (GstPad *pad, GstBuffer *buf);

#define DEFAULT_PROP_CONTRAST   1.0
#define DEFAULT_PROP_BRIGHTNESS 0.0
#define DEFAULT_PROP_HUE        0.0
#define DEFAULT_PROP_SATURATION 1.0
static GstOmxBaseFilterClass *parent_class;

enum
{
    ARG_0,
    ARG_CONTRAST,
    ARG_BRIGHTNESS,
    ARG_HUE,
    ARG_SATURATION,
    ARG_VIDEO_DISABLE,
    ARG_FRAME_TYPE,
    ARG_DEINTERLACE_TYPE,
    ARG_OUTPUT_YUV
};

static GstCaps *
generate_pad_src_template (void)
{
    GstCaps *caps;
    GstStructure *struc;
    GstStructure *struc1;
    GstStructure *struc2;

    caps = gst_caps_new_empty ();

    struc = gst_structure_new ("video/x-raw-gl",
                               "width", GST_TYPE_INT_RANGE, 16, 4096,
                               "height", GST_TYPE_INT_RANGE, 16, 4096,
                               "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1,
                               NULL);

    struc1 = gst_structure_new ("video/x-nv-yuv",
                               "width", GST_TYPE_INT_RANGE, 16, 4096,
                               "height", GST_TYPE_INT_RANGE, 16, 4096,
                               "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1,
                               NULL);
    struc2 = gst_structure_new ("video/x-raw-yuv",
                               "width", GST_TYPE_INT_RANGE, 16, 4096,
                               "height", GST_TYPE_INT_RANGE, 16, 4096,
                               "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1,
                               NULL);

    {
        GValue list;
        GValue val;

        list.g_type = val.g_type = 0;

        g_value_init (&list, GST_TYPE_LIST);
        g_value_init (&val, GST_TYPE_FOURCC);

        gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('R', 'G', 'B', 'A'));
        gst_value_list_append_value (&list, &val);

        gst_structure_set_value (struc, "format", &list);
        g_value_unset (&val);
        g_value_unset (&list);
    }

    {
        GValue list;
        GValue val;

        list.g_type = val.g_type = 0;

        g_value_init (&list, GST_TYPE_LIST);
        g_value_init (&val, GST_TYPE_FOURCC);

        gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('I', '4', '2', '0'));
        gst_value_list_append_value (&list, &val);

        gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('Y', 'U', 'Y', '2'));
        gst_value_list_append_value (&list, &val);

        gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('U', 'Y', 'V', 'Y'));
        gst_value_list_append_value (&list, &val);

        gst_structure_set_value (struc1, "format", &list);

        g_value_unset (&val);
        g_value_unset (&list);
    }
    {
        GValue list;
        GValue val;

        list.g_type = val.g_type = 0;

        g_value_init (&list, GST_TYPE_LIST);
        g_value_init (&val, GST_TYPE_FOURCC);

        gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('I', '4', '2', '0'));
        gst_value_list_append_value (&list, &val);
        gst_structure_set_value (struc2, "format", &list);

        g_value_unset (&val);
        g_value_unset (&list);
    }

    gst_caps_append_structure (caps, struc);
    gst_caps_append_structure (caps, struc1);
    gst_caps_append_structure (caps, struc2);

    return caps;
}

static GstCaps *
generate_pad_sink_template (void)
{
    GstCaps *caps;
    GstStructure *struc;
    GstStructure *struc1;

    caps = gst_caps_new_empty ();

    struc = gst_structure_new ("video/x-nv-yuv",
                               "width", GST_TYPE_INT_RANGE, 16, 4096,
                               "height", GST_TYPE_INT_RANGE, 16, 4096,
                               "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1,
                               NULL);
    struc1 = gst_structure_new ("video/x-raw-yuv",
                               "width", GST_TYPE_INT_RANGE, 16, 4096,
                               "height", GST_TYPE_INT_RANGE, 16, 4096,
                               "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1,
                               NULL);

    {
        GValue list;
        GValue val;

        list.g_type = val.g_type = 0;

        g_value_init (&list, GST_TYPE_LIST);
        g_value_init (&val, GST_TYPE_FOURCC);

        gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('I', '4', '2', '2'));
        gst_value_list_append_value (&list, &val);

        gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('I', '4', '2', '0'));
        gst_value_list_append_value (&list, &val);

        gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('Y', 'U', 'Y', '2'));
        gst_value_list_append_value (&list, &val);

        gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('U', 'Y', 'V', 'Y'));
        gst_value_list_append_value (&list, &val);

        gst_structure_set_value (struc, "format", &list);
        g_value_unset (&val);
        g_value_unset (&list);
    }
    {
        GValue list;
        GValue val;

        list.g_type = val.g_type = 0;

        g_value_init (&list, GST_TYPE_LIST);
        g_value_init (&val, GST_TYPE_FOURCC);

        gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('I', '4', '2', '0'));
        gst_value_list_append_value (&list, &val);

        gst_structure_set_value (struc1, "format", &list);
        g_value_unset (&val);
        g_value_unset (&list);
    }

    gst_caps_append_structure (caps, struc);
    gst_caps_append_structure (caps, struc1);

    return caps;
}


static void
settings_changed_cb (GOmxCore *core)
{
    GstOmxBaseFilter *omx_base;
    GstOmxVideoMixer      *self;
    guint width;
    guint height;
    guint32 format = 0;
    omx_base = core->object;
    self = GST_OMX_VIDEOMIXER (omx_base);
    GST_DEBUG_OBJECT (omx_base, "settings changed for omx mixer");

    {
        OMX_PARAM_PORTDEFINITIONTYPE param;

        memset (&param, 0, sizeof (param));

        param.nSize = sizeof (OMX_PARAM_PORTDEFINITIONTYPE);
        param.nVersion.s.nVersionMajor = 1;
        param.nVersion.s.nVersionMinor = 1;

        param.nPortIndex = 1;
        OMX_GetParameter (omx_base->gomx->omx_handle, OMX_IndexParamPortDefinition, &param);

        width = param.format.video.nFrameWidth;
        height = param.format.video.nFrameHeight;

        if (omx_base->out_port->buffer_type == BUFFER_TYPE_NVBUFFER)
        {
            switch (param.format.video.eColorFormat)
            {
                #ifdef TEGRA
                case  OMX_COLOR_FormatYUV422Planar:
                    format = GST_MAKE_FOURCC ('I', '4', '2', '2'); break;
                #endif
                case OMX_COLOR_FormatYUV420Planar:
                    format = GST_MAKE_FOURCC ('I', '4', '2', '0'); break;
                case OMX_COLOR_FormatYCbYCr:
                    format = GST_MAKE_FOURCC ('Y', 'U', 'Y', '2'); break;
                case OMX_COLOR_FormatCbYCrY:
                    format = GST_MAKE_FOURCC ('U', 'Y', 'V', 'Y'); break;
                default:
                    break;
            }
        }
        else if (omx_base->out_port->buffer_type == BUFFER_TYPE_EGLIMAGE)
        {
            format = GST_MAKE_FOURCC ('R', 'G', 'B', 'A');
        }
        else if (omx_base->out_port->buffer_type == BUFFER_TYPE_RAW)
        {
            format = GST_MAKE_FOURCC ('I', '4', '2', '0');
        }
    }

    {
        GstCaps *new_caps;
        GstStructure *struc = NULL;
        new_caps = gst_caps_new_empty ();
        if (omx_base->out_port->buffer_type == BUFFER_TYPE_NVBUFFER)
        {
            struc = gst_structure_new("video/x-nv-yuv",
                                       "width", G_TYPE_INT, width,
                                       "height", G_TYPE_INT, height,
                                       "format", GST_TYPE_FOURCC, format,
                                       "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1,
                                       NULL);
        }
        else if (omx_base->out_port->buffer_type == BUFFER_TYPE_EGLIMAGE)
        {
            struc = gst_structure_new("video/x-raw-gl",
                                       "width", G_TYPE_INT, width,
                                       "height", G_TYPE_INT, height,
                                       "format", GST_TYPE_FOURCC, format,
                                       "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1,
                                       NULL);
        }
        else if (omx_base->out_port->buffer_type == BUFFER_TYPE_RAW)
        {
            struc = gst_structure_new("video/x-raw-yuv",
                                       "width", G_TYPE_INT, width,
                                       "height", G_TYPE_INT, height,
                                       "format", GST_TYPE_FOURCC, format,
                                       "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1,
                                       NULL);

        }

        if(self->framerate_denom != 0)
        {
               gst_structure_set(struc, "framerate", GST_TYPE_FRACTION,
                               self->framerate_num, self->framerate_denom,
                               NULL);
        }
        else
        {
               gst_structure_set(struc, "framerate", GST_TYPE_FRACTION,
                               0, 1 ,
                               NULL);
        }
       if(self->pixel_aspect_ratio_denom != 0)
        {
             gst_structure_set(struc, "pixel-aspect-ratio", GST_TYPE_FRACTION,
                               self->pixel_aspect_ratio_num, self->pixel_aspect_ratio_denom, NULL);
        }
        else
            gst_structure_set(struc, "pixel-aspect-ratio", GST_TYPE_FRACTION, 1, 1 , NULL);

        gst_caps_append_structure (new_caps, struc);
        GST_INFO_OBJECT (omx_base, "caps are: %" GST_PTR_FORMAT, new_caps);
        gst_pad_set_caps (omx_base->srcpad, new_caps);
    }
}

static void
get_property (GObject *object,
              guint prop_id,
              GValue *value,
              GParamSpec *pspec)
{
    GstOmxVideoMixer *self;

    self = GST_OMX_VIDEOMIXER (object);
    switch (prop_id)
    {
        case ARG_HUE:
              g_value_set_double (value, self->hue);
              break;
        case ARG_CONTRAST:
              g_value_set_double (value, self->contrast);
              break;
        case ARG_BRIGHTNESS:
              g_value_set_double (value, self->brightness);
              break;
        case ARG_SATURATION:
              g_value_set_double (value, self->saturation);
              break;
        case ARG_VIDEO_DISABLE:
              g_value_set_boolean (value,self->bvideodisable);
            break;
        case ARG_DEINTERLACE_TYPE:
            g_value_set_enum(value, self->deinterlacetype);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
type_base_init (gpointer g_class)
{
    GstElementClass *element_class;

    element_class = GST_ELEMENT_CLASS (g_class);

    gst_element_class_set_details_simple (element_class,
                                          "OpenMAX IL Nvidia Mixer component",
                                          "Codec/Decoder/Video",
                                          "Converts YUV420 to RGB or YUV420 with OpenMAX IL",
                                          "Felipe Contreras");

    {
        GstPadTemplate *template;

        template = gst_pad_template_new ("sink", GST_PAD_SINK,
                                         GST_PAD_ALWAYS,
                                         generate_pad_sink_template ());

        gst_element_class_add_pad_template (element_class, template);

        template = gst_pad_template_new ("src", GST_PAD_SRC,
                                         GST_PAD_ALWAYS,
                                         generate_pad_src_template ());

        gst_element_class_add_pad_template (element_class, template);
    }
}


static void
set_property (GObject *object,
              guint prop_id,
              const GValue *value,
              GParamSpec *pspec)
{
    GstOmxVideoMixer *self;
    self = GST_OMX_VIDEOMIXER (object);
    switch (prop_id)
    {
        case ARG_HUE:
              self->hue = g_value_get_double (value);
              break;
        case ARG_CONTRAST:
              self->contrast = g_value_get_double (value);
              break;
        case ARG_BRIGHTNESS:
              self->brightness = g_value_get_double (value);
              break;
        case ARG_SATURATION:
              self->saturation = g_value_get_double (value);
              break;
        case ARG_VIDEO_DISABLE:
            self->bvideodisable = g_value_get_boolean (value);
            break;
        case ARG_DEINTERLACE_TYPE:
            self->deinterlacetype = g_value_get_enum(value);
        break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
type_class_init (gpointer g_class,
                 gpointer class_data)
{
    GObjectClass *gobject_class;
//    GstElementClass *gstelement_class;

    gobject_class = G_OBJECT_CLASS (g_class);
//    gstelement_class = GST_ELEMENT_CLASS (g_class);

    parent_class = g_type_class_ref (GST_OMX_BASE_FILTER_TYPE);

    gobject_class->set_property = set_property;
    gobject_class->get_property = get_property;
    g_object_class_install_property (gobject_class, ARG_CONTRAST,
                                       g_param_spec_double ("contrast", "Contrast", "The contrast of the video",
                                                          0.0, 2.0 , DEFAULT_PROP_CONTRAST, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, ARG_BRIGHTNESS,
                                       g_param_spec_double ("brightness", "Brightness",
                                                         "The brightness of the video", -1, 1, DEFAULT_PROP_BRIGHTNESS,
                                                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
    g_object_class_install_property (gobject_class, ARG_HUE,
                                       g_param_spec_double ("hue", "Hue", "The hue of the video", -1 , 1, DEFAULT_PROP_HUE,
                                                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, ARG_SATURATION,
                                       g_param_spec_double ("saturation", "Saturation",
                                                         "The saturation of the video", 0.0 , 2.0 , DEFAULT_PROP_SATURATION,
                                                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property (gobject_class, ARG_VIDEO_DISABLE,
                                       g_param_spec_boolean ("disablevideo", "disable video display", "Disables the Video Disiplay", FALSE,
                                                          G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class, ARG_DEINTERLACE_TYPE,
                                     g_param_spec_enum ("deint", "deinterlace mode",
                                                        "performs interlacing in BOB or Advanced1 Mode",
                                                        GST_TYPE_OMX_VIDEOMIXER_DEINTERLACE_TYPE, DEINTERLACE_TYPE_BOB, G_PARAM_READWRITE));
}

static void
setup_ports (GstOmxBaseFilter *self)
{
    GOmxCore *core;
    OMX_PARAM_PORTDEFINITIONTYPE param;
    GstOmxVideoMixer *omx_mixer = GST_OMX_VIDEOMIXER (self);
    GstCaps *bufcaps = GST_BUFFER_CAPS(self->buffer_data);
    GstStructure *structure = NULL;
    gboolean     bcropinput = FALSE;
    GstCaps *peercaps = NULL;
    GstStructure *peerstruct = NULL;
    guint framesize;

    structure = gst_caps_get_structure (bufcaps, 0);
    if (structure)
    {
        const GValue *framerate = NULL;
        framerate = gst_structure_get_value (structure, "framerate");
        if (framerate)
        {
            omx_mixer->framerate_num = gst_value_get_fraction_numerator (framerate);
            omx_mixer->framerate_denom = gst_value_get_fraction_denominator (framerate);
        }

        if (!omx_mixer->framerate_num || !omx_mixer->framerate_denom)
        {
            /* send default value of 30 fps */
            omx_mixer->framerate_num = 30;
            omx_mixer->framerate_denom = 1;
        }

        gst_structure_get_int (structure, "width", &omx_mixer->width);
        gst_structure_get_int (structure, "height", &omx_mixer->height);
    }

    core = self->gomx;
    memset (&param, 0, sizeof (param));
    param.nSize = sizeof (OMX_PARAM_PORTDEFINITIONTYPE);
    param.nVersion.s.nVersionMajor = 1;
    param.nVersion.s.nVersionMinor = 1;

    /* Input port configuration. */
    param.nPortIndex = 0;
    OMX_GetParameter (core->omx_handle, OMX_IndexParamPortDefinition, &param);
    self->in_port = g_omx_core_setup_port (core, &param);
    param.format.video.eColorFormat = OMX_COLOR_FormatYUV420Planar;
     /* send it Q16 format */
    param.format.video.xFramerate = ((omx_mixer->framerate_num << 16) / omx_mixer->framerate_denom);
    if(omx_mixer->width && omx_mixer->height) {
        param.format.video.nFrameWidth  = omx_mixer->width;
        param.format.video.nFrameHeight = omx_mixer->height;
    }
    OMX_SetParameter (core->omx_handle, OMX_IndexParamPortDefinition, &param);

#ifdef TEGRA
    omx_mixer->frametype = GST_NVOMX_BUFFER (self->buffer_data)->frametype;

    if (omx_mixer->frametype == FRAME_TYPE_PROGRESSIVE)
    {
        omx_mixer->deinterlacetype = DEINTERLACE_TYPE_NONE;
    }
    gstomx_use_deinterlace_extension(core->omx_handle, omx_mixer->deinterlacetype);
#endif

    /* Output port configuration. */
    param.nPortIndex = 1;
    OMX_GetParameter (core->omx_handle, OMX_IndexParamPortDefinition, &param);

    if (G_UNLIKELY(!omx_mixer->width || !omx_mixer->height))
    {
        bcropinput = FALSE;
        /* FIXME , query output dimensions from display configuration */
        param.format.video.nFrameWidth  = 800;
        param.format.video.nFrameHeight = 480;
    }
    else
    {
        bcropinput = TRUE;
        param.format.video.nFrameWidth  = omx_mixer->width;
        param.format.video.nFrameHeight = omx_mixer->height;
    }

    framesize = param.format.video.nFrameWidth * param.format.video.nFrameHeight * 1.5;
    if (gst_pad_is_linked(self->sinkpad)) {
        peercaps = gst_pad_get_negotiated_caps (self->sinkpad);
        if (peercaps && peercaps->structs->len > 0) {
              peerstruct = gst_caps_get_structure(peercaps, 0);
              if (!strcmp(gst_structure_get_name(peerstruct), "video/x-nv-yuv")) {
#ifdef TEGRA
                gstomx_use_nvbuffer_extension(core->omx_handle, GOMX_PORT_INPUT);
#endif
                self->in_port->buffer_type = BUFFER_TYPE_NVBUFFER;
            }
            else if ( !strcmp(gst_structure_get_name(peerstruct), "video/x-raw-yuv")) {
                self->in_port->buffer_type = BUFFER_TYPE_RAW;
                core->share_input_buffer = TRUE;
            }
        }
        else {
            self->in_port->buffer_type = BUFFER_TYPE_RAW;
            core->share_input_buffer = TRUE;
        }
    }
    if (gst_pad_is_linked(self->srcpad)) {
        peercaps = gst_pad_peer_get_caps (self->srcpad);
        if (peercaps && peercaps->structs->len > 0) {
              peerstruct = gst_caps_get_structure(peercaps, 0);
            if (!strcmp(gst_structure_get_name(peerstruct), "video/x-nv-yuv")) {
                self->out_port->buffer_type = BUFFER_TYPE_NVBUFFER;
                param.format.video.eColorFormat = OMX_COLOR_FormatYUV420Planar;
#ifdef TEGRA
                gstomx_use_nvbuffer_extension(core->omx_handle, GOMX_PORT_OUTPUT);
#endif
            }
            else if ( !strcmp(gst_structure_get_name(peerstruct), "video/x-raw-yuv")) {
                param.format.video.eColorFormat = OMX_COLOR_FormatYUV420Planar;
                self->out_port->buffer_type = BUFFER_TYPE_RAW;
                core->share_output_buffer = TRUE;
                if(param.nBufferSize < framesize)
                      param.nBufferSize = framesize;
            }
            else if (!strcmp(gst_structure_get_name(peerstruct), "video/x-raw-gl")) {
                param.format.video.eColorFormat = OMX_COLOR_Format32bitARGB8888;
                self->out_port->buffer_type = BUFFER_TYPE_EGLIMAGE;
            }
        }
        /* Default to raw yuv */
        else {
                param.format.video.eColorFormat = OMX_COLOR_FormatYUV420Planar;
                self->out_port->buffer_type = BUFFER_TYPE_RAW;
                core->share_output_buffer = TRUE;
                if(param.nBufferSize < framesize)
                    param.nBufferSize = framesize;
        }

    }

    OMX_SetParameter (core->omx_handle, OMX_IndexParamPortDefinition, &param);
    self->out_port = g_omx_core_setup_port (core, &param);

    if (bcropinput)
    {
        OMX_CONFIG_RECTTYPE ocropparam;
        ocropparam.nSize = sizeof (OMX_CONFIG_RECTTYPE);
        ocropparam.nVersion.s.nVersionMajor = 1;
        ocropparam.nVersion.s.nVersionMinor = 1;

        ocropparam.nPortIndex = 0;
        ocropparam.nLeft   = 0;
        ocropparam.nTop    = 0;
        ocropparam.nWidth  = omx_mixer->width;
        ocropparam.nHeight = omx_mixer->height;
        OMX_SetConfig(core->omx_handle, OMX_IndexConfigCommonInputCrop, &ocropparam);
    }

    if(self->out_port->buffer_type == BUFFER_TYPE_EGLIMAGE)
    {
#ifdef USE_EGLIMAGE
        GstQuery *query;
        GstStructure * querystruct;
        querystruct = gst_structure_new("query_struct","display_data",G_TYPE_POINTER,NULL,NULL);
        query = gst_query_new_application(GST_QUERY_CUSTOM, querystruct);
        gst_pad_peer_query(self->srcpad, query);
        gst_structure_get(querystruct, "display_data", G_TYPE_POINTER ,&self->gomx->display_data, NULL);
        gst_query_unref(query);
#else
        g_error("EGL Image Display Info could not be found \n");
#endif
    }

}

static GstFlowReturn gst_omx_videomixer_pad_chain (GstPad *pad, GstBuffer *buf)
{
    GstOmxBaseFilter *omx_base;
    GstOmxVideoMixer *omx_mixer;
    GOmxCore *gomx;
    GstCaps *bufcaps = GST_BUFFER_CAPS(buf);
    GstStructure *bufstruct = NULL;
    GstFlowReturn result = GST_FLOW_ERROR;

    omx_base = GST_OMX_BASE_FILTER (GST_PAD_PARENT (pad));
    omx_mixer = GST_OMX_VIDEOMIXER(gst_pad_get_parent (pad));

    gomx = omx_base->gomx;

    if (bufcaps)
    {
        bufstruct = gst_caps_get_structure (bufcaps, 0);
    }

    if (bufstruct)
    {
        const GValue *gvalue_pixel_aspect_ratio = NULL;

        gvalue_pixel_aspect_ratio = gst_structure_get_value(bufstruct, "pixel-aspect-ratio");

        if (gvalue_pixel_aspect_ratio)
        {
            if ((omx_mixer->pixel_aspect_ratio_num   != gst_value_get_fraction_numerator(gvalue_pixel_aspect_ratio)) ||
                (omx_mixer->pixel_aspect_ratio_denom != gst_value_get_fraction_denominator(gvalue_pixel_aspect_ratio)))
            {
                omx_mixer->pixel_aspect_ratio_num   = gst_value_get_fraction_numerator(gvalue_pixel_aspect_ratio);
                omx_mixer->pixel_aspect_ratio_denom = gst_value_get_fraction_denominator(gvalue_pixel_aspect_ratio);
                omx_base->update_src_caps = TRUE;
            }
        }
    }

    if (omx_mixer->current_brightness != omx_mixer->brightness)
    {
        OMX_CONFIG_BRIGHTNESSTYPE brightnessStruct;
        OMX_GetConfig(gomx->omx_handle,  OMX_IndexConfigCommonBrightness, &brightnessStruct);
        brightnessStruct.nBrightness =  0 + ((1.0 + omx_mixer->brightness)/2.0 * 100);
        OMX_SetConfig(gomx->omx_handle, OMX_IndexConfigCommonBrightness, &brightnessStruct);
        omx_mixer->current_brightness = omx_mixer->brightness;
    }

    if (omx_mixer->current_contrast != omx_mixer->contrast)
    {
        OMX_CONFIG_CONTRASTTYPE contrastStruct;
        OMX_GetConfig(gomx->omx_handle,  OMX_IndexConfigCommonContrast, &contrastStruct);
        contrastStruct.nContrast = -100 + (omx_mixer->contrast/2.0 * 200);
        OMX_SetConfig(gomx->omx_handle, OMX_IndexConfigCommonContrast, &contrastStruct);
        omx_mixer->current_contrast = omx_mixer->contrast;
    }

    if (omx_mixer->current_saturation != omx_mixer->saturation)
    {
        OMX_CONFIG_SATURATIONTYPE saturationStruct;
        OMX_GetConfig(gomx->omx_handle,  OMX_IndexConfigCommonSaturation, &saturationStruct);
        saturationStruct.nSaturation = -100 + (omx_mixer->saturation/2.0 * 200);
        OMX_SetConfig(gomx->omx_handle, OMX_IndexConfigCommonSaturation, &saturationStruct);
        omx_mixer->current_saturation = omx_mixer->saturation;
    }

    if (omx_mixer->base_chain_func)
    {
        result = omx_mixer->base_chain_func(pad, buf);
    }
    gst_object_unref(GST_ELEMENT(omx_mixer));

    return result;
}

static void
type_instance_init (GTypeInstance *instance,
                    gpointer g_class)
{
    GstOmxVideoMixer *omx_mixer = GST_OMX_VIDEOMIXER (instance);
    GstOmxBaseFilter *omx_base_filter = GST_OMX_BASE_FILTER (instance);
    omx_base_filter->setup_ports = setup_ports;
    omx_mixer->deinterlacetype = DEINTERLACE_TYPE_BOB;

    omx_mixer->brightness = omx_mixer->current_brightness = DEFAULT_PROP_BRIGHTNESS;
    omx_mixer->saturation = omx_mixer->current_saturation = DEFAULT_PROP_SATURATION;
    omx_mixer->contrast   = omx_mixer->current_contrast   = DEFAULT_PROP_CONTRAST;

    omx_mixer->base_chain_func = GST_PAD_CHAINFUNC(omx_base_filter->sinkpad);
    gst_pad_set_chain_function (omx_base_filter->sinkpad, gst_omx_videomixer_pad_chain);

    omx_base_filter->gomx->settings_changed_cb = settings_changed_cb;
}

GType
gst_omx_videomixer_get_type (void)
{
    static GType type = 0;

    if (G_UNLIKELY (type == 0))
    {
        GTypeInfo *type_info;

        type_info = g_new0 (GTypeInfo, 1);
        type_info->class_size = sizeof (GstOmxVideoMixerClass);
        type_info->base_init = type_base_init;
        type_info->class_init = type_class_init;
        type_info->instance_size = sizeof (GstOmxVideoMixer);
        type_info->instance_init = type_instance_init;

        type = g_type_register_static (GST_OMX_BASE_FILTER_TYPE, "GstOmxVideoMixer", type_info, 0);

        g_free (type_info);
    }

    return type;
}

static GType
gst_omx_videomixer_deinterlace_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static GEnumValue vals[] =
        {
            {DEINTERLACE_TYPE_BOB,        "Bob filter",             "Deinterlace with Bob Filter"},
            {DEINTERLACE_TYPE_ADVANCED1,   "Advanced1 filter",        "Deinterlace with Advanced1 filter"},
            {0, NULL, NULL},
        };

        type = g_enum_register_static ("GstOmxVideoMixerFilterMode", vals);
    }

    return type;
}
