/*
 * Copyright (c) 2011-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * Based on code copyright/by:
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
#include "config.h"
#include "gstomx_overlaysink.h"
#include "gstomx_base_sink.h"
#include "gstomx.h"
#include <gst/interfaces/xoverlay.h>
#include <string.h> /* for memset, strcmp */
#ifdef TEGRA
#include "gstomx_nvutils.h"
#endif

#define DEFAULT_PROP_CONTRAST   1.0
#define DEFAULT_PROP_BRIGHTNESS 0.0
#define DEFAULT_PROP_HUE        0.0
#define DEFAULT_PROP_SATURATION 1.0

static GstOmxBaseSinkClass *parent_class = NULL;

enum
{
    ARG_0,
    ARG_X_SCALE,
    ARG_Y_SCALE,
    ARG_ROTATION,
    PROP_DISPLAY,
    ARG_FORCE_ASPECT_RATIO,
    ARG_CONTRAST,
    ARG_BRIGHTNESS,
    ARG_HUE,
    ARG_SATURATION,
    ARG_HANDLE_EVENTS,
    ARG_X_WINDOW_ID,
    PROP_HANDLE_EVENTS
};

static GstCaps *
generate_sink_template (void)
{
    GstCaps *caps;
    GstStructure *struc;

    caps = gst_caps_new_empty ();
    struc = gst_structure_new ("video/x-nv-yuv",
                               "width", GST_TYPE_INT_RANGE, 16, 4096,
                               "height", GST_TYPE_INT_RANGE, 16, 4096,
                               "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1,
                                NULL);

    {
        GValue list;
        GValue val;

        list.g_type = val.g_type = 0;

        g_value_init (&list, GST_TYPE_LIST);
        g_value_init (&val, GST_TYPE_FOURCC);

        gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('I', '4', '2', '0'));
        gst_value_list_append_value (&list, &val);

        gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('Y', 'U', 'Y', '2'));
        gst_value_list_append_value (&list, &val);

        gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('U', 'Y', 'V', 'Y'));
        gst_value_list_append_value (&list, &val);

        gst_structure_set_value (struc, "format", &list);

        g_value_unset (&val);
        g_value_unset (&list);
    }

    gst_caps_append_structure (caps, struc);

    return caps;
}

static void
type_base_init (gpointer g_class)
{
    GstElementClass *element_class;

    element_class = GST_ELEMENT_CLASS (g_class);

    gst_element_class_set_details_simple (element_class,
                                          "OpenMAX IL overlaysink element",
                                          "Video/Sink",
                                          "Renders video",
                                          "Felipe Contreras");

    {
        GstPadTemplate *template;

        template = gst_pad_template_new ("sink", GST_PAD_SINK,
                                         GST_PAD_ALWAYS,
                                         generate_sink_template ());

        gst_element_class_add_pad_template (element_class, template);
    }
}

static gboolean
setcaps (GstBaseSink *gst_sink,
         GstCaps *caps)
{
    GstOmxBaseSink *omx_base;
    GstOmxOverlaySink *self;
    GOmxCore *gomx;
    GOmxPort *port;
    omx_base = GST_OMX_BASE_SINK (gst_sink);
    self = GST_OMX_OVERLAYSINK (gst_sink);
    gomx = (GOmxCore *) omx_base->gomx;
    GST_INFO_OBJECT (omx_base, "setcaps (sink): %" GST_PTR_FORMAT, caps);

    g_return_val_if_fail (gst_caps_get_size (caps) == 1, FALSE);

    {
        GstStructure *structure;
        const GValue *framerate = NULL;
        const GValue *pixel_aspect_ratio = NULL;
        gint width;
        gint height;

        OMX_COLOR_FORMATTYPE color_format = OMX_COLOR_FormatUnused;
        structure = gst_caps_get_structure (caps, 0);

        gst_structure_get_int (structure, "width", &width);
        gst_structure_get_int (structure, "height", &height);
        port = g_omx_core_get_port (gomx, 0);

        if (strcmp (gst_structure_get_name (structure), "video/x-nv-yuv") == 0)
        {
            guint32 fourcc;
            framerate = gst_structure_get_value (structure, "framerate");

            self->fps_n = gst_value_get_fraction_numerator (framerate);
            self->fps_d = gst_value_get_fraction_denominator (framerate);

            if (gst_structure_get_fourcc (structure, "format", &fourcc))
            {
                switch (fourcc)
                {
                    case GST_MAKE_FOURCC ('I', '4', '2', '0'):
                        color_format = OMX_COLOR_FormatYUV420Planar;
                        break;
                    case GST_MAKE_FOURCC ('Y', 'U', 'Y', '2'):
                        color_format = OMX_COLOR_FormatYCbYCr;
                        break;
                    case GST_MAKE_FOURCC ('U', 'Y', 'V', 'Y'):
                        color_format = OMX_COLOR_FormatCbYCrY;
                        break;
                }
            }

            pixel_aspect_ratio = gst_structure_get_value (structure, "pixel-aspect-ratio");

            if (pixel_aspect_ratio)
            {
                self->pixel_aspect_ratio_num  = gst_value_get_fraction_numerator (pixel_aspect_ratio);
                self->pixel_aspect_ratio_denom = gst_value_get_fraction_denominator (pixel_aspect_ratio);
            }

            self->property_change_flags |= GOMX_PROPERTY_CHANGE_FLAGS_KEEPASPECT;
        }

        {
            OMX_PARAM_PORTDEFINITIONTYPE param;
            memset (&param, 0, sizeof (param));
            param.nSize = sizeof (OMX_PARAM_PORTDEFINITIONTYPE);
            param.nVersion.s.nVersionMajor = 1;
            param.nVersion.s.nVersionMinor = 1;

            param.nPortIndex = 0;
            OMX_GetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);

            switch (color_format)
            {
                case OMX_COLOR_FormatYUV420Planar:
                    param.nBufferSize = (width * height * 3)/2;
                    break;
                case OMX_COLOR_FormatYCbYCr:
                case OMX_COLOR_FormatCbYCrY:
                    param.nBufferSize = (width * height * 2);
                    break;
                default:
                    param.nBufferSize = (width * height * 3) / 2;
                  break;
            }
            param.format.video.nFrameWidth = width;
            param.format.video.nFrameHeight = height;
            param.format.video.eCompressionFormat = OMX_VIDEO_CodingUnused;
            param.format.video.eColorFormat = color_format;
            param.format.video.xFramerate = 30 << 16;
            param.nBufferSize = (width * height * 3)/2;
            if (framerate)
            {
                /* convert to Q.16 */
                param.format.video.xFramerate =
                    (gst_value_get_fraction_numerator (framerate) << 16) /
                    gst_value_get_fraction_denominator (framerate);
            }

            if (gomx->omx_state == OMX_StateExecuting || gomx->omx_state == OMX_StateIdle)
            {
                g_omx_port_disable(port);
            }

            OMX_SetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);

            g_omx_port_setup(port);

            //set_tunnelling_mode(omx_base);
            if (gomx->omx_state == OMX_StateExecuting || gomx->omx_state == OMX_StateIdle)
            {
                g_omx_port_enable(port);
            }
        }

    }
    return TRUE;
}



static void
get_times (GstBaseSink * bsink, GstBuffer * buf,
    GstClockTime * start, GstClockTime * end)
{
  GstOmxOverlaySink *self;

  self = GST_OMX_OVERLAYSINK (bsink);

  if (GST_BUFFER_TIMESTAMP_IS_VALID (buf)) {
    *start = GST_BUFFER_TIMESTAMP (buf);
    if (GST_BUFFER_DURATION_IS_VALID (buf)) {
      *end = *start + GST_BUFFER_DURATION (buf);
    } else {
      if (self->fps_n > 0) {
        *end = *start +
          gst_util_uint64_scale_int (GST_SECOND, self->fps_d,
              self->fps_n);
      }
    }
  }
}


static void
set_property (GObject *object,
              guint prop_id,
              const GValue *value,
              GParamSpec *pspec)
{
    GstOmxOverlaySink *self;

    self = GST_OMX_OVERLAYSINK (object);
    switch (prop_id)
    {
        case PROP_HANDLE_EVENTS:
            self->handle_events = g_value_get_boolean (value);
            break;
        case ARG_X_WINDOW_ID:
            self->client_window = g_value_get_uint(value);
            break;
        case ARG_X_SCALE:
            self->x_scale = g_value_get_uint (value);
            break;
        case ARG_Y_SCALE:
            self->y_scale = g_value_get_uint (value);
            break;
        case ARG_ROTATION:
            self->rotation = g_value_get_uint (value);
            break;
        case ARG_HUE:
              self->hue = g_value_get_double (value);
              break;
        case ARG_CONTRAST:
              self->contrast = g_value_get_double (value);
              break;
        case ARG_BRIGHTNESS:
              self->brightness = g_value_get_double (value);
              break;
        case ARG_SATURATION:
              self->saturation = g_value_get_double (value);
              break;
         case ARG_FORCE_ASPECT_RATIO:
              self->keep_aspect = g_value_get_boolean (value);
              self->property_change_flags |= GOMX_PROPERTY_CHANGE_FLAGS_KEEPASPECT;
              break;
        case PROP_DISPLAY:
              self->display_name = g_strdup (g_value_get_string (value));
              break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
get_property (GObject *object,
              guint prop_id,
              GValue *value,
              GParamSpec *pspec)
{
    GstOmxOverlaySink *self;

    self = GST_OMX_OVERLAYSINK (object);
    switch (prop_id)
    {
        case PROP_HANDLE_EVENTS:
            self->handle_events = TRUE;
            g_value_set_boolean (value, self->handle_events);
            break;
        case ARG_X_WINDOW_ID:
            g_value_set_boolean (value, self->client_window);
            break;
        case ARG_X_SCALE:
            g_value_set_uint (value, self->x_scale);
            break;
        case ARG_Y_SCALE:
            g_value_set_uint (value, self->y_scale);
            break;
        case ARG_ROTATION:
            g_value_set_uint (value, self->rotation);
            break;
        case ARG_HUE:
              g_value_set_double (value, self->hue);
              break;
        case ARG_CONTRAST:
              g_value_set_double (value, self->contrast);
              break;
        case ARG_BRIGHTNESS:
              g_value_set_double (value, self->brightness);
              break;
        case ARG_SATURATION:
              g_value_set_double (value, self->saturation);
              break;
          case ARG_FORCE_ASPECT_RATIO:
              g_value_set_boolean (value, self->keep_aspect);
              break;
        case PROP_DISPLAY:
              g_value_set_string (value, self->display_name);
              break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
setup_ports (GstOmxBaseSink *self)
{
    GOmxCore *core;
    OMX_PARAM_PORTDEFINITIONTYPE param;
    core = self->gomx;

    memset (&param, 0, sizeof (param));
    param.nSize = sizeof (OMX_PARAM_PORTDEFINITIONTYPE);
    param.nVersion.s.nVersionMajor = 1;
    param.nVersion.s.nVersionMinor = 1;

    /* Input port configuration. */
    param.nPortIndex = 0;
    OMX_GetParameter (core->omx_handle, OMX_IndexParamPortDefinition, &param);
    self->in_port = g_omx_core_setup_port (core, &param);
    gst_pad_set_element_private (self->sinkpad, self->in_port);
    self->in_port->buffer_type = BUFFER_TYPE_NVBUFFER;
#ifdef TEGRA
    gstomx_use_nvbuffer_extension(core->omx_handle, GOMX_PORT_INPUT);
#endif

}

static gboolean
start (GstBaseSink *gst_base)
{
    GstOmxBaseSink *self;
    ListElement *element = NULL;
    XSetWindowAttributes winAttribs;
    GstOmxOverlaySink *omxoverlaysink = GST_OMX_OVERLAYSINK (gst_base);
    self = GST_OMX_BASE_SINK (gst_base);
    GST_LOG_OBJECT (self, "begin");

    setup_ports (self);

    GST_LOG_OBJECT (self, "end");

    //default overlay is running
    omxoverlaysink->client_window_type =  NV_OVERLAY_WINDOW_TYPE_OVERLAY;

    omxoverlaysink->display = InitDisplay();
    if (!omxoverlaysink->display)
    {
        return FALSE;
    }

    if (!omxoverlaysink->client_window)
    {
        winAttribs.event_mask = NoEventMask;
        winAttribs.do_not_propagate_mask = NoEventMask;

        omxoverlaysink->client_window = XCreateWindow(omxoverlaysink->display,
                                                      XDefaultRootWindow(omxoverlaysink->display),
                                                      0, 0, 800, 480, 0,
                                                      0,
                                                      InputOnly,
                                                      CopyFromParent,
                                                      CWEventMask | CWDontPropagate,
                                                      &winAttribs);

        if (!omxoverlaysink->client_window)
        {
            return FALSE;
        }

        XMapWindow(omxoverlaysink->display, omxoverlaysink->client_window);
    }
    else
    {
        //The window should have been already passed by QT.
    }

    AddWindow(omxoverlaysink->client_window);

    element = FindElement(omxoverlaysink->client_window);

    if (element)
    {
        /*For now, default the Element Settings to Overlay */
        element->info.type = NV_OVERLAY_WINDOW_TYPE_OVERLAY;
    }

    return TRUE;
}

static GstFlowReturn
render (GstBaseSink *gst_base,
        GstBuffer *buf)
{
    GstOmxBaseSink *self;
    GstOmxOverlaySink *omxoverlaysink = GST_OMX_OVERLAYSINK(gst_base);
    GOmxCore *gomx;
    NvOverlayWindowInfo *client_window_info = NULL;
    GstFlowReturn err = GST_FLOW_OK;

    self = GST_OMX_BASE_SINK (gst_base);

    gomx = self->gomx;

    if (omxoverlaysink->client_window)
    {
          client_window_info = NvOverlayGetWindowInfo(omxoverlaysink->client_window);
    }

     if (client_window_info)
     {
         if (client_window_info->type == NV_OVERLAY_WINDOW_TYPE_NOT_OVERLAY)
         {
             omxoverlaysink->client_window_type = NV_OVERLAY_WINDOW_TYPE_NOT_OVERLAY;
         }
         else
         {
             omxoverlaysink->client_window_type = NV_OVERLAY_WINDOW_TYPE_OVERLAY;
         }
     }
     else
     {
         omxoverlaysink->client_window_type = NV_OVERLAY_WINDOW_TYPE_NOT_OVERLAY;
     }

#if 0
    if (omxoverlaysink->current_brightness != omxoverlaysink->brightness)
    {
        OMX_CONFIG_BRIGHTNESSTYPE brightnessStruct;
        brightnessStruct.nBrightness =  0 + ((1.0 + omxoverlaysink->brightness)/2.0 * 100);
        OMX_SetConfig(gomx->omx_handle, OMX_IndexConfigCommonBrightness, &brightnessStruct);
        omxoverlaysink->current_brightness = omxoverlaysink->brightness;
    }

    if (omxoverlaysink->current_contrast != omxoverlaysink->contrast)
    {
        OMX_CONFIG_CONTRASTTYPE contrastStruct;
        contrastStruct.nContrast = -100 + (omxoverlaysink->contrast/2.0 * 200);
        OMX_SetConfig(gomx->omx_handle, OMX_IndexConfigCommonContrast, &contrastStruct);
        omxoverlaysink->current_contrast = omxoverlaysink->contrast;
    }

    if (omxoverlaysink->current_saturation != omxoverlaysink->saturation)
    {
        OMX_CONFIG_SATURATIONTYPE saturationStruct;
        saturationStruct.nSaturation = -100 + (omxoverlaysink->saturation/2.0 * 100);
        OMX_SetConfig(gomx->omx_handle, OMX_IndexConfigCommonSaturation, &saturationStruct);
        omxoverlaysink->current_saturation = omxoverlaysink->saturation;
    }
#endif

 //   if (omxoverlaysink->client_window_info.type == NV_OVERLAY_WINDOW_TYPE_NOT_OVERLAY)
    if (omxoverlaysink->client_window_type == NV_OVERLAY_WINDOW_TYPE_NOT_OVERLAY)
    {
        if (omxoverlaysink->overlay_engine_running)
        {
            g_omx_core_stop(gomx);
            g_omx_core_unload(gomx);
        }
        omxoverlaysink->overlay_engine_running = FALSE;
    }
    else
    {
        //ListElement *window_element = NULL;
        if (GST_BASE_SINK_CLASS(parent_class)->render)
        {
            err = GST_BASE_SINK_CLASS(parent_class)->render(gst_base, buf);
            omxoverlaysink->overlay_engine_running = TRUE;
        }
          /* a) Make sure we do setconfig after render call, In case overlay engine is not running,
            The render call to base_sink shall make openmax to running  state

//        if (omxoverlaysink->current_brightness != omxoverlaysink->brightness)
            b) Its possible that color control request comes from two different control paths,
               one is through gstreamer and other is through Manager, When both of them conflict,
               Manager is given higher priority to set the brightness. But It is assumed here that
               Two control paths wouldn't be used at the same time */
         if ((omxoverlaysink->current_brightness != omxoverlaysink->brightness) ||
              client_window_info->brightness_valid)

        {
            OMX_CONFIG_BRIGHTNESSTYPE brightnessStruct;
             if (client_window_info->brightness_valid)
             {
                 omxoverlaysink->current_brightness = client_window_info->brightness;
                 omxoverlaysink->brightness = client_window_info->brightness;
                 client_window_info->brightness_valid = 0;
             }
             else
             {
                 omxoverlaysink->current_brightness = omxoverlaysink->brightness;
             }


            OMX_GetConfig(gomx->omx_handle, OMX_IndexConfigCommonBrightness, &brightnessStruct);
//            brightnessStruct.nBrightness =  0 + ((1.0 + omxoverlaysink->brightness)/2.0 * 100);
            brightnessStruct.nBrightness =  0 + ((1.0 + omxoverlaysink->current_brightness)/2.0 * 100);
            OMX_SetConfig(gomx->omx_handle, OMX_IndexConfigCommonBrightness, &brightnessStruct);
//            omxoverlaysink->current_brightness = omxoverlaysink->brightness;
        }

//        if (omxoverlaysink->current_contrast != omxoverlaysink->contrast)
         if ((omxoverlaysink->current_contrast != omxoverlaysink->contrast) ||
              client_window_info->contrast_valid)
        {
            OMX_CONFIG_CONTRASTTYPE contrastStruct;
             if (client_window_info->contrast_valid)
             {
                 omxoverlaysink->current_contrast = client_window_info->contrast;
                 client_window_info->contrast_valid = 0;
                 omxoverlaysink->contrast = client_window_info->contrast;
             }
             else
             {
                 omxoverlaysink->current_contrast = omxoverlaysink->contrast;
             }

            OMX_GetConfig(gomx->omx_handle, OMX_IndexConfigCommonContrast, &contrastStruct);
//            contrastStruct.nContrast = -100 + (omxoverlaysink->contrast/2.0 * 200);
            contrastStruct.nContrast = -100 + (omxoverlaysink->current_contrast/2.0 * 200);
            OMX_SetConfig(gomx->omx_handle, OMX_IndexConfigCommonContrast, &contrastStruct);
//            omxoverlaysink->current_contrast = omxoverlaysink->contrast;
        }

//        if (omxoverlaysink->current_saturation != omxoverlaysink->saturation)
         if ((omxoverlaysink->current_saturation != omxoverlaysink->saturation) ||
              client_window_info->saturation_valid)
        {
            OMX_CONFIG_SATURATIONTYPE saturationStruct;

             if (client_window_info->saturation_valid)
             {
                 omxoverlaysink->current_saturation = client_window_info->saturation;
                 client_window_info->saturation_valid = 0;
                 omxoverlaysink->saturation = client_window_info->saturation;
             }
             else
             {
                 omxoverlaysink->current_saturation = omxoverlaysink->saturation;
             }


            OMX_GetConfig(gomx->omx_handle, OMX_IndexConfigCommonSaturation, &saturationStruct);
//            saturationStruct.nSaturation = -100 + (omxoverlaysink->saturation/2.0 * 100);
            saturationStruct.nSaturation = -100 + (omxoverlaysink->current_saturation/2.0 * 100);
            OMX_SetConfig(gomx->omx_handle, OMX_IndexConfigCommonSaturation, &saturationStruct);
//            omxoverlaysink->current_saturation = omxoverlaysink->saturation;
        }

//        window_element = FindElement(omxoverlaysink->client_window);

//        if (window_element)
         if (client_window_info->src_rect_valid)
         {
             OMX_CONFIG_RECTTYPE src_rect;
             OMX_GetConfig(gomx->omx_handle, OMX_IndexConfigCommonInputCrop, &src_rect);
             src_rect.nLeft   = client_window_info->src_rect.x;
             src_rect.nTop    = client_window_info->src_rect.y;
             src_rect.nWidth  = client_window_info->src_rect.width;
             src_rect.nHeight = client_window_info->src_rect.height;

             OMX_SetConfig(gomx->omx_handle, OMX_IndexConfigCommonInputCrop, &src_rect);
             client_window_info->src_rect_valid = 0;
         }

        if (client_window_info->dst_rect_valid)
        {
#if 0
            if (window_element->info.src_rect_valid)
            {
                OMX_CONFIG_RECTTYPE src_rect;
                OMX_GetConfig(gomx->omx_handle, OMX_IndexConfigCommonInputCrop, &src_rect);
                src_rect.nLeft   = window_element->info.src_rect.x;
                src_rect.nTop    = window_element->info.src_rect.y;
                src_rect.nWidth  = window_element->info.src_rect.width;
                src_rect.nHeight = window_element->info.src_rect.height;
                OMX_SetConfig(gomx->omx_handle, OMX_IndexConfigCommonInputCrop, &src_rect);
                window_element->info.src_rect_valid = 0;
            }

            if (window_element->info.dst_rect_valid)
            {
                OMX_CONFIG_RECTTYPE dst_rect;
                OMX_GetConfig(gomx->omx_handle, OMX_IndexConfigCommonOutputCrop, &dst_rect);
                dst_rect.nLeft   = window_element->info.dst_rect.x;
                dst_rect.nTop    = window_element->info.dst_rect.y;
                dst_rect.nWidth  = window_element->info.dst_rect.width;
                dst_rect.nHeight = window_element->info.dst_rect.height;
                OMX_SetConfig(gomx->omx_handle, OMX_IndexConfigCommonOutputCrop, &dst_rect);
                window_element->info.dst_rect_valid = 0;
            }
#endif
             OMX_CONFIG_RECTTYPE dst_rect;
             OMX_GetConfig(gomx->omx_handle, OMX_IndexConfigCommonOutputCrop, &dst_rect);
             dst_rect.nLeft   = client_window_info->dst_rect.x;
             dst_rect.nTop    = client_window_info->dst_rect.y;
             dst_rect.nWidth  = client_window_info->dst_rect.width;
             dst_rect.nHeight = client_window_info->dst_rect.height;
             OMX_SetConfig(gomx->omx_handle, OMX_IndexConfigCommonOutputCrop, &dst_rect);
             client_window_info->dst_rect_valid = 0;
        }

        if (omxoverlaysink->property_change_flags & GOMX_PROPERTY_CHANGE_FLAGS_KEEPASPECT)
        {
            gstomx_use_aspectratio_extension(gomx->omx_handle, omxoverlaysink->keep_aspect,
                omxoverlaysink->pixel_aspect_ratio_num, omxoverlaysink->pixel_aspect_ratio_denom);

            omxoverlaysink->property_change_flags &= ~GOMX_PROPERTY_CHANGE_FLAGS_KEEPASPECT;
        }
    }

    return err;
}

static gboolean
stop (GstBaseSink *gst_base)
{
    GstOmxBaseSink *self;
    GstOmxOverlaySink *omxoverlaysink;

    self = GST_OMX_BASE_SINK (gst_base);
    omxoverlaysink = GST_OMX_OVERLAYSINK(gst_base);

    GST_LOG_OBJECT (self, "begin");
    if (omxoverlaysink->client_window)
    {
        RemoveWindow(omxoverlaysink->client_window);
    }

    if (!omxoverlaysink->using_external_xwindowid)
    {
        if (omxoverlaysink->client_window)
        {
            XDestroyWindow((Display *)omxoverlaysink->display,
                           (Window) omxoverlaysink->client_window);
            omxoverlaysink->client_window = 0;
        }

        omxoverlaysink->using_external_xwindowid = FALSE;
        XCloseDisplay(omxoverlaysink->display);
    }
#if 0
    g_omx_core_stop (self->gomx);
    g_omx_core_unload (self->gomx);
#endif

    if (self->gomx->omx_error)
        return GST_STATE_CHANGE_FAILURE;

    GST_LOG_OBJECT (self, "end");

    return TRUE;
}

static GstStateChangeReturn
change_state (GstElement * element, GstStateChange transition)
{
  GstOmxBaseSink  *omx_base = GST_OMX_BASE_SINK(element);
  GstOmxOverlaySink* self = GST_OMX_OVERLAYSINK (omx_base);
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  switch (transition) {
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      self->fps_n = 0;
      self->fps_d = 1;
      g_omx_core_stop (omx_base->gomx);
      g_omx_core_unload (omx_base->gomx);
      break;
     default:
       break;
  }

  return ret;
}

static void
type_class_init (gpointer g_class,
                 gpointer class_data)
{
    GObjectClass *gobject_class;
    GstBaseSinkClass *gst_base_sink_class;
    GstElementClass *gstelement_class;

    gobject_class = (GObjectClass *) g_class;
    gst_base_sink_class = GST_BASE_SINK_CLASS (g_class);
    gstelement_class = (GstElementClass *) g_class;

    parent_class = g_type_class_ref (GST_OMX_BASE_SINK_TYPE);

    gstelement_class->change_state = change_state;
    gst_base_sink_class->set_caps = setcaps;
    gst_base_sink_class->start = start;
    gst_base_sink_class->render = render;
    gst_base_sink_class->stop = stop;
    gst_base_sink_class->get_times = get_times;

    gobject_class->set_property = set_property;
    gobject_class->get_property = get_property;

    g_object_class_install_property (gobject_class, ARG_X_SCALE,
                                     g_param_spec_uint ("x-scale", "X Scale",
                                                        "How much to scale the image in the X axis (100 means nothing)",
                                                        0, G_MAXUINT, 100, G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class, ARG_Y_SCALE,
                                     g_param_spec_uint ("y-scale", "Y Scale",
                                                        "How much to scale the image in the Y axis (100 means nothing)",
                                                        0, G_MAXUINT, 100, G_PARAM_READWRITE));

    g_object_class_install_property (gobject_class, ARG_ROTATION,
                                     g_param_spec_uint ("rotation", "Rotation",
                                                        "Rotation angle",
                                                        0, G_MAXUINT, 360, G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, PROP_DISPLAY,
                                       g_param_spec_string ("display", "Display", "X Display name",
                                                             NULL, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
    g_object_class_install_property (gobject_class, ARG_CONTRAST,
                                       g_param_spec_double ("contrast", "Contrast", "The contrast of the video",
                                                          0, 2, 0, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
      g_object_class_install_property (gobject_class, ARG_BRIGHTNESS,
                                       g_param_spec_double ("brightness", "Brightness",
                                                         "The brightness of the video", -1, 1, 0,
                                                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
      g_object_class_install_property (gobject_class, ARG_HUE,
                                       g_param_spec_double ("hue", "Hue", "The hue of the video", -1, 1, 0,
                                                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
      g_object_class_install_property (gobject_class, ARG_SATURATION,
                                       g_param_spec_double ("saturation", "Saturation",
                                                         "The saturation of the video", 0, 2, 0,
                                                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
     g_object_class_install_property (gobject_class, ARG_FORCE_ASPECT_RATIO,
                                                          g_param_spec_boolean ("force-aspect-ratio", "Force aspect ratio",
                                                          "When enabled, scaling will respect original aspect ratio", FALSE,
                                                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

     g_object_class_install_property (gobject_class, PROP_HANDLE_EVENTS,
                                      g_param_spec_boolean ("handle-events", "Handle XEvents",
                                                            "When enabled, XEvents will be selected and handled", TRUE,
                                                            G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

     g_object_class_install_property (gobject_class, ARG_X_WINDOW_ID,
                                     g_param_spec_uint ("xwindow", "Xwindow ID",
                                                        "Xwindow ID",
                                                        0, G_MAXUINT, 0, G_PARAM_READWRITE));
}

static void
#ifndef GST_DISABLE_OLD_SETXWINDOW
omx_overlaysink_set_xwindow_id (GstXOverlay * overlay, XID xwindow_id)
#else
omx_overlaysink_set_window_handle (GstXOverlay * overlay, guintptr xwindow_id)
#endif
{
    GstOmxOverlaySink *self;
    self = GST_OMX_OVERLAYSINK (overlay);

    self->client_window = xwindow_id;
    self->using_external_xwindowid = TRUE;
}

static void
omx_overlaysink_expose (GstXOverlay * overlay)
{
    //TODO
    /* Nothing to do as of now */
}

static void
omx_overlaysink_set_event_handling (GstXOverlay * overlay,
    gboolean handle_events)
{
    GstOmxOverlaySink *self;

    self = GST_OMX_OVERLAYSINK (overlay);
    self->handle_events = handle_events;
}

static void
omx_overlaysink_xoverlay_init (GstXOverlayClass * iface)
{
#ifndef GST_DISABLE_OLD_SETXWINDOW
  iface->set_xwindow_id = omx_overlaysink_set_xwindow_id;
#else
  iface->set_window_handle = omx_overlaysink_set_window_handle;
#endif
  iface->expose = omx_overlaysink_expose;
  iface->handle_events = omx_overlaysink_set_event_handling;
}

static gboolean
interface_supported (GstImplementsInterface *iface,
                     GType type)
{
    g_assert (type == GST_TYPE_X_OVERLAY);
    return TRUE;
}

static void
interface_init (GstImplementsInterfaceClass *klass)
{
    klass->supported = interface_supported;
}

static void
type_instance_init (GTypeInstance *instance,
                    gpointer g_class)
{
    GstOmxBaseSink *omx_base;
    GstOmxOverlaySink *omxoverlaysink = GST_OMX_OVERLAYSINK(instance);

    omx_base = GST_OMX_BASE_SINK (instance);
    omx_base->audio_sink = FALSE;
    omxoverlaysink->brightness = omxoverlaysink->current_brightness = DEFAULT_PROP_BRIGHTNESS;
    omxoverlaysink->saturation = omxoverlaysink->current_saturation = DEFAULT_PROP_SATURATION;
    omxoverlaysink->contrast   = omxoverlaysink->current_contrast   = DEFAULT_PROP_CONTRAST;
    omxoverlaysink->keep_aspect = TRUE;
    omxoverlaysink->pixel_aspect_ratio_num = 1;
    omxoverlaysink->pixel_aspect_ratio_denom = 1;

    GST_DEBUG_OBJECT (omx_base, "start");
}

GType
gst_omx_overlaysink_get_type (void)
{
    static GType type = 0;
    static const GInterfaceInfo overlay_info = {
      (GInterfaceInitFunc) omx_overlaysink_xoverlay_init,
      NULL,
      NULL,
    };

    if (G_UNLIKELY (type == 0))
    {
        GTypeInfo *type_info;
        GInterfaceInfo *iface_info;

        type_info = g_new0 (GTypeInfo, 1);
        type_info->class_size = sizeof (GstOmxOverlaySinkClass);
        type_info->base_init = type_base_init;
        type_info->class_init = type_class_init;
        type_info->instance_size = sizeof (GstOmxOverlaySink);
        type_info->instance_init = type_instance_init;

        type = g_type_register_static (GST_OMX_BASE_SINK_TYPE, "GstOmxOverlaySink", type_info, 0);
        iface_info = g_new0 (GInterfaceInfo, 1);
        iface_info->interface_init = (GInterfaceInitFunc) interface_init;

        g_type_add_interface_static (type, GST_TYPE_IMPLEMENTS_INTERFACE, iface_info);
        g_free (iface_info);
        g_type_add_interface_static (type, GST_TYPE_X_OVERLAY,&overlay_info);

        g_free (type_info);
    }

    return type;
}
