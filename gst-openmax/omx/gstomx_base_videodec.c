/*
 * Copyright (C) 2007-2009 Nokia Corporation.
 * Copyright (c) 2011-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "gstomx_base_videodec.h"
#include "gstomx.h"
#include "config.h"

#ifdef TEGRA
#include "gstomx_nvutils.h"
#endif

GSTOMX_BOILERPLATE (GstOmxBaseVideoDec, gst_omx_base_videodec, GstOmxBaseFilter,
    GST_OMX_BASE_FILTER_TYPE);

static void
setup_ports (GstOmxBaseFilter *self)
{
  GOmxCore *core;
  OMX_PARAM_PORTDEFINITIONTYPE param;
  GstCaps *peercaps = NULL;
  GstStructure *peerstruct = NULL;
  GstStructure *bufstruct = NULL;
  GstCaps *bufcaps = NULL;
  int frame_width = 0;
  int frame_height = 0;
  GstOmxBaseVideoDec *omx_base_dec;

  omx_base_dec = (GstOmxBaseVideoDec*) self;
  core = self->gomx;
  G_OMX_INIT_PARAM(param);

  /* Input port configuration. */
  param.nPortIndex = 0;
  OMX_GetParameter (core->omx_handle, OMX_IndexParamPortDefinition, &param);
  self->in_port = g_omx_core_setup_port (core, &param);
  gst_pad_set_element_private (self->sinkpad, self->in_port);

  /* Output port configuration. */
  param.nPortIndex = 1;
  OMX_GetParameter (core->omx_handle, OMX_IndexParamPortDefinition, &param);
  self->out_port = g_omx_core_setup_port (core, &param);
  gst_pad_set_element_private (self->srcpad, self->out_port);

  if (g_getenv ("OMX_ALLOCATE_ON")) {
    self->in_port->omx_allocate = TRUE;
    self->out_port->omx_allocate = TRUE;
    core->share_input_buffer = FALSE;
    core->share_output_buffer = FALSE;
  } else if (g_getenv ("OMX_SHARE_HACK_ON")) {
    core->share_input_buffer = TRUE;
    core->share_output_buffer = TRUE;
  } else if (g_getenv ("OMX_SHARE_HACK_OFF")) {
    core->share_input_buffer = FALSE;
    core->share_output_buffer = FALSE;
  }

  if (self->buffer_data) {
    bufcaps = GST_BUFFER_CAPS(self->buffer_data);
  }

  if (bufcaps) {
    bufstruct = gst_caps_get_structure (bufcaps, 0);
  }

  if (bufstruct) {
    const GValue *gvalue_width  = NULL;
    const GValue *gvalue_height = NULL;
    const GValue *gvalue_pixel_aspect_ratio = NULL;

    gvalue_pixel_aspect_ratio = gst_structure_get_value(bufstruct, "pixel-aspect-ratio");
    if (gvalue_pixel_aspect_ratio) {
      omx_base_dec->pixel_aspect_ratio_num   = gst_value_get_fraction_numerator(gvalue_pixel_aspect_ratio);
      omx_base_dec->pixel_aspect_ratio_denom = gst_value_get_fraction_denominator(gvalue_pixel_aspect_ratio);
    }

    gvalue_width  = gst_structure_get_value(bufstruct, "width");
    if (gvalue_width) {
      frame_width   = g_value_get_int(gvalue_width);
    }

    gvalue_height = gst_structure_get_value(bufstruct, "height");
    if (gvalue_height) {
      frame_height = g_value_get_int(gvalue_height);
    }
  }

  if (gst_pad_is_linked(self->srcpad)) {
    peercaps = gst_pad_peer_get_caps (self->srcpad);

    if (peercaps && peercaps->structs->len > 0) {
      peerstruct = gst_caps_get_structure(peercaps, 0);

      if (!strcmp(gst_structure_get_name(peerstruct), "video/x-raw-gl")) {
#ifdef USE_EGLIMAGE
        GstQuery *query;
        GstStructure * querystruct;
        querystruct = gst_structure_new("query_struct","display_data",G_TYPE_POINTER,NULL,NULL);
        query = gst_query_new_application(GST_QUERY_CUSTOM, querystruct);
        gst_pad_peer_query(self->srcpad, query);
        gst_structure_get(querystruct, "display_data", G_TYPE_POINTER ,&self->gomx->display_data, NULL);
        self->out_port->buffer_type = BUFFER_TYPE_EGLIMAGE;
        gst_query_unref(query);
#else
        g_error("EGL Image Display Info could not be found \n");
#endif
      } else if (!strcmp(gst_structure_get_name(peerstruct), "video/x-nv-yuv")) {
#ifdef TEGRA
            gstomx_use_nvrmsurf_extension(core->omx_handle);
#endif
          self->out_port->buffer_type = BUFFER_TYPE_NVBUFFER;
      }
      else if (!strcmp(gst_structure_get_name(peerstruct), "video/x-nvrm-yuv")) {
#ifdef TEGRA
          gstomx_use_nvrmsurf_extension(core->omx_handle);
#endif
          self->out_port->buffer_type = BUFFER_TYPE_NVRMBUFFER;
      }
      else if (!strcmp(gst_structure_get_name(peerstruct), "video/x-raw-yuv")) {
        self->out_port->buffer_type = BUFFER_TYPE_RAW;
      }
    } else {
      self->out_port->buffer_type = BUFFER_TYPE_NVBUFFER;
    }
  } else {
    self->out_port->buffer_type = BUFFER_TYPE_NVBUFFER;
  }

  if (self->out_port->buffer_type == BUFFER_TYPE_NVBUFFER) {
#ifdef TEGRA
    gstomx_use_nvbuffer_extension(core->omx_handle, GOMX_PORT_OUTPUT);
#endif
  } else if (self->out_port->buffer_type == BUFFER_TYPE_RAW) {
    /* Output port configuration. */
    param.nPortIndex = 1;
    OMX_GetParameter (core->omx_handle, OMX_IndexParamPortDefinition, &param);

    if (!frame_width || !frame_height) {
      param.format.video.nFrameWidth = 1920;
      param.format.video.nFrameHeight = 1080;
    } else {
      param.format.video.nFrameWidth  = frame_width;
      param.format.video.nFrameHeight = frame_height;
    }

    param.format.video.eColorFormat = OMX_COLOR_FormatYUV420Planar;
    param.nBufferSize = (param.format.video.nFrameWidth * param.format.video.nFrameHeight * 3) >> 1;
    self->out_port->buffer_size = param.nBufferSize;
    OMX_SetParameter (core->omx_handle, OMX_IndexParamPortDefinition, &param);
  } else if(g_getenv ("USE_XIMAGE_SINK")) {
    core->share_output_buffer = TRUE;
  }

  self->in_port->is_audio_port  = FALSE;
  self->out_port->is_audio_port = FALSE;
}

static GstCaps *
generate_src_template (void)
{
  GstCaps *caps;
  GstStructure *struc1;
  GstStructure *struc2;
  GstStructure *struc3;
  GstStructure *struc4;
  GstStructure *struc5;

  caps = gst_caps_new_empty ();

  struc1 = gst_structure_new ("video/x-raw-yuv",
      "width", GST_TYPE_INT_RANGE, 16, 4096,
      "height", GST_TYPE_INT_RANGE, 16, 4096,
      "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1, NULL);

  struc2 = gst_structure_new ("video/x-nv-yuv",
      "width", GST_TYPE_INT_RANGE, 16, 4096,
      "height", GST_TYPE_INT_RANGE, 16, 4096,
      "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1, NULL);

  struc3 = gst_structure_new ("video/x-nv-overlay",
      "width", GST_TYPE_INT_RANGE, 16, 4096,
      "height", GST_TYPE_INT_RANGE, 16, 4096,
      "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1, NULL);

  struc4 = gst_structure_new ("video/x-raw-gl",
      "width", GST_TYPE_INT_RANGE, 16, 4096,
      "height", GST_TYPE_INT_RANGE, 16, 4096,
      "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1, NULL);

  struc5 = gst_structure_new ("video/x-nvrm-yuv",
          "width", GST_TYPE_INT_RANGE, 16, 4096,
          "height", GST_TYPE_INT_RANGE, 16, 4096,
          "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1, NULL);

  {
    GValue list;
    GValue val;

    list.g_type = val.g_type = 0;

    g_value_init (&list, GST_TYPE_LIST);
    g_value_init (&val, GST_TYPE_FOURCC);

    gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('I', '4', '2', '0'));
    gst_value_list_append_value (&list, &val);

    gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('Y', 'U', 'Y', '2'));
    gst_value_list_append_value (&list, &val);

    gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('U', 'Y', 'V', 'Y'));
    gst_value_list_append_value (&list, &val);

    gst_structure_set_value (struc1, "format", &list);
    gst_structure_set_value (struc2, "format", &list);
    gst_structure_set_value (struc3, "format", &list);
    gst_structure_set_value (struc5, "format", &list);

    g_value_unset (&val);
    g_value_unset (&list);
  }

  {
    GValue list;
    GValue val;
    list.g_type = val.g_type = 0;

    g_value_init (&list, GST_TYPE_LIST);
    g_value_init (&val, GST_TYPE_FOURCC);

    gst_value_set_fourcc (&val, GST_MAKE_FOURCC ('R', 'G', 'B', 'A'));
    gst_value_list_append_value (&list, &val);

    gst_structure_set_value (struc4, "format", &list);

    g_value_unset (&val);
    g_value_unset (&list);
  }

  gst_caps_append_structure (caps, struc1);
  gst_caps_append_structure (caps, struc2);
  gst_caps_append_structure (caps, struc3);
  gst_caps_append_structure (caps, struc4);
  gst_caps_append_structure (caps, struc5);

  return caps;
}

static void
type_base_init (gpointer g_class)
{
  GstElementClass *element_class;

  element_class = GST_ELEMENT_CLASS (g_class);

  {
    GstPadTemplate *template;

    template = gst_pad_template_new ("src", GST_PAD_SRC,
        GST_PAD_ALWAYS, generate_src_template ());

    gst_element_class_add_pad_template (element_class, template);
  }
}

static void
type_class_init (gpointer g_class, gpointer class_data)
{
  parent_class = g_type_class_ref (GST_OMX_BASE_FILTER_TYPE);
}

static void
settings_changed_cb (GOmxCore * core)
{
  GstOmxBaseFilter *omx_base;
  GstOmxBaseVideoDec *self;
//  GOmxPort *port;
  guint width;
  guint height;
  guint32 format = 0;

  omx_base = core->object;
  self = GST_OMX_BASE_VIDEODEC (omx_base);

  GST_DEBUG_OBJECT (omx_base, "settings changed");

  {
    OMX_CONFIG_RECTTYPE rect;
    OMX_PARAM_PORTDEFINITIONTYPE param;

    G_OMX_INIT_PARAM (rect);

    rect.nPortIndex = omx_base->out_port->port_index;
    /* pass only actual width and height */
    OMX_GetConfig (omx_base->gomx->omx_handle, OMX_IndexConfigCommonOutputCrop, &rect);

    width = rect.nWidth;
    height = rect.nHeight;

    G_OMX_INIT_PARAM (param);

    param.nPortIndex = omx_base->out_port->port_index;
    OMX_GetParameter (omx_base->gomx->omx_handle, OMX_IndexParamPortDefinition, &param);

    /* assign port params only when output Rectangle is not defined */
    if (!width || !height) {
      width  = param.format.video.nFrameWidth;
      height = param.format.video.nFrameHeight;
    }

    if (omx_base->out_port->buffer_type == BUFFER_TYPE_NVBUFFER ||
            omx_base->out_port->buffer_type == BUFFER_TYPE_NVRMBUFFER) {
      switch (param.format.video.eColorFormat) {
        case OMX_COLOR_FormatYUV420Planar:
          format = GST_MAKE_FOURCC ('I', '4', '2', '0');
          break;
        case OMX_COLOR_FormatYCbYCr:
          format = GST_MAKE_FOURCC ('Y', 'U', 'Y', '2');
          break;
        case OMX_COLOR_FormatCbYCrY:
          format = GST_MAKE_FOURCC ('U', 'Y', 'V', 'Y');
          break;
        default:
          break;
      }
    } else if (omx_base->out_port->buffer_type == BUFFER_TYPE_EGLIMAGE) {
      format = GST_MAKE_FOURCC ('R', 'G', 'B', 'A');
    } else if (omx_base->out_port->buffer_type == BUFFER_TYPE_RAW) {
      switch (param.format.video.eColorFormat) {
        case OMX_COLOR_FormatYUV420Planar:
          format = GST_MAKE_FOURCC ('I', '4', '2', '0'); break;
        case OMX_COLOR_FormatYCbYCr:
          format = GST_MAKE_FOURCC ('Y', 'U', 'Y', '2'); break;
        case OMX_COLOR_FormatCbYCrY:
          format = GST_MAKE_FOURCC ('U', 'Y', 'V', 'Y'); break;
        default:
          break;
      }
    }
  }

//  port = g_omx_core_get_port (core,1);
  {
    GstCaps *new_caps;
    GstStructure *struc = NULL;
    guint stereo_flags=0;
    new_caps = gst_caps_new_empty ();
    if(self->stereo_mode_flag == TRUE)
    {
      //1 bit for stereo enable-1/0 left shifted by 2 followed by 2 bits for either
      //1=Top-Bottom OR 2=side-by-side
      stereo_flags = STEREO_TOP_BOTTOM;
      //Double the height in case of the TOP-BOTTOM stitching
      height = height << 1;
    }
    if (omx_base->out_port->buffer_type == BUFFER_TYPE_NVBUFFER) {
      struc = gst_structure_new("video/x-nv-yuv",
          "width", G_TYPE_INT, width,
          "height", G_TYPE_INT, height,
          "format", GST_TYPE_FOURCC, format,
          "stereoflags",G_TYPE_INT,stereo_flags, NULL);
    } else if (omx_base->out_port->buffer_type == BUFFER_TYPE_NVRMBUFFER) {
        struc = gst_structure_new("video/x-nvrm-yuv",
                "width", G_TYPE_INT, width,
                "height", G_TYPE_INT, height,
                "format", GST_TYPE_FOURCC, format,
                "stereoflags",G_TYPE_INT,stereo_flags, NULL);
    } else if (omx_base->out_port->buffer_type == BUFFER_TYPE_EGLIMAGE) {
      struc = gst_structure_new("video/x-raw-gl",
          "width", G_TYPE_INT, width,
          "height", G_TYPE_INT, height,
          "format", GST_TYPE_FOURCC, format,
          "stereoflags",G_TYPE_INT,stereo_flags, NULL);

    } else if (omx_base->out_port->buffer_type == BUFFER_TYPE_RAW) {
      struc = gst_structure_new("video/x-raw-yuv",
          "width", G_TYPE_INT, width,
          "height", G_TYPE_INT, height,
          "format", GST_TYPE_FOURCC, format,
          "stereoflags",G_TYPE_INT,stereo_flags, NULL);
    }

    if (self->framerate_denom != 0)
      gst_structure_set (struc, "framerate", GST_TYPE_FRACTION,
          self->framerate_num, self->framerate_denom, NULL);
    else
      /* FIXME this is a workaround for xvimagesink */
      gst_structure_set (struc, "framerate", GST_TYPE_FRACTION, 0, 1, NULL);

     if(self->pixel_aspect_ratio_denom != 0)
       gst_structure_set(struc, "pixel-aspect-ratio", GST_TYPE_FRACTION,
           self->pixel_aspect_ratio_num, self->pixel_aspect_ratio_denom, NULL);
    else
       gst_structure_set(struc, "pixel-aspect-ratio", GST_TYPE_FRACTION, 0, 1 , NULL);

    gst_caps_append_structure (new_caps, struc);

    GST_INFO_OBJECT (omx_base, "caps are: %" GST_PTR_FORMAT, new_caps);
    gst_pad_set_caps (omx_base->srcpad, new_caps);
  }
}

static gboolean
sink_setcaps (GstPad * pad, GstCaps * caps)
{
  GstStructure *structure;
  GstOmxBaseVideoDec *self;
  GstOmxBaseFilter *omx_base;
  GOmxCore *gomx;
  OMX_PARAM_PORTDEFINITIONTYPE param;
  gint width = 0;
  gint height = 0;
  gdouble framerate_num,framerate_denom;

  self = GST_OMX_BASE_VIDEODEC (GST_PAD_PARENT (pad));
  omx_base = GST_OMX_BASE_FILTER (self);

  gomx = (GOmxCore *) omx_base->gomx;

  GST_INFO_OBJECT (self, "setcaps (sink): %" GST_PTR_FORMAT, caps);

  g_return_val_if_fail (gst_caps_get_size (caps) == 1, FALSE);

  structure = gst_caps_get_structure (caps, 0);

  gst_structure_get_int (structure, "width", &width);
  gst_structure_get_int (structure, "height", &height);

  if (!width || !height) {
    width = 720;
    height = 480;
    GST_ELEMENT_WARNING(gomx->object,STREAM,DECODE,(NULL),
        ("Received Invalid Width/Height - using 720/480"));
  }

  {
    const GValue *framerate = NULL;
    self->framerate_num = 0;
    self->framerate_denom = 0;
    framerate = gst_structure_get_value (structure, "framerate");
    if (framerate) {
      self->framerate_num = gst_value_get_fraction_numerator (framerate);
      self->framerate_denom = gst_value_get_fraction_denominator (framerate);

      framerate_num = (gdouble)self->framerate_num;
      framerate_denom = (gdouble)self->framerate_denom;

      omx_base->duration = (GstClockTime)((framerate_denom /framerate_num)*GST_SECOND );
    }
  }

  G_OMX_INIT_PARAM (param);

  {
    const GValue *codec_data;
    GstBuffer *buffer;

    codec_data = gst_structure_get_value (structure, "codec_data");
    if (codec_data) {
      buffer = gst_value_get_buffer (codec_data);
      omx_base->codec_data = buffer;
      gst_buffer_ref (buffer);
    }
  }

  /* Input port configuration. */
  {
    param.nPortIndex = 0;
    OMX_GetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);

    param.format.video.nFrameWidth = width;
    param.format.video.nFrameHeight = height;

    OMX_SetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
  }

  /* Output port configuration. */
  {
    param.nPortIndex = 1;
    OMX_GetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
    param.format.video.nFrameWidth = width;
    param.format.video.nFrameHeight = height;
    OMX_SetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
  }
  return gst_pad_set_caps (pad, caps);
}

static void
omx_setup (GstOmxBaseFilter * omx_base)
{
  GstOmxBaseVideoDec *self;
  GOmxCore *gomx;
  OMX_ERRORTYPE status = OMX_ErrorNone;
  self = GST_OMX_BASE_VIDEODEC (omx_base);
  gomx = (GOmxCore *) omx_base->gomx;

  GST_INFO_OBJECT (omx_base, "begin");

  {
    OMX_PARAM_PORTDEFINITIONTYPE param;

    G_OMX_INIT_PARAM (param);
    if (omx_base->full_frame_data == FALSE) {
       gstomx_set_decoder_property (gomx->omx_handle);
    }

    //Sets TvMR's FilterTimestamp attribute
    status = gstomx_set_FilterTimestamp (gomx->omx_handle);
    if (status != OMX_ErrorNone)
      g_print ("\n%s error while setting FilterTimestamp\n", __FUNCTION__);

    /* Input port configuration. */
    {
      param.nPortIndex = 0;
      OMX_GetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);

      param.format.video.eCompressionFormat = self->compression_format;

      OMX_SetParameter (gomx->omx_handle, OMX_IndexParamPortDefinition, &param);
    }
  }

  GST_INFO_OBJECT (omx_base, "end");
}

static void
src_caps_change_if_needed(GstOmxBaseFilter *omx_base, GstBuffer *buffer)
{
  GstCaps *bufcaps = GST_BUFFER_CAPS(buffer);
  GstStructure *bufstruct = NULL;
  GstOmxBaseVideoDec *omx_base_dec = (GstOmxBaseVideoDec*) omx_base;

  if (bufcaps) {
    bufstruct = gst_caps_get_structure (bufcaps, 0);
  }

  if (bufstruct) {
    const GValue *gvalue_pixel_aspect_ratio = NULL;

    gvalue_pixel_aspect_ratio = gst_structure_get_value(bufstruct, "pixel-aspect-ratio");

    if (gvalue_pixel_aspect_ratio) {
      if ((omx_base_dec->pixel_aspect_ratio_num   != gst_value_get_fraction_numerator(gvalue_pixel_aspect_ratio)) ||
          (omx_base_dec->pixel_aspect_ratio_denom != gst_value_get_fraction_denominator(gvalue_pixel_aspect_ratio)))
      {
        omx_base_dec->pixel_aspect_ratio_num   = gst_value_get_fraction_numerator(gvalue_pixel_aspect_ratio);
        omx_base_dec->pixel_aspect_ratio_denom = gst_value_get_fraction_denominator(gvalue_pixel_aspect_ratio);
        omx_base->update_src_caps = TRUE;
      }
    }
  }
}

static void
type_instance_init (GTypeInstance * instance, gpointer g_class)
{
  GstOmxBaseFilter *omx_base;
  GstOmxBaseVideoDec *omx_base_dec;

  omx_base = GST_OMX_BASE_FILTER (instance);
  omx_base->omx_setup = omx_setup;
  omx_base->setup_ports = setup_ports;
  omx_base->gomx->settings_changed_cb = settings_changed_cb;

  omx_base_dec = (GstOmxBaseVideoDec*) omx_base;
  omx_base_dec->pixel_aspect_ratio_num = 1;
  omx_base_dec->pixel_aspect_ratio_denom = 1;
  omx_base_dec->src_caps_change_if_needed = src_caps_change_if_needed;
  omx_base_dec->stereo_mode_flag = FALSE;
  gst_pad_set_setcaps_function (omx_base->sinkpad, sink_setcaps);
}
