/*
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * NVIDIA Corporation and its licensors retain all intellectual property
 * and proprietary rights in and to this software, related documentation
 * and any modifications thereto.  Any use, reproduction, disclosure or
 * distribution of this software and related documentation without an express
 * license agreement from NVIDIA Corporation is strictly prohibited.
 *
 * version: 0.2
 */

#ifndef __GST_NVCAMERA_DEFS_H__
#define __GST_NVCAMERA_DEFS_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define GST_PHOTOGRAPHY_NV_PAUSED_AFTER_CAPTURE   "paused-after-capture"

#ifdef GST_ENABLE_PHOTOGRAPHY
/* picked up */
#include <gst/interfaces/photography.h>
#include <gst/interfaces/photography-enumtypes.h>

typedef enum {
  GST_PHOTOGRAPHY_NV_FLASH_MODE_TORCH = GST_PHOTOGRAPHY_FLASH_MODE_RED_EYE + 1
} GstNvFlashMode;


typedef enum {
  GST_PHOTOGRAPHY_NV_WB_MODE_SHADE = GST_PHOTOGRAPHY_WB_MODE_FLUORESCENT + 1,
  GST_PHOTOGRAPHY_NV_WB_MODE_INCANDESCENT,
  GST_PHOTOGRAPHY_NV_WB_MODE_FLASH,
  GST_PHOTOGRAPHY_NV_WB_MODE_HORIZON,
  GST_PHOTOGRAPHY_NV_WB_MODE_OFF
} GstNvWhiteBalanceMode;


typedef enum {
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_NOISE = GST_PHOTOGRAPHY_COLOUR_TONE_MODE_SKIN_WHITEN + 1,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_EMBOSS,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_SKETCH,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_OIL_PAINT,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_HATCH,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_GPEN,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_ANTIALIAS,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_DERING,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_POSTERIZE,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_BW,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_MANUAL,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_AQUA
} GstNvColourToneMode;


typedef enum {
  GST_PHOTOGRAPHY_NV_SCENE_MODE_ACTION = GST_PHOTOGRAPHY_SCENE_MODE_AUTO + 1,
  GST_PHOTOGRAPHY_NV_SCENE_MODE_BEACH,
  GST_PHOTOGRAPHY_NV_SCENE_MODE_CANDLELIGHT,
  GST_PHOTOGRAPHY_NV_SCENE_MODE_FIREWORKS,
  GST_PHOTOGRAPHY_NV_SCENE_MODE_NIGHTPORTRAIT,
  GST_PHOTOGRAPHY_NV_SCENE_MODE_PARTY,
  GST_PHOTOGRAPHY_NV_SCENE_MODE_SNOW,
  GST_PHOTOGRAPHY_NV_SCENE_MODE_SUNSET,
  GST_PHOTOGRAPHY_NV_SCENE_MODE_THEATRE,
  GST_PHOTOGRAPHY_NV_SCENE_MODE_BARCODE
} GstNvSceneMode;

typedef enum {
  GST_PHOTOGRAPHY_NV_FOCUS_MODE_CONTINUOUS_PICTURE = GST_PHOTOGRAPHY_FOCUS_MODE_CONTINUOUS_EXTENDED + 1,
  GST_PHOTOGRAPHY_NV_FOCUS_MODE_CONTINUOUS_VIDEO,
  GST_PHOTOGRAPHY_NV_FOCUS_MODE_FIXED
} GstNvFocusMode;


#else
/* Custom GstMessage name that will be sent to GstBus when autofocusing
   is complete */
#define GST_PHOTOGRAPHY_AUTOFOCUS_DONE        "autofocus-done"

/* Custom GstMessage name that will be sent to GstBus when shake risk changes */
#define GST_PHOTOGRAPHY_SHAKE_RISK            "shake-risk"

/* Interface property names */
#define GST_PHOTOGRAPHY_PROP_WB_MODE      "white-balance-mode"
#define GST_PHOTOGRAPHY_PROP_COLOUR_TONE  "colour-tone-mode"
#define GST_PHOTOGRAPHY_PROP_SCENE_MODE   "scene-mode"
#define GST_PHOTOGRAPHY_PROP_FLASH_MODE   "flash-mode"
#define GST_PHOTOGRAPHY_PROP_NOISE_REDUCTION   "noise-reduction"
#define GST_PHOTOGRAPHY_PROP_FOCUS_STATUS "focus-status"
#define GST_PHOTOGRAPHY_PROP_CAPABILITIES "capabilities"
#define GST_PHOTOGRAPHY_PROP_SHAKE_RISK   "shake-risk"
#define GST_PHOTOGRAPHY_PROP_EV_COMP      "ev-compensation"
#define GST_PHOTOGRAPHY_PROP_ISO_SPEED    "iso-speed"
#define GST_PHOTOGRAPHY_PROP_APERTURE     "aperture"
#define GST_PHOTOGRAPHY_PROP_EXPOSURE     "exposure"
#define GST_PHOTOGRAPHY_PROP_IMAGE_CAPTURE_SUPPORTED_CAPS \
    "image-capture-supported-caps"
#define GST_PHOTOGRAPHY_PROP_IMAGE_PREVIEW_SUPPORTED_CAPS \
    "image-preview-supported-caps"
#define GST_PHOTOGRAPHY_PROP_FLICKER_MODE "flicker-mode"
#define GST_PHOTOGRAPHY_PROP_FOCUS_MODE   "focus-mode"
#define GST_PHOTOGRAPHY_PROP_ZOOM   "zoom"


/**
 * GstPhotographyNoiseReduction:
 * @GST_PHOTOGRAPHY_NOISE_REDUCTION_BAYER: Adaptive noise reduction on Bayer
 * format
 * @GST_PHOTOGRAPHY_NOISE_REDUCTION_YCC: reduces the noise on Y and 2-chroma
 * images.
 * @GST_PHOTOGRAPHY_NOISE_REDUCTION_TEMPORAL: Multi-frame adaptive NR,
 * provided for the video mode
 * @GST_PHOTOGRAPHY_NOISE_REDUCTION_FPN: Fixed Pattern Noise refers to noise
 * that does not change between frames. The noise is removed from the sensor
 * image, by subtracting a previously-captured black image in memory.
 * @GST_PHOTOGRAPHY_NOISE_REDUCTION_EXTRA: Extra Noise Reduction. In the case
 * of high-ISO capturing, some noise remains after YCC NR. XNR reduces this
 * remaining noise.
 *
 * Noise Reduction features of a photography capture or filter element.
 *
 * Since: 0.10.21
 */
typedef enum
{
  GST_PHOTOGRAPHY_NOISE_REDUCTION_BAYER = ( 1<<0 ),
  GST_PHOTOGRAPHY_NOISE_REDUCTION_YCC = ( 1<<1 ),
  GST_PHOTOGRAPHY_NOISE_REDUCTION_TEMPORAL= ( 1<< 2),
  GST_PHOTOGRAPHY_NOISE_REDUCTION_FIXED = (1 << 3),
  GST_PHOTOGRAPHY_NOISE_REDUCTION_EXTRA = (1 << 4)
} GstPhotographyNoiseReduction;

typedef enum
{
  GST_PHOTOGRAPHY_WB_MODE_AUTO = 0,
  GST_PHOTOGRAPHY_WB_MODE_DAYLIGHT,
  GST_PHOTOGRAPHY_WB_MODE_CLOUDY,
  GST_PHOTOGRAPHY_WB_MODE_SUNSET,
  GST_PHOTOGRAPHY_WB_MODE_TUNGSTEN,
  GST_PHOTOGRAPHY_WB_MODE_FLUORESCENT,
  GST_PHOTOGRAPHY_NV_WB_MODE_SHADE = GST_PHOTOGRAPHY_WB_MODE_FLUORESCENT + 1,
  GST_PHOTOGRAPHY_NV_WB_MODE_INCANDESCENT,
  GST_PHOTOGRAPHY_NV_WB_MODE_FLASH,
  GST_PHOTOGRAPHY_NV_WB_MODE_HORIZON,
  GST_PHOTOGRAPHY_NV_WB_MODE_OFF
} GstWhiteBalanceMode;

typedef enum
{
  GST_PHOTOGRAPHY_COLOUR_TONE_MODE_NORMAL = 0,
  GST_PHOTOGRAPHY_COLOUR_TONE_MODE_SEPIA,
  GST_PHOTOGRAPHY_COLOUR_TONE_MODE_NEGATIVE,
  GST_PHOTOGRAPHY_COLOUR_TONE_MODE_GRAYSCALE,
  GST_PHOTOGRAPHY_COLOUR_TONE_MODE_NATURAL,
  GST_PHOTOGRAPHY_COLOUR_TONE_MODE_VIVID,
  GST_PHOTOGRAPHY_COLOUR_TONE_MODE_COLORSWAP,
  GST_PHOTOGRAPHY_COLOUR_TONE_MODE_SOLARIZE,
  GST_PHOTOGRAPHY_COLOUR_TONE_MODE_OUT_OF_FOCUS,
  GST_PHOTOGRAPHY_COLOUR_TONE_MODE_SKY_BLUE,
  GST_PHOTOGRAPHY_COLOUR_TONE_MODE_GRASS_GREEN,
  GST_PHOTOGRAPHY_COLOUR_TONE_MODE_SKIN_WHITEN,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_NOISE = GST_PHOTOGRAPHY_COLOUR_TONE_MODE_SKIN_WHITEN + 1,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_EMBOSS,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_SKETCH,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_OIL_PAINT,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_HATCH,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_GPEN,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_ANTIALIAS,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_DERING,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_POSTERIZE,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_BW,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_MANUAL,
  GST_PHOTOGRAPHY_NV_COLOUR_TONE_MODE_AQUA
} GstColourToneMode;

typedef enum
{
  GST_PHOTOGRAPHY_SCENE_MODE_MANUAL = 0,
  GST_PHOTOGRAPHY_SCENE_MODE_CLOSEUP,
  GST_PHOTOGRAPHY_SCENE_MODE_PORTRAIT,
  GST_PHOTOGRAPHY_SCENE_MODE_LANDSCAPE,
  GST_PHOTOGRAPHY_SCENE_MODE_SPORT,
  GST_PHOTOGRAPHY_SCENE_MODE_NIGHT,
  GST_PHOTOGRAPHY_SCENE_MODE_AUTO,
  GST_PHOTOGRAPHY_NV_SCENE_MODE_ACTION = GST_PHOTOGRAPHY_SCENE_MODE_AUTO + 1,
  GST_PHOTOGRAPHY_NV_SCENE_MODE_BEACH,
  GST_PHOTOGRAPHY_NV_SCENE_MODE_CANDLELIGHT,
  GST_PHOTOGRAPHY_NV_SCENE_MODE_FIREWORKS,
  GST_PHOTOGRAPHY_NV_SCENE_MODE_NIGHTPORTRAIT,
  GST_PHOTOGRAPHY_NV_SCENE_MODE_PARTY,
  GST_PHOTOGRAPHY_NV_SCENE_MODE_SNOW,
  GST_PHOTOGRAPHY_NV_SCENE_MODE_SUNSET,
  GST_PHOTOGRAPHY_NV_SCENE_MODE_THEATRE,
  GST_PHOTOGRAPHY_NV_SCENE_MODE_BARCODE
} GstSceneMode;

typedef enum
{
  GST_PHOTOGRAPHY_FLASH_MODE_AUTO = 0,
  GST_PHOTOGRAPHY_FLASH_MODE_OFF,
  GST_PHOTOGRAPHY_FLASH_MODE_ON,
  GST_PHOTOGRAPHY_FLASH_MODE_FILL_IN,
  GST_PHOTOGRAPHY_FLASH_MODE_RED_EYE,
  GST_PHOTOGRAPHY_NV_FLASH_MODE_TORCH = GST_PHOTOGRAPHY_FLASH_MODE_RED_EYE + 1
} GstFlashMode;

typedef enum
{
  GST_PHOTOGRAPHY_FOCUS_STATUS_NONE = 0,
  GST_PHOTOGRAPHY_FOCUS_STATUS_RUNNING,
  GST_PHOTOGRAPHY_FOCUS_STATUS_FAIL,
  GST_PHOTOGRAPHY_FOCUS_STATUS_SUCCESS
} GstFocusStatus;

typedef enum
{
  GST_PHOTOGRAPHY_CAPS_NONE = (0 << 0),
  GST_PHOTOGRAPHY_CAPS_EV_COMP = (1 << 0),
  GST_PHOTOGRAPHY_CAPS_ISO_SPEED = (1 << 1),
  GST_PHOTOGRAPHY_CAPS_WB_MODE = (1 << 2),
  GST_PHOTOGRAPHY_CAPS_TONE = (1 << 3),
  GST_PHOTOGRAPHY_CAPS_SCENE = (1 << 4),
  GST_PHOTOGRAPHY_CAPS_FLASH = (1 << 5),
  GST_PHOTOGRAPHY_CAPS_ZOOM = (1 << 6),
  GST_PHOTOGRAPHY_CAPS_FOCUS = (1 << 7),
  GST_PHOTOGRAPHY_CAPS_APERTURE = (1 << 8),
  GST_PHOTOGRAPHY_CAPS_EXPOSURE = (1 << 9),
  GST_PHOTOGRAPHY_CAPS_SHAKE = (1 << 10),
  GST_PHOTOGRAPHY_CAPS_NOISE_REDUCTION = (1 << 11),
  GST_PHOTOGRAPHY_CAPS_FLICKER_REDUCTION = (1 << 12),
  GST_PHOTOGRAPHY_CAPS_ALL = (~0)
} GstPhotoCaps;

typedef enum
{
  GST_PHOTOGRAPHY_SHAKE_RISK_LOW = 0,
  GST_PHOTOGRAPHY_SHAKE_RISK_MEDIUM,
  GST_PHOTOGRAPHY_SHAKE_RISK_HIGH
} GstPhotoShakeRisk;

typedef enum
{
  GST_PHOTOGRAPHY_FLICKER_REDUCTION_OFF = 0,
  GST_PHOTOGRAPHY_FLICKER_REDUCTION_50HZ,
  GST_PHOTOGRAPHY_FLICKER_REDUCTION_60HZ,
  GST_PHOTOGRAPHY_FLICKER_REDUCTION_AUTO
} GstFlickerReductionMode;

typedef enum {
  GST_PHOTOGRAPHY_FOCUS_MODE_AUTO = 0,
  GST_PHOTOGRAPHY_FOCUS_MODE_MACRO,
  GST_PHOTOGRAPHY_FOCUS_MODE_PORTRAIT,
  GST_PHOTOGRAPHY_FOCUS_MODE_INFINITY,
  GST_PHOTOGRAPHY_FOCUS_MODE_HYPERFOCAL,
  GST_PHOTOGRAPHY_FOCUS_MODE_EXTENDED,
  GST_PHOTOGRAPHY_FOCUS_MODE_CONTINUOUS_NORMAL,
  GST_PHOTOGRAPHY_FOCUS_MODE_CONTINUOUS_EXTENDED,
  GST_PHOTOGRAPHY_NV_FOCUS_MODE_CONTINUOUS_PICTURE = GST_PHOTOGRAPHY_FOCUS_MODE_CONTINUOUS_EXTENDED + 1,
  GST_PHOTOGRAPHY_NV_FOCUS_MODE_CONTINUOUS_VIDEO,
  GST_PHOTOGRAPHY_NV_FOCUS_MODE_FIXED
} GstFocusMode;

typedef struct
{
  GstWhiteBalanceMode wb_mode;
  GstColourToneMode tone_mode;
  GstSceneMode scene_mode;
  GstFlashMode flash_mode;
  guint32 exposure;
  guint aperture;
  gfloat ev_compensation;
  guint iso_speed;
  gfloat zoom;
  GstFlickerReductionMode flicker_mode;
  GstFocusMode focus_mode;
  GstPhotographyNoiseReduction noise_reduction;
} GstPhotoSettings;
#endif


typedef enum
{
  /* MODE_PREVIEW = 0, No use for this */
  GST_NV_MODE_IMAGE = 1,
  GST_NV_MODE_VIDEO = 2,
} GstNvCameraMode;


typedef enum
{
  GST_NV_METERING_MODE_AVG,
  GST_NV_METERING_MODE_SPOT,
  GST_NV_METERING_MODE_MATRIX
} GstNvMeteringMode;


typedef enum
{
  GST_NV_STEREO_MODE_OFF,
  GST_NV_STEREO_MODE_LEFT_ONLY,
  GST_NV_STEREO_MODE_RIGHT_ONLY,
  GST_NV_STEREO_MODE_FULL
} GstNvStereoMode;


typedef enum
{
  GST_NV_SENSOR_PRIMARY = 0,
  GST_NV_SENSOR_SECONDARY = 1,
  GST_NV_SENSOR_STEREO = 3,
} GstNvSensor;


typedef enum
{
  GST_NV_ROTATION_0 = 0,
  GST_NV_ROTATION_90 = 90,
  GST_NV_ROTATION_180 = 180,
  GST_NV_ROTATION_270 = 270
} GstNvRotation;


#ifdef GST_ENABLE_PHOTOGRAPHY

GType gst_nv_flash_mode_get_type (void);
#define GST_TYPE_NV_FLASH_MODE (gst_nv_flash_mode_get_type())
GType gst_nv_white_balance_mode_get_type (void);
#define GST_TYPE_NV_WHITE_BALANCE_MODE (gst_nv_white_balance_mode_get_type())
GType gst_nv_colour_tone_mode_get_type (void);
#define GST_TYPE_NV_COLOUR_TONE_MODE (gst_nv_colour_tone_mode_get_type())
GType gst_nv_scene_mode_get_type (void);
#define GST_TYPE_NV_SCENE_MODE  (gst_nv_scene_mode_get_type())
GType gst_nv_focus_mode_get_type (void);
#define GST_TYPE_NV_FOCUS_MODE  (gst_nv_focus_mode_get_type())

#else

/* enumerations from "photography.h" */
GType gst_photography_noise_reduction_get_type (void);
#define GST_TYPE_PHOTOGRAPHY_NOISE_REDUCTION (gst_photography_noise_reduction_get_type())
GType gst_white_balance_mode_get_type (void);
#define GST_TYPE_WHITE_BALANCE_MODE (gst_white_balance_mode_get_type())
GType gst_colour_tone_mode_get_type (void);
#define GST_TYPE_COLOUR_TONE_MODE (gst_colour_tone_mode_get_type())
GType gst_scene_mode_get_type (void);
#define GST_TYPE_SCENE_MODE (gst_scene_mode_get_type())
GType gst_flash_mode_get_type (void);
#define GST_TYPE_FLASH_MODE (gst_flash_mode_get_type())
GType gst_focus_status_get_type (void);
#define GST_TYPE_FOCUS_STATUS (gst_focus_status_get_type())
GType gst_photo_caps_get_type (void);
#define GST_TYPE_PHOTO_CAPS (gst_photo_caps_get_type())
GType gst_photo_shake_risk_get_type (void);
#define GST_TYPE_PHOTO_SHAKE_RISK (gst_photo_shake_risk_get_type())
GType gst_flicker_reduction_mode_get_type (void);
#define GST_TYPE_FLICKER_REDUCTION_MODE (gst_flicker_reduction_mode_get_type())
GType gst_focus_mode_get_type (void);
#define GST_TYPE_FOCUS_MODE (gst_focus_mode_get_type())

#endif

GType gst_nv_camera_mode_get_type (void);
#define GST_TYPE_NV_CAMERA_MODE (gst_nv_camera_mode_get_type ())
GType gst_nv_metering_mode_get_type (void);
#define GST_TYPE_NV_METERING_MODE (gst_nv_metering_mode_get_type ())
GType gst_nv_stereo_mode_get_type (void);
#define GST_TYPE_NV_STEREO_MODE (gst_nv_stereo_mode_get_type ())
GType gst_nv_sensor_get_type (void);
#define GST_TYPE_NV_SENSOR (gst_nv_sensor_get_type ())
GType gst_nv_rotation_get_type (void);
#define GST_TYPE_NV_ROTATION (gst_nv_rotation_get_type ())

G_END_DECLS

#endif
