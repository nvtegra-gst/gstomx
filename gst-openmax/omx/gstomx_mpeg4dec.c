/*
 * Copyright (C) 2007-2009 Nokia Corporation.
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "gstomx_mpeg4dec.h"
#include "gstomx.h"
#include "string.h"
#include "stdlib.h"

#define VOP_START_CODE 0x000001B6
#define I_FRAME  0x0
#define INVALID_FRAME -1

static GstFlowReturn gst_omx_mpeg4dec_pad_chain (GstPad *pad, GstBuffer *buf);

static GstCaps *
generate_sink_template_mpeg4 (void)
{
  GstCaps *caps;
  GstStructure *struc;

  caps = gst_caps_new_empty ();

  struc = gst_structure_new ("video/mpeg",
      "mpegversion", G_TYPE_INT, 4,
      "systemstream", G_TYPE_BOOLEAN, FALSE,
      "width", GST_TYPE_INT_RANGE, 16, 4096,
      "height", GST_TYPE_INT_RANGE, 16, 4096,
      "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1, NULL);

  gst_caps_append_structure (caps, struc);

  struc = gst_structure_new ("video/x-divx",
      "divxversion", GST_TYPE_INT_RANGE, 4, 5,
      "width", GST_TYPE_INT_RANGE, 16, 4096,
      "height", GST_TYPE_INT_RANGE, 16, 4096,
      "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1, NULL);

  gst_caps_append_structure (caps, struc);

  struc = gst_structure_new ("video/x-xvid",
      "width", GST_TYPE_INT_RANGE, 16, 4096,
      "height", GST_TYPE_INT_RANGE, 16, 4096,
      "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1, NULL);

  gst_caps_append_structure (caps, struc);

  struc = gst_structure_new ("video/x-3ivx",
      "width", GST_TYPE_INT_RANGE, 16, 4096,
      "height", GST_TYPE_INT_RANGE, 16, 4096,
      "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1, NULL);

  gst_caps_append_structure (caps, struc);

  return caps;
}

static GstFlowReturn gst_omx_mpeg4dec_pad_chain (GstPad *pad, GstBuffer *buf)
{
  GstOmxBaseFilter *omx_base;
  GstOmxMpeg4Dec   *omx_mpeg4dec;
  GstOmxBaseVideoDec *omx_base_dec;
  unsigned char    *data;
  guint            pic_type = INVALID_FRAME, last_pictype = INVALID_FRAME, i,frame_size=0;
  guint            start_offset = 0, end_offset = 0;
  GstFlowReturn result = GST_FLOW_OK;

  omx_base = GST_OMX_BASE_FILTER (GST_PAD_PARENT (pad));
  omx_mpeg4dec = GST_OMX_MPEG4DEC(gst_pad_get_parent (pad));

  omx_base_dec = (GstOmxBaseVideoDec*) omx_base;

  omx_base_dec->src_caps_change_if_needed(omx_base, buf);

  if (omx_base->currplayback_rate < 0.0  || omx_base->currplayback_rate > 2.0) {
    guint32 vop_header = 0xFFFFFFFF;
    i = 0;
    start_offset = 0;
    last_pictype = INVALID_FRAME;

    /* Send only Key Frames */
    data = GST_BUFFER_DATA(buf);

    while (i < GST_BUFFER_SIZE(buf)) {
      vop_header = (vop_header << 8) | data[i];
      i ++;

      if (vop_header == VOP_START_CODE) {
        pic_type  = (data[i] >> 6) & 0x3;

        if (pic_type == I_FRAME) {
          last_pictype = I_FRAME;
          start_offset = i - 4;
        } else {
          if (last_pictype == I_FRAME) {
            GstBuffer *frame_buffer = NULL;
            end_offset = i - 4;
            frame_size = end_offset - start_offset;
            frame_buffer = gst_buffer_new_and_alloc(frame_size);
            memcpy(GST_BUFFER_DATA(frame_buffer), GST_BUFFER_DATA(buf) + start_offset, frame_size);
            GST_BUFFER_TIMESTAMP(frame_buffer) = GST_BUFFER_TIMESTAMP(buf);
            result = omx_mpeg4dec->base_chain_func(pad, frame_buffer);
          }
          last_pictype = pic_type;
        }
        /* Reset the vop header */
        vop_header = 0xFFFFFFF;
      }
    }

    if (last_pictype == I_FRAME && start_offset < GST_BUFFER_SIZE(buf)) {
      GstBuffer *frame_buffer = NULL;
      frame_size = GST_BUFFER_SIZE(buf) - start_offset;
      frame_buffer = gst_buffer_new_and_alloc(frame_size);
      memcpy(GST_BUFFER_DATA(frame_buffer), GST_BUFFER_DATA(buf) + start_offset, frame_size);
      GST_BUFFER_TIMESTAMP(frame_buffer) = GST_BUFFER_TIMESTAMP(buf);
      result = omx_mpeg4dec->base_chain_func(pad, frame_buffer);
    }
    gst_buffer_unref(buf);
  } else {
    result = omx_mpeg4dec->base_chain_func(pad, buf);
  }

  gst_object_unref(GST_ELEMENT(omx_mpeg4dec));

  return result;
}

static void
type_base_init_mpeg4 (gpointer g_class)
{
  GstElementClass *element_class;

  element_class = GST_ELEMENT_CLASS (g_class);

  gst_element_class_set_details_simple (element_class,
      "OpenMAX IL MPEG-4 video decoder",
      "Codec/Decoder/Video",
      "Decodes video in MPEG-4 format with OpenMAX IL", "Felipe Contreras");

  {
    GstPadTemplate *template;

    template = gst_pad_template_new ("sink", GST_PAD_SINK,
        GST_PAD_ALWAYS, generate_sink_template_mpeg4 ());

    gst_element_class_add_pad_template (element_class, template);
  }
}

static void
type_class_init (gpointer g_class, gpointer class_data)
{
}

static void
type_instance_init (GTypeInstance * instance, gpointer g_class)
{
  GstOmxBaseVideoDec *omx_base;
  GstOmxBaseFilter *omx_base_filter;
  GstOmxMpeg4Dec   *omx_mpeg4dec;

  omx_base = GST_OMX_BASE_VIDEODEC (instance);

  omx_base->compression_format = OMX_VIDEO_CodingMPEG4;
  omx_mpeg4dec = GST_OMX_MPEG4DEC (instance);
  omx_base_filter = GST_OMX_BASE_FILTER (instance);
  omx_mpeg4dec->base_chain_func = GST_PAD_CHAINFUNC(omx_base_filter->sinkpad);

  gst_pad_set_chain_function (omx_base_filter->sinkpad, gst_omx_mpeg4dec_pad_chain);
}

GType
gst_omx_mpeg4dec_get_type (void)
{
  static GType type = 0;

  if (G_UNLIKELY (type == 0)) {
    GTypeInfo *type_info;

    type_info = g_new0 (GTypeInfo, 1);
    type_info->class_size = sizeof (GstOmxMpeg4DecClass);
    type_info->base_init = type_base_init_mpeg4;
    type_info->class_init = type_class_init;
    type_info->instance_size = sizeof (GstOmxMpeg4Dec);
    type_info->instance_init = type_instance_init;

    type = g_type_register_static (GST_OMX_BASE_VIDEODEC_TYPE, "GstOmxMpeg4Dec", type_info, 0);

    g_free (type_info);
  }

  return type;
}
