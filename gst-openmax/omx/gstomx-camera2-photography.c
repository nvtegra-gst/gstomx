/*
 * Copyright (c) 2009-2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * NVIDIA Corporation and its licensors retain all intellectual property
 * and proprietary rights in and to this software, related documentation
 * and any modifications thereto.  Any use, reproduction, disclosure or
 * distribution of this software and related documentation without an express
 * license agreement from NVIDIA Corporation is strictly prohibited.
 *
 * version: 0.2
 */


#include "gstomx-camera2-photography.h"
#include "gstomx-camera2-priv.h"
#include "gstomx-camera2.h"

gboolean
gst_omx_pcamera_get_ev_compensation (GstPhotography *photo, gfloat *ev_comp)
{
  return gst_omx_camera_get_ev_compensation (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, ev_comp);
}


gboolean
gst_omx_pcamera_set_ev_compensation (GstPhotography *photo, gfloat ev_comp)
{
  return gst_omx_camera_set_ev_compensation (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, ev_comp);
}


gboolean
gst_omx_pcamera_get_iso_speed (GstPhotography *photo, guint* iso_speed)
{
  return gst_omx_camera_get_iso_speed (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, iso_speed);
}


gboolean
gst_omx_pcamera_set_iso_speed (GstPhotography *photo, guint iso_speed)
{
  return gst_omx_camera_set_iso_speed (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, iso_speed);
}


gboolean
gst_omx_pcamera_get_aperture (GstPhotography *photo, guint* aperture)
{
  return FALSE;
}


gboolean
gst_omx_pcamera_set_aperture (GstPhotography *photo, guint aperture)
{
  return FALSE;
}


gboolean
gst_omx_pcamera_get_exposure (GstPhotography *photo, guint32 *exposure)
{
  return gst_omx_camera_get_exposure (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, exposure);
}


gboolean
gst_omx_pcamera_set_exposure (GstPhotography *photo, guint32 exposure)
{
  return gst_omx_camera_set_exposure (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, exposure);
}


gboolean
gst_omx_pcamera_get_white_balance_mode (GstPhotography *photo, GstWhiteBalanceMode *wb_mode)
{
  return gst_omx_camera_get_white_balance_mode (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, wb_mode);
}


gboolean
gst_omx_pcamera_set_white_balance_mode (GstPhotography *photo, GstWhiteBalanceMode wb_mode)
{
  return gst_omx_camera_set_white_balance_mode (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, wb_mode);
}


gboolean
gst_omx_pcamera_get_colour_tone_mode (GstPhotography *photo, GstColourToneMode *tone_mode)
{
  return gst_omx_camera_get_color_tone (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, tone_mode);
}


gboolean
gst_omx_pcamera_set_colour_tone_mode (GstPhotography *photo, GstColourToneMode tone_mode)
{
  return gst_omx_camera_set_color_tone (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, tone_mode);
}


gboolean
gst_omx_pcamera_get_flash_mode (GstPhotography *photo, GstFlashMode *flash_mode)
{
  return gst_omx_camera_get_flash_mode (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, flash_mode);
}


gboolean
gst_omx_pcamera_set_flash_mode (GstPhotography *photo, GstFlashMode flash_mode)
{
  return gst_omx_camera_set_flash_mode (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, flash_mode);
}


gboolean
gst_omx_pcamera_get_flicker_mode (GstPhotography *photo, GstFlickerReductionMode* flicker_mode)
{
  return gst_omx_camera_get_flicker_reduction_mode (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, flicker_mode);
}


gboolean
gst_omx_pcamera_set_flicker_mode (GstPhotography *photo, GstFlickerReductionMode flicker_mode)
{
  return gst_omx_camera_set_flicker_reduction_mode (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, flicker_mode);
}


gboolean
gst_omx_pcamera_get_focus_mode (GstPhotography *photo, GstFocusMode* focus_mode)
{
  *focus_mode = GST_OMX_CAMERA2(photo)->focus_mode;
  return FALSE;
}


gboolean
gst_omx_pcamera_set_focus_mode (GstPhotography *photo, GstFocusMode focus_mode)
{
  GST_OMX_CAMERA2(photo)->focus_mode = focus_mode;
  return FALSE;
}


gboolean
gst_omx_pcamera_get_zoom (GstPhotography *photo, gfloat* zoom)
{
  return gst_omx_camera_get_zoom (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, zoom);
}


gboolean
gst_omx_pcamera_set_zoom (GstPhotography *photo, gfloat zoom)
{
  return gst_omx_camera_set_zoom (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, zoom);
}


gboolean
gst_omx_pcamera_get_noise_reduction (GstPhotography *photo, GstPhotographyNoiseReduction* noise_reduction)
{
  return gst_omx_camera_get_noise_reduction (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, noise_reduction, NULL, NULL);
}


gboolean
gst_omx_pcamera_set_noise_reduction (GstPhotography *photo, GstPhotographyNoiseReduction noise_reduction)
{
  return gst_omx_camera_set_noise_reduction (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, noise_reduction, 1, 105);
}


gboolean
gst_omx_pcamera_get_scene_mode (GstPhotography *photo, GstSceneMode *scene_mode)
{
  *scene_mode = GST_OMX_CAMERA2(photo)->scene_mode;
  return TRUE;
}


gboolean
gst_omx_pcamera_set_scene_mode (GstPhotography *photo, GstSceneMode scene_mode)
{
  GST_OMX_CAMERA2 (photo)->scene_mode = scene_mode;
  return gst_omx_camera_set_scene_mode (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, scene_mode);
}


GstPhotoCaps
gst_omx_pcamera_get_capabilities (GstPhotography *photo)
{
  return (GST_OMX_CAMERA2 (photo)->capabilities);
}


gboolean
gst_omx_pcamera_prepare_for_capture (GstPhotography *photo,
    GstPhotoCapturePrepared func, GstCaps *capture_caps, gpointer user_data)
{
  return gst_omx_camera_prepare_for_capture (GST_OMX_CAMERA2 (photo), func,
      capture_caps, user_data);
}


void
gst_omx_pcamera_set_autofocus (GstPhotography *photo, gboolean on)
{
  if (on)
    g_signal_emit_by_name (GST_OMX_CAMERA2 (photo), "start-auto-focus", 0);
  else
    g_signal_emit_by_name (GST_OMX_CAMERA2 (photo), "stop-auto-focus", 0);
}


gboolean
gst_omx_pcamera_set_config (GstPhotography *photo, GstPhotoSettings *config)
{
  gboolean ret = TRUE;

  if (!gst_omx_camera_set_white_balance_mode (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, config->wb_mode))
    return FALSE;

  if (!gst_omx_camera_set_color_tone (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, config->tone_mode))
    return FALSE;

  if (!gst_omx_camera_set_scene_mode (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, config->scene_mode))
    return FALSE;
  GST_OMX_CAMERA2 (photo)->scene_mode = config->scene_mode;

  GST_OMX_CAMERA2(photo)->focus_mode = config->focus_mode;

  if (!gst_omx_camera_set_flash_mode (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, config->flash_mode))
    return FALSE;

  if (!gst_omx_camera_set_exposure (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, config->exposure))
    return FALSE;

  if (!gst_omx_camera_set_ev_compensation (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, config->ev_compensation))
    return FALSE;

  if (!gst_omx_camera_set_iso_speed (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, config->iso_speed))
    return FALSE;

  if (!gst_omx_camera_set_zoom (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, config->zoom))
    return FALSE;

  if (!gst_omx_camera_set_flicker_reduction_mode (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, config->flicker_mode))
    return FALSE;

  if (!gst_omx_camera_set_noise_reduction (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, config->noise_reduction, 1, 105))
    return FALSE;

  return ret;
}


gboolean
gst_omx_pcamera_get_config (GstPhotography *photo, GstPhotoSettings *config)
{
  gboolean ret = TRUE;

  if (!gst_omx_camera_get_white_balance_mode (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, &config->wb_mode))
    return FALSE;

  if (!gst_omx_camera_get_color_tone (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, &config->tone_mode))
    return FALSE;

  config->scene_mode = GST_OMX_CAMERA2(photo)->scene_mode;

  config->focus_mode = GST_OMX_CAMERA2(photo)->focus_mode;

  if (!gst_omx_camera_get_flash_mode (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, &config->flash_mode))
    return FALSE;

  if (!gst_omx_camera_get_exposure (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, &config->exposure))
    return FALSE;

  if (!gst_omx_camera_get_ev_compensation (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, &config->ev_compensation))
    return FALSE;

  if (!gst_omx_camera_get_iso_speed (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, &config->iso_speed))
    return FALSE;

  if (!gst_omx_camera_get_zoom (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, &config->zoom))
    return FALSE;

  if (!gst_omx_camera_get_flicker_reduction_mode (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, &config->flicker_mode))
    return FALSE;

  if (!gst_omx_camera_get_noise_reduction (GST_OMX_CAMERA2 (photo)->gomx->omx_handle, &config->noise_reduction, NULL, NULL))
    return FALSE;

  return ret;
}
