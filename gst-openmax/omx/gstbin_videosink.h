/*
 * Copyright (c) 2009-2011, NVIDIA CORPORATION.  All rights reserved.
 *
 * Based on code copyright/by:
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef GSTBIN_VIDEOSINK_H
#define GSTBIN_VIDEOSINK_H

#include "gstomx_util.h"
#include <gst/gst.h>
#include <gst/video/gstvideosink.h>

G_BEGIN_DECLS

#define GST_BIN_VIDEOSINK(obj) (GstBinVideoSink *) (obj)
#define GST_BIN_VIDEOSINK_TYPE (gst_omx_bin_videosink_get_type ())

typedef struct GstBinVideoSink GstBinVideoSink;
typedef struct GstBinVideoSinkClass GstBinVideoSinkClass;


struct GstBinVideoSink
{
    GstBin  element;

    /* sink ghost pad */
    GstPad *sinkgpad;

    guint x_scale;
    guint y_scale;
    guint rotation;

    char *omx_component;
    char *omx_library;
    gchar *display_name;
    gboolean keep_aspect;
    double hue;
    double contrast;
    double brightness;
    double saturation;
    guint adaptor_no;
    gboolean enable_framedump;
    gboolean handle_events;
    gboolean bvideodisable;
    guint start_frame;
    guint end_frame;
    guint step;
    char *videodumpfile_name;
    /* Framerate numerator and denominator */
    gint fps_n;
    gint fps_d;
    gint width;
    gint height;
//    GstPadActivateModeFunction base_activatepush;
    gboolean initialized;
    gboolean ballownvbufferrelease;
    gint     framenum;
    eDeinterlaceType deinterlacetype;
    eFrameType       frametype;
    double current_contrast;
    double current_brightness;
    double current_saturation;
    double current_hue;

    gint   rendertarget;
    gboolean boutputyuv;
};

struct GstBinVideoSinkClass
{
    GstBinClass parent_class;
};

GType gst_omx_bin_videosink_get_type (void);

G_END_DECLS

#endif /* GSTBIN_VIDEOSINK_H */
