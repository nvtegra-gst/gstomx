#include <gst/gst.h>

GstElement* pipeline, *src, *sink, *isf, *isink, *vsink, *vsf;
GstPad* psrc, *psink;
GMainLoop* loop;


static gboolean
on_input (GIOChannel * ichannel, GIOCondition cond, gpointer data)
{
  static gchar tbuffer[256];
  int bytes_read;
  gint fd;
  GstPad* pad;
  gint mode, w, h, k;
  GstCaps* caps;

  fd = g_io_channel_unix_get_fd (ichannel);
  bytes_read = read (fd, tbuffer, 256);
  tbuffer[bytes_read - 1] = '\0';

  if (buffer[0] == 'm') {
    g_print ("enter mode:");
    scanf ("%d", &mode);
    g_object_set (src, "mode", mode, NULL);
    g_print (">>mode done\n");

  } else if (buffer[0] == 'r') {
    g_print ("enter mode:");
    scanf ("%d", &k);
    g_print ("enter width:");
    scanf ("%d", &w);
    g_print ("enter heigth:");
    scanf ("%d", &h);

    caps = gst_caps_new_simple ("video/x-nv-yuv",
        "width", G_TYPE_INT, w,
        "height", G_TYPE_INT, h,
        NULL);

    if (k == 1) {
      g_print ("renegotiating img\n");
      g_object_set (isf, "caps", caps, NULL);
      pad = gst_element_get_static_pad (src, "imgsrc");
    } else {
      g_print ("renegotiating vid\n");
      g_object_set (vsf, "caps", caps, NULL);
      pad = gst_element_get_static_pad (src, "vidsrc");
    }

    gst_caps_unref (caps);
    g_print ("-----> %d\n", gst_pad_send_event (pad, gst_event_new_custom (GST_EVENT_CUSTOM_BOTH,
          gst_structure_new ("renegotiate", NULL))));
    gst_object_unref (pad);
    g_print (">>renegotiation done\n");

  } else if (buffer[0] == 'z') {
    gfloat z;

    g_print ("enter zoom:");
    scanf ("%f", &z);

    g_object_set (src, "zoom", z, NULL);
    g_print (">>zoom done\n");

  } else if (buffer[0] == 's') {
    g_signal_emit_by_name (src, "start-capture", NULL);
    g_print (">>start-capture done\n");

  } else if (buffer[0] == 't') {
    g_signal_emit_by_name (src, "stop-capture", NULL);
    g_print (">>stop-capture done\n");

  }

  return TRUE;
}


static gboolean
bus_call (GstBus     *bus,
    GstMessage *msg,
    gpointer    data)
{
  switch (GST_MESSAGE_TYPE (msg)) {
    case GST_MESSAGE_STATE_CHANGED: {
      GstState old, new, pending;

      gst_message_parse_state_changed (msg, &old, &new, &pending);

      g_print ("element %s changed state from %s to %s, pending %s\n",
          GST_OBJECT_NAME (msg->src),
          gst_element_state_get_name (old),
          gst_element_state_get_name (new),
          gst_element_state_get_name (pending));

    }
    break;

    default:
    break;
  }

  return TRUE;
}


static void
_intr_handler (int signum)
{
  struct sigaction action;

  g_print ("User Interrupted.. \n");

  memset (&action, 0, sizeof (action));
  action.sa_handler = SIG_DFL;

  sigaction (SIGINT, &action, NULL);

  g_main_loop_quit (loop);
}


static gboolean
image_probe (GstPad* pad, GstMiniObject *obj, gpointer data)
{
  if (GST_IS_EVENT (obj)) {
    g_print ("%s : %s\n", __func__, GST_EVENT_TYPE_NAME (obj));
    if (GST_EVENT_TYPE (obj) == GST_EVENT_EOS)
      return FALSE;
  } else {
    g_print ("%s : %s\n %"GST_TIME_FORMAT"\n", __func__, gst_caps_to_string(GST_BUFFER_CAPS (obj)), GST_TIME_ARGS(GST_BUFFER_TIMESTAMP(obj)));
  }

  return TRUE;
}


static gboolean
video_probe (GstPad* pad, GstMiniObject *obj, gpointer data)
{
  if (GST_IS_EVENT (obj)) {
    g_print ("%s : %s\n", __func__, GST_EVENT_TYPE_NAME (obj));
    if (GST_EVENT_TYPE (obj) == GST_EVENT_EOS)
      return FALSE;
  } else {
    g_print ("%s : %s\n %"GST_TIME_FORMAT"\n", __func__, gst_caps_to_string(GST_BUFFER_CAPS (obj)), GST_TIME_ARGS(GST_BUFFER_TIMESTAMP(obj)));
  }

  return TRUE;
}


int
main ()
{
  GstBus* bus;
  struct sigaction action;
  GstCaps* caps;
  GstPad *pad;

  gst_init (NULL, NULL);
  loop = g_main_loop_new (NULL, FALSE);

  pipeline = gst_pipeline_new (NULL);
  src = gst_element_factory_make ("nv_omx_camera2", NULL);
  sink = gst_element_factory_make ("nv_omx_videosink", NULL);
  isf = gst_element_factory_make ("capsfilter", NULL);
  isink = gst_element_factory_make ("fakesink", NULL);
  vsf = gst_element_factory_make ("capsfilter", NULL);
  vsink = gst_element_factory_make ("fakesink", NULL);

  gst_bin_add_many (pipeline, src, isf, isink, vsf, vsink, sink , NULL);

  psrc = gst_element_get_static_pad (src, "vfsrc");
  psink = gst_element_get_static_pad (sink, "sink");
  gst_pad_link (psrc, psink);
  gst_object_unref (psrc);
  gst_object_unref (psink);

  psrc = gst_element_get_static_pad (src, "imgsrc");
  psink = gst_element_get_static_pad (isf, "sink");
  gst_pad_link (psrc, psink);
  gst_object_unref (psrc);
  gst_object_unref (psink);
  gst_element_link (isf, isink);

  psrc = gst_element_get_static_pad (src, "vidsrc");
  psink = gst_element_get_static_pad (vsf, "sink");
  gst_pad_link (psrc, psink);
  gst_object_unref (psrc);
  gst_object_unref (psink);
  gst_element_link (vsf, vsink);

  g_object_set (G_OBJECT (src), "mode", 1, NULL);
  g_object_set (G_OBJECT (sink), "sync", 0, NULL);
  g_object_set (G_OBJECT (sink), "async", 0, NULL);
  g_object_set (G_OBJECT (isink), "sync", 0, NULL);
  g_object_set (G_OBJECT (isink), "async", 0, NULL);
  g_object_set (G_OBJECT (vsink), "sync", 0, NULL);
  g_object_set (G_OBJECT (vsink), "async", 0, NULL);
  caps = gst_caps_new_simple ("video/x-nv-yuv",
      "width", G_TYPE_INT, 320,
      "height", G_TYPE_INT, 240,
      NULL);
  g_object_set (isf, "caps", caps, NULL);
  g_object_set (vsf, "caps", caps, NULL);
  gst_caps_unref (caps);

  gst_element_set_state (pipeline, GST_STATE_PLAYING);

  bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
  gst_bus_add_watch (bus, bus_call, NULL);
  gst_object_unref (bus);

  g_timeout_add (10000, func, NULL);

  pad = gst_element_get_static_pad (isink, "sink");
  gst_pad_add_data_probe (pad, G_CALLBACK (image_probe), NULL);
  gst_object_unref (pad);

  pad = gst_element_get_static_pad (vsink, "sink");
  gst_pad_add_data_probe (pad, G_CALLBACK (video_probe), NULL);
  gst_object_unref (pad);

  memset (&action, 0, sizeof (action));
  action.sa_handler = _intr_handler;
  sigaction (SIGINT, &action, NULL);

  g_main_loop_run (loop);

  gst_element_set_state (pipeline, GST_STATE_NULL);
  gst_object_unref (pipeline);
}
