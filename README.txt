_--------------------------------------------------------------------------------
                            Pre-Reqs for ARM libs
_--------------------------------------------------------------------------------
1. X11 source and dependencies

2. Debian Packages of following dependent modules are required.
These can be obtained from any standard distribution or can also be self-built.
NVIDIA has used the Debian package files from Ubuntu 12.04 (Precise) file system repository for armhf.
(Downloaded from http://launchpad.net/ubuntu/)

Details of the required packages with version info is given below.

  a. libglib2.0-0_2.32.3-0ubuntu1_armhf.deb:
     Package: libglib2.0-0, Source: glib2.0, Version: 2.32.3-0ubuntu1 , Architecture: armhf
  b. libpcre3_8.12-4_armhf.deb:
     Package: libpcre3, Source: pcre3, Version: 8.12-4,  Architecture: armhf
  c. libglib2.0-dev_2.32.1-0ubuntu2_armhf.deb:
     Package: libglib2.0-dev, Source: glib2.0, Version: 2.32.1-0ubuntu2 , Architecture: armhf
  d. libpcre3-dev_8.12-4_armhf.deb:
     Package: libpcre3-dev, Source: pcre3 , Version: 8.12-4, Architecture: armhf
  e. libgstreamer0.10-0_0.10.36-1ubuntu1_armhf.deb:
     Package: libgstreamer0.10-0, Source: gstreamer0.10 , Version: 0.10.36-1ubuntu1, Architecture: armhf
  f. libxml2_2.7.8.dfsg-5.1ubuntu4.1_armhf.deb:
     Package: libxml2, Version: 2.7.8.dfsg-5.1ubuntu4.1,  Architecture: armhf
  g. libgstreamer0.10-dev_0.10.36-1ubuntu1_armhf.deb:
     Package: libgstreamer0.10-dev, Source: gstreamer0.10 ,Version: 0.10.36-1ubuntu1, Architecture: armhf
  h. libxml2-dev_2.7.8.dfsg-5.1ubuntu4.1_armhf.deb:
     Package: libxml2-dev, Source: libxml2, Version: 2.7.8.dfsg-5.1ubuntu4.1, Architecture: armhf
  i. libgstreamer-plugins-base0.10-0_0.10.36-1_armhf.deb:
     Package: libgstreamer-plugins-base0.10-0, Source: gst-plugins-base0.10 , Version: 0.10.36-1+ti1.6.4.1+1, Architecture: armhf
  j. zlib1g_1.2.3.4.dfsg-3ubuntu4_armhf.deb:
     Package: zlib1g, Source: zlib, Version: 1:1.2.3.4.dfsg-3ubuntu4, Architecture: armhf
  k. libgstreamer-plugins-base0.10-dev_0.10.36-1_armhf.deb:
     Package: libgstreamer-plugins-base0.10-dev, Source: gst-plugins-base0.10, Version: 0.10.30-2, Architecture: armhf
  l. zlib1g-dev_1.2.3.4.dfsg-3ubuntu4_armhf.deb:
     Package: zlib1g-dev, Source: zlib, Version: 1:1.2.3.4.dfsg-3ubuntu4, Architecture: armhf
  m. libffi6_3.0.11~rc1-5_armhf.deb:
     Package: libffi, Source: ffi, Version: 3.0.11~rc1-5, Architecture: armhf
  n. libffi-dev_3.0.11~rc1-5_armhf.deb:
     Package: libffi-dev, Source: ffi, Version: 3.0.11~rc1-5, Architecture: armhf

3. Pre-Reqs for host:
   Install autoconf and autotools-dev as below.

sudo apt-get install autotools-dev
sudo apt-get install autoconf

--------------------------------------------------------------------------------------------------------------
                                    Steps to build:
--------------------------------------------------------------------------------------------------------------

1. Create a directory to build the NVIDIA gst-openmax source
    mkdir nv-tegra
    cp gstomx_srcrel<RELEASE>.<VERSION>.tar.gz nv-tegra
    cd nv-tegra

2. Create a common directory to store the above deb files
   and copy all the .deb files into it

    mkdir myDebDir
    cp *.deb myDebDir

3. Extract the gst-openmax source and included openmax khronos headers

    tar -xvf gstomx_srcrel<RELEASE>.<VERSION>.tar.gz
    cd gstomx_srcrel/gst-openmax/

3. Generate configure from autoconf

    export NOCONFIGURE=true
    ./autogen.sh

4. Export your cross compiler tools with the following enviroment vars:

    export CC="<your ARM toolchain's cc (path + exec name)>"
    export CPP="<your ARM toolchain's ccp (path + exec name)>"
    export NM="<your ARM toolchain's nm (path + exec name)>"
    export CFLAGS="-I<dir>/gstomx_srcrel/nv_headers" -mfloat-abi=hard
    export EGL_CFLAGS="-I <some path>/egl/include/khronos -I <some path>/egl/include/nvidia -I <some path>/opengles/include"
    export EGL_LIBS="-L <library path to libEGL.so and libGLESv2.so>  -lEGL -lGLESv2"
    export X11_CFLAGS="-I <some path>/xorg-server/include"
    export X11_LIBS="-L <library path to X11 libs>"

5. Run configure to build the Makefiles

    export CONF_PARAMS ="--host=arm-linux --enable-experimental --enable-tegra --enable-eglimage --with-debdir=$PWD/myDebDir --with-pkg-config-path=.debs/usr/lib/pkgconfig"
    ./configure $CONF_PARAMS

6. Build gst-openmax source to create libgstomx.so
       make
